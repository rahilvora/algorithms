package HashMap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by rahilvora on 2/20/18.
 * Given [1,1,1,2,2,3] and k = 2, return [1,2]
 * Time o(N)
 */
public class TopKFrequentElements {
    public List<Integer> topKFrequent(int[] nums, int k) {
        List<Integer>[] bucket = new List[nums.length+1];
        Map<Integer, Integer> frequencies = new HashMap<>();
        for(int n: nums){
            frequencies.put(n, frequencies.getOrDefault(n,0)+1);
        }
        for(int key: frequencies.keySet()){
            int frequency = frequencies.get(key);
            if(bucket[frequency] == null){
                bucket[frequency] = new ArrayList<>();
            }
            bucket[frequency].add(key);
        }
        List<Integer> res = new ArrayList<>();
        for(int pos = bucket.length-1;pos>=0&&res.size()<k;pos--){
            res.addAll(bucket[pos]);
        }
        return res;
    }
}
