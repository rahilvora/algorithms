package HashMap;

import java.util.*;

public class UniqueEmailAddresses {
    public static void main(String args[]){
        String[] emails = {"test.email+alex@leetcode.com","test.e.mail+bob.cathy@leetcode.com","testemail+david@lee.tcode.com"};
        System.out.println(new UniqueEmailAddresses().numUniqueEmails(emails));
    }
    public int numUniqueEmails(String[] emails) {
        Map<String, Set<String>> domainToId = new HashMap<>();

        for(String email: emails){
            String localName = getLocalName(email.split("@")[0]);
            String domain = email.split("@")[1];
            if(!domainToId.containsKey(domain)){
                domainToId.put(domain, new HashSet<>());
            }
            domainToId.get(domain).add(localName);
        }
        int answer = 0;
        for(String key: domainToId.keySet()){
            answer += domainToId.get(key).size();
        }
        return answer;
    }

    public String getLocalName(String localName){
        if(localName.contains("+")){
            return localName.split("\\+")[0].replaceAll(".", "");
        }
        return localName.replaceAll(".", "");
    }
}
