package HashMap;

import java.util.HashMap;

/**
 * Created by rahilvora on 2/14/18.
 */
public class CountPairofSum {
    public int getPairsCount(int[] arr, int sum){
        HashMap<Integer, Integer> map = new HashMap<>();
        for(int i = 0; i < arr.length; i++){
            map.put(arr[i], map.getOrDefault(arr[i],0)+1);
        }
        int totalCount = 0;
        for(int i = 0; i < arr.length; i++){
            if(map.containsKey(sum - arr[i])){
                totalCount++;
            }
            if(sum - arr[i] == arr[i]){
                totalCount--;
            }
        }
        return totalCount/2;
    }
    public static void main(String args[]){
        int[] arr = {1,5,7,-1};
        System.out.print(new CountPairofSum().getPairsCount(arr, 6));
    }
}
