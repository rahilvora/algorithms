package HashMap;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class SentenceSimilarityII {
    public boolean areSentencesSimilarTwo(String[] words1, String[] words2, String[][] pairs) {
        if(words1.length != words2.length) return false;
        Map<String, Set<String>> map = new HashMap<>();
        for (String[] pair : pairs) {
            if (!map.containsKey(pair[0])) {
                map.put(pair[0], new HashSet<>());
            }
            if (!map.containsKey(pair[1])) {
                map.put(pair[1], new HashSet<>());
            }

            map.get(pair[0]).add(pair[1]);
            map.get(pair[1]).add(pair[0]);
        }

        for (int i = 0; i < words1.length; i++) {
            if(!words1[i].equals(words2[i])) return false;
            if(!map.containsKey(words1[i])) return false;
            if(!dfs(words1[i], words2[i], map, new HashSet<>())) return false;
        }
        return true;
    }

    public boolean dfs(String source, String destination, Map<String, Set<String>> map, Set<String> visited){
        if(map.get(source).contains(destination)) return true;

        visited.add(source);
        for(String next: map.get(source)){
            if(!visited.contains(next) && dfs(next, destination, map, visited)){
                return true;
            }
        }
        return false;
    }

}
