package HashMap;

import java.util.*;

/**
 * Created by rahilvora on 1/28/18.
 * Time Complexity = O (nlogk) n is length of words array and k is frequency
 */
public class TopKFrequentWords {
    public static void main(String args[]){
        String[] words = {"love", "i", "leetcode", "i", "love", "coding", "the", "the", "the"};
        new TopKFrequentWords().topKFrequent(words, 3);
    }
    public List<String> topKFrequent(String[] words, int k) {
        Map<String, Integer> map = new HashMap<>();
        List<String> answer = new ArrayList<>();
        for(int i = 0; i < words.length; i++){
            map.put(words[i], map.getOrDefault(words[i], 0) + 1);
        }
        PriorityQueue<Map.Entry<String, Integer>> pq = new PriorityQueue<>(new  Comparator<Map.Entry<String, Integer>>() {
            @Override
            public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {

                //If dont want to sort lexicogrphically
                //if(o1.getValue().equals(o2.getValue())){
                //      return 1;
                //}

                //if want to sort keys lexicographically
                if(o2.getValue().equals(o1.getValue())){
                    return o2.getKey().compareTo(o1.getKey());
                }
                return o1.getValue() - o2.getValue();
            }
        });

        for(Map.Entry<String, Integer> entry: map.entrySet()){
            pq.offer(entry);
            if(pq.size() > k){
                pq.poll();
            }
        }
        while(!pq.isEmpty()){
            answer.add(0, pq.poll().getKey());
        }
        return answer;
    }
}
