package HashMap;

import java.util.*;

public class NearestSteakHouses {
    List<List<Integer>> nearestXsteakHouses(int totalSteakhouses,
                                            List<List<Integer>> allLocations,
                                            int numSteakhouses)
    {
        // WRITE YOUR CODE HERE
        List<List<Integer>> answer = new ArrayList<>();
        int maxSteakHouse = numSteakhouses;
        TreeMap<Double, List<List<Integer>>> map = new TreeMap<>();
        int customer_x = 0;
        int customer_y = 0;
        for(int i = 0; i < totalSteakhouses; i++){
            List<Integer> currSteakHouse = allLocations.get(i);
            int x = Math.abs(customer_x - currSteakHouse.get(0));
            int y = Math.abs(customer_y - currSteakHouse.get(1));
            double distance = Math.sqrt(x*x + y*y);
            if(map.containsKey(distance)){
                map.get(distance).add(currSteakHouse);
            }
            else{
                List<List<Integer>> temp = new ArrayList();
                temp.add(currSteakHouse);
                map.put(distance, temp);
            }
        }

        for(Map.Entry m : map.entrySet()){
            answer.addAll((Collection<? extends List<Integer>>) m.getValue());
            maxSteakHouse -= ((Collection<? extends List<Integer>>) m.getValue()).size();
            if(maxSteakHouse <= 0) return answer;
        }
        return answer;
    }
}
