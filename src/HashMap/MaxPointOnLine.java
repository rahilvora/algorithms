package HashMap;

import java.util.HashMap;

/**
 * Created by rahilvora on 5/27/17.
 */
public class MaxPointOnLine {
    public static void main(String args[]){
        Point point1 = new Point(-2,-2);
        Point point2 = new Point(-1,-1);
        Point point3 = new Point(1,1);
        Point point4 = new Point(2,2);
        Point point5 = new Point(3,3);
        Point point6 = new Point(1,1);
        Point[] points = {point3, point4, point5, point6};
        new MaxPointOnLine().maxPoints(points);
    }
    public int maxPoints(Point[] points){
        if(points == null) return 0;
        int size = points.length;
        int result = 0;
        if(size <= 2) return size;
        for(int i = 0; i < size; i++){
            HashMap<Double, Integer> map = new HashMap<>();
            int samex = 1;
            int samep = 0;
            for(int j = 0; j < size; j++){
                if(j != i){
                    if(points[j].x == points[i].x && points[j].y == points[i].y){
                        samep++;
                    }
                    if(points[j].x == points[i].x){
                        samex++; // Since slope of the line with same is x coordinate is infinite we need to increment x counter and continue the loop
                        continue;
                    }
                    double k = (double)(points[j].y - points[i].y) / (double)(points[j].x - points[i].x);
                    if(map.containsKey(k)){
                        map.put(k,map.get(k) + 1);
                    }
                    else{
                        map.put(k, 2);
                    }
                    result = Math.max(result, map.get(k) + samep);
                }
            }
            result = Math.max(result, samex);
        }
        return result;
    }
}
