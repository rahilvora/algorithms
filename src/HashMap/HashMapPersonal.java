package HashMap;

/**
 * Created by rahilvora on 2/9/18.
 * This is hashmap implementation from scratch for learning purpose
 *
 */
public class HashMapPersonal {
    private int bucketSize = 256;
    private Node[] bucketArray = new Node[bucketSize];
    HashMapPersonal(){

    }
    HashMapPersonal(int initialSize){
        this.bucketSize = initialSize;
    }

    /**
     * Used to put a key-value pair into the data structure.
     * @param key Key string that is used identify the key-value pair
     * @param value Value string in which the key string maps to.
     */

    public void put(String key, String value){
        //Get hashcode
        int hash = Math.abs(key.hashCode() % bucketSize);
        //create node
        Node entry = new Node(key, value);

        //Insert in the bucketArray at hashindex
        if(bucketArray[hash] == null){
            bucketArray[hash] = entry;
        }
        else{
            //Collision occurred

            Node current = bucketArray[hash];
            while(current.getNext() != null){
                //Check if key already exists
                if(current.getKey().equals(entry.getKey())){
                    current.setValue(entry.getValue());;
                    return;
                }
                current = current.getNext();
            }
            current.setNext(entry);
        }
    }
    public String get(String key){
        /* Get the hash */
        int hash = Math.abs(key.hashCode() % bucketSize);
        /* Search for key in linked list */
        Node n = bucketArray[hash];
        /* Traverse linked list */
        while(n != null){
            if(n.getKey().equals(key)){
                return n.getValue();
            }
            n = n.getNext();
        }
        /* Not found? then return null */
        return null;
    }
}

class Node{
    private String key, value;
    private Node next;

    Node(){

    }
    Node(String key, String value){
        this.key = key;
        this.value = value;
    }

    public String getKey(){
        return key;
    }

    public void setKey(String key){
        this.key = key;
    }

    public String getValue(){
        return value;
    }
    public void setValue(String value){
        this.value =  value;
    }
    public Node getNext(){
        return next;
    }
    public void setNext(Node next){
        this.next = next;
    }
}
