package HashMap;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by rahilvora on 5/28/17.
 */
public class RandomListNodeProblem {
    public static void main(String args[]){

    }
    public RandomListNode copyRandomList(RandomListNode head) {
        if(head == null) return null;
        if(head.next == null && head.random == null) return new RandomListNode(head.label);
        Map<RandomListNode, RandomListNode> map = new HashMap<>();
        RandomListNode copy = new RandomListNode(head.label);
        map.put(head, copy);
        RandomListNode tempCopy = copy;
        RandomListNode tempHead = head;
        while(tempHead != null && tempHead.next != null){
            tempCopy.next = new RandomListNode(tempHead.next.label);
            tempHead = tempHead.next;
            tempCopy = tempCopy.next;
            map.put(tempHead, tempCopy);
        }
        tempCopy = copy;
        tempHead = head;
        while(tempHead != null){
            if(tempHead.random != null)
                tempCopy.random = map.get(tempHead.random);
            tempHead = tempHead.next;
            tempCopy = tempCopy.next;
        }
        return copy;
    }
}
