package HashMap;

import java.util.HashMap;
import java.util.Map;

public class FindAnagramMappings {
    public static void main(String args[]){

    }

    public int[] anagramMappings(int[] A, int[] B) {
        if(B == null || A == null || A.length == 0 || B.length == 0){
            return new int[0];
        }
        int[] indexes = new int[A.length];
        Map<Integer, Integer> valueToIndexMappingOfB = new HashMap<>();
        for(int i = 0; i < B.length; i++){
            valueToIndexMappingOfB.put(B[i], i);
        }
        for(int i = 0; i < A.length; i++){
            indexes[i] = valueToIndexMappingOfB.get(A[i]);
        }
        return indexes;
    }
}
