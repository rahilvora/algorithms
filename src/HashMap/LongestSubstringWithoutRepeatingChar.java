package HashMap;

import java.util.HashMap;

/**
 * Created by rahilvora on 5/27/17.
 */
public class LongestSubstringWithoutRepeatingChar {
    public static  void main(String args[]){

    }
    public int lengthOfLongestSubstring(String s) {
        int max = 0;
        HashMap<Character, Integer> map = new HashMap<>();
        for(int i = 0, j = 0; i < s.length(); i++){
            char currCharacter = s.charAt(i);
            if(map.containsKey(currCharacter)){
                j = Math.max(j, map.get(currCharacter) + 1);
            }
            map.put(currCharacter, i);
            max = Math.max(max, i - j + 1);
        }
        return max;
    }
}
