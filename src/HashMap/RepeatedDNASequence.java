package HashMap;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by rahilvora on 10/6/17.
 */
public class RepeatedDNASequence {
    public static void main(String args[]){
        System.out.print(new RepeatedDNASequence().findRepeatedDnaSequences("AAAAACCCCCAAAAACCCCCCAAAAAGGGTTT"));
    }

    public List<String> findRepeatedDnaSequences(String s) {
        Set<String> seen = new HashSet(), repeated = new HashSet();

        for (int i = 0; i + 9 < s.length(); i++){
            String ten = s.substring(i, i + 10);
            if(!seen.add(ten)){
                repeated.add(ten);
            }
        }
        return new ArrayList(repeated);

    }
}
