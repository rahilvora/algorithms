package HashMap;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by rahilvora on 9/5/17.
 * Time: O(N)
 * Space: O(N)
 */
public class CountPairsDifferenceOfK {
    public int countPair(int[] nums, int K){
        int count = 0;
        Map<Integer, Integer> map = new HashMap<>();
        for(int i = 0; i < nums.length; i++){
            int currValue = nums[i];
            if(map.containsKey(currValue + K)){
                count++;
            }
            if(map.containsKey(currValue - K)){
                count++;
            }
            map.put(currValue, i);
        }
        return count;
    }
}
