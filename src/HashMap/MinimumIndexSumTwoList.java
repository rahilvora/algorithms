package HashMap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by rahilvora on 1/4/18.
 */
public class MinimumIndexSumTwoList {
    public String[] findRestaurant(String[] list1, String[] list2) {
        Map<String, Integer> map = new HashMap<>();
        for(int i = 0; i < list1.length; i++){
            map.put(list1[i], i);
        }
        List<String> res = new ArrayList<>();
        int min_sum = Integer.MAX_VALUE;
        for (int j = 0; j < list2.length; j++){
            if(map.containsKey(list2[j])){
                int sum = map.get(list2[j]) + j;
                if(sum < min_sum){
                    res.clear();
                    res.add(list2[j]);
                    min_sum = sum;
                }
                else if(sum == min_sum){
                    res.add(list2[j]);
                }
            }
        }
        return res.toArray(new String[res.size()]);
    }
}
