package HashMap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

class CurrencyRate{
    String country1, country2;
    double ratio;
    CurrencyRate(String country1, String country2, double ratio){
        this.country1 = country1;
        this.country2 = country2;
        this.ratio = ratio;
    }
}

class CustomCountryRatio{
    String destinationCountry;
    double ratio;
    CustomCountryRatio(String destinationCountry, double ratio){
        this.destinationCountry = destinationCountry;
        this.ratio = ratio;
    }
}
public class CurrencyConversion {

    public static void main(String args[]){
        CurrencyRate rate1 = new CurrencyRate("USD", "INR", 65.47);
        CurrencyRate rate2 = new CurrencyRate("AUD", "USD", 0.67);
        CurrencyRate rate3 = new CurrencyRate("GBP", "YEN", 176.33);
        CurrencyRate rate4 = new CurrencyRate("RUB", "GBP", 53.44);
        CurrencyRate rate5 = new CurrencyRate("USD", "PKR", 120);
        List<CurrencyRate> currencyRates = new ArrayList<>();
        currencyRates.add(rate1);
        currencyRates.add(rate2);
        currencyRates.add(rate3);
        currencyRates.add(rate4);
        new CurrencyConversion().getCurrencyConversion(currencyRates, "USD", "INR");
    }

    public double getCurrencyConversion(List<CurrencyRate> currencyRates, String country1, String country2){
        HashMap<String, List<CustomCountryRatio>> countryMap = new HashMap<>();

        //Create Map using currentyRates list
        /*Example
        * {
        *   "USA":[
        *           {"INR", 65.47}
        *           {"PKR", 120}
        *           ]
        * }
        *
        *
        * */
        for(int i = 0; i < currencyRates.size(); i++){
            String c1 = currencyRates.get(i).country1;
            String c2 = currencyRates.get(i).country2;
            double ratio = currencyRates.get(i).ratio;
            if(countryMap.containsKey(c1)){
                countryMap.get(c1).add(new CustomCountryRatio(c2, currencyRates.get(i).ratio));
            }
            else{
                List<CustomCountryRatio> temp = new ArrayList<>();
                temp.add(new CustomCountryRatio(c2, ratio));
                countryMap.put(c1, temp);
            }
        }

        if(countryMap.containsKey(country1)){
            List<CustomCountryRatio> customCountryRatios = countryMap.get(country1);
            for(int i = 0; i < customCountryRatios.size(); i++) {
                if (customCountryRatios.get(i).destinationCountry == country2) {
                    return customCountryRatios.get(i).ratio;
                }
            }
        }
        else if (countryMap.containsKey(country2)){
            List<CustomCountryRatio> customCountryRatios = countryMap.get(country2);
            for(int i = 0; i < customCountryRatios.size(); i++) {
                if (customCountryRatios.get(i).destinationCountry == country1) {
                    return 1/customCountryRatios.get(i).ratio;
                }
            }
        }
        return 0.0; // No conversion rate found
    }
}
