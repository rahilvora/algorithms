package HashMap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SubdomainVisitCount {
    public static void main(String args[]){
        String[] cpdomains = {"900 google.mail.com", "50 yahoo.com", "1 intel.mail.com", "5 wiki.org"};
        new SubdomainVisitCount().subdomainVisits(cpdomains);
    }
    public List<String> subdomainVisits(String[] cpdomains) {
        Map<String, Integer> map = new HashMap<>();
        List<String> solution = new ArrayList<>();
        for(String domain: cpdomains){
            String[] temp = domain.split(" ");
            int count = Integer.parseInt(temp[0]);
            String[] subdomains = temp[1].split("\\.");
            StringBuilder sb = new StringBuilder();
            for(int i = subdomains.length - 1; i >=0; i--){
                sb.insert(0, subdomains[i]);
                map.put(sb.toString(), map.getOrDefault(sb.toString(), 0) + count);
                if (i != 0)
                    sb.insert(0, '.');
            }
        }

        for(Map.Entry<String, Integer> entry : map.entrySet()){
            StringBuilder sb = new StringBuilder();
            solution.add(0, sb.append(String.valueOf(entry.getValue())).append(" ").append(entry.getKey()).toString() );
        }
        return solution;
    }
}
