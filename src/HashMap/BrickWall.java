package HashMap;

import java.util.*;

/**
 * Created by rahilvora on 9/3/17.
 */
public class BrickWall {
    public static void main(String args[]){
        List<List<Integer>> wall = new ArrayList<>();
        wall.add(Arrays.asList(1,2,2,1));
        wall.add(Arrays.asList(3,1,2));
        wall.add(Arrays.asList(1,3,2));
        wall.add(Arrays.asList(2,4));
        wall.add(Arrays.asList(3,1,2));
        wall.add(Arrays.asList(1,3,1,1));
        new BrickWall().leastBricks(wall);

    }
    public int leastBricks(List<List<Integer>> wall) {
        int answer = 0;
        int wallwidth = 0;
        for(int i = 0; i < wall.get(0).size(); i++){
            wallwidth += wall.get(0).get(i);
        }
        Map<Integer, Integer> map = new HashMap<>();
        for(int i = 0; i < wall.size(); i++){
            int sum = 0;
            List<Integer> currList = wall.get(i);
            for(int j = 0; j < currList.size(); j++){
                sum += currList.get(j);
                map.put(sum, map.getOrDefault(sum, 0) + 1);
            }
        }
        for(int key: map.keySet()){
            if(key != wallwidth){
                answer = Math.max(answer, map.get(key));
            }
        }
        return wall.size() - answer;
    }
}
