package Design;

import java.util.Iterator;
import java.util.List;

public class FlattenTwoDVector implements Iterator<Integer> {
    int indexList = 0;
    int indexElement = 0;
    List<List<Integer>> list;
    public FlattenTwoDVector(List<List<Integer>> vec2d) {
        this.list = vec2d;
    }


    @Override
    public boolean hasNext() {
        while(indexList < list.size()){
            if(indexElement < list.get(indexList).size()){
                return true;
            }
            else{
                indexElement = 0;
                indexList++;
            }
        }
        return false;
    }

    @Override
    public Integer next() {
        return list.get(indexList).get(indexElement++);
    }
}
