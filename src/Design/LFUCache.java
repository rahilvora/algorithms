package Design;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;

public class LFUCache {
    Map<Integer, Integer> count; // Map for key to count
    Map<Integer, Integer> val; // Map for Key to value
    Map<Integer, LinkedHashSet<Integer>> lists; // Map for count to all the keys with same count
    int min;
    int capacity;

    public LFUCache(int capacity){
        this.capacity = capacity;
        min = 0;
        count = new HashMap<>();
        val = new HashMap<>();
        lists = new HashMap<>();
    }

    public int get(int key){
        if(!val.containsKey(key)) return -1;
        update(key);
        return val.get(key);
    }

    public void update(int key){
        int keyCount = count.get(key);
        count.put(key, keyCount + 1);
        lists.get(keyCount).remove(key);

        if(keyCount == min && lists.get(keyCount).size() == 0) min++;

        addToList(keyCount + 1, key);
    }

    public void addToList(int keyCount, int key){
        if(!lists.containsKey(keyCount)){
            lists.put(keyCount, new LinkedHashSet<>());
        }
        lists.get(keyCount).add(key);
    }

    public void evict () {
        int key = lists.get(min).iterator().next();
        lists.get(min).remove(key);
        val.remove(key);
        count.remove(key);
    }

    public void put(int key, int value){
        if(capacity == 0) return;

        if(val.containsKey(key)){
            val.put(key, value);
            update(key);
            return;
        }

        if(val.size() >= capacity) evict();

        val.put(key, value);
        count.put(key, 1);
        addToList(1, key);
        min = 1;
    }

}
