package Design;

/**
 * Created by rahilvora on 1/15/18.
 * addWord() time complexity = o(m) where m is length of new word
 * search() time complexity - o(n) where n total of chars of all the words in a trie ds
 * Time complexity : https://stackoverflow.com/questions/13032116/trie-complexity-and-searching
 */
public class AddAndSearchWord {
    private TrieNode root;

    /** Initialize your data structure here. */
    public AddAndSearchWord() {
        root = new TrieNode();
    }

    /** Adds a word into the data structure. */
    public void addWord(String word) {
        TrieNode node = root;
        for (char c : word.toCharArray()) {
            if (node.children[c - 'a'] == null) {
                node.children[c - 'a'] = new TrieNode();
            }
            node = node.children[c - 'a'];
        }
        node.item = word;
    }

    /** Returns if the word is in the data structure. A word could contain the dot character '.' to represent any one letter. */
    public boolean search(String word) {
        return match(word.toCharArray(), 0, root);
    }

    public boolean match(char[] word, int k, TrieNode node){
        if(word.length == k) return !node.item.equals("");
        if(word[k] != '.'){
            return node.children[word[k] - 'a'] != null && match(word, k+1, node.children[word[k]-'a']);
        }
        else{
            for(int i = 0; i < node.children.length; i++){
                if(node.children[i] != null){
                    if(match(word, k+1, node.children[i])){
                        return true;
                    };
                }
            }
        }
        return false;
    }
}

class TrieNode{
    String item = "";
    TrieNode[] children = new TrieNode[26];
}
