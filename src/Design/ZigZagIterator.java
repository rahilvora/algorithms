package Design;

import java.util.*;

public class ZigZagIterator {
    private boolean pickListOne;
    private int indexOne, indexTwo;
    private List<Integer> v1, v2;

    public ZigZagIterator(List<Integer> v1, List<Integer> v2) {
        this.v1 = v1;
        this.v2 = v2;
        indexOne = 0;
        indexTwo = 0;
        pickListOne = true;
    }

    public int next() {
        if(indexOne == v1.size()) return v2.get(indexTwo++);
        else if( indexTwo == v2.size()) return v1.get(indexOne++);

        if(pickListOne){
            pickListOne = false;
            return v1.get(indexOne++);
        }
        else{
            pickListOne = true;
            return v2.get(indexTwo++);
        }
    }

    public boolean hasNext() {
        return (indexOne != v1.size() || indexTwo != v2.size());
    }

    public static void main(String args[]){
        List<Integer> v1 = new ArrayList<>();
        List<Integer> v2 = new ArrayList<>();
        v1.add(1);
        v1.add(2);
        v2.add(3);
        ZigZagIterator obj = new ZigZagIterator(v1, v2);
        while(obj.hasNext()){
            System.out.println(obj.next());
        }
    }
}
