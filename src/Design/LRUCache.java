package Design;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by rahilvora on 8/16/17.
 */
public class LRUCache {
    int capacity;
    Node head = null, tail = null;
    Map<Integer, Node> cacheMap = new HashMap<>();

    public LRUCache(int capacity) {
        this.capacity = capacity;
    }

    public void remove(Node node){
        if(node.pre != null) node.pre.next = node.next;
        else head = node.next;

        if(node.next != null) node.next.pre = node.pre;
        else tail = node.pre;
    }

    public void setHead(Node node){
        node.next = head;
        node.pre = null;

        if(head != null) head.pre = node;

        head = node;

        if(tail == null)
            tail = head;
    }
    public int get(int key) {
        if(cacheMap.containsKey(key)){
            Node currNode = cacheMap.get(key);
            remove(currNode);
            setHead(currNode);
            return currNode.value;
        }
        return -1;
    }

    public void put(int key, int value) {
        if(cacheMap.containsKey(key)){
            Node old = cacheMap.get(key);
            old.value = value;
            remove(old);
            setHead(old);
        }
        else{
            Node newNode = new Node(key, value);

            if(cacheMap.size() >= capacity){
                cacheMap.remove(tail.key);
                remove(tail);
                setHead(newNode);
            }
            else{
                setHead(newNode);
            }
            cacheMap.put(key, newNode);

        }
    }
}
