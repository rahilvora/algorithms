package Design;

/**
 * Created by rahilvora on 8/16/17.
 */
public class LRUCacheRunner {
    public static void main(String args[]) {
        LRUCache obj = new LRUCache(1);
        obj.put(2, 1);
        obj.get(2);
        obj.put(3,2);
        obj.get(2);
        obj.get(3);
    }
}
