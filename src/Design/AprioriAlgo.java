package Design;

import java.util.*;

public class AprioriAlgo {
    public static void main(String args[]){
        Map<Integer, Set<String>> map = new HashMap<>();
        Set<String> set1 = new HashSet<>();
        set1.add("apple");
        set1.add("banana");
        set1.add("lemon");
        map.put(1, set1);
        Set<String> set2 = new HashSet<>();
        set2.add("banana");
        set2.add("berry");
        set2.add("lemon");
        set2.add("orange");
        map.put(2, set2);
        Set<String> set3 = new HashSet<>();
        set3.add("banana");
        set3.add("berry");
        set3.add("lemon");
        map.put(3, set3);

        new AprioriAlgo().apriori(map);
    }

    public void apriori(Map<Integer, Set<String>> transactions) {
        Map<String, Integer> map = new HashMap<>();
        Set<String> items = new HashSet<>();
        for(Integer key: transactions.keySet()){
            items.addAll(transactions.get(key));
        }
        List<String> arr = new ArrayList<>();
        arr.addAll(items);
        List<List<String>> subsets = subSet(arr);
        Map<List<String>, Integer> answer = new HashMap<>();
        for(List<String> subset: subsets){
            for(int key: transactions.keySet()){
                if(transactions.get(key).containsAll(subset)){
                    answer.put(subset, answer.getOrDefault(subset, 0)  + 1);
                }
            }
        }

        for (List<String> key: answer.keySet()){
            System.out.println(String.valueOf(key) + " " + answer.get(key));
        }

    }

    public List<List<String>> subSet(List<String> arr){
        List<List<String>> answer = new ArrayList<>();
        subSet(answer, new ArrayList<String>(), arr, 0);
        return answer;
    }

    public void subSet(List<List<String>> answer, List<String> temp, List<String> arr, int start){
        if(temp.size() != 0){
            answer.add(new ArrayList<>(temp));
        }
        for(int i = start; i < arr.size(); i++){
            if(temp.contains(arr.get(i))) continue;;
            temp.add(arr.get(i));
            subSet(answer, temp, arr, i+1);
            temp.remove(temp.size() - 1);
        }

    }
}
