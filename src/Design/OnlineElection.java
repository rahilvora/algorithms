package Design;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//Time Complexity: O(N+QlogN), where N is the number of votes, and Q is the number of queries.
//
//Space Complexity: O(N).

public class OnlineElection {

    List<Vote> list;
    public OnlineElection(int[] persons, int[] times) {
        list = new ArrayList<>();
        Map<Integer, Integer> map = new HashMap<>();
        int vote = 0; // current leader votes
        int leader = -1; //current leader
        for(int i = 0; i < persons.length; i++){
            int p = persons[i];
            int time = times[i];
            int c = map.getOrDefault(p,0)  + 1;
            if(c >= vote){
                if(p != leader){
                    list.add(new Vote(p, time));
                }
                if(c > vote){
                    vote = c;
                }
            }
        }
    }

    public int q(int t) {
        int lo = 0, hi = list.size();
        while (lo < hi) {
            int mi = lo + (hi - lo) / 2;
            if (list.get(mi).time <= t)
                lo = mi + 1;
            else
                hi = mi;
        }

        return list.get(lo - 1).person;

    }

}

class Vote {
    int person;
    int time;
    Vote(int person, int time){
        this.person = person;
        this.time = time;
    }
}