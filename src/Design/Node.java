package Design;

/**
 * Created by rahilvora on 8/16/17.
 */
public class Node {
    int key, value;
    Node pre = null, next = null;
    public Node(int key, int value){
        this.key = key;
        this.value = value;
    }
}
