package Design;

import java.util.ArrayList;
import java.util.List;

public class RLEIterator {
    List<Integer> nums;
    int currIndex = 0;
    public RLEIterator(int[] A) {
        nums = new ArrayList<>();
        for(int i = 0; i < A.length - 1; i += 2){
            int j = 0;
            while(j < A[i]){
                nums.add(A[i+1]);
                j++;
            }
        }
    }

    public int next(int n) {
        if(currIndex + n < nums.size()){
            currIndex += n;
            return nums.get(currIndex);
        }
        return -1;
    }

    public static void main(String args[]){
        int[] nums = {3,8,0,9,2,5};
        RLEIterator rle = new RLEIterator(nums);
        for(int num: rle.nums){
            System.out.println(num);
        }
    }
}
