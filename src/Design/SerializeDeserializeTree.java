package Design;

import Trees.TreeNode;

import java.util.Arrays;
import java.util.Deque;
import java.util.LinkedList;

/**
 * Created by rahilvora on 8/18/17.
 */
public class SerializeDeserializeTree {
    // Encodes a tree to a single string.
    public String serialize(TreeNode root) {
        StringBuffer serializeBuffer = new StringBuffer();
        preOrderTraversal(root, serializeBuffer);
        return serializeBuffer.toString();
    }

    public void preOrderTraversal(TreeNode root, StringBuffer sBuffer){
        if(root == null) sBuffer.append("X").append(",");
        else{
            sBuffer.append(root.val).append(",");
            preOrderTraversal(root.left, sBuffer);
            preOrderTraversal(root.right, sBuffer);
        }
    }

    // Decodes your encoded data to tree.
    public TreeNode deserialize(String data) {
        Deque<String> nodes = new LinkedList<>();
        nodes.addAll(Arrays.asList(data.split(",")));
        return deserializeStringBuilder(nodes);
    }

    public TreeNode deserializeStringBuilder(Deque<String> nodes){
        String node = nodes.remove();
        if(node.equals("X")){
            return null;
        }
        else{
            TreeNode newNode = new TreeNode(Integer.valueOf(node));
            newNode.left = deserializeStringBuilder(nodes);
            newNode.right = deserializeStringBuilder(nodes);
            return newNode;
        }
    }
}
