package Design;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by rahilvora on 9/4/17.
 */
public class MovingAverageFromDataStream {
    int total = 0, size = 0;
    Queue<Integer> queue = new LinkedList<>();

    public MovingAverageFromDataStream(int size) {
        this.size = size;
    }

    public double next(int val) {
        if(queue.size() == size){
            total -= queue.poll();
        }
        queue.add(val);
        total += val;
        return (double) total/queue.size();
    }
}
