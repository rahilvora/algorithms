package Design;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class QueueWithFixedSizedArray {
    int capacity;
    int head;
    int tail;
    int count;
    List<Object> headList;
    List<Object> tailList;
    QueueWithFixedSizedArray(int capacity){
        this.capacity = capacity;
        head = 0;
        tail = 0;
        count = 0;
        headList = new ArrayList<>();
        tailList = headList;
    }

    public void offer(int num){
        if (tail == capacity - 1) {
            List<Object> newList = new ArrayList<>();
            newList.add(num);
            tailList.add(newList);
            tailList = (List<Object>) tailList.get(tail);
            tail = 0;
        } else {
            tailList.add(num);
        }
        count++;
        tail++;
    }

    public Integer poll(){
        if (count == 0) {
            return null;
        }

        int num = (int) headList.get(head);
        head++;
        count--;

        if (head == capacity - 1) {
            List<Object> newList = (List<Object>) headList.get(head);
            headList.clear();
            headList = newList;
            head = 0;
        }
        return num;
    }

    public static void main(String args[]){
        QueueWithFixedSizedArray obj = new QueueWithFixedSizedArray(5);
        obj.offer(1);
        obj.offer(2);
        obj.offer(3);
        obj.offer(4);
        obj.offer(5);
        obj.offer(6);
        obj.poll();
        obj.poll();
        obj.poll();
        obj.poll();
        obj.poll();
        obj.offer(7);
        obj.offer(8);
        obj.offer(9);
        obj.offer(10);
    }
}
