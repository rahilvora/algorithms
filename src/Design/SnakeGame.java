package Design;

import java.util.*;

public class SnakeGame {
    public static void main(String args[]){
        int[][] food = {{1,2},{0,1}};
        SnakeGame obj = new SnakeGame(3, 2, food);
        System.out.println(obj.move("R"));
        System.out.println(obj.move("D"));
        System.out.println(obj.move("R"));
        System.out.println(obj.move("U"));
        System.out.println(obj.move("L"));
        System.out.println(obj.move("U"));
    }

    int width;
    int height;
    int index;
    int[][] food;
    int x;
    int y;
    int sum;
    Queue<int[]> queue = new LinkedList<>();
    /** Initialize your data structure here.
     @param width - screen width
     @param height - screen height
     @param food - A list of food positions
     E.g food = [[1,1], [1,0]] means the first food is positioned at [1,1], the second is at [1,0]. */
    public SnakeGame(int width, int height, int[][] food) {
        queue.offer(new int[]{0,0});
        this.width = width;
        this.height = height;
        this.index = 0;
        sum = 0;
        x = 0;
        y = 0;
        this.food = food;
    }

    /** Moves the snake.
     @param direction - 'U' = Up, 'L' = Left, 'R' = Right, 'D' = Down
     @return The game's score after the move. Return -1 if game over.
     Game over when snake crosses the screen boundary or bites its body. */
    public int move(String direction) {
        switch (direction){
            case "U":{
                x--;
                break;
            }

            case "R":{
                y++;
                break;
            }
            case "L":{
                y--;
                break;
            }
            case "D": {
                x++;
                break;
            }
        }
        if(!isValid(x,y)) return -1;
        return process(x,y);
    }

    public int process(int x, int y){
        if(index == food.length){
            queue.poll();
        }
        else if(food[index][0] == x && food[index][1] == y){
            index++;
            sum++;
        }
        else{
            queue.poll();
        }
        for(int[] p : queue){
            if(p[0] == x && p[1] == y) return -1;
        }

        queue.offer(new int[]{x, y});
        return sum;
    }

    public boolean isValid(int x, int y){
        if(x < 0|| x >= height || y < 0 || y>=width){
            return false;
        }
        return true;
    }
}


/**
 * Your SnakeGame object will be instantiated and called as such:
 * SnakeGame obj = new SnakeGame(width, height, food);
 * int param_1 = obj.move(direction);
 */
