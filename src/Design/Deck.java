package Design;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

//https://github.com/kinshuk4/Kode/blob/master/OODExamples/src/com/vaani/ood/deck/CardFactory.java

enum Suit {
    CLUBS(1),
    SPADES(2),
    HEARTS (3),
    DIAMONDS(4) ;

    private int suitValue;

    private Suit(int suitValue_){
        suitValue = suitValue_;
    }

    public int getSuitValue()
    {
        return this.suitValue;
    }
}

enum Rank {
    TWO(2),
    THREE(3),
    FOUR(4),
    FIVE(5),
    SIX(6),
    SEVEN(7),
    EIGHT(8),
    NINE(9),
    TEN(10),
    JACK(11),
    QUEEN(12),
    KING(13),
    ACE(14)

    ;
    private int cardValue;

    private Rank(int value)
    {
        this.cardValue = value;
    }

    public int getCardValue(){
        return cardValue;
    }


}

class Card {
    Rank rank;
    Suit suit;
    Card(Rank rank, Suit suit) {
        this.rank = rank;
        this.suit = suit;
    }
    //getters and setters
    public Suit getSuit() {
        return suit;
    }

    public void setSuit(Suit suit) {
        this.suit = suit;
    }

    public Rank getRank() {
        return rank;
    }

    public void setRank(Rank rank) {
        this.rank = rank;
    }
}

public class Deck {
    private List<Card> cards = new ArrayList<>();

    public Deck() { }

    public Deck(int numberOfCards) {
        for (int j = 1; j <=4; j++ )
        for (int i=0; i<13; i++) {
            cards.add(new Card(getRank(j), getSuit(j)));
        }
    }

    private void shuffle() {
        Collections.shuffle(this.cards);
    }

    private static Suit getSuit(int cardSuit) {
        switch(cardSuit){
            case 1 : return Suit.CLUBS;
            case 2 : return Suit.DIAMONDS;
            case 3 : return Suit.HEARTS;
            case 4 : return Suit.SPADES;
        }
        return null;
    }

    private static Rank getRank(int rankNum) {
        switch(rankNum){
            case 1 : return Rank.ACE;
            case 2 : return Rank.TWO;
            case 3 : return Rank.THREE;
            case 4 : return Rank.FOUR;
            case 5 : return Rank.FIVE;
            case 6 : return Rank.SIX;
            case 7 : return Rank.SEVEN;
            case 8 : return Rank.EIGHT;
            case 9 : return Rank.NINE;
            case 10 : return Rank.TEN;
            case 11 : return Rank.JACK;
            case 12 : return Rank.QUEEN;
            case 13 : return Rank.KING;

        }

        return null;
    }

//    public void sort() {
//        Collections.sort(this.cards);
//    }
//
//    public void removeAllCards() {
//        this.cards.removeAll();
//    }

//    public void removeCard(Card c) {
//        int i = this.cards.search(c);
//        this.cards.remove(i);
//    }
//
//    public Card getCard(Card c) {
//        int i = this.cards.search(c);
//        return this.cards.get(i);
//    }
//
//    public Card getTopCard() {
//        return this.cards.pop();
//    }
//
//    public Card getNthCard(int i) {
//        return this.cards.get(i);
//    }
//
//    public Card addCard(Card c) {
//        this.cards.push(c);
//        return c;
//    }
    public static void main (String args[]) {
        Rank a = Rank.ACE;
        System.out.println(a);
    }
}
