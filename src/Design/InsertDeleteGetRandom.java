package Design;

import java.util.*;

/*

duplicate values are allowed

// Init an empty set.
RandomizedSet randomSet = new RandomizedSet();

// Inserts 1 to the set. Returns true as 1 was inserted successfully.
randomSet.insert(1);

// Returns false as 2 does not exist in the set.
randomSet.remove(2);

// Inserts 2 to the set, returns true. Set now contains [1,2].
randomSet.insert(2);

// getRandom should return either 1 or 2 randomly.
randomSet.getRandom();

// Removes 1 from the set, returns true. Set now contains [2].
randomSet.remove(1);

// 2 was already in the set, so return false.
randomSet.insert(2);

// Since 2 is the only number in the set, getRandom always return 2.
randomSet.getRandom();
* */
public class InsertDeleteGetRandom {
    /** Initialize your data structure here. */
    List<Integer> nums;
    Map<Integer, Set<Integer>> locs;
    Random random = new Random();
    public InsertDeleteGetRandom() {
        locs = new HashMap<>();
        nums = new ArrayList<>();
    }

    /** Inserts a value to the set. Returns true if the set did not already contain the specified element. */
    public boolean insert(int val) {
        boolean contains = locs.containsKey(val);
        if( ! contains) locs.put(val, new HashSet<>());
        locs.get(val).add(nums.size());
        nums.add(val);
        return !contains;
    }

    /** Removes a value from the set. Returns true if the set contained the specified element. */
    public boolean remove(int val) {
        if(!locs.containsKey(val)) return false;
        int loc = locs.get(val).iterator().next();
        locs.get(val).remove(loc);
        if(loc < nums.size() - 1){
            int lastOne = nums.get(nums.size() - 1);
            nums.set(loc, lastOne);
            locs.get(lastOne).remove(nums.size() -1);
            locs.get(lastOne).add(loc);
        }
        nums.remove(nums.size() - 1);
        if(locs.get(val).isEmpty()) locs.remove(val);
        return true;
    }

    /** Get a random element from the set. */
    public int getRandom() {
        return nums.get(random.nextInt(nums.size()));
    }
}
