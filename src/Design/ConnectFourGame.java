package Design;

import java.util.Arrays;

public class ConnectFourGame {
    int[][] grid;
    int size;
    static int count;
    ConnectFourGame(int size){
        count = 0;
        this.size = size;
        grid = new int[size][size];
        for(int[] row: grid) Arrays.fill(row, 0);
    }

    // For simplcity lets assume user provides the row and columns number.
    // In actual case, user only mentions the column number
    public void move(int row, int col, int player){
        if(player == 1){
            grid[row][col] = 1;
        }
        else {
            grid[row][col] = -1;
        }
        if(check(grid, player)){
            System.out.println("WInner");
        }
        else System.out.println("Not a winner");
    }

    public boolean check(int[][] grid, int player){
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                count = 0;
                if(dfs(grid, i, j, player) == size ) return true;
            }
        }
        return false;
    }

    public int dfs(int[][] grid, int i, int j, int player){
        if(i < 0 || j < 0 || i>=size || j >= size || grid[i][j] != player || grid[i][j] == 0) return 0;
        if(count == size) return count;
        grid[i][j] = 0;
        count++;
        dfs(grid, i - 1, j, player);
        dfs(grid, i + 1, j, player);
        dfs(grid, i, j - 1, player);
        dfs(grid, i, j + 1, player);
        grid[i][j] = player;
        return count;
    }

    public static void main(String args[]){
        ConnectFourGame obj = new ConnectFourGame(4);
        obj.move(3, 0, 1);
        obj.move(2, 0, 1);
        obj.move(1, 0, 1);
        obj.move(0, 0, 1);
    }
}
