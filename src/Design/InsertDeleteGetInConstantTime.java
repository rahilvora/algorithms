package Design;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Created by rahilvora on 1/27/18.
 */
public class InsertDeleteGetInConstantTime {
    ArrayList<Integer> nums;
    Map<Integer, Integer> map;
    Random rand = new Random();
    InsertDeleteGetInConstantTime(){
        nums = new ArrayList<>();
        map = new HashMap<>();
    }

    /** Inserts a value to the set. Returns true if the set did not already contain the specified element. */
    public boolean insert(int val) {
        boolean contains = map.containsKey(val);
        if(contains) return false;
        map.put(val, nums.size());
        nums.add(val);
        return true;
    }

    /** Removes a value from the set. Returns true if the set contained the specified element. */
    public boolean remove(int val) {
        boolean contains = map.containsKey(val);
        if(!contains) return false;
        int last = map.get(val);
        // if the val to be removed is not the last one we need to place element in the last
        // So that we can remove it in O(1) time
        if(last < nums.size()-1){
            int lastOne = nums.get(nums.size()-1);
            nums.set(last, lastOne);
            map.put(lastOne, last);
        }
        map.remove(val);
        nums.remove(nums.size() - 1);
        return true;

    }

    /** Get a random element from the set. */
    public int getRandom() {
        return nums.get(rand.nextInt((nums.size())));
    }


}


