package Design;

/**
 * Created by rahilvora on 1/17/18.
 * Time O(1);
 */
public class TicTacToe {
    private int[] rows;
    private int[] cols;
    private int diagonal;
    private int antidiagonal;

    TicTacToe(int size){
        rows = new int[size];
        cols = new int[size];
    }

    public int move(int row, int col, int player){
        int toAdd = (player == 1) ? 1: -1;
        rows[row] += toAdd;
        cols[col] += toAdd;
        if(row == col){
            diagonal += toAdd;
        }
        if(col == (cols.length - row - 1)){
            antidiagonal += toAdd;
        }
        int size = rows.length;
        if(Math.abs(rows[row]) == size ||
            Math.abs(cols[col]) == size ||
            Math.abs(diagonal) == size ||
            Math.abs(antidiagonal) == size){
            return player;
        }
        return 0;
    }
}
