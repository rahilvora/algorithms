package Design;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/**
 * Created by rahilvora on 10/28/17.
 */
public class TwoSumClass {
    HashMap<Integer, Integer> map;

    /** Initialize your data structure here. */
    public TwoSumClass() {
        map = new HashMap<>();
    }

    /** Add the number to an internal data structure.. */
    public void add(int number) {
        map.put(number, map.getOrDefault(number, 0) + 1);
    }

    /** Find if there exists any pair of numbers which sum is equal to the value. */
    public boolean find(int value) {
        for(Map.Entry<Integer, Integer> entry: map.entrySet()){
            int number = entry.getKey();
            int remainder = value - number;
            if(number == remainder && (map.get(number) > 1) || number != remainder && map.containsKey(remainder))
                return true;
        }
        return false;
    }
}
