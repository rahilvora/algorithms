package BitManipulation;

/**
 * Created by rahilvora on 1/19/18.
 */
public class CountingBits {
    public int[] countBits(int num) {
        int[] answer = new int[num + 1];
        for(int i = 0; i < answer.length; i++){
            int count = 0;
            int currNumber = i;
            while(currNumber != 0){
                if((currNumber&1) != 0){
                    count++;
                }
                currNumber = currNumber >>1;
            }
            answer[i] = count;
        }
        return answer;
    }
}
