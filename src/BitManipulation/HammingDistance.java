package BitManipulation;

/**
 * Created by rahilvora on 7/7/17.
 * ALgo
 * 1. First cal the xor of the two number
 * 2. then right shift the result and & by 1
 */
public class HammingDistance {
    //Normal
    public int hammingDistanceI(int x, int y) {
        int xor = x^y, count  = 0;
        for(int i = 0; i < 32; i++){
            count += (xor >> i) & 1;
        }
        return count;
    }
    //Using internal Function
    public int hammingDistanceII(int x, int y) {
        return Integer.bitCount(x ^ y);
    }
}
