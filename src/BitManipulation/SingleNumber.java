package BitManipulation;

/**
 * Created by rahilvora on 5/27/17.
 * Logic: XOR will return 1 only on two different bits. So if two numbers are the same, XOR will return 0. Finally only one number left.
 A ^ A = 0 and A ^ B ^ A = B.
 */
public class SingleNumber {
    public static void main(String args[]){

    }
    public int singleNumber(int[] nums){
        int result = 0;
        for(int num: nums){
            result ^= num;
        }
        return result;
    }
}
