package BitManipulation;

/**
 * Created by rahilvora on 9/3/17.
 * For each bit position 1-32 in a 32-bit integer, we count the number of integers in the array
 * which have that bit set. Then, if there are n integers in the array and k of them have a particular
 * bit set and (n-k) do not, then that bit contributes k*(n-k) hamming distance to the total.
 */

public class TotalHammingDistance {
    public int totalHammingDistance(int[] nums) {
        int total = 0, n = nums.length;
        for(int i = 0; i < 32; i++){
            int bitCount = 0;
            for(int j = 0; j < n; j++){
                bitCount += (nums[j] >> i) & 1;
            }
            total += bitCount*(n - bitCount);
        }
        return total;
    }
}
