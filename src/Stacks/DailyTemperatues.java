package Stacks;

import java.util.Arrays;
import java.util.Stack;

public class DailyTemperatues {
    public static void main(String args[]){
        int[] temperatues = {73, 74, 75, 71, 69, 72, 76, 73};
        new DailyTemperatues().dailyTemperatures(temperatues);
    }

    public int[] dailyTemperatures(int[] temperatures) {
        if(temperatures == null || temperatures.length == 0){
            return null;
        }
        int[] answer = new int[temperatures.length];
        Arrays.fill(answer, -1);
        Stack<Integer> stack = new Stack<>();

        for(int i = 0; i < temperatures.length; i++){
            if(stack.isEmpty()){
                stack.push(i);
            }
            else{
                while(!stack.isEmpty() && temperatures[stack.peek()] < temperatures[i]){
                    answer[stack.peek()] = i - stack.peek();
                    stack.pop();
                }
                stack.push(i);
            }
        }

        for(int i = 0 ; i < temperatures.length; i++){
            if(answer[i] == -1)
                answer[i] = 0;
        }

        return answer;
    }
}
