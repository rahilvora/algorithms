package Stacks;

import java.util.Stack;

/**
 * Created by rahilvora on 6/7/17.
 */
public class DecodeString {
    public String decodeString(String s) {
        if(s.length() <= 0 || s == null) return "";
        String res = "";
        Stack<String> charStack = new Stack<>();
        Stack<Integer> countStack = new Stack<>();
        int index = 0;
        while(index < s.length()){
            if(Character.isDigit(s.charAt(index))){
                int count = 0;
                while(Character.isDigit(s.charAt(index))){
                    count = 10 * count + (s.charAt(index)- '0');
                    index++;
                }
                countStack.push(count);
            }
            else if(s.charAt(index) == '['){
                charStack.push(res);
                res = "";
                index++;
            }
            else if(s.charAt(index) == ']'){
                StringBuffer temp = new StringBuffer(charStack.pop());
                int repeat =  countStack.pop();
                for(int i = 0; i < repeat; i++){
                    temp.append(res);
                }
                res = temp.toString();
                index++;
            }
            else{
                res += s.charAt(index);
                index++;
            }
        }
        return res;
    }
}
