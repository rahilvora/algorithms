package Stacks;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.TreeMap;

/**
 * Created by rahilvora on 2/20/18.
 */
public class MaxStack {
    Stack<Integer> stack, maxStack;

    MaxStack(){
        stack = new Stack<>();
        maxStack = new Stack<>();
    }

    public void push(int x) {
        pushHelper(x);
    }

    public void pushHelper(int x){
        int tempMax = maxStack.isEmpty() ? Integer.MIN_VALUE:maxStack.peek();
        if(x > tempMax){
            tempMax = x;
        }
        stack.push(x);
        maxStack.push(tempMax);
    }

    public int pop() {
        maxStack.pop();
        return stack.pop();
    }

    public int top() {
        return stack.peek();
    }

    public int peekMax() {
        return maxStack.peek();
    }

    public int popMax() {
        Stack<Integer> temp = new Stack<>();
        int max = maxStack.peek();
        while(stack.peek()!=max){
            temp.push(stack.pop());
            maxStack.pop();
        }
        stack.pop();
        maxStack.pop();
        while(!temp.isEmpty()){
            int x = temp.pop();
            pushHelper(x);
        }
        return max;
    }
}

class MaxStackII {
    TreeMap<Integer, List<Node>> treeMap;
    DoublyLinkedList dll;
    MaxStackII(){
        treeMap = new TreeMap<>();
        dll = new DoublyLinkedList();
    }

    public void push(int val){
        Node node = dll.add(val);
        if(!treeMap.containsKey(val)) treeMap.put(val, new ArrayList<>());
        treeMap.get(val).add(node);
    }

    public int pop() {
        int val = dll.pop();
        List<Node> L = treeMap.get(val);
        L.remove(L.size() - 1);
        if (L.isEmpty()) treeMap.remove(val);
        return val;
    }

    public int top(){
        return dll.peek();
    }

    public int peekMax(){
        return treeMap.lastKey();
    }

    public int MaxPop() {
        int max = peekMax();
        List<Node> L = treeMap.get(max);
        Node node = L.remove(L.size() - 1);
        dll.unlink(node);
        if (L.isEmpty()) treeMap.remove(max);
        return max;
    }

}

class DoublyLinkedList {
    Node head, tail;
    DoublyLinkedList(){
        head = new Node(0);
        tail = new Node(0);
        head.next = tail;
        tail.prev = head;

    }
    public  Node add(int val){
        Node node = new Node(val);
        node.next = tail;
        node.prev = tail.prev;
        tail.prev.next = node;
        tail.prev = node;
        return node;
    }

    public int pop() {
        return unlink(tail.prev).val;
    }

    public int peek() {
        return tail.prev.val;
    }

    public Node unlink(Node node){
        node.prev.next = node.next;
        node.next.prev = node.prev;
        return node;
    }

}
class Node {
    int val;
    Node next, prev;
    Node(int val){
        this.val = val;
        next = null;
        prev = null;
    }
}
