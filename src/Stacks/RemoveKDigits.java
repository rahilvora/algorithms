package Stacks;

import java.util.Stack;

/**
 * Created by rahilvora on 7/13/17.
 */
public class RemoveKDigits {
    public static void main(String args[]){

    }
    public String removeKdigits(String num, int k) {
        int digits = num.length() - k;
        char[] str = new char[num.length()];
        int top = 0;
        for(int i = 0; i < num.length(); i++){
            char currentChar = num.charAt(i);
            while(top > 0 && str[top - 1] > currentChar && k > 0){
                k--;
                top--;
            }
            str[top++] = currentChar;
        }
        int idx = 0;
        while(idx < digits && str[idx] == '0'){
            idx++;
        }
        return idx == digits?"0":new String(str,idx,digits-idx);
    }
}
