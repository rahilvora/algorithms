package Stacks;

import java.util.Stack;

/**
 * Created by rahilvora on 2/20/18.
 * For eg., consider following array of colors :
 R G B B G R Y

 1. BB is one couple, so remove it :
 R G G R Y

 2. GG is one such couple after removing BB, so remove it :
 R R Y

 3. RR is one such couple, so remove it :
 Y

 So, the maximum number of couples is 3.
 */
public class CoupleWithSameColor {
    public int count(String s){
        int count = 0;
        Stack<Character> stack = new Stack<>();
        for(int i = 0; i < s.length(); i++){
            if(stack.isEmpty()){
                stack.push(s.charAt(i));
            }
            else if(stack.peek() == s.charAt(i)){
                stack.pop();
                count++;
            }
            else{
                stack.push(s.charAt(i));
            }
        }
        return count;
    }
}

