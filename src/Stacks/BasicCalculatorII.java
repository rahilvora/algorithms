package Stacks;

import java.util.Stack;

//(1+1)
public class BasicCalculatorII {
    public int calculate(String s) {
        Stack<Integer> stack = new Stack<>();
        int sign = 1, result = 0, number = 0;
        for(int i = 0; i < s.length(); i++){
            char curr = s.charAt(i);
            if(Character.isDigit(curr)){
                number = number*10 + curr - '0';
            }
            else if(curr == '+') {
                result += sign*number;
                number = 0;
                sign = 1;
            }
            else if(curr == '-'){
                result += sign*number;
                number = 0;
                sign = -1;
            }
            else if(curr == '('){
                stack.push(result);
                stack.push(sign);
                result = 0;
                sign = 1;
            }
            else if(curr == ')'){
                result += sign*number;
                number = 0;
                result *= stack.pop(); //stack.pop() is the sign before the parenthesis
                result += stack.pop(); //stack.pop() now is the result calculated before the parenthesis
            }
        }
        return result;
    }
}
