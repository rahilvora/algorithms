package Stacks;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

//Time Complexity: O(n)
//Space: O(n/2) as stack grows up to n/2, where n is number of elements

/**
 * Created by rahilvora on 9/2/17.
 */

    public class ExclusiveTimeOfFunction {
        public static void main(String args[]){
            List<String> logs = new ArrayList<>();
            logs.add("0:start:0");
            logs.add("1:start:2");
            logs.add("1:end:5");
            logs.add("0:end:6");
            new ExclusiveTimeOfFunction().exclusiveTime(2, logs);
        }
        public int[] exclusiveTime(int n, List<String> logs) {
            Stack<Integer> stack = new Stack<>();
            int[] res = new int[n];
            int i = 1;
            String[] log = logs.get(0).split(":");
            stack.push(Integer.parseInt(log[0]));
            int prev = Integer.parseInt(log[2]);
            while(i < logs.size()){
                log = logs.get(i).split(":");
                if(log[1].equals("start")){
                    if(!stack.isEmpty()){
                        res[stack.peek()] += Integer.parseInt(log[2])- prev;
                    }
                    stack.push(Integer.parseInt(log[0]));
                    prev = Integer.parseInt(log[2]);
                }
                else{
                    res[stack.peek()] += Integer.parseInt(log[2])- prev + 1;
                    stack.pop();
                    prev = Integer.parseInt(log[2]) + 1;
                }
                i++;
            }
            return res;
        }
    }

