package Stacks;

import java.util.Stack;

/**
 * Created by rahilvora on 11/1/17.
 */
public class PrefixToPostfix {
    static String[] prefixToPostfix(String[] prefixes) {

        String[] result = new String[prefixes.length];
        for(int i = 0; i < prefixes.length; i++){
            result[i] = convertor(prefixes[i]);
        }
        return result;
    }
    public static String convertor(String prefixString){
        Stack<String> stack = new Stack<>();
        StringBuffer result = new StringBuffer();
        for(int i = prefixString.length(); i >=0; i--){
            char currChar = prefixString.charAt(i);
            if(currChar >=48 && currChar <= 57){
                stack.push(String.valueOf(currChar));
            }
            else{
                String op1 = stack.pop();
                String op2 = stack.pop();
                stack.push(op1 + op2 + currChar);
            }
        }
        while(!stack.isEmpty()){
            result.append(stack.pop());
        }
        return result.toString();
    }
}
