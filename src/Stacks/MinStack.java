package Stacks;

import java.util.Stack;

/**
 * Created by rahilvora on 6/4/17.
 */
public class MinStack {
    Stack<Integer> stack;
    int min = Integer.MAX_VALUE;
    public MinStack() {
        stack = new Stack<>();
    }
    public void push(int x) {
        if(x <= min){
            stack.push(min);
            min = x;
        }
        stack.push(x);
    }
    public void pop() {
        if(stack.pop() == min) min = stack.pop();
    }
    public int top() {
        return stack.peek();
    }
    public int getMin() {
        return min;
    }

    public static void main(String args[]){
        MinStack obj= new MinStack();
        obj.push(4);
        obj.push(2);
        obj.push(3);
        obj.push(1);
        obj.pop();
        System.out.print(obj.getMin());
    }
}
