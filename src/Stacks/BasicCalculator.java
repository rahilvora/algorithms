package Stacks;

import java.util.Stack;

/**
 * Created by rahilvora on 3/1/18.
 */
public class BasicCalculator {
    public static void main(String args[]){
        System.out.print(new BasicCalculator().calculate("3+2*2"));
    }
    public int calculate(String s) {
        if(s.isEmpty()) return 0;
        Stack<Integer> stack = new Stack<>();
        char sign = '+';
        int num = 0;
        for(int i = 0; i < s.length(); i++){
            char curr = s.charAt(i);
            if(Character.isDigit(curr)){
                num = num * 10 + curr - '0';
            }
            if(!Character.isDigit(curr) && ' '!=curr || i == s.length() - 1){
                switch(sign){
                    case '+':
                        stack.push(num);
                        break;
                    case '-':
                        stack.push(-num);
                        break;
                    case '*':
                        stack.push(stack.pop() * num);
                        break;
                    case '/':
                        stack.push(stack.pop() / num);
                        break;
                }
                sign = curr;
                num = 0;
            }

        }
        num = 0;
        while(!stack.empty()){
            num += stack.pop();
        }
        return num;
    }
}
