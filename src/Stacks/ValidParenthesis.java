package Stacks;

import java.util.Stack;

/**
 * Created by rahilvora on 6/8/17.
 */
public class ValidParenthesis {
    public boolean isValid(String s) {
        Stack<Character> stack = new Stack<>();
        int i = 0;
        while(i < s.length()){
            char currentChar = s.charAt(i);
            if(currentChar == '[' || currentChar == '{' || currentChar == '('){
                stack.push(currentChar);
            }
            else if(currentChar == ']' && stack.peek() != '{' && stack.peek() != '('){
                stack.pop();
            }
            else if(currentChar == '}' && stack.peek() != '[' && stack.peek() != '('){

            }
            else if(currentChar == ')' && stack.peek() != '{' && stack.peek() != '['){
                stack.pop();
            }
            else {
                return false;
            }
        }
        return stack.isEmpty();
    }
}
