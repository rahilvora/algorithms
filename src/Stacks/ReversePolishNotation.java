package Stacks;

import java.util.Stack;

/**
 * Created by rahilvora on 1/26/18.
 *   ["2", "1", "+", "3", "*"] -> ((2 + 1) * 3) -> 9
 */
public class ReversePolishNotation {
    public int evalRPN(String[] tokens) {
        Stack<Integer> S = new Stack<>();
        for(String token: tokens){
            if(token.equals("+")){
                S.add(S.pop() + S.pop());
            }
            else if(token.equals("/")){
                int b = S.pop(), a = S.pop();
                S.add(a/b);
            }
            else if(token.equals("*")){
                S.add(S.pop() * S.pop());
            }
            else if(token.equals("-")){
                int b = S.pop(), a = S.pop();
                S.add(a-b);
            }
            else{
                S.push(Integer.valueOf(token));
            }
        }
        return S.pop();
    }
}
