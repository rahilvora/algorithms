package Stacks;

/**
 * Created by rahilvora on 8/14/17.
 */
public class MaximalRectangle {
    public int maximalRectangle(char[][] matrix) {
        int maxArea = 0, area = 0;
        if(matrix.length == 0) return maxArea;
        int[] tempArray = new int[matrix[0].length];
        LargestRectangleInHistogram obj = new LargestRectangleInHistogram();
        for(int i = 0; i < matrix.length; i++){
            for(int j = 0; j < matrix[0].length; j++){
                if(matrix[i][j] == '0'){
                    tempArray[j] = 0;
                }
                else{
                    tempArray[j] += matrix[i][j] - '0';
                }
            }
            area = obj.largestRectangleArea(tempArray);
            maxArea = Math.max(area, maxArea);
        }
        return maxArea;
    }
}
