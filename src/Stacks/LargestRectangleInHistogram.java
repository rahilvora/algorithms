package Stacks;

import java.util.Stack;

/**
 * Created by rahilvora on 8/14/17.
 */
public class LargestRectangleInHistogram {
    public int largestRectangleArea(int[] heights) {
        int maxArea = 0, area = 0;
        Stack<Integer> stack = new Stack<>();
        int i = 0;
        for(;i<heights.length;){
            if(stack.isEmpty() || heights[stack.peek()] <= heights[i]){
                stack.push(i++);
            }
            else{
                int top = stack.pop();
                if(stack.isEmpty()){
                    area = heights[top] * i;
                }
                else{
                    area = heights[top] * (i - stack.peek() - 1);

                }
                maxArea = Math.max(area, maxArea);
            }
        }
        while(!stack.isEmpty()){
            int top = stack.pop();
            if(stack.isEmpty()){
                area = heights[top] * i;
            }
            else{
                area = heights[top] * (i - stack.peek() - 1);

            }
            maxArea = Math.max(area, maxArea);
        }
        return maxArea;
    }
}