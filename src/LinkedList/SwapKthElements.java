package LinkedList;

/**
 * Created by rahilvora on 6/28/17.
 */
public class SwapKthElements {
    public ListNode swapNodes(ListNode head, int k) {
        ListNode temp = head;
        int listLength = 0;
        // Calculate List Length
        while(temp != null){
            listLength++;
            temp = temp.next;
        }

        //No need to swap if the kth element from start and end is the same
        if(listLength == 2*k-1){
            return head;
        }

        // Keep track of left and right pointers along with their previous nodes

        // Left pointer
        ListNode leftPrevNode = null;
        ListNode leftNode = head;

        //Move leftNode to Kth Element from the beginning

        for(int i = 0; i < k; i++){
            leftPrevNode = leftNode;
            leftNode = leftNode.next;
        }

        //Right pointer
        ListNode rightPrevNode = null;
        ListNode rightNode = head;

        //Mode rightNode to kth Element from the end

        for(int i = 0; i < listLength - k + 1; i++){
            rightPrevNode = rightNode;
            rightNode = rightNode.next;
        }
        // Swap
        ListNode tmp = rightNode;
        leftPrevNode.next = rightNode;
        rightPrevNode.next = leftNode;
        rightNode.next = leftNode.next;
        leftNode.next = tmp.next;

        return head;

    }
}
