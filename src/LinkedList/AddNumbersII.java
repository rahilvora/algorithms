package LinkedList;

import java.util.Stack;

public class AddNumbersII {
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        Stack<Integer> s1 = new Stack<>();
        Stack<Integer> s2 = new Stack<>();
        Stack<ListNode> s3 = new Stack<>();
        while(l1 != null){
            s1.push(l1.val);
            l1 = l1.next;
        }
        while(l2 != null){
            s2.push(l2.val);
            l2 = l2.next;
        }
        int carry = 0;
        while(!s1.isEmpty() || !s2.isEmpty()){
            int sum = (s1.isEmpty() ? 0: s1.pop()) + (s2.isEmpty() ? 0: s2.pop());
            ListNode node = new ListNode(sum%10);
            s3.push(node);
            carry = sum/10;
        }
        if(carry > 0) s3.push(new ListNode(carry));
        ListNode newHead = new ListNode(-1);
        ListNode dummy = newHead;
        while(!s3.isEmpty()){
            dummy.next = s3.pop();
            dummy = dummy.next;
        }
        return newHead.next;
    }
}
