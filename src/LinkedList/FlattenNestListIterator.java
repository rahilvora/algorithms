package LinkedList;

import java.util.Iterator;
import java.util.List;
import java.util.Stack;

/**
 * Created by rahilvora on 8/23/17.
 */
public class FlattenNestListIterator implements Iterator<NestedInteger>{
    Stack<NestedInteger> stack = new Stack<>();
    FlattenNestListIterator(List<NestedInteger> nestedList){
        for(int i = nestedList.size() - 1; i >= 0; i--){
            stack.push(nestedList.get(i));
        }
    }
    @Override
    public boolean hasNext() {
        while(!stack.isEmpty()){
            NestedInteger currNode = stack.peek();
            if(currNode.isInteger())return true;
            stack.pop();
            for(int i = currNode.getList().size() - 1; i>=0; i--){
                stack.push(currNode.getList().get(i));
            }
        }
        return false;
    }

    @Override
    public NestedInteger next() {
        stack.pop().getInteger();
        return null;
    }
}
