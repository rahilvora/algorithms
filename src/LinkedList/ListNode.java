package LinkedList;

/**
 * Created by rahilvora on 5/29/17.
 */
public class ListNode {
    public int val;
    public ListNode next;
    public ListNode(int x) { val = x; }
}
