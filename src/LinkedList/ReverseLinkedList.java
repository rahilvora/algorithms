package LinkedList;

/**
 * Created by rahilvora on 5/29/17.
 */
public class ReverseLinkedList {

    public ListNode reverse(ListNode head){
        ListNode headNew = null;
        while(head != null){
            ListNode nextNode = head.next;
            head.next = headNew;
            headNew = head;
            head = nextNode;

        }
        return headNew;
    }
}
