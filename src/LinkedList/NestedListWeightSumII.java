package LinkedList;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class NestedListWeightSumII {
    public int depthSumInverse(List<NestedInteger> nestedList) {
        Queue<NestedInteger> queue = new LinkedList<>();
        int prev = 0, total = 0;
        for(NestedInteger nestedInteger: nestedList){
            queue.offer(nestedInteger);
        }
        while (!queue.isEmpty()){
            int size = queue.size();
            int levelSum = 0;
            for(int i = 0; i <  size; i++){
                NestedInteger curr = queue.poll();
                if(curr.isInteger()) levelSum += curr.getInteger();
                List<NestedInteger> currList = curr.getList();
                if(currList != null){
                    queue.addAll(currList);
                }
            }
            prev += levelSum;
            total += prev;
        }
        return total;
    }
}
