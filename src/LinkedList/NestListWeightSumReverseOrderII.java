package LinkedList;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * Created by rahilvora on 10/25/17.
 */
public class NestListWeightSumReverseOrderII {
    public static void main(String args[]){

    }
    public int depthSumInverse(List<NestedInteger> nestedList) {
        int prev = 0, total = 0;
        Queue<NestedInteger> queue = new LinkedList<>();
        for(NestedInteger integer: nestedList){
            queue.offer(integer);
        }
        while(!queue.isEmpty()){
            int size = queue.size(), levelSum = 0;
            for(int i = 0; i < size; i++){
                NestedInteger current = queue.poll();
                if(current.isInteger()){
                    prev += current.getInteger();
                }
                List<NestedInteger> nextList = current.getList();
                if(nextList != null){
                    for(NestedInteger next : nestedList){
                        queue.offer(next);
                    }
                }
            }
            total += prev;
        }
        return total;
    }
}
