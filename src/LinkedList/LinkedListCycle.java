package LinkedList;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by rahilvora on 5/31/17.
 * Using Floyd Cycle Detection Algorithm to detect the cycle present in Singly LinkedList
 */
public class LinkedListCycle {
    //O(N) space complexity using extra space
    public boolean hasCycleI(ListNode head) {
        Set<ListNode> set = new HashSet<>();
        while(head != null){
            if(set.add((head)))
                head = head.next;
            else
                return true;
        }
        return false;
    }
    //Using constant space
    public boolean hasCycleII(ListNode head) {
        if(head==null) return false;
        ListNode walker = head;
        ListNode runner = head;
        while(runner.next!=null && runner.next.next!=null) {
            walker = walker.next;
            runner = runner.next.next;
            if(walker==runner) return true;
        }
        return false;
    }
}
