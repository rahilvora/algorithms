package LinkedList;

/**
 * Created by rahilvora on 8/30/17.
 */
public class ReverseNodeInKGroup {
    public ListNode reverseKGroup(ListNode head, int k) {
        if (head == null) return null;
        ListNode temp = head;
        int len = 0;
        while(temp != null) {
            temp = temp.next;
            len++;
        }

        if ( len == 1 ) return head;

        ListNode prevStart = null;
        ListNode currNode = head;
        ListNode startNode = head;
        for (int i = 0; i < len/k; i++) {
            int counter = 0;
            ListNode prevNode = null;
            ListNode nextNode = null;

            while (counter < k) {
                nextNode = currNode.next;
                currNode.next = prevNode;
                prevNode = currNode;
                currNode = nextNode;
                counter++;
            }
            if (prevStart != null) {
                prevStart.next = prevNode;
            } else head = prevNode;
            prevStart = startNode;
            startNode = currNode;
        }

        if (prevStart != null)
            prevStart.next = currNode;
        return head;

    }
}
