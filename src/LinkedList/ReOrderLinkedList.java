package LinkedList;

/**
 * Created by rahilvora on 6/10/17.
 */
public class ReOrderLinkedList {
    public void reorderList(ListNode head) {
        if(head == null || head.next == null) return;
        ListNode slow = head, fast = head;
        while(fast.next != null && fast.next.next != null){
            slow = slow.next;
            fast = fast.next.next;
        }
        ListNode preMiddle = slow;
        ListNode preCurrent = slow.next;
        while(preCurrent.next != null){
            ListNode current = preCurrent.next;
            preCurrent.next = current.next;
            current.next = preMiddle.next;
            preMiddle.next = current;
        }
        ListNode p1 = head, p2 = preMiddle.next;
        while(p1 != preMiddle){
            preMiddle.next = p2.next;
            p2.next = p1.next;
            p1.next = p2;
            p1 = p2.next;
            p2 = preMiddle.next;
        }
    }

    /*
    * Simpler approach
    * Algo:
    * 1. Divide the list
    * 2. reverse second part of the list
    * 3. Merge the list
    * */

    public void reorderList1(ListNode head){
        ListNode prev = null, slow = head, fast = head;
        if(head == null || head.next == null) return;
        while(fast != null && fast.next != null){
            prev = slow;
            slow = slow.next;
            fast = fast.next.next;
        }
        // prev is end of first list and slow is start of another list
        prev.next = null;

        // Reverse second half of the list
        slow = new ReverseLinkedList().reverse(slow);
        merge(head, slow);
    }
    public void merge(ListNode l1, ListNode l2){
        while(l1 != null){
            ListNode n1 = l1.next, n2 = l2.next;
            l1.next = l2;
            if(n1 == null) break;
            l2.next = n2;
            l1 = n1;
            l2 = n2;
        }

    }
}
