package LinkedList;

import java.util.Comparator;
import java.util.PriorityQueue;

/**
 * Created by rahilvora on 1/29/18.
 * Time Complexity: O(nLogk): n is total number of elements and K is size of lists
 */
public class MergeKSortedList {
    public ListNode mergeKLists(ListNode[] lists) {
        if(lists == null || lists.length == 0){
            return null;
        }

        PriorityQueue<ListNode> queue = new PriorityQueue<>(new Comparator<ListNode>(){
            @Override
            public int compare(ListNode l1, ListNode l2){
                return l1.val - l2.val;
            }
        });

        for(ListNode list: lists){
            if(list != null){
                queue.offer(list);
            }
        }

        ListNode headNew = new ListNode(0);
        ListNode p = headNew;

        while(!queue.isEmpty()){
            p.next = queue.poll();
            p = p.next;

            if(p.next != null){
                queue.offer(p.next);
            }
        }
        return headNew.next;
    }
}
