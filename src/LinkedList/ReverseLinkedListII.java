package LinkedList;

/**
 * Created by rahilvora on 2/20/18.
 */
public class
ReverseLinkedListII {
    public ListNode reverseBetween(ListNode head, int m, int n) {
        ListNode dummy = new ListNode(0);
        ListNode pre = dummy;
        pre.next = head;
        for(int i = 0; i < m-1; i++){
            pre = pre.next;
        }
        ListNode start = pre.next;
        ListNode then = start.next;

        for(int i = 0; i < n-m; i++){
            start.next = then.next;
            then.next = pre.next;
            pre.next = then;
            then = start.next;
        }
        return dummy.next;
    }
}
