package LinkedList;

/**
 * Created by rahilvora on 5/29/17.
 */
public class DeleteNode {
    public void deleteNode(ListNode node) {
        node.val = node.next.val;
        node.next = node.next.next;
    }
}
