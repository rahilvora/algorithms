package LinkedList;

public class PlusOne {
    public static void main(String args[]){
        ListNode head = new ListNode(1);
        head.next = new ListNode(2);
        head.next.next = new ListNode(3);
        new PlusOne().plusOne(head);
    }
    public ListNode plusOne(ListNode head) {
        if(head == null) return head;
        ListNode newHead = reverse(head);
        ListNode temp = newHead;
        int carry = 1;
        while(temp != null){
            int sum = temp.val + carry;
            temp.val = sum%10;
            carry = sum/10;
            temp = temp.next;
        }
        if(carry != 0){
            ListNode updateHead = new ListNode(carry);
            updateHead.next = reverse(newHead);
            return updateHead;
        }
        return reverse(newHead);
    }

    public ListNode reverse(ListNode head){
        ListNode newHead = null;
        while(head != null){
            ListNode next = head.next;
            head.next = newHead;
            newHead = head;
            head = next;
        }
        return newHead;
    }
}
