package LinkedList;

import java.util.List;

/**
 * Created by rahilvora on 5/14/17.
 */
  interface NestedInteger {

    // @return true if this NestedInteger holds a single integer, rather than a nested list.
    public boolean isInteger();

    // @return the single integer that this NestedInteger holds, if it holds a single integer
    // Return null if this NestedInteger holds a nested list
    public Integer getInteger();

    // @return the nested list that this NestedInteger holds, if it holds a nested list
    // Return null if this NestedInteger holds a single integer
    public List<NestedInteger> getList();
 }
public class NestedListWeightSum implements NestedInteger{
    public static void main(String args[]){

    }
    public int depthSum(List<NestedInteger> nestedList) {
        return helper(nestedList,1);
    }

    public int helper(List<NestedInteger> nestedList, int depth){
        int total = 0;
        for(NestedInteger integer:nestedList){
            total += integer.isInteger()?integer.getInteger()*depth:helper(integer.getList(),++depth);
        }
        return total;
    }

    @Override
    public boolean isInteger() {
        return false;
    }

    @Override
    public Integer getInteger() {
        return null;
    }

    @Override
    public List<NestedInteger> getList() {
        return null;
    }
}
