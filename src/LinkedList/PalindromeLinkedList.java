package LinkedList;

import java.util.Stack;

/**
 * Created by rahilvora on 8/24/17.
 */
public class PalindromeLinkedList {
    public boolean isPalindrome(ListNode head) {
        ListNode fast = head, slow = head;
        Stack<ListNode> stack = new Stack<>();
        while(fast != null && fast.next != null){
            stack.push(slow);
            fast = fast.next.next;
            slow = slow.next;
        }
        if(fast != null) slow = slow.next;
        while(slow != null){
            if(stack.pop().val != slow.val)return false;
            slow = slow.next;
        }
        return true;
    }
}
