package LinkedList;

/**
 * Created by rahilvora on 1/18/18.
 */
public class RemoveNthNodeFromEnd {
    public ListNode removeNthFromEnd(ListNode head, int n) {
        ListNode newHead = new ListNode(234);
        newHead.next = head;
        ListNode fast = newHead;
        ListNode slow = newHead;
        while(fast.next != null){
            if(n <= 0){
                slow = slow.next;
            }
            else{
                n--;
            }
            fast = fast.next;
        }
        if(slow.next != null){
            slow.next = slow.next.next;
        }
        return newHead.next;
    }
}
