package Graphs;

import java.util.LinkedList;
import java.util.Queue;

public class FriendCircles {
    public static void main(String args[]){

    }

    public int findCircleNum(int[][] M) {
        int[] visited = new int[M.length];
        int count = 0;
        Queue<Integer> queue = new LinkedList<>();
        for(int i = 0; i < M.length; i++){
            if(visited[i] == 0){
                queue.add(i);
                while(!queue.isEmpty()){
                    int curr = queue.poll();
                    visited[curr] = 1;
                    for(int j = 0; j < M.length; j++){
                        if(M[curr][j] == 1 && visited[j] != 1){
                            queue.offer(j);
                        }
                    }
                }
                count++;
            }
        }
        return count;
    }
}
