package Graphs;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class EvaluateDivision {
    public double[] calcEquation(String[][] equations, double[] values, String[][] queries) {
        Map<String, Map<String, Double>> graph = new HashMap<>();
        buildGraph(equations, graph, values);
        Set<String> visited = new HashSet<>();
        double[] res = new double[queries.length];
        for(int i = 0; i < queries.length; i++){
            if(graph.containsKey(queries[i][0]) && graph.containsKey(queries[i][1])){
                res[i] = dfs(graph, queries[i][0], queries[i][1], visited, 1.0);
                visited.clear();
            }
            else{
                res[i] = -1;
            }
        }
        return res;
    }

    public double dfs(Map<String, Map<String, Double>> graph, String start, String end, Set<String> visited, double prod){
        if(visited.contains(start)) return -1;
        if(start.equals(end)) return prod;
        visited.add(start);
        for(Map.Entry<String, Double> entry: graph.get(start).entrySet()){
            double res = dfs(graph, entry.getKey(), end, visited, prod*entry.getValue());
            if(res != -1) return res;
        }
        return -1;
    }

    public void buildGraph(String[][] equations, Map<String, Map<String, Double>> graph, double[] values){
        for(int i = 0; i < equations.length; i++){
            graph.putIfAbsent(equations[i][0], new HashMap<>());
            graph.get(equations[i][0]).put(equations[i][1], values[i]);
            graph.putIfAbsent(equations[i][1], new HashMap<>());
            graph.get(equations[i][1]).put(equations[i][0], 1/values[i]);
        }
    }
}
