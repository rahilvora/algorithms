package Graphs;

import java.util.*;

/**
 * Created by rahilvora on 2/19/18.
 */
public class ReconstructItinary {
    public static void main(String args[]){
        String[][] tickets = {
                {"MUC","LHR"},
                {"JFK", "MUC"},
                {"SFO", "SJC"},
                {"LHR", "SFO"}
        };
        new ReconstructItinary().findItinerary(tickets);
    }
    public List<String> findItinerary(String[][] tickets) {
        List<String> route = new ArrayList<>();
        Map<String, PriorityQueue<String>> map = new HashMap<>();
        for(int i = 0; i < tickets.length; i++){
            PriorityQueue<String> queue = new PriorityQueue<>();
            if(map.containsKey(tickets[i][0])){
                queue = map.get(tickets[i][0]);
            }
            queue.add(tickets[i][1]);
            map.put(tickets[i][0], queue);
        }

        Stack<String> stack = new Stack<>();
        stack.push("JFK");
        while(!stack.isEmpty()){
            while (map.containsKey(stack.peek()) && !map.get(stack.peek()).isEmpty()){
                stack.push(map.get(stack.peek()).poll());
            }
            route.add(0, stack.pop());
        }

        return route;
    }
}