package Graphs;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by rahilvora on 6/10/17.
 */
public class CloneGraph {
    public UndirectedGraphNode cloneGraph(UndirectedGraphNode node) {
        if(node == null) return node;
        Map<UndirectedGraphNode, UndirectedGraphNode> map = new HashMap<>();
        LinkedList<UndirectedGraphNode> queue = new LinkedList<>();
        UndirectedGraphNode rootNew = new UndirectedGraphNode(node.label);
        map.put(node, rootNew);
        queue.offer(node);
        while(!queue.isEmpty()){
            UndirectedGraphNode curr = queue.poll();
            List<UndirectedGraphNode> currNeighbours = map.get(curr).neighbors;
            for(UndirectedGraphNode aNeighbor: currNeighbours){
                if(!map.containsKey(aNeighbor)){
                    UndirectedGraphNode copy = new UndirectedGraphNode(aNeighbor.label);
                    map.put(aNeighbor,copy);
                    map.get(curr).neighbors.add(copy);
                    queue.add(aNeighbor);
                }
                else{
                    map.get(curr).neighbors.add(map.get(aNeighbor));
                }
            }
        }
        return rootNew;
    }
}
