package Graphs;

import java.util.LinkedList;

/**
 * Created by rahilvora on 2/26/18.
 */
public class Graph {
    private int size;
    public LinkedList<Integer> adj[];

    Graph(int v){
        size = v;
        adj = new LinkedList[size];
        for(int i = 0; i < size; i++){
            adj[i] = new LinkedList();
        }
    }
    public void addEdge(int a, int b){
        adj[a].add(b);
    }
}
