package Graphs;

import java.util.ArrayList;
import java.util.List;

public class GraphValidTree {
    public static void main(String args[]){
        int[][] edge = {{0,1}, {1,2}, {2,3}, {1,3}, {1,4}};
        new GraphValidTree().validTree(5, edge);
    }
    public boolean validTree(int n, int[][] edges) {
        List<List<Integer>> adjList = new ArrayList<List<Integer>>(n);
        for(int i = 0; i < n; i++){
            adjList.add(i, new ArrayList<>());
        }
        for(int[] edge: edges){
            adjList.get(edge[0]).add(edge[1]);
            adjList.get(edge[1]).add(edge[0]);
        }

        boolean[] visited = new boolean[n];

        if(hasCycle(adjList, visited, 0, -1)) return false;

        for(int i = 0; i < visited.length; i++){
            if(!visited[i]) return false;
        }
        return true;
    }

    public boolean hasCycle(List<List<Integer>> adjList, boolean[] visited, int node, int parent){
        visited[node] = true;
        for(int i = 0;  i < adjList.get(node).size(); i++){
            int v = adjList.get(node).get(i);

            if((visited[v] && parent != v) || (!visited[v] && hasCycle(adjList, visited, v, node))) return true;
        }

        return false;
    }

}
