package Graphs;

import java.util.*;

public class SentenceSimilarity {
    public static void main(String args[]){
        String[] w1 = {"great", "acting", "skills"};
        String[] w2 = {"fine", "drama", "talent"};
        String[][] pairs = {{"great", "good"}, {"fine", "good"}, {"acting","drama"}, {"skills","talent"}};
        System.out.println(new SentenceSimilarity().areSentencesSimilarTwo(w1, w2, pairs));
    }
    public boolean areSentencesSimilarTwo(String[] words1, String[] words2, String[][] pairs) {
        if (words1.length != words2.length) {
            return false;
        }
        Map<String, Set<String>> map = new HashMap<>();
        for(int i = 0; i < pairs.length; i++){
            map.putIfAbsent(pairs[i][0], new HashSet<>());
            map.putIfAbsent(pairs[i][1], new HashSet<>());
            map.get(pairs[i][1]).add(pairs[i][0]);
            map.get(pairs[i][0]).add(pairs[i][1]);
        }
        for (int i = 0; i < words1.length; i++) {
            if (words1[i].equals(words2[i])) continue;
            if (!map.containsKey(words1[i])) return false;
            if (!dfs(map, words1[i], words2[i], new HashSet<>())) return false;
        }
        return true;
    }

    public boolean dfs(Map<String, Set<String>> map, String source, String target, Set<String> visited){
        if (map.get(source).contains(target)) return true;
        if(visited.add(source)){
            for(String str: map.get(source)){
                if(!visited.contains(str) && dfs(map, str, target, visited)) return true;
            }
        }
        return false;
    }
}
