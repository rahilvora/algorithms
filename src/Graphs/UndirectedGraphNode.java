package Graphs;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rahilvora on 6/10/17.
 */
public class UndirectedGraphNode {
    int label;
    List<UndirectedGraphNode> neighbors;
    UndirectedGraphNode(int x) { label = x; neighbors = new ArrayList<UndirectedGraphNode>(); }
}
