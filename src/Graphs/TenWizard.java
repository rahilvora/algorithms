package Graphs;

import java.util.*;

public class TenWizard {
    class Wizard{
        int id;
        int dist;
        Wizard(int id){
            this.id = id;
            this.dist = Integer.MAX_VALUE;
        }
    }

    public static void main(String args[]){
        int[][] ids = {{1, 5, 9}, {2, 3, 9}, {4}, {}, {}, {9}, {}, {}, {}, {}};
        List<List<Integer>> wizards = new ArrayList<>();
        for (int i = 0; i < ids.length; i++) {
            List<Integer> wizard = new ArrayList<>();
            for (int j = 0; j < ids[i].length; j++) {
                wizard.add(ids[i][j]);
            }
            wizards.add(wizard);
        }
        List<Integer> res = new TenWizard().getShortestPath(wizards, 0, 9);
    }
    public List<Integer> getShortestPath(List<List<Integer>> wizards, int source, int target) {
        int n = wizards.size();
        int[] parent = new int[n];
        Map<Integer, Wizard> map = new HashMap<>();
        for (int i = 0; i < n; i++) {
            parent[i] = i;
            map.put(i, new Wizard(i));
        }
        map.get(source).dist = 0;
        PriorityQueue<Wizard> pq = new PriorityQueue<>(new Comparator<Wizard>() {
            @Override
            public int compare(Wizard o1, Wizard o2) {
                return o1.dist - o2.dist;
            }
        });
        pq.offer(map.get(source));
        while(!pq.isEmpty()){
            Wizard curr = pq.poll();
            List<Integer> neighbors = wizards.get(curr.id);
            for(int neighbor: neighbors){
                Wizard next = map.get(neighbor);
                int weight = (int) Math.pow(next.id - curr.id , 2);
                if(curr.dist + weight < next.dist){
                    parent[next.id] = curr.id;
                    pq.remove(next);;
                    next.dist = curr.dist + weight;
                    pq.add(next);
                }
            }
        }
        List<Integer> res = new ArrayList<>();
        int t = target;
        while (t != source) {
            res.add(t);
            t = parent[t];
        }
        res.add(source);
        Collections.reverse(res);
        return res;
    }
}
