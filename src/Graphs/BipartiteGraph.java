package Graphs;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

/**
 * Created by rahilvora on 1/21/18.
 */
public class BipartiteGraph {
    public static void main(String args[]){
        int[][] graph = {
                        {0, 1, 0, 1},
                        {1, 0, 1, 0},
                        {0, 1, 0, 1},
                        {1, 0, 1, 0}
        };
        System.out.print(new BipartiteGraph().checkBipartiteGraph(graph, 0));

    }
    public boolean checkBipartiteGraph(int[][] graph, int src){
        Queue<Integer> queue = new LinkedList<>();
        queue.offer(src);
        // Sort color for each vertex
        int[] colorOfVertices = new int[graph[0].length];

        // -1 means no color is assigned to that particular vertex
        Arrays.fill(colorOfVertices, -1);

        colorOfVertices[src] = 1;

        while(!queue.isEmpty()){
            int vertex1 = queue.poll();

            // Check if there is self connecting node,
            // if yes return false as there cannot be self connecting node in bipartite graph
            if(graph[vertex1][vertex1] == 1){
                return false;
            }

            for(int vertex2 = 0; vertex2 < graph[0].length; vertex2++){
                if(graph[vertex1][vertex2] == 1 && colorOfVertices[vertex2] == -1){
                    colorOfVertices[vertex2] = 1 - colorOfVertices[vertex1];
                    queue.offer(vertex2);
                }
                else if(graph[vertex1][vertex2] == 1 && colorOfVertices[vertex1] == colorOfVertices[vertex2]){
                    return false;
                }
            }
        }
        return true;
    }

    public boolean isBipartitie(int[][] graph){
        int n = graph.length;
        int[] colors = new int[n];
        Arrays.fill(colors, -1);
        for(int i = 0; i < n; i++){
            if(colors[i] == -1){
                Stack<Integer> stack = new Stack<>();
                stack.push(i);
                colors[i] = 0;

                while(!stack.isEmpty()){
                    Integer node  =  stack.pop();
                    for(int nei: graph[node]){

                    }
                }

            }
        }
        return true;
    }
}
