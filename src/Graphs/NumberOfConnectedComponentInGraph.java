package Graphs;

import java.util.*;

public class NumberOfConnectedComponentInGraph {
    public int countComponents(int n, int[][] edges) {
        if(n <= 1) return n;
        Map<Integer, List<Integer>>  map = new HashMap<>();
        for(int i = 0; i < n; i++){
            map.put(i, new ArrayList<>());
        }
        for (int[] edge: edges){
            map.get(edge[0]).add(edge[1]);
            map.get(edge[1]).add(edge[0]);
        }

        boolean[] visited = new boolean[n];
        int count = 0;
        for(int i = 0; i < n; i++){
            if(!visited[i]){
                visited[i] = true;
                dfsVisited(i, map, visited);
                count++;
            }
        }
        return count;
    }

    public void dfsVisited(int i, Map<Integer, List<Integer>> map, boolean[] visited){
        for(int j : map.get(i)){
            if(!visited[j]){
                visited[j] = true;
                dfsVisited(j, map, visited);
            }
        }
    }
}
