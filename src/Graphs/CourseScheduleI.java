package Graphs;

import java.util.LinkedList;

/**
 * Created by rahilvora on 6/12/17.
 *
 * // Analysis: This problem can be assumed of as finding a cycle in a graph
 */
public class CourseScheduleI {
    public boolean canFinish(int numCourses, int[][] prerequisites) {
        if(prerequisites.length == 0 || numCourses == 0) return true;
        int len = prerequisites.length;
        int pCounter[] = new int[len];

        //Count number of prerequisites
        for(int i = 0; i < len; i++){
            pCounter[prerequisites[i][0]]++;
        }

        LinkedList<Integer> queue = new LinkedList<>();

        //Number of course that have no prerequisites
        for(int i = 0; i < len; i++){
            if(pCounter[i] == 0){
                queue.add(i);
            }
        }
        // Number of course that has no prerequisities or have finished its prerequisites
        int numNoPre = queue.size();
        while(!queue.isEmpty()){
            int top = queue.poll();
            for(int i = 0; i < len; i++){
                if(prerequisites[i][1] == top){
                    pCounter[prerequisites[i][0]]--;
                    if(prerequisites[i][0] == 0){
                        numNoPre++;
                        queue.add(prerequisites[i][0]);
                    }
                }
            }
        }
        return numNoPre == numCourses;
    }
}
