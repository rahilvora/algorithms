package Graphs;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Stack;

import static java.lang.Boolean.FALSE;

/**
 * Created by rahilvora on 2/26/18.
 */
public class TopologicalSort {

    public void TSortUtil(Graph root, int node, boolean[] visited, Stack<Integer> stack){
        visited[node] = true;
        Integer i;
        Iterator<Integer> itr = root.adj[node].iterator();
        while(itr.hasNext()){
            i = itr.next();
            if(!visited[i]){
                TSortUtil(root, i, visited, stack);
            }
        }
        stack.push(node);
    }

    public void TSort(Graph root, int numberOfNodes){
        Stack<Integer> stack = new Stack<>();
        boolean[] visited = new boolean[numberOfNodes];
        Arrays.fill(visited, FALSE);
        for(int i = 0; i < numberOfNodes; i++){
            if(!visited[i]){
                TSortUtil(root, i, visited, stack);
            }
        }
        while(!stack.isEmpty()){
            System.out.print(stack.pop() + " ");
        }
    }

    public static void main(String args[]){
        Graph root = new Graph(6);
        root.addEdge(0, 2);
        root.addEdge(1, 2);
        root.addEdge(2, 3);
        root.addEdge(2, 4);
        root.addEdge(4, 5);
        root.addEdge(3, 5);
        new TopologicalSort().TSort(root, 6);
    }
}
