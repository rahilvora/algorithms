package Graphs;

import java.util.*;

/**
 * Created by rahilvora on 9/2/17.
 * In Docker, building an image has dependencies. An image can only be built once
 its dependency is built (If the dependency is from outside, then the image can
 be built immediately).

 Sometimes, engineers make mistakes by forming a cycle dependency of local images.
 In this case, ignore the cycle and all the images depending on this cycle.

 Input is vector of pair of images (image, its dependency).

 Output the order of images to be built in order.

 ##Example:
 ```
 Example 1:
 {{"master", "ubuntu"}, {"numpy", "master"}, {"tensorflow", "numpy"}}
 Output: master, numpy, tensorflow

 Example 2:
 {{"python", "numpy"}, {"numpy", "python"}, {"tensorflow", "ubuntu"}}
 Output: tensorflow

 Example 3:
 {{"b", "c"}, {"c", "d"}, {"a", "b"}, {"d", "e"}, {"e","c"}, {"f", "g"}}
 Ouput: f
 */
public class DockerBuildSystemUsingTopologicalSort {
    static List<String> TopologicalSort(String[][]  imagePrereqs){
        Map<String, List<String>> imageToDependencies = new HashMap<>();
        Map<String, Integer> imagetoPrereqsCount = new HashMap<>();
        Set<String> images = new HashSet<>();
        parseInput(imagePrereqs, imageToDependencies, imagetoPrereqsCount, images);
        Queue<String> queue = new ArrayDeque<>();
        getIndependentImages(queue, images, imagetoPrereqsCount);

        List<String> imageOrdering = new ArrayList<>();
        while(!queue.isEmpty()) {
            String image = queue.remove();
            imageOrdering.add(image);
            if(!imageToDependencies.containsKey(image)) {
                continue;
            }
            for(String dependency : imageToDependencies.get(image)) {
                int newPrereqCount = imagetoPrereqsCount.get(dependency) - 1;
                if(newPrereqCount == 0) {
                    queue.add(dependency);
                    imagetoPrereqsCount.remove(dependency);
                } else {
                    imagetoPrereqsCount.put(dependency, newPrereqCount);
                }
            }
        }

        return imageOrdering;

    }
    static void parseInput(String[][] imagePrereqs, Map<String, List<String>> imageToDependencies,
                           Map<String, Integer> imagetoPrereqsCount, Set<String> images){
        for(int i = 0; i < imagePrereqs.length; i++){
            String image = imagePrereqs[i][0];
            String prereqsImage = imagePrereqs[i][1];
            images.add(prereqsImage);

            imagetoPrereqsCount.put(image, imagetoPrereqsCount.getOrDefault(image, 0) + 1);
            imageToDependencies.putIfAbsent(prereqsImage, new ArrayList<String>());
            imageToDependencies.get(prereqsImage).add(image);
        }
    }

    static void getIndependentImages(Queue<String> queue, Set<String> images,
                                     Map<String, Integer> imageToPrereqCount) {
        for(String image : images) {
            if(!imageToPrereqCount.containsKey(image)) {
                queue.add(image);
            }
        }
    }
}
