package Graphs;

import java.util.*;

public class MinimumVerticestoTraverseDirectedGraph {
    public static void main(String args[]){
        int[][] edges = {{0, 1}, {1, 0}, {2, 1}, {3, 1}, {3, 2}};;
        List<Integer> res = new MinimumVerticestoTraverseDirectedGraph().getMin(edges, 4);
        System.out.println(res);
    }
    public List<Integer> getMin(int[][] edges, int n) {
        Map<Integer, Set<Integer>> map = new HashMap<>();
        for (int[] edge: edges) {
            if (!map.containsKey(edge[0])) map.put(edge[0], new HashSet<>());
            map.get(edge[0]).add(edge[1]);
        }

        Set<Integer> visited = new HashSet<>();
        Set<Integer> res = new HashSet<>();
        for(int i = 0; i < n; i++){
            if(!res.contains(i)){
                visited.add(i);
                res.add(i);
                search(visited, map, res, i, i, new HashSet<>());
            }
        }
        return new ArrayList<>(res);
    }

    public void search(Set<Integer> visited, Map<Integer, Set<Integer>> nodes, Set<Integer> res, int cur, int start, Set<Integer> currVisited){
        currVisited.add(cur);
        visited.add(cur);
        for (int next : nodes.get(cur)) {
            if (res.contains(next) && next != start) {
                res.remove(next);
            }
            if (!currVisited.contains(next)) {
                search(visited, nodes, res, next, start, currVisited);
            }
        }
    }

}
