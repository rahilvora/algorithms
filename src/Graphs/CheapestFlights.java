package Graphs;

import java.util.*;

/**
 * Created by rahilvora on 2/28/18.
 * Cheapest Flights Within K Stops
 Input:
 n = 3, edges = [[0,1,100],[1,2,100],[0,2,500]]
 src = 0, dst = 2, k = 1
 Output: 200
 */
public class CheapestFlights {

        public static void main(String args[]){
            List<String> lines = new ArrayList<>();
            lines.add("A->B,100");
            lines.add("A->C,400");
            lines.add("B->C,100");
            lines.add("C->D,100");
            lines.add("C->A,10");
            CheapestFlights obj = new CheapestFlights();
            System.out.println(obj.cheapestFlight(lines, "A", "D", 2));
        }
        public int findCheapestPrice(int n, int[][] flights, int src, int dst, int K) {
            int[][] graph = new int[n][n];
            for (int[] flight: flights)
                graph[flight[0]][flight[1]] = flight[2];

            Map<int[], Integer> best = new HashMap();

            PriorityQueue<int[]> pq = new PriorityQueue<int[]>((a, b) -> a[0] - b[0]);
            pq.offer(new int[]{0, 0, src});

            while (!pq.isEmpty()) {
                int[] info = pq.poll();
                int cost = info[0], k = info[1], place = info[2];
                if (k > K+1 || cost > best.getOrDefault(new int[]{k, place}, Integer.MAX_VALUE))
                    continue;
                if (place == dst)
                    return cost;

                for (int nei = 0; nei < n; ++nei) if (graph[place][nei] > 0) {
                    int newcost = cost + graph[place][nei];
                    if (newcost < best.getOrDefault(new int[]{k+1, nei}, Integer.MAX_VALUE)) {
                        pq.offer(new int[]{newcost, k+1, nei});
                        best.put(new int[]{k+1, nei}, newcost);
                    }
                }
            }

            return -1;
        }

        //Djikstra approach
        // TIme complexity O(E + NlogN) where E is number of Edge and N is number of city/node
        public int findCheapestPriceII(int n, int[][] flights, int src, int dst, int k) {
            Map<Integer, Map<Integer, Integer>> prices = new HashMap<>();
            for (int[] f : flights) {
                if (!prices.containsKey(f[0])) prices.put(f[0], new HashMap<>());
                prices.get(f[0]).put(f[1], f[2]);
            }
            Queue<int[]> pq = new PriorityQueue<>((a, b) -> (Integer.compare(a[0], b[0])));
            pq.add(new int[] {0, src, k + 1});
            while (!pq.isEmpty()) {
                int[] top = pq.remove();
                int price = top[0];
                int city = top[1];
                int stops = top[2];
                if (city == dst) return price;
                if (stops > 0) {
                    Map<Integer, Integer> adj = prices.getOrDefault(city, new HashMap<>());
                    for (int a : adj.keySet()) {
                        pq.add(new int[] {price + adj.get(a), a, stops - 1});
                    }
                }
            }
            return -1;
        }

        public int cheapestFlight(List<String> flights, String src, String des, int k){
            Map<String , Map<String, Integer>> map = new HashMap<>();
            for(String flight: flights){
                String[] temp = flight.split(",");
                String sr = temp[0].split("->")[0];
                String ds = temp[0].split("->")[1];
                if(!map.containsKey(sr)) map.put(sr, new HashMap<>());
                map.get(sr).put(ds, Integer.parseInt(temp[1]));
            }
            PriorityQueue<Flight> pq = new PriorityQueue<>(new Comparator<Flight>() {
                @Override
                public int compare(Flight o1, Flight o2) {
                    return o1.cost - o2.cost;
                }
            });

            pq.add(new Flight(0, src, k+1));
            while(!pq.isEmpty()){
                Flight top = pq.poll();
                int cost = top.cost;
                int stop = top.stop;
                String city = top.city;
                if(city.equals(des)) return cost;
                if(stop > 0){
                    Map<String, Integer> adj = map.getOrDefault(city, new HashMap<>());
                    for(String key: adj.keySet()){
                        pq.add(new Flight(cost + adj.get(key), key, stop - 1));
                    }
                }
            }
            return -1;
        }

        class Flight {
            int cost;
            String city;
            int stop;
            Flight(int cost, String city, int stop){
                this.city = city;
                this.cost = cost;
                this.stop = stop;
            }
        }
}
