//import java.io.*;
//import java.util.*;
//import java.text.*;
//import java.math.*;
//import java.util.regex.*;
//
//public class Solution {
//    static Map<String, Integer> inDegree = new HashMap<>();
//    static Map<String, List<String>> graph = new HashMap<>();
//    static Map<String, List<String>> parentChild = new HashMap<>();
//    static List<String> answer = new ArrayList<>();
//    /*
//     * Complete the function below.
//     */
//
//    static void doIt(String[] input) {
//        buildGraph(input);
//        for (String in: input) {
//            String[] command = splitStringUtil(in, " ");
//            switch (command[0].trim()){
//                case "INSTALL":
//                    String dependency = command[1].trim();
//                    System.out.println(in);
//                    if (!inDegree.containsKey(dependency)){
//                        inDegree.put(dependency, 0);
//                    }
//                    if (inDegree.get(dependency) == 0) {
//                        System.out.println("Installing " + dependency);
//                        answer.add(dependency);
//                        inDegree.put(dependency, -1);
//                    }
//                    else {
//                        while (inDegree.get(dependency) != 0){
//                            Queue<String> queue = new LinkedList<>();
//                            queue.add(dependency);
//                            while (!queue.isEmpty()) {
//                                String curr = queue.poll();
//                                if (inDegree.get(curr) < 0 || inDegree.get(curr) == 0){
//                                    inDegree.put(dependency, inDegree.get(dependency) - 1);
//                                }
//                                if (inDegree.get(curr) == 0){
//                                    System.out.println("Installing " + curr);
//                                    answer.add(curr);
//                                    inDegree.put(curr, -1);
//                                }
//                                else if (inDegree.get(curr) > 0){
//                                    System.out.println("Installing " + curr);
//                                    inDegree.put(dependency, inDegree.get(dependency) - 1);
//                                    queue.addAll(graph.get(curr));
//                                }
//                            }
//                        }
//                        System.out.println("Installing " + dependency);
//                        answer.add(dependency);
//                    }
//                    break;
//
//                case "REMOVE":
//                    System.out.println(in);
//                    if (parentChild.containsKey(command[1].trim()) && parentChild.get(command[1].trim()).size() == 0){
//                        System.out.println("Removing "  + command[1].trim());
//                        parentChild.remove(command[1].trim());
//                        for (Map.Entry<String, List<String>> entry: parentChild.entrySet()) {
//                            if (entry.getValue().contains(command[1])){
//                                entry.getValue().remove(command[1]);
//                            }
//                        }
//                    }
//                    else {
//                        System.out.println(command[1].trim() +" is still needed");
//                    }
//                    break;
//
//                case "LIST":
//                    System.out.println(in);
//                    for (String ans: answer){
//                        System.out.println(ans);
//                    }
//                    break;
//
//                case "END":
//                    System.out.println(in);
//                    break;
//
//
//            }
//        }
//    }
//    static String[] splitStringUtil(String in, String regex){
//        return in.split(regex);
//    }
//
//    static void buildGraph(String[] input) {
//        for (String in: input) {
//            String[] command = in.split(" ");
//            String cmd = command[0].trim();
//            if (cmd.equals("DEPEND")) {
//                System.out.println(in);
//                String parent = command[1].trim();
//                if (!graph.containsKey(parent)){
//                    graph.put(parent, new ArrayList<String>());
//                }
//                for (int i = 2; i < command.length; i++) {
//                    if (!inDegree.containsKey(command[i])) {
//                        inDegree.put(command[i], 0);
//                    }
//                    if (graph.containsKey(command[i]) && graph.get(command[i]).contains(parent)){
//                        System.out.println(command[i] + " depends on "+parent+", ignoring command");
//                    }
//                    else {
//                        graph.get(parent).add(command[i]);
//                        inDegree.put(parent, inDegree.getOrDefault(parent, 0) + 1);
//                        if (!parentChild.containsKey(command[i])) {
//                            parentChild.put(command[i], new ArrayList<>());
//                        }
//                        parentChild.get(command[i]).add(parent);
//                    }
//                }
//            }
//        }
//    }
//    public static void main(String[] args){
//        Scanner in = new Scanner(System.in);
//
////        int _input_size = 0;
////        _input_size = Integer.parseInt(in.nextLine().trim());
////        String[] _input = new String[_input_size];
////        String _input_item;
////        for(int _input_i = 0; _input_i < _input_size; _input_i++) {
////            try {
////                _input_item = in.nextLine();
////            } catch (Exception e) {
////                _input_item = null;
////            }
////            _input[_input_i] = _input_item;
////        }
//        String[] cmds = {"22", "DEPEND A B C", "DEPEND B C", "DEPEND C B", "D B C", "DEPEND E B F",
//                "INSTALL C", "INSTALL A"};//"INSTALL H", "REMOVE C", "INSTALL E", "INSTALL D", "LIST"};
//        doIt(cmds);
//        String.v
//    }
//}
