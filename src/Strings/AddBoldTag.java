package Strings;

import java.util.*;

class Interval {
    int start, end;
    Interval(int start, int end){
        this.start = start;
        this.end = end;
    }
}

public class AddBoldTag {
    public String addBoldTag(String s, String[] dict) {
        List<Interval> list = new ArrayList<>();
        for(String str: dict){
            int index = -1;
            index = s.indexOf(str, index);
            while(index != -1){
                list.add(new Interval(index, index + str.length()));
                index += 1;
                index = s.indexOf(str, index);
            }

        }
        list = merge(list);
        int prev = 0;
        StringBuffer sb = new StringBuffer();
        for(Interval interval: list){
            sb.append(s.substring(prev, interval.start));
            sb.append("<b>");
            sb.append(s.substring(interval.start, interval.end ));
            sb.append("</b>");
            prev = interval.end;
        }
        if(prev < s.length()){
            sb.append(s.substring(prev));
        }
        return sb.toString();


    }

    public List<Interval> merge(List<Interval> list){
        List<Interval> answer = new ArrayList<>();
        Collections.sort(list, new Comparator<Interval>() {
            @Override
            public int compare(Interval o1, Interval o2) {
                return o1.start - o2.start;
            }
        });
        int start = list.get(0).start;
        int end = list.get(0).end;
        for(Interval interval: list){
            if(interval.start <= end){
                end = Math.max(end, interval.end);
            }
            else{
                answer.add(new Interval(start, end));
                start = interval.start;
                end = interval.end;
            }
        }
        answer.add(new Interval(start, end));
        return answer;
    }
}
