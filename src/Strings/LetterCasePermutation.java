package Strings;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rahilvora on 2/28/18.
 */
public class LetterCasePermutation {
    public static void main(String args[]){
        System.out.println(new LetterCasePermutation().letterCasePermutation("abc").size());
    }
    public List<String> letterCasePermutation(String S) {
       List<String> res = new ArrayList<>();
       dfs(S, res, "", 0);
       return res;
    }

    public void dfs(String S, List<String> res, String item, int start){
        if(start == S.length()){
            res.add(item);
        }
        char c = S.charAt(start);
        if (Character.isLetter(c)){
            dfs(S, res, item + Character.toUpperCase(c), start + 1);
            dfs(S, res, item + Character.toLowerCase(c), start + 1);
        }else{
            dfs(S, res, item + c, start + 1);
        }
    }
}
