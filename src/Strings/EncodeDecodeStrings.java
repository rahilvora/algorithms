package Strings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class EncodeDecodeStrings {
    // Encodes a list of strings to a single string.
    public String encode(List<String> strs) {
        StringBuffer sb = new StringBuffer();
        for(String s: strs){
            sb.append(s.length()).append("#").append(s);
        }
        return sb.toString();
    }

    // Decodes a single string to a list of strings.
    public List<String> decode(String s) {
        List<String> answer = new ArrayList<>();
        int i = 0;
        while ( i < s.length() ) {
            int hashTag = s.indexOf("#", i);
            int size = Integer.valueOf(s.substring(i, hashTag));
            answer.add(s.substring(hashTag + 1, hashTag + size + 1));
            i = hashTag + size + 1;
        }
        return answer;
    }
}
