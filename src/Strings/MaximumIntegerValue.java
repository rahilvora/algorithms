package Strings;

/**
 * Created by rahilvora on 9/3/17.
 */
public class MaximumIntegerValue {
    public static void main(String args[]){
        System.out.print(new MaximumIntegerValue().maxValue("5401"));
    }
    public int maxValue(String str){
        int ans = Integer.parseInt(String.valueOf(str.charAt(0)));
        for(int i = 1; i < str.length(); i++){
            int currValue = str.charAt(i)- '0';
            if(currValue < 2 || ans < 2){
                ans += currValue;
            }
            else{
                ans *= currValue;
            }
        }
        return ans;
    }
}
