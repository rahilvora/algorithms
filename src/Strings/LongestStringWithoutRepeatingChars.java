package Strings;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by rahilvora on 8/1/17.
 */
public class LongestStringWithoutRepeatingChars {
    public int lengthOfLongestSubstring(String s) {
        Map<Character, Integer> map = new HashMap<>();
        int max = 0;
        for(int i = 0, j = 0; i < s.length(); i++){
            char currentChar = s.charAt(i);
            if(map.containsKey(currentChar)){
                j = Math.max(j, map.get(currentChar) + 1);
            }
            map.put(currentChar, i);
            max = Math.max(max, i-j+1);
        }
        return max;
    }
}
