package Strings;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rahilvora on 2/21/18.
 */
public class EncodeAndDecodeStrings {
    public static void main(String args[]){
        List<String> str = new ArrayList<>();
        str.add("this");
        str.add("is");
        str.add("life");
        EncodeAndDecodeStrings obj = new EncodeAndDecodeStrings();
        String ans = obj.encode(str);

        System.out.print(obj.decode(ans));
    }
    public String encode(List<String> str){
        StringBuffer sb = new StringBuffer();
        for(String s: str){
            sb.append(s.length()).append("/").append(s);
        }
        return sb.toString();
    }

    public List<String> decode(String s){
        List<String> ret = new ArrayList<>();
        int i = 0;
        while(i < s.length()){
            int slash = s.indexOf('/', i);
            int size = Integer.valueOf(s.substring(i, slash));
            ret.add(s.substring(slash + 1, slash + size + 1));
            i = slash + size + 1;
        }
        return ret;
    }
}
