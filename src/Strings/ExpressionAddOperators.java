package Strings;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rahilvora on 8/19/17.
 * overflow: we use a long type once it is larger than Integer.MAX_VALUE or minimum, we get over it.
 0 sequence: because we can't have numbers with multiple digits started with zero, we have to deal with it too.
 a little trick is that we should save the value that is to be multiplied in the next recursion.

 T(n) = 3 * T(n-1) + 3 * T(n-2) + 3 * T(n-3) + ... + 3 *T(1);
 T(n-1) = 3 * T(n-2) + 3 * T(n-3) + ... 3 * T(1);
 Thus T(n) = 4T(n-1);
 So the time complexity is O(4^n)
 */
public class ExpressionAddOperators {
    public static void main(String args[]){
        new ExpressionAddOperators().addOperators("232",8);
    }
    public List<String> addOperators(String num, int target) {
        List<String> rst = new ArrayList<String>();
        if(num == null || num.length() == 0) return rst;
        helper(rst, "", num, target, 0, 0, 0);
        return rst;
    }
    public void helper(List<String> rst, String path, String num, int target, int pos, long eval, long multed){
        if(pos == num.length()){
            if(target == eval)
                rst.add(path);
            return;
        }
        for(int i = pos; i < num.length(); i++){
            if(i != pos && num.charAt(pos) == '0') break;
            long cur = Long.parseLong(num.substring(pos, i + 1));
            if(pos == 0){
                helper(rst, path + cur, num, target, i + 1, cur, cur);
            }
            else{
                helper(rst, path + "+" + cur, num, target, i + 1, eval + cur , cur);

                helper(rst, path + "-" + cur, num, target, i + 1, eval -cur, -cur);

                helper(rst, path + "*" + cur, num, target, i + 1, eval - multed + multed * cur, multed * cur );
            }
        }
    }
}
