package Strings;

/**
 * Created by rahilvora on 8/19/17.
 */
public class RearrangeStringInSortedOrderFollowedByIntegerSum {
    public static void main(String args[]){
        System.out.print(new RearrangeStringInSortedOrderFollowedByIntegerSum().arrangString("AC2BEW3"));
    }
    static int MAX_CHAR = 26;
    public String arrangString(String str){
        int[] chars = new int[MAX_CHAR];
        int sum = 0;
        for(int i = 0; i < str.length(); i++){
            char currChar = str.charAt(i);
            if(Character.isDigit(currChar)){
                sum += str.charAt(i) - '0';
            }
            else{
                chars[str.charAt(i) - 'A']++;
            }
        }
        StringBuffer answer = new StringBuffer();
        for(int i = 0; i < chars.length; i++){
            char ch = (char) ('A' + i);
            while(chars[i]--!=0){
                answer.append(ch);
            }
        }
        answer.append(sum);
        return answer.toString();
    }
}
