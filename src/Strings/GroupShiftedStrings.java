package Strings;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GroupShiftedStrings {
    public static void main(String args[]){
        String[] strings = {"abc", "bcd", "acef", "xyz", "az", "ba", "a", "z"};
        new GroupShiftedStrings().groupStrings(strings);
    }
    public List<List<String>> groupStrings(String[] strings) {
        Map<String, List<String>> map = new HashMap<String, List<String>>();
        for (String str : strings) {
            int offset = str.charAt(0) - 'a';
            String key = "";
            for (int i = 0; i < str.length(); i++) {
                char c = (char) (str.charAt(i) - offset);
                if (c < 'a') {
                    c += 26;
                }
                key += c;
            }
            if (!map.containsKey(key)) {
                List<String> list = new ArrayList<String>();
                map.put(key, list);
            }
            map.get(key).add(str);
        }
        List<List<String>> answer = new ArrayList<>();
        for(Map.Entry<String,List<String>> m : map.entrySet()){
            answer.add(m.getValue());
        }
        return answer;
    }
}
