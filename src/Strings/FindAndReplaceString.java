package Strings;

import java.util.HashMap;
import java.util.Map;

public class FindAndReplaceString {
    public static void main(String args[]){
        String S = "abcd";
        int[] indexes = {0,2};
        String[] sources = {"ab","ec"};
        String[] targets = {"eee","ffff"};
        new FindAndReplaceString().findReplaceString(S, indexes, sources, targets);
    }
    public String findReplaceString(String S, int[] indexes, String[] sources, String[] targets) {
        Map<Integer, Integer> table = new HashMap<>();
        StringBuffer sb = new StringBuffer();
        for(int i = 0; i < indexes.length; i++){
            if(S.startsWith(sources[i], indexes[i])){
                table.put(indexes[i], i);
            }
        }

        for(int i = 0; i < S.length();){
            if(table.containsKey(i)){
                sb.append(targets[table.get(i)]);
                i += sources[table.get(i)].length();
            }
            else{
                sb.append(S.charAt(i));
                i++;
            }
        }
        return sb.toString();
    }

}
