package Strings;

import java.util.Map;
import java.util.TreeMap;

/**
 * Created by rahilvora on 1/3/18.
 */
public class PalidromePermutation {
    public static boolean flag = false;
    public static void main(String args[]){
        System.out.print(new PalidromePermutation().canPermutePalindrome("as"));
    }
    public boolean canPermutePalindrome(String s) {
        int[] map = new int[128];
        int count = 0;
        for (int i = 0; i < s.length(); i++) {
            map[s.charAt(i)]++;
            if (map[s.charAt(i)] % 2 == 0)
                count--;
            else
                count++;
        }
        return count <= 1;
    }

}
