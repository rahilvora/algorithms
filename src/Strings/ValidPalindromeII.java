package Strings;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by rahilvora on 1/6/18.
 * Input: "abca"
 * Output: True
 * Explanation: You could delete the character 'c'.
 */
public class ValidPalindromeII {
    public static void main(String args[]){
        System.out.print(new ValidPalindromeII().validPalindrome("abcbca"));

    }
    public boolean validPalindrome(String s) {
        int left = 0, right = s.length() - 1;
        while(left < right && s.charAt(left) == s.charAt(right)){
            left++;
            right--;
        }
        if(left >= right) return true;

        if(isValidPalindrome(s, left+1, right) || isValidPalindrome(s, left, right-1)) return true;

        return false;
    }
    public boolean isValidPalindrome(String s, int left, int right){
        while(left < right){
            if( s.charAt(left) != s.charAt(right)){
                return false;
            }
            left++;
            right--;
        }
        return true;
    }
}
