package Strings;

/**
 * Created by rahilvora on 5/14/17.
 */
public class ZigZagConversion {
    public static void main(String args[]){
        System.out.print(new ZigZagConversion().zigzag("PAYPALISHIRING",3));
    }
    public String zigzag(String s, int numRows){
        if(s.length()<=numRows) return s;
        char[] c= s.toCharArray();
        int len = c.length;
        StringBuffer[] answer = new StringBuffer[numRows];

        for(int i = 0; i < answer.length; i++){
            answer[i] = new StringBuffer();
        }
        int i = 0;
        while (i < len ){
            for(int index = 0; index < numRows && i < len; index++){
                answer[index].append(c[i++]);
            }
            for(int index = numRows - 2; index>=1 && i < len; index--){
                answer[index].append(c[i++]);
            }
        }
        for (int index = 1; index<numRows; index++){
            answer[0].append(answer[index]);
        }
        return answer[0].toString();
    }
}
