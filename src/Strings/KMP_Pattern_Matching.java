package Strings;

/**
 * Created by rahilvora on 9/13/17.
 * Time Complexity O(n+m);
 * Space : O(m);
 */
public class KMP_Pattern_Matching {
    public static void main(String args[]){
        System.out.print(new KMP_Pattern_Matching().KMP("abcxabcdabcdabcy", "abcdabcy"));
    }
    public int[] computeTempArray(String pattern){
        int[] lps = new int[pattern.length()];
        int index = 0;
        for(int i = 1; i < pattern.length();){
            if(pattern.charAt(i) == pattern.charAt(index)){
                lps[i] = index + 1;
                i++;
                index++;
            }
            else{
                if(index != 0){
                    index = lps[index - 1];
                }
                else{
                    lps[i] = 0;
                    i++;
                }
            }

        }
        return lps;
    }
    public boolean KMP(String string, String pattern){
        int[] lps = computeTempArray(pattern);
        int i = 0, j = 0;
        while (i < string.length() && j < pattern.length()){
            if(string.charAt(i) == pattern.charAt(j)){
                i++;
                j++;
            }
            else{
                if(j != 0){
                    j = lps[j-1];
                }
                else{
                    i++;
                }
            }
        }
        if(j == pattern.length()) return true;
        return false;
    }
}
