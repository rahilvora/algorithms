package Strings;

/**
 * Created by rahilvora on 6/13/17.
 */
public class StudentAttendanceRecord {
    public static void main(String args[]){
        System.out.print(new StudentAttendanceRecord().checkRecord("LLLALL"));
    }
    public boolean checkRecord(String s) {
        int size = s.length();
        if(size == 0) return true;
        int absentCount = 0, lateCount = 0, lastLateIndex = -1;
        for(int i = 0; i < size; i++){
            if(s.charAt(i) == 'A'){
                absentCount++;
                if(absentCount > 1) break;
            }
            else if(s.charAt(i) == 'L'){
                lateCount++;
                if(i - lastLateIndex == 1){
                    lastLateIndex = i;
                    if(lateCount > 2) break;
                    continue;
                }
                else{
                    lastLateIndex = i;
                    lateCount = 1;
                }
            }
        }
        if(lateCount>2 || absentCount>1){
            return false;
        }
        return true;

    }
}
