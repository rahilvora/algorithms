package Strings;

import java.util.ArrayList;
import java.util.List;

public class RemoveComments {
    public static void main(String args[]){

    }
    public List<String> removeComments(String[] source) {
        List<String> answer = new ArrayList<>();
        boolean openingBlock = false;
        StringBuilder newLine = null;
        for(String string:source){
            char[] chars = string.toCharArray();
            if(!openingBlock) newLine = new StringBuilder();
            int i = 0;
            while(i < chars.length){
                if(!openingBlock && i+1 < chars.length && chars[i] == '/' && chars[i+1] == '*'){
                    openingBlock = true;
                    i++;
                }
                else if(openingBlock && i+1 < chars.length && chars[i] == '*' && chars[i+1] == '/'){
                    openingBlock = false;
                    i++;
                }
                else if(!openingBlock && i+1 < chars.length && chars[i] == '/' && chars[i+1] == '/'){
                    break;
                }
                else if(!openingBlock){
                    newLine.append(chars[i]);
                }
                i++;
            }
            if(!openingBlock && newLine.length() > 0){
                answer.add(newLine.toString());
            }
        }
        return answer;
    }
}
