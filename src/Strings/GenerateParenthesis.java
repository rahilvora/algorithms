package Strings;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rahilvora on 6/13/17.
 * Time and space complexity : O(4^n / sqrt(n))
 * https://leetcode.com/problems/generate-parentheses/solution/
 */
public class GenerateParenthesis {
    List<String> answer = new ArrayList<>();
    public static void main(String[] args){
        new GenerateParenthesis().generateParenthesis(3);
    }
    public List<String> generateParenthesis(int n) {
        backTrack("",0,0,n);
        return answer;
    }
    public void backTrack(String s, int open, int close, int n) {
        if (s.length() == n * 2) {
            answer.add(s);
            System.out.println(s);
            return;
        }
        if (open < n)
            backTrack(s + "(", open + 1, close, n);
        if (close < open)
            backTrack(s + ")", open, close + 1, n);
    }
}
