package Strings;

public class ReverseWordsStringII {
    public static void main(String args[]){
        char[] str = {'t', 'h', 'e', ' ', 's', 'k', 'y', ' ', 'i', 's', ' ', 'b', 'l', 'u', 'e'};
        new ReverseWordsStringII().reverseWords(str);
    }
    public void swap(char[] str, int low, int high){
        while(low < high){
            char temp = str[low];
            str[low++] = str[high];
            str[high--] = temp;
        }
    }
    public void reverseWords(char[] str) {
        int low = 0, high = str.length - 1;
        swap(str, low, high);
        int start = 0, end = 0;
        while(end < str.length){
            while(end < str.length && str[end] != ' '){
                end++;
            }
            swap(str, start, end - 1);
            end++;
            start = end;
        }
        swap(str, start, end);
    }
}
