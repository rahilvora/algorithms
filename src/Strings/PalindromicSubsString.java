package Strings;

/**
 * Created by rahilvora on 9/29/17.
 * Time O(n^2)
 */
public class PalindromicSubsString {
    int count = 0;
    public int countSubstrings(String s) {
        for(int i = 0; i < s.length(); i++){
            extendPalindrome(s, i, i);
            extendPalindrome(s, i, i + 1);
        }
        return count;
    }
    public void extendPalindrome(String s, int left, int right){
        while (left>=0 && right < s.length() && s.charAt(left) == s.charAt(right)){
            count++;
            left--;
            right++;
        }
    }
    // O(N^2)
    public int countSubstringsII(String s){
        int ans = 0, size = s.length();
        for(int center = 0; center< 2*size - 1; center++){
            int left = center/2;
            int right = left + center%2;
            while(left >=0 && right < size && s.charAt(left) == s.charAt(right)){
                ans++;
                left--;
                right++;
            }
        }
        return ans;
    }

}
