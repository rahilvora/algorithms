package Strings;

/**
 * Created by rahilvora on 8/19/17.
 *
 * Input : 01231
 Output :
 ((((0 + 1) + 2) * 3) + 1) = 10
 In above manner, we get the maximum value i.e. 10
 */
public class CalMaxValue {
    public int maxValue(String str){
        int answer = str.charAt(0) - '0';
        if(str.length() == 1) return Integer.parseInt(str);
        for(int i = 1; i < str.length(); i++){
            if( (str.charAt(i) == 0)||
                (str.charAt(i) == 1) ||
                (str.charAt(i-1) == 1) ||
                (str.charAt(i) == 1)){
                answer += str.charAt(i) - '0';
            }
            else{
                answer *= str.charAt(i) - '0';
            }
        }
        return answer;
    }
}
