package Strings;

import java.util.ArrayList;
import java.util.List;

public class CVSParser {
    public static void main(String args[]){
        String test = "\"Alexandra \"\"Alex\"\"\",Menendez,alex.menendez@gmail.com,Miami,1";
        String expected = "Jane|Roberts|janer@msn.com|San Francisco, CA|0";
        System.out.println(new CVSParser().cvsParser(test));
    }
    public String cvsParser(String str){
        List<String> res = new ArrayList<>();
        StringBuffer sb = new StringBuffer();
        boolean isQuote = false;

        for(int i = 0; i < str.length(); i++){
            char c = str.charAt(i);
            if(isQuote){
                if (c == '\"') {
                    if (i < str.length() - 1 && str.charAt(i + 1) == '\"') {
                        sb.append("\"");
                        i++;
                    } else {
                        isQuote = false;
                    }
                } else {
                    sb.append(c);
                }
            }
            else{
                if(c == '\"'){
                    isQuote = true;
                }
                else if(c == ','){
                    res.add(sb.toString());
                    sb = new StringBuffer();
                }
                else{
                    sb.append(c);
                }
            }
        }
        if (sb.length() > 0)
            res.add(sb.toString());
        return String.join("|", res);
    }
}
