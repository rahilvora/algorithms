package Strings;


import java.util.ArrayList;
import java.util.List;

public class PartitionLabels {
    public static void main(String args[]){
        new PartitionLabels().partitionLabels("ababcbacadefegdehijhklij");
    }
    public List<Integer> partitionLabels(String S) {
        int[] map = new int[26];
        for(int i = 0; i < S.length(); i++){
            map[S.charAt(i) - 'a'] = i;
        }
        List<Integer> res = new ArrayList<>();
        int j = 0, anchor = 0;
        for(int i = 0; i < S.length(); i++){
            j = Math.max(j, map[S.charAt(i) - 'a']);
            if(i == j){
                res.add(i - anchor + 1);
                anchor = i+1;
            }
        }
        return res;
    }
}
