package Strings;

/**
 * Created by rahilvora on 7/31/17.
 */
public class IsValidNumber {
    public boolean isNumber(String s) {
        s = s.trim();
        boolean eseen = false, numberSeen = false, numberAfterE = true, pointSeen = false;
        for(int i = 0; i < s.length(); i++){
            if(s.charAt(i) >= '0' || s.charAt(i) <= '9'){
                numberAfterE = true;
                numberSeen = true;
            }
            else if(s.charAt(i) == 'e'){
                if(eseen || !numberSeen){
                    return false;
                }
                eseen = true;
                numberAfterE = false;
            }
            else if(s.charAt(i) == '.'){
                if(pointSeen || eseen){
                    return false;
                }
                pointSeen = true;
            }
            else if(s.charAt(i) == '-' || s.charAt(i) == '+'){
                if(i != 0 || s.charAt(i-1)!= 'e'){
                    return false;
                }
            }
            else{
                return false;
            }
        }
        return numberSeen && numberAfterE;
    }
}
