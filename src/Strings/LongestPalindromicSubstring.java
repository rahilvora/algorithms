package Strings;

/**
 * Created by rahilvora on 6/5/17.
 */
public class LongestPalindromicSubstring {

    public static void main(String args[]){
        System.out.print(new LongestPalindromicSubstring().longestPalindromeII("babad"));
    }

    //Brute Force Solution
    public String longestPalindrome(String s) {
        if(s.length() <= 1 || s == null) return s;
        StringBuffer str = new StringBuffer();
        str.append(s.charAt(0));
        for(int i = 0; i < s.length(); i++){
            StringBuffer temp = new StringBuffer();
            temp.append(s.charAt(i));
            int end = 0;
            for(int j = i+1; j < s.length(); j++){
                temp.append(s.charAt(j));
                if(isPalindrome(temp.toString())){
                    end = j;
                    if(str.length() < temp.length()){
                        str = new StringBuffer(temp.toString());
                    }
                }
            }
        }
        return str.toString();
    }
    public boolean isPalindrome(String s){
        int left = 0, right = s.length() -1;
        while(left<right){
             if(s.charAt(left++) != s.charAt(right--)){
                 return false;
             }
        }
        return true;
    }
    private int maxLength, lo;
    //Optimal Solution
    public String longestPalindromeII(String s) {
        String res = "";
        for(int i = 0; i < s.length(); i++){
            extendPalindrome(s, i, i);
            extendPalindrome(s, i, i+1);
            res = s.substring(lo, lo + maxLength);
        }
        return res;
    }
    public void extendPalindrome(String s, int j, int k){
        while(j >=0 && k < s.length() && s.charAt(k) == s.charAt(j)){
            j--;
            k++;
        }
        if(maxLength < k - j -1){
            lo = j+1;
            maxLength = k-j-1;
        }
    }
}
