package Strings;

import java.util.HashMap;

/**
 * Created by rahilvora on 8/21/17.
 * time O(n)
 */
public class LongestSubStringAtMostKDistinctChar {
    public static void main(String args[]){
        new LongestSubStringAtMostKDistinctChar().lengthOfLongestSubstringKDistinct("eceba", 2);
    }
    public int lengthOfLongestSubstringKDistinct(String s, int k) {
        if(k == 0 || s == null || s.length() == 0) return 0;
        if(s.length() < k) return s.length();
        HashMap<Character, Integer> map = new HashMap<>();
        int left = 0, maxLen = 0;
        for(int i = 0; i < s.length(); i++){
            char curr = s.charAt(i);
            map.put(curr, map.getOrDefault(curr, 0)+1);
            if(map.size() > k){
                maxLen = Math.max(maxLen, i-left);
                while(map.size() > k){
                    char fc = s.charAt(left);
                    if(map.get(fc) == 1){
                        map.remove(fc);
                    }
                    else{
                        map.put(fc, map.get(fc) - 1);
                    }
                    left++;
                }
            }
        }
        return Math.max(maxLen, s.length() - left);
    }
}
