package Strings;

/**
 * Created by rahilvora on 6/13/17.
 */
public class LongestCommonPrefix {
    public String longestCommonPrefix(String[] strs){
        if(strs.length == 0) return "";
        if(strs.length == 1) return strs[0];
        for(int i = 0; i < strs[0].length(); i++){
            StringBuffer prefix = new StringBuffer(strs[0].substring(0, i+1));
            for(int j = 1; j < strs.length; i++){
                if(strs[j].equals("")) return "";
                if(strs[j].length() < prefix.length() || !strs[j].substring(0,i+1).equals(prefix)){
                    return prefix.substring(0,prefix.length()-1);
                }
                if(prefix.length() == strs[0].length()) return prefix.toString();
            }
        }
        return  "";
    }
}
