package Strings;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by rahilvora on 8/20/17.
 * N = 23
 2 3 23
 2 -> 2
 3 -> 3
 23 -> 6
 this number is a COLORFUL number since product of every digit of a
 sub-sequence are different.

 */
public class ColorfulNumber {
    public int colorful(int a){
        String s = String.valueOf(a);

        Set<Integer> set = new HashSet<>();

        int temp = 0;

        while (temp < s.length()) {
            //if consecutive Integer is same return 0
            if (set.contains(s.charAt(temp) - '0')) return 0;
            set.add(s.charAt(temp) - '0');
            temp++;
        }

        int i = 0;
        int j = 1;
        int n = s.length();

        int val1 = 0;
        int val2 = 0;

        while (j < n) {

            val1 = s.charAt(i) - '0';
            val2 = s.charAt(j) - '0';

            if (set.contains(val1*val2))
                return 0;

            set.add(val1 * val2);

            i++;
            j++;
        }
        return 1;
    }
}
