package Strings;

/**
 * Created by rahilvora on 8/1/17.
 * For explaination refer https://www.careercup.com/question?id=4425679
 */
public class CountAndSay {
    public String countAndSay(int n){
        String s = "1";
        for(int i = 1; i < n; i++){
            s = CountIdx(s);
        }
        return s;
    }
    public String CountIdx(String s){
        StringBuffer str = new StringBuffer();
        int count = 1;
        char c = s.charAt(0);
        for(int i = 1; i < s.length(); i++){
            if(s.charAt(i) == c){
                count++;
            }
            else{
                str.append(count);
                str.append(c);
                count = 1;
                c = s.charAt(i);
            }
        }
        str.append(count);
        str.append(c);
        return str.toString();
    }
}
