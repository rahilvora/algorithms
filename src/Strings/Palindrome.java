package Strings;

/**
 * Created by rahilvora on 8/23/17.
 */
public class Palindrome {
    public boolean isPalindrome(String str){
        if(str.length() == 0) return false;
        str = str.toLowerCase();
        str = str.replace("([^a-zA-Z0-9]\\s)",""); // replace all chars and white spaces with "" and not a-z, A-Z and 0-9
        int left = 0, right = str.length()-1;
        while(left < right){
            if(str.charAt(left) != str.charAt(right)) return false;
            left++;
            right--;
        }
        return true;
    }
}
