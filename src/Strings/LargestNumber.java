package Strings;

import java.util.Arrays;
import java.util.Comparator;

public class LargestNumber {
    public static void main(String args[]){
        int[] num = { 3,30,34,5,9};
        System.out.println(new LargestNumber().largestNumber(num));
    }
    public String largestNumber(int[] num) {
        String[] s_num = new String[num.length];
        for(int i = 0; i < num.length; i++){
            s_num[i] = String.valueOf(num[i]);
        }

        Comparator<String> comparator = new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                String s1 = o1 + o2;
                String s2 = o2 + o1;
                return s2.compareTo(s1);
            }
        };
        Arrays.sort(s_num, comparator);
        if(s_num[0].charAt(0) == '0') return "0";
        StringBuilder sb = new StringBuilder();
        for(String str: s_num){
            sb.append(str);
        }
        return sb.toString();
    }
}
