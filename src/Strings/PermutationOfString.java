package Strings;

import java.util.*;

/**
 * Created by rahilvora on 8/24/17.
 * Time is factorial time and space is length of string input
 */
public class PermutationOfString {
    public static void main(String args[]){
        new PermutationOfString().permute("ABC".toCharArray());
    }
    public List<String> permute(char input[]) {
        Map<Character, Integer> countMap = new TreeMap<>();
        for (char ch : input) {
            countMap.compute(ch, (key, val) -> {
                if (val == null) {
                    return 1;
                } else {
                    return val + 1;
                }
            });
        }
        char str[] = new char[countMap.size()];
        int count[] = new int[countMap.size()];
        int index = 0;
        for (Map.Entry<Character, Integer> entry : countMap.entrySet()) {
            str[index] = entry.getKey();
            count[index] = entry.getValue();
            index++;
        }
        List<String> resultList = new ArrayList<>();
        char result[] = new char[input.length];
        permuteUtil(str, count, result, 0, resultList);
        return resultList;
    }

    public void permuteUtil(char str[], int count[], char result[], int level, List<String> resultList) {
        if (level == result.length) {
            printArray(result);
            resultList.add(new String(result));
            return;
        }

        for(int i = 0; i < str.length; i++) {
            if(count[i] == 0) {
                continue;
            }
            result[level] = str[i];
            count[i]--;
            permuteUtil(str, count, result, level + 1, resultList);
            count[i]++;
        }
    }

    private void printArray(char input[]) {
        for(char ch : input) {
            System.out.print(ch);
        }
        System.out.println();
    }
}

/*
Same code in JS
permute(['A','B','C'])
function permute(inputArray){
	var countMap = {}
  for(var i = 0; i < inputArray.length; i++){
  	if(countMap[inputArray[i]]){
    	countMap[inputArray[i]]++;
    }
    else{
    	countMap[inputArray[i]] = 1;
    }
  }
  var str = [], count = [], result = [], index = 0;
  for(var key in countMap){
  	str.push(key);
    count.push(countMap[key]);
  }

  permuteUtil(str, count, result,0);
}
function permuteUtil(str, count, result, level){
	if(level == 3){
  	console.log(result);
    return;
  }
  for(var i = 0; i < str.length; i++){
  	if(count[i] == 0){
    	continue;
    }
		result[level] = str[i];
    count[i]--;
    permuteUtil(str, count, result, level + 1);
    count[i]++;
  }

}
*
* */