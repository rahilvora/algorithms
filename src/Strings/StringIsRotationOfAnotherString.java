package Strings;

/**
 * Created by rahilvora on 2/14/18.
 * Algo
 * 1. Merger String 1 and String 2
 * 2. Check String 2 is substr of merged String or not (Use KMP)
 *
 */
public class StringIsRotationOfAnotherString {
    public boolean isRotation(String str1, String str2){
        return ((str1.length() == str2.length()) &&
                (str1 + str2).indexOf(str2) != -1);
    }
}
