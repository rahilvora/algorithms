package Strings;

/**
 * Created by rahilvora on 8/20/17.
 */
public class DecodeWays {
    public static void main(String args[]){
        new DecodeWays().numDecodings("12");
    }
    public int numDecodings(String s) {
        int size = s.length();
        if(size == 0) return 0;
        int[] dp = new int[size + 1];
        dp[0] = 1;
        dp[1] = s.charAt(0) != '0'? 1:0;
        for(int i = 2; i <= size; i++){
            int first = Integer.valueOf(s.substring(i-1,i));
            int second= Integer.valueOf(s.substring(i-2,i));
            if(first>=1 && first<=9){
                dp[i] += dp[i-1];
            }
            if(second>=10 && second<=26){
                dp[i] += dp[i-2];
            }
        }
        return dp[size];
    }
}
