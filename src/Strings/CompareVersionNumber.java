package Strings;

/**
 * Created by rahilvora on 1/16/18.
 */
public class CompareVersionNumber {
    public int compareVersion(String version1, String version2) {
        String[] level1 = version1.split("\\.");
        String[] level2 = version2.split("\\.");
        int length = Math.max(version1.length(), version1.length());
        for(int i = 0; i < length; i++){
            Integer l1 = (i<level1.length)?Integer.parseInt(level1[i]):0;
            Integer l2 = (i<level2.length)?Integer.parseInt(level2[i]):0;
            int compare = l1.compareTo(l2);
            if(compare != 0) return compare;
        }
        return 0;
    }
}
