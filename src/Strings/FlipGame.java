package Strings;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rahilvora on 2/16/18.
 */
public class FlipGame {
    public static void main(String args[]){
        new FlipGame().generatePossibleNextMoves("++++");
    }
    public List<String> generatePossibleNextMoves(String s) {
        List<String> answer = new ArrayList<>();
        for(int i = 1; i < s.length(); i++){
            if(s.substring(i-1, i+1).equals("++")){
                answer.add(s.substring(0,i-1) + "--" + s.substring(i+1,s.length()));
            }
        }
        return answer;
    }
}
