package Strings;

/**
 * Created by rahilvora on 2/17/18.
 * Input:
 ["a","b","b","b","b","b","b","b","b","b","b","b","b"]

 Output:
 Return 4, and the first 4 characters of the input array should be: ["a","b","1","2"].
 */
public class StringCompress {

    public int compress(char[] chars) {
        int index = 0;
        int indexAns = 0;
        while(index < chars.length){
            char currentChar = chars[index];
            int count = 0;
            while(index < chars.length && chars[index] == currentChar){
                count++;
                index++;
            }
            chars[indexAns++] = currentChar;
            if(count != 1){
                char[] cnt = Integer.toString(count).toCharArray();
                for(int i = 0; i <  cnt.length; i++)
                    chars[indexAns++] = cnt[i];
            }
        }
        return indexAns;
    }
    //INPUT:AABBCC
    //OUTPUT:A2B2C2
    public static String compression(String str) {
        StringBuffer sb = new StringBuffer();
        char last = str.charAt(0);
        int count = 0;
        for (int i=0; i< str.length(); i++) {
            if (str.charAt(i) == last) {
                count++;
            } else {
                sb.append(last + "" + count);
                count = 1;  // here count should be 1 but not 0
            }
            last = str.charAt(i);
        }
        sb.append(last + "" + count); //reflush the last part

        String newStr = sb.toString();
        if (newStr.length() < str.length()) {
            return newStr;
        } else {
            return str;
        }
    }
}
