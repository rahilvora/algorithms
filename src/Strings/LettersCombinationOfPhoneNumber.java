package Strings;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rahilvora on 7/23/17.
 * Time Complexity : k^n :  k = avg length of the string and n is the length of digits
 */
public class LettersCombinationOfPhoneNumber {
    public String[] KEYS = {"","","abc","def","ghi","jkl","mno","pqrs","tuv","wxyz"};
    public List<String> letterCombinations(String digits) {
        List<String> answer = new ArrayList<>();
        if(digits.length() == 0) return answer;
        helper("", answer, digits, 0);
        return answer;
    }
    public void helper(String prefix, List<String> answer, String digits, int offset){
        if(offset>=digits.length()){
            answer.add(prefix);
            return;
        }
        String letter = KEYS[digits.charAt(offset) - '0'];
        for(int i = 0; i < letter.length(); i++){
            helper(prefix + letter.charAt(i), answer, digits, offset+1);
        }
    }
}
