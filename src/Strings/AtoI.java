package Strings;

/**
 * Created by rahilvora on 7/7/17.
 */
public class AtoI {
    public void main(String args[]){
        new AtoI().myAtoI("1");
    }
    public int myAtoI(String str){
        int index = 0, total = 0, sign = 1;
        if(str == null || str.length() == 0) return total;
        //Remove leading spaces
        while(str.charAt(index) == ' ' && index < str.length()){
            index++;
        }
        // check for sign
        if(str.charAt(index) == '+' || str.charAt(index) == '-'){
            sign = str.charAt(index) == '+'? 1:-1;
        }
        while(index < str.length()){
            int digit = str.charAt(index) - '0';
            if(digit < 0 || digit > 9) break;

            //
            if(Integer.MAX_VALUE/10 < total || Integer.MAX_VALUE/10 == total)
                return sign == 1 ? Integer.MAX_VALUE: Integer.MIN_VALUE;
            total = total*10 + digit;
        }
        return total*sign;
    }
    public String myItoA(int n){
        if(n == 0){
            return "0";
        }
        boolean isPositive = true;
        if(n < 0){
            isPositive = false;
            n = Math.abs(n);
        }
        StringBuffer str = new StringBuffer();
        while (n >0){
            int curr = n%10;
            n = n/10;
            str.append(curr);
        }
        return str.reverse().toString();
    }
}
