package Strings;

import java.util.ArrayList;
import java.util.List;

public class LongestWordInDictionaryThroughDeleting {
    public static void main(String args[]){
        List<String> words = new ArrayList<>();
        words.add("ale");
        words.add("apple");
        words.add("monkey");
        words.add("plea");
        new LongestWordInDictionaryThroughDeleting().findLongestWord("abpcplea", words);
    }
    public String findLongestWord(String s, List< String > d) {
        String max_str = "";
        for(String str : d){
                if(isSubsequence(str, s)){
                    if(str.length() > max_str.length() || (str.length() == max_str.length() && str.compareTo(max_str) < 0)){
                        max_str = str;
                    }
                }
        }
        return max_str;
    }

    public boolean isSubsequence(String x, String y){
        int j = 0;
        for(int i = 0; i < y.length() && j < x.length(); i++){
            if(y.charAt(i) == x.charAt(j)) j++;
            if(j == x.length()) return true;
        }
        return false;
    }
}
