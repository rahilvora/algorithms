package Strings;

import java.util.Arrays;
import java.util.Comparator;

public class ReorganizeStrings {
    public static void main(String args[]){
        new ReorganizeStrings().reorganizeString("aabbcc");
    }
    public String reorganizeString(String S) {
        int[][] counts = new int[26][2];
        for(int i = 0; i < 26; i++) counts[i][1] = i;
        for(int i = 0; i < S.length(); i++) counts[S.charAt(i) - 'a'][0]++;
        Arrays.sort(counts, new Comparator<int[]>(){
            public int compare(int[] a, int[] b){
                return b[0] - a[0];
            }
        });
        if(counts[0][0] > (S.length() + 1) / 2) return "";
        char[] result = new char[S.length()];
        int index = 0;
        for(int i = 0; i < 26; i++) {
            for(int j = 0; j < counts[i][0]; j++) {
                result[index] = (char)(counts[i][1] + 'a');
                index += 2;
                if(index >= result.length) index = 1;
            }
        }
        return String.valueOf(result);
    }
}
