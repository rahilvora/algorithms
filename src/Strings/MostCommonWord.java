package Strings;

import java.util.*;

public class MostCommonWord {
    public static void main(String args[]){
        String[] temp = {"a"};
        System.out.print(new MostCommonWord().mostCommonWordII("a, a, a, a, b,b,b,c, c" , temp));
    }

    public String mostCommonWordII(String paragraph, String[] banned) {
        Set<String> set = new HashSet<>();
        Map<String, Integer> map = new HashMap<>();
        for(String banWord: banned){
            set.add(banWord);
        }
        String[] modified = paragraph.replaceAll("\\W+" , " ").toLowerCase().split("\\s+");
        for(String word: modified){
            if(!word.equals("") && !set.contains(word)){
                map.put(word, map.getOrDefault(word, 0) + 1);
            }
        }
        int count = 0;
        String answer = "";
        for(String key: map.keySet()){
            if(count < map.get(key)){
                count = map.get(key);
                answer = key;
            }
        }
        return answer;
    }
}
