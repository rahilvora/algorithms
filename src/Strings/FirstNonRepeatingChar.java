package Strings;

/**
 * Created by rahilvora on 1/15/18.
 * TIme O(n)
 * Memory Constant
 *
 */
public class FirstNonRepeatingChar {
    public int firstUniqChar(String s) {
        int freq [] = new int[26];
        for(int i = 0; i < s.length(); i ++)
            freq [s.charAt(i) - 'a'] ++;
        for(int i = 0; i < s.length(); i ++)
            if(freq [s.charAt(i) - 'a'] == 1)
                return i;
        return -1;
    }
}
