package Strings;

import java.util.ArrayList;
import java.util.List;

public class SecondMostRepeatedChar {
    public static void main(String args[]){
        System.out.println(new SecondMostRepeatedChar().secondHighCharacters("aabbbcdee"));
    }

    public List<Character> secondHighCharacters(String str) {
        List<Character> secondFreqChars = new ArrayList<>();
        if(str == null || str.length() == 0) return secondFreqChars;

        int[] map = new int[26];

        for(int i = 0; i < str.length(); i++){
            map[str.charAt(i) - 'a']++;
        }

        int firstChar = 0, secondChar = 0;

        for(int i = 0; i < map.length; i++){
            if(map[i] > map[firstChar]){
                secondChar = firstChar;
                firstChar = i;
            }
            else if(map[i] >= map[secondChar] && map[i] != map[firstChar] ){
                System.out.print((char) (i + 'a'));
                secondFreqChars.add((char)(i + 'a'));
                secondChar = i;
            }

        }

        return secondFreqChars;

    }
}
