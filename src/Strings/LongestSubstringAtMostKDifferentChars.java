package Strings;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by rahilvora on 9/23/17.
 */
public class LongestSubstringAtMostKDifferentChars {
    public static void main(String args[]){

    }
    public int lengthOfLongestSubstringKDistinct(String s, int k) {
        Map<Character, Integer> map = new HashMap<>();
        int left = 0, best = 0;
        for(int i = 0; i < s.length(); i++){
            char c = s.charAt(i);
            map.put(c, map.getOrDefault(c, 0) + 1);
            while (map.size() > k){
                char leftChar = s.charAt(left);
                if(map.containsKey(leftChar)){
                    map.put(leftChar, map.get(leftChar) - 1);
                    if(map.get(leftChar) == 0){
                        map.remove(leftChar);
                    }
                }
                left++;
            }
            best = Math.max(i - left + 1, best);
        }
        return best;
    }
}
