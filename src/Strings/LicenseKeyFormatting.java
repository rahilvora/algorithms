package Strings;

public class LicenseKeyFormatting {
    public static void main(String args[]){
        System.out.println(new LicenseKeyFormatting().licenseKeyFormatting("---", 3));
    }

    public String licenseKeyFormatting(String S, int k) {
        if(S == null || S.length() == 0) return S;

        StringBuffer sb = new StringBuffer();
        StringBuffer answer = new StringBuffer();
        for(String word: S.split("-")){
            sb.append(word);
        }
        int i = sb.length() - 1;
        while(i >= 0){
            int j = 0;
            while( i >= 0 &&j<k){
                answer.append(sb.charAt(i--));
                j++;
            }
            answer.append("-");
        }
        if(answer.length() == 0) return "";
        return answer.reverse().substring(1).toString().toUpperCase();
    }
}
