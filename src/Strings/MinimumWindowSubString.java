package Strings;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by rahilvora on 9/21/17.
 */
public class MinimumWindowSubString {
    public static void main(String args[]){
        System.out.print(new MinimumWindowSubString().minWindow("ADOBECODEBANC", "ABC"));
    }
    public String minWindow(String s, String t) {
        Map<Character, Integer> map = new HashMap<>();
        int end = 0, begin = 0, length = Integer.MAX_VALUE, head = 0, count = t.length();
        for(char c : t.toCharArray()){
            map.put(c, map.getOrDefault(c, 0) + 1);
        }
        while(end < s.length()){
            char c=s.charAt(end);

            if(map.containsKey(c)){
                if(map.get(c) > 0) count--;
                map.put(c, map.get(c) - 1);
            }
            end++;
            while(count == 0){
                if(length > end - begin){
                    head = begin;
                    length = end - begin;
                }
                char bc = s.charAt(begin );
                if(map.containsKey(bc)){
                    map.put(bc, map.get(bc) + 1);
                    if(map.get(bc) > 0) count++;
                }
                begin++;
            }
        }
        return length == Integer.MAX_VALUE ? "": s.substring(head, head + length);
    }
}
