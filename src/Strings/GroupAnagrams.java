package Strings;

import java.util.*;

/**
 * Created by rahilvora on 6/27/17.
 */
public class GroupAnagrams {
    // Time O(n*slogs)n is len of str array and s len of longest string in that array
    public List<List<String>> groupAnagrams(String[] strs) {
        List<List<String>> answer = new ArrayList<>();
        Map<String, List<String>> map = new HashMap<>();
        for(int i = 0; i < strs.length; i++){
            char[] temp = strs[i].toCharArray();
            Arrays.sort(temp);
            if(!map.containsKey(String.valueOf(temp))){
                List<String> strings = new ArrayList<>();
                strings.add(strs[i]);
                map.put(String.valueOf(temp), strings);
            }
            else{
                map.get(String.valueOf(temp)).add(strs[i]);
            }
            for(String key: map.keySet()){
                answer.add(map.get(key));
            }

        }
        return answer;
    }

    // O(NK)
    public List<List<String>> groupAnagramsII (String[] strs) {
        List<List<String>> answer = new ArrayList<>();
        Map<String, List<String>> map = new HashMap<>();
        int[] count = new int[26];
        for(String str: strs) {
            Arrays.fill(count, 0);
            for(char ch: str.toCharArray()) count[ch - 'a']++;
            StringBuffer sb = new StringBuffer();
            for(int i = 0; i < 26; i++){
                sb.append('#');
                sb.append(count[i]);
            }
            String key = sb.toString();
            if(!map.containsKey(key)) map.put(key, new ArrayList<>());
            map.get(key).add(str);
        }
        for(List<String> val: map.values()){
            answer.add(val);
        }
        return answer;
    }
}
