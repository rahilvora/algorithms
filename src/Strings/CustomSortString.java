package Strings;

import java.util.HashSet;
import java.util.Set;

public class CustomSortString {
    public static void main(String args[]){
        System.out.println(new CustomSortString().customSortString("cba", "abcd"));
    }
    public String customSortString(String S, String T) {
        int[] bucket = new int[26];
        for (char c : T.toCharArray()) {
            bucket[c - 'a']++;
        }

        StringBuilder sb = new StringBuilder();
        for (char c : S.toCharArray()) {
            for (int i = 0; i < bucket[c - 'a']; i++) {
                sb.append(c);
            }
            bucket[c - 'a'] = 0;
        }

        for (int i = 0; i < 26; i++) {
            for (int j = 0; j < bucket[i]; j++) {
                sb.append((char) (i + 'a'));
            }
        }

        return sb.toString();
    }
}
