package Strings;

/**
 * Created by rahilvora on 6/3/17.
 */
public class ReverseString {
    public String reverseString(String s) {
        StringBuffer str = new StringBuffer();
        if(s == null || s.length() == 0){
            return "";
        }
        else{
            for(int i = s.length() - 1; i >=0; i--){
                str.append(s.charAt(i));
            }
            return str.toString();
        }
    }

}
