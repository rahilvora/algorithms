package OOD;

import java.util.Arrays;
import java.util.List;

class Node {
    char value;
    Node pre = null, next = null;
    public Node(char value){
        this.value = value;
    }
}

class Point {
    int x, y;
    Node head = null, tail = null;
    Point (int x, int y) {
        this.x = x;
        this.y = y;
        this.head = this.tail = null;
    }

    public void remove(Node node){
        if(node.pre != null) node.pre.next = node.next;
        else head = node.next;

        if(node.next != null) node.next.pre = node.pre;
        else tail = node.pre;
    }

    public void setHead(Node node){
        node.next = head;
        node.pre = null;

        if(head != null) head.pre = node;

        head = node;

        if(tail == null)
            tail = head;
    }
}

public class Canvas {

    Point[][] grid;
    int rows, cols;
    int z_index = 0;
    Canvas (int rows, int cols) {
        this.rows = rows;
        this.cols = cols;
        this.grid = new Point[rows][cols];
        initGrid();
    }

    public void initGrid() {
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                grid[i][j] = new Point(i, j);
                Node newNode = new Node('-');
                grid[i][j].setHead(newNode);
            }
        }
    }

    public static void main (String args[]) {
        Canvas canvas = new Canvas(6, 10);
        canvas.draw_rectangle('L', 1, 1, 4, 4);
        canvas.draw_rectangle('R', 2, 1, 4, 4);
        canvas.printUtil();;
        canvas.erase_area(3, 2, 3, 3);
        canvas.printUtil();;
        canvas.draw_rectangle('#', 1, 3, 8, 4);
        canvas.printUtil();;
    }

    public void draw_rectangle (char fill, int left_x, int top_y, int right_x, int bottom_y) {
        for (int i = top_y; i <= bottom_y; i++) {
            for (int j = left_x; j <= right_x; j++) {
                Node newNode = new Node(fill);
                Point currPoint = grid[i][j];
                currPoint.setHead(newNode);
            }
        }
    }

    public void erase_area (int left_x, int top_y, int right_x, int bottom_y) {
        for (int i = top_y; i <= bottom_y; i++) {
            for (int j = left_x; j <= right_x; j++) {
                Point currPoint = grid[i][j];
                while (currPoint.head.value != '-') {
                    currPoint.head = currPoint.head.next;
                }
            }
        }
    }

    public void bring_to_front(int x, int y) {

    }

    public void printUtil () {
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                System.out.print(grid[i][j].head.value);
            }
            System.out.println();
        }
    }

}
