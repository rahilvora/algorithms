package OOD;

import java.util.ArrayList;
import java.util.List;

public class Solution {


    public boolean solution(int[] A, int K) {
        int n = A.length;
        for (int i = 0; i < n - 1; i++) {
            if (A[i] + 1 < A[i + 1])
                return false;
        }
        if (A[0] != 1 && A[n - 1] != K)
            return false;
        else
            return true;
    }

    public int solution(String S) {
        int lastIndexOfA = Integer.MIN_VALUE;
        for (int i = 0; i < S.length(); i++) {
            if (S.charAt(i) == 'A'){
                lastIndexOfA = i;
            }
        }
        if (lastIndexOfA == Integer.MIN_VALUE) return 0;
        int count = 0;
        for (int i = 0; i < lastIndexOfA; i++) {
            if (S.charAt(i) == 'B') count++;
        }
        return count;
    }

    public int solution(int[] A, int[] B) {
        // write your code in Java SE 8
        if (A.length != B.length) return 0;
        int totalSumA = 0, totalSumB = 0;
        for (int num: A) {
            totalSumA += num;
        }
        for (int num: B) {
            totalSumB += num;
        }
        if (totalSumA != totalSumB) return 0;

        int currSumA = 0;
        int currSumB = 0;
        int count = 0;
        for (int i = 0; i < A.length - 1; i++) {
            currSumA += A[i];
            currSumB += B[i];
            if  ((totalSumA - currSumA == currSumA) && (totalSumB - currSumB == currSumB)){
                count++;
            }
        }
        return count;
    }

    public static void main (String args[]){
        Solution obj = new Solution();
//        System.out.println(obj.solution("BAAABAB"));
//        System.out.println(obj.solution("AAAAA"));
//        System.out.println(obj.solution("AAAAABBB"));
//        System.out.println(obj.solution("BABAABAB"));

        int[] A = {1, 4, 2, -2, 5};
        int[] B = {7, -2, -2, 2, 5};
        int[] A2 = {3, 2, 6};
        int[] B2 = {4, 1, 6};
        int[] A3 = {4, -1, 0, 3};
        int[] B3 = {-2, 6, 0, 4};
        int[] A4 = {-1, -1, -1, -1};
        int[] B4 = {-1, -1, -1, -1};

        int[] A5 = {5, -3, 3, -1};
        int[] B5 = {3, -1, 1, 1};
        System.out.println(obj.solution(A, B));
        System.out.println(obj.solution(A2, B2));
        System.out.println(obj.solution(A3, B3));
        System.out.println(obj.solution(A4, B4));
        System.out.println(obj.solution(A5, B5));
    }
}
