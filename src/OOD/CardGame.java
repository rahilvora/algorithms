package OOD;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

enum SUIT {
    HEART,
    SPADE,
    CLUB,
    DIAMOND
}

enum CARDNUMBER {
    TWO(2),
    THREE(3),
    FOUR(4),
    FIVE(5),
    SIX(6),
    SEVEN(7),
    EIGHT(8),
    NINE(9),
    TEN(10),
    JACK(11),
    QUEEN(12),
    KING(13),
    ACE(14);

    private int value;

    CARDNUMBER(int newValue) {
        value = newValue;
    }

    public int getValue() { return value; }
}

class Card {
    SUIT suit;
    CARDNUMBER cardnumber;
    Card (SUIT suit, CARDNUMBER cardnumber) {
        this.suit = suit;
        this.cardnumber = cardnumber;
    }
    // Can add getters and setter by making class variables private
}

class Deck {
    List<Card> cardDeck;
    Deck() {
        for (SUIT suit: SUIT.values()) {
            for (CARDNUMBER cardnumber: CARDNUMBER.values()) {
                cardDeck.add(new Card(suit, cardnumber));
            }
        }
    }
    public void dealCard(Player player){
        //Get next card and add to hand of the player
        Card removedCard = cardDeck.remove(0);
        player.getHand().add(removedCard);
    }

    public void shuffle() {
        Random rand = new Random();
        //Generate two random numbers between 0 to 51
        for(int i = 0 ; i < 20 ; i ++){
            int firstCard = rand.nextInt(52);
            int secondCard = rand.nextInt(52);
            Collections.swap(cardDeck,firstCard,secondCard);
        }
    }

    public int getSizeOfDeck() {
        return cardDeck.size();
    }
}

class Player {
    List<Card> hand;

    public Player() {
        this.hand = new ArrayList<Card>();
    }

    public List<Card> getHand() {
        return hand;
    }

    @Override
    public String toString() {
        return "Player{" +
                "hand=" + hand +
                '}';
    }
}


public class CardGame {
    public static void main (String args[]) {
        Deck deck = new Deck();
        System.out.println(deck);
        System.out.println("Size of deck is: "+deck.getSizeOfDeck());
        deck.shuffle();
        System.out.println("Deck after shuffling is "+deck);

        Player player1 = new Player();
        Player player2 = new Player();
    }
}
