package Queues;

import java.util.*;

/**
 * Created by rahilvora on 8/12/17.
 *
 * Time complexity = n*2^(n-1)
 * n: to traverse each string to check its valid or not and
 * 2^(n-1): to generate all teh combinations of the substring
 *
 */
public class RemoveInvalidParenthesis {
    public static void main(String args[]){
        new RemoveInvalidParenthesis().removeInvalidParentheses("()())()");
    }
    public List<String> removeInvalidParentheses(String s) {
        List<String> answer = new ArrayList<>();
        if(s == null || s.length() == 0) {
            answer.add("");
            return answer;
        }
        Set<String> visited  = new HashSet<>();
        Queue<String> queue = new LinkedList<>();
        boolean found = false;
        visited.add(s);
        queue.offer(s);

        while(!queue.isEmpty()){
            s = queue.poll();

            if(isValid(s)){
                answer.add(s);
                found = true;
            }
            if(found) continue;

            for(int i = 0; i < s.length(); i++){
                if (s.charAt(i) != '(' && s.charAt(i) != ')') continue;
                String t  = s.substring(0,i) + s.substring(i+1);
                if(!visited.contains(t)){
                    visited.add(t);
                    queue.add(t);
                }
            }
        }
        return answer;
    }
    public boolean isValid(String s){
        int count = 0;
        for(int i = 0; i < s.length(); i++){
            char c = s.charAt(i);
            if(c == '(')count++;
            if(c == ')' && --count < 0)return false;
        }
        return count == 0;
    }
}
