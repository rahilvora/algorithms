package Queues;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Stack;

/**
 * Created by rahilvora on 10/31/17.
 */
public class QueueReconstruction {
    public static void main(String args[]){
        int[][] people = {{7,0},{4,4},{7,1},{5,0},{6,1},{5,2}};
        new QueueReconstruction().reconstuction(people);
    }
    public int[][] reconstuction(int[][] people){
        if(people == null || people.length ==  0 || people[0].length == 0)
            return new int[0][0];

        Arrays.sort(people, new Comparator<int[]>(){

            @Override
            public int compare(int[] a, int[] b) {
                if(b[0] == a[0]) return a[1] - b[1];
                return b[0] - a[0];
            }
        });

        int n  = people.length;
        ArrayList<int[]> temp = new ArrayList<>();
        for(int i = 0; i < n; i++){
            temp.add(people[i][1], new int[]{people[i][0], people[i][1]});
        }
        int[][] res = new int[n][2];
        int i = 0;
        for(int[] k : temp){
            res[i][0] = k[0];
            res[i++][1] = k[1];
        }
        return res;
    }
}
