package Queues;

import java.util.*;

/**
 * Created by rahilvora on 8/31/17.
 */
public class WordLadder {
    public static void main(String args[]){

    }
    // Time complexity = O(M*N) where N is size of dictionary and M size of the word
    public int ladderLength(String beginWord, String endWord, List<String> wordList) {
        // Use queue for BFS
        Queue<String> queue = new LinkedList<>();
        queue.add(beginWord);
        queue.add(null);

        Set<String> words = new HashSet<>(wordList);
        if(!words.contains(endWord)) return 0;
        Set<String> visited = new HashSet<>();
        visited.add(beginWord);
        int level = 1;
        while(!queue.isEmpty()){
            String currStr = queue.poll();
            if(currStr != null){
                for(int i = 0; i < currStr.length(); i++){
                    char[] chars = currStr.toCharArray();
                    for(char c = 'a'; c<='z'; c++){
                        chars[i] = c;
                        String word = new String(chars);
                        // Found word
                        if(word.equals(endWord)) return level+1;

                        // Put it to queue and visited
                        if(words.contains(word) && !visited.contains(word)){
                            queue.add(word);
                            visited.add(word);
                        }
                    }
                }
            }
            else {
                level++;
                if(!queue.isEmpty()){
                    queue.add(null);
                }
            }

        }
        return 0;
    }
    //Time complexity 26 * O(N)

    public int ladderLength1(String beginWord, String endWord, List<String> wordList) {
        Set<String> beginSet = new HashSet<>();
        Set<String> endSet = new HashSet<>();
        beginSet.add(beginWord);
        endSet.add(endWord);
        Set<String> visited = new HashSet<>();
        int len = 1;
        while(!beginSet.isEmpty() && !endSet.isEmpty()){
            if(beginSet.size() > endSet.size()){
                Set<String> set = beginSet;
                beginSet = endSet;
                endSet = set;
            }
            Set<String> temp = new HashSet<>();
            for(String word: beginSet){
                char[] chs = word.toCharArray();
                for(int i = 0; i < word.length(); i++){
                    for(char c = 'a'; c < 'z'; c++){
                        char old  = chs[i];
                        chs[i] = c;
                        String target = new String(chs);
                        if(endSet.contains(target))return len + 1;

                        if(!visited.contains(target) && wordList.contains(target)){
                            temp.add(target);
                            visited.add(target);
                        }
                        chs[i] = old;
                    }
                }
            }
            beginSet = temp;
            len++;
        }
        return 0;

    }

    public int ladderLength11(String beginWord, String endWord, List<String> wordList) {
        Set<String> beginSet = new HashSet<>();
        Set<String> endSet = new HashSet<>();
        int len = 1;
        beginSet.add(beginWord);
        endSet.add(endWord);
        Set<String> visited = new HashSet<>();
        while(!beginSet.isEmpty() && !endSet.isEmpty()){
            if(beginSet.size() > endSet.size()){
                Set<String> set = beginSet;
                beginSet = endSet;
                endSet = beginSet;
            }
            Set<String> tempSet = new HashSet<>();
            for(String word : beginSet){
                char[] words = word.toCharArray();
                for(int i = 0; i < words.length; i++){
                    for(char c = 'a'; c <= 'z'; c++){
                        char old = words[i];
                        words[i] = c;
                        String target = new String(words);
                        if(endSet.contains(target)) return len+1;
                        if(!visited.contains(target) && wordList.contains(target)){
                            tempSet.add(target);
                            visited.add(target);
                        }
                        words[i] = old;

                    }
                }
            }
            beginSet = tempSet;
            len++;
        }
        return 0;
    }
}
