package Trees;

import com.sun.source.tree.Tree;

import java.util.Stack;

/**
 * Created by rahilvora on 5/22/17.
 */
public class MaximumDepthBinaryTree {
    public int maxDepth(TreeNode root) {
        int maxDepth  = 1;
        Stack<TreeNode> nodes = new Stack<>();
        Stack<Integer> depth = new Stack<>();
        if(root == null) return 0;
        nodes.push(root);
        depth.push(maxDepth);
        while(!nodes.empty()){
            TreeNode node = nodes.pop();
            int currentDepth = depth.pop();
            maxDepth = Math.max(currentDepth, maxDepth);
            if(node.left != null){
                nodes.push(node.left);
                depth.push(currentDepth + 1);
            }
            if(node.right != null) {
                nodes.push(node.right);
                depth.push(currentDepth + 1);
            }
        }
        return maxDepth;
    }
    public int maxDepthRecursion(TreeNode root){
        if(root == null) return 0;
        if(root.left == null && root.right == null) return 1;
        if(root.left == null)
            return maxDepthRecursion(root.right) + 1;
        if(root.right == null)
            return maxDepthRecursion(root.left) + 1;
        return Math.min(maxDepthRecursion(root.left), maxDepthRecursion(root.right)) + 1;
    }
}
