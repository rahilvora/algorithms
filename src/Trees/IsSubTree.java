package Trees;

/**
 * Created by rahilvora on 8/17/17.
 */
public class IsSubTree {
    public boolean isSubtree(TreeNode s, TreeNode t) {
        return traverse(s, t);
    }

    public boolean traverse(TreeNode s, TreeNode t) {
        return s != null && (isEqual(s, t) || traverse(s.left, t) || traverse(s.right, t));
    }

    public boolean isEqual(TreeNode s, TreeNode t) {
        if (s == null && t == null) return true;
        if (s == null || t == null) return false;
        return (s.val == t.val && isEqual(s.left, t.left) && isEqual(s.right, t.right));
    }
}
