package Trees;

import java.util.HashMap;

/**
 * Created by rahilvora on 2/14/18.
 */
public class ConstructTreeFromInorderPostorder {
    public TreeNode buildTreePostIn(int[] inorder, int[] postorder) {
        if(inorder == null || postorder == null || inorder.length != postorder.length) return null;
        HashMap<Integer, Integer> map = new HashMap<>();
        for(int i = 0; i < inorder.length; i++){
            map.put(inorder[i],i);
        }
        return buildTreePostIn(inorder, 0, inorder.length-1, postorder, 0, postorder.length-1, map);
    }
    public TreeNode buildTreePostIn(int[] inorder, int is, int ie, int[] postorder, int ps, int pe, HashMap<Integer, Integer> map){
        if(is>ie||ps>pe) return null;

        TreeNode root = new TreeNode(postorder[pe]);
        int rootIndex = map.get(postorder[pe]);
        TreeNode leftNode = buildTreePostIn(inorder, is, rootIndex-1,postorder, ps, ps+rootIndex-is-1, map);
        TreeNode rightNode = buildTreePostIn(inorder, rootIndex+1,ie,postorder,ps+rootIndex-is,pe-1, map);
        root.left = leftNode;
        root.right = rightNode;
        return root;
    }
}
