package Trees;

/**
 * Created by rahilvora on 6/2/17.
 */
public class PopulatingNextRightPointerEachNode {
    public void connect(TreeLinkNode root) {
        TreeLinkNode previousNode = root;
        while(previousNode != null){
            TreeLinkNode currentNode = previousNode;
            while(currentNode != null){
                if(currentNode.left != null){
                    currentNode.left.next = currentNode.right;
                }
                if(currentNode.right != null && currentNode.next != null){
                    currentNode.right.next = currentNode.next.left;
                }
                currentNode = currentNode.next;
            }
            previousNode = previousNode.left;
        }
    }
}

