package Trees;

import java.util.Stack;

/**
 * Created by rahilvora on 1/16/18.For example,

  1
 / \
 2   3
 The root-to-leaf path 1->2 represents the number 12.
 The root-to-leaf path 1->3 represents the number 13.

 Return the sum = 12 + 13 = 25.


 */
public class SumRootToLeafNumbers {
    public int sumNumbers(TreeNode root) {
        int sum = 0, val = 0;
        if(root == null) return sum;
        Stack<TreeNode> stack = new Stack<>();
        Stack<Integer> valStack = new Stack<>();
        stack.push(root);
        valStack.push(root.val);
        while(!stack.isEmpty()){
            TreeNode node = stack.pop();
            val = valStack.pop();
            if(node.left == null && node.right == null){
                sum += val;
            }
            if(node.right != null){
                stack.push(node.right);
                valStack.push(val*10 + node.right.val);
            }
            if(node.left != null){
                stack.push(node.left);
                valStack.push(val*10 + node.left.val);
            }
        }
        return sum;
    }

    public int sumNumbersII(TreeNode root){
        return helper(root, 0);
    }

    public int helper(TreeNode root, int sum){
        if(root == null) return 0;
        if(root.left == null && root.right == null) return sum*10 + root.val;
        return helper(root.left, sum*10 + root.val) + helper(root.right, sum*10 + root.val);
    }
}
