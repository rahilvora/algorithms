package Trees;

import java.util.Stack;

/**
 * Created by rahilvora on 6/15/17.
 * Algo: Its a simple example of post order traversal. Traverse left subtree and right subtree and get the difference
 */
public class BinaryTreeTilt {
    int result = 0;
    public int findTilt(TreeNode root){
        postOrder(root);
        return result;
    }
    public int postOrder(TreeNode node){
        if(node == null) return 0;
        int left = postOrder(node.left);
        int right = postOrder(node.right);
        result = Math.abs(left - right);
        return node.val + left + right;
    }
}
