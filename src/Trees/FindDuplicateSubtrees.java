package Trees;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FindDuplicateSubtrees {
    List<TreeNode> ans;
    Map<String, Integer> countMap;
    public static void main(String args[]){

    }
    public List<TreeNode> findDuplicateSubtrees(TreeNode root) {
        ans = new ArrayList<>();
        countMap = new HashMap<>();
        findDuplicate(root);
        return ans;
    }
    // O(n^2) approach
    public String findDuplicate(TreeNode root){
        if(root == null) return "#";
        String serializedString = root.val + "," + findDuplicate(root.left) + "," + findDuplicate(root.right);
        countMap.put(serializedString, countMap.getOrDefault(serializedString, 0) + 1);
        if(countMap.get(serializedString) == 2) ans.add(root);
        return serializedString;
    }
}
