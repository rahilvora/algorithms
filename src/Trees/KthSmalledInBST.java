package Trees;

import java.util.Stack;

/**
 * Created by rahilvora on 9/18/17.
 */
public class KthSmalledInBST {
    public int kthSmalled(TreeNode root, int k){
        Stack<TreeNode> stack = new Stack<>();
        TreeNode pre = null;
        while(root != null || !stack.isEmpty()){
            while(root != null){
                stack.push(root);
                root = root.left;
            }
            root = stack.pop();
            if (--k == 0) break;
            pre = root;
            root = root.right;
        }
        return root.val;
    }
}
