package Trees;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by rahilvora on 2/7/18.
 * Time Complexity O(n)
 *
 */
public class LeftMostRightMostNodes {
    public void printCornerNodes(TreeNode root){
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        queue.offer(null);
        // check is it is the first node of the level
        boolean isFirst = false;
        // Check if it is the only node of the level
        boolean isOne = false;
        int last = 0;
        while(!queue.isEmpty()){
            TreeNode temp = queue.poll();
            if(isFirst){
                System.out.print(temp.val + " ");

                if(temp.left!= null){
                    queue.offer(temp.left);
                }
                if(temp.right != null){
                    queue.offer(temp.right);
                }
                isFirst = false;
                isOne = true;
            }
            else if(temp == null){
                if(queue.size()>1){
                    queue.offer(null);
                }
                isFirst = true;
                if(!isOne){
                    System.out.print(last + " ");
                }
            }
            else{
                last = temp.val;

                isOne = false;
                if(temp.left != null){
                    queue.offer(temp.left);
                }
                if(temp.right != null){
                    queue.offer(temp.right);
                }
            }
        }
    }
}
