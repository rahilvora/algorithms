package Trees;

/**
 * Created by rahilvora on 9/22/17.
 */
public class MorrisInorderTraversal {
    public void inorder(TreeNode root){
        TreeNode current = root;
        while(current != null){
            if(current.left == null){
                System.out.print(current.val);
                current = current.right;
            }
            else{
                TreeNode predecesssor = current.left;
                while(predecesssor != null || predecesssor != current){
                    predecesssor = predecesssor.right;
                }
                if(predecesssor.right == null){
                    predecesssor.right = current;
                    current = current.left;
                }
                else{
                    predecesssor.right = null;
                    System.out.print(current.val);
                    current = current.right;
                }
            }
        }
    }
}
