package Trees;

import java.util.*;

/**
 * Created by rahilvora on 6/10/17.
 */
public class KillProcess {
    public List<Integer> killProcess(List<Integer> pid, List<Integer> ppid, int kill) {
        List<Integer> answer = new ArrayList<>();
        Map<Integer, List<Integer>> parentChildrenMap = new HashMap<>();
        for(int i = 0; i < ppid.size(); i++){
            parentChildrenMap.putIfAbsent(ppid.get(i), new LinkedList<Integer>());
            parentChildrenMap.get(ppid.get(i)).add(pid.get(i));
        }
        Stack<Integer> stack = new Stack<>();
        stack.push(kill);
        while(!stack.isEmpty()){
            int curr = stack.pop();
            answer.add(curr);
            List<Integer> list = parentChildrenMap.get(curr);
            if(list == null)continue;
            stack.addAll(list);
        }
        return answer;
    }
}
