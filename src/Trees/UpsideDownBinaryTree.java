package Trees;

/**
 * Created by rahilvora on 10/26/17.
 */
public class UpsideDownBinaryTree {
    public TreeNode upsideDownBinaryTree(TreeNode root){
        TreeNode curr = root, prev = null, temp = null, next = null;

        while(curr != null){
            next = curr.left;

            //Swap node now, we need temp to store right node of previous node;
            curr.left = temp;
            temp = curr.right;
            curr.right = prev;

            prev = curr;
            curr = next;
        }
        return prev;
    }
}
