package Trees;

/**
 * Created by rahilvora on 8/26/17.
 */
public class PrefixTree {
    TrieNodeNew root;

    PrefixTree(){
        root = new TrieNodeNew();
        root.val = ' ';
    }
    public void insert(String word) {
        TrieNodeNew node = root;
        for(int i = 0; i < word.length(); i++){
            char c = word.charAt(i);
            if(node.children[c - 'a'] == null){
                node.children[c - 'a'] = new TrieNodeNew();
            }
            node = node.children[c - 'a'];
        }
        node.isWord = true;
    }
    public boolean search(String word) {
        TrieNodeNew node = root;
        for(int i = 0; i < word.length(); i++){
            char c = word.charAt(i);
            if(node.children[c - 'a'] == null) return false;
            node = node.children[c - 'a'];
        }
        return node.isWord;
    }

    public boolean startsWith(String prefix) {
        TrieNodeNew node = root;
        for(int i = 0; i < prefix.length(); i++){
            char c = prefix.charAt(i);
            if(node.children[c - 'a'] == null) return false;
            node = node.children[c - 'a'];
        }
        return true;
    }

    public static void main(String args[]){
        PrefixTree node = new PrefixTree();
        System.out.println("asf");
    }
}
