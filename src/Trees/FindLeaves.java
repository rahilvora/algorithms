package Trees;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rahilvora on 5/31/17.
 *     1
     /  \
    2   3
   / \
  4   5
 Returns [4, 5, 3], [2], [1].
 * Steps
 * 1. Create a helper function height which would return height of the current node
 * 2. All the leaves would be at height 0
 */
public class FindLeaves {
    public List<List<Integer>> findLeaves(TreeNode root) {
        List<List<Integer>> leaves = new ArrayList<>();
        height(root, leaves);
        return leaves;
    }
    public int height(TreeNode node, List<List<Integer>> leaves){
        if(node == null) return -1;
        //Level of current node is 1 + max  of left/ right. Incase of leave node it would be 1 + {-1};
        int level = 1 + Math.max(height(node.left, leaves), height(node.right, leaves));
        if(leaves.size() < level + 1) leaves.add(new ArrayList<>());
        leaves.get(level).add(node.val);
        return level;
    }
}
