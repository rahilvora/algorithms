package Trees;

/**
 * Created by rahilvora on 9/29/17.
 */
public class MergeBinaryTree {
    public TreeNode mergeTrees(TreeNode t1, TreeNode t2) {
        return helper(t1, t2);
    }
    public TreeNode helper(TreeNode t1, TreeNode t2){
        if(t1 == null && t2 == null){
            return null;
        }
        int sum = ((t1 == null) ? 0: t1.val) + ((t2 == null) ? 0 : t2.val);
        TreeNode root = new TreeNode(sum);
        root.left = helper(((t1.left == null)?null : t1.left),((t2.left == null)?null : t2.left));
        root.right = helper(((t1.right == null)?null : t1.right),((t2.right == null)?null : t2.right));
        return root;
    }

}
