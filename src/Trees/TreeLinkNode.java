package Trees;

/**
 * Created by rahilvora on 6/2/17.
 */
public class TreeLinkNode {
    int val;
    TreeLinkNode left, right, next;
    TreeLinkNode(int x) { val = x; }
}
