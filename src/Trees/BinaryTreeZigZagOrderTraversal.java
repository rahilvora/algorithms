package Trees;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rahilvora on 1/19/18.
 */
public class BinaryTreeZigZagOrderTraversal {
    public List<List<Integer>> zigzagLevelOrder(TreeNode root){
        List<List<Integer>> sol = new ArrayList<>();
        traverse(sol, root, 0);
        return sol;
    }
    public void traverse(List<List<Integer>> sol, TreeNode root, int level){
        if(root == null) return;
        if(sol.size() <= level){
            List<Integer> temp = new ArrayList<>();
            sol.add(temp);
        }
        List<Integer> collection = sol.get(level);
        if(level %2 == 0){
            collection.add(root.val);
        }
        else {
            collection.add(0, root.val);
        }
        traverse(sol, root.left, level+1);
        traverse(sol, root.right, level+1);
    }
}
