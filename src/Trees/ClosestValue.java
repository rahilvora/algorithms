package Trees;

/**
 * Created by rahilvora on 7/28/17.
 */
public class ClosestValue {
    public int closestValue(TreeNode root, double target) {
        int answer = root.val;
        while(root != null){
            if(Math.abs(target - answer) > Math.abs(target - root.val)){
                answer = root.val;
            }
            root = (root.val > target) ? root.left:root.right;
        }
        return answer;
    }
}
