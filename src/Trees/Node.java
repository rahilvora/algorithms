package Trees;

/**
 * Created by rahilvora on 9/6/17.
 */
public class Node {
    public int data;
    public Node left;
    public Node right;
    public int height;
    public int size;

    public Node(int x) { data = x; }
    public static Node newNode(int data){
        return new Node(data);
    }
}
