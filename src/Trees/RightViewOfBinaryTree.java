package Trees;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rahilvora on 8/3/17.
 */
public class RightViewOfBinaryTree {
    public List<Integer> rightSideView(TreeNode root) {
        List<Integer> answer = new ArrayList<>();
        rightView(root, answer, 0);
        return answer;
    }
    public void rightView(TreeNode curr, List<Integer> list, int currDepth){
        if(curr == null) return;
        if(list.size() == currDepth){
            list.add(curr.val);
        }
        rightView(curr.right, list, currDepth+1);
        rightView(curr.left, list, currDepth+1);

    }
}
