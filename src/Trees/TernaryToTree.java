package Trees;

import java.util.Stack;

/**
 * Created by rahilvora on 8/20/17.
 */
public class TernaryToTree {
    public  TreeNode ternaryToTree(String exp) {
        Stack<TreeNode> stack = new Stack<>();
        TreeNode root = new TreeNode(-1);
        stack.push(root);
        for(int i = 0; i < exp.length(); i++){
            char curr = exp.charAt(i);
            if(exp.charAt(i) == '?'){
                stack.peek().left = new TreeNode(0);
                stack.push(stack.peek().left);
            }
            else if(exp.charAt(i) == ':'){
                stack.pop();
                stack.peek().right = new TreeNode(0);
                stack.push(stack.peek().right);
            }
            else{
                stack.peek().val = curr;
            }
        }
        return root;
    }
}
