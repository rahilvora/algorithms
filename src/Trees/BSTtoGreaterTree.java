package Trees;

/**
 * Created by rahilvora on 6/16/17.
 * Since this is a BST, we can do a reverse inorder traversal to traverse the nodes of the tree in descending order.
 * In the process, we keep track of the running sum of all nodes which we have traversed thus far.
 */
public class BSTtoGreaterTree {
    int sum = 0;
    public TreeNode convertBST(TreeNode root) {
        convert(root);
        return root;
    }
    public void convert(TreeNode node){
        if(node == null) return;
        convert(node.right);
        node.val += sum;
        sum = node.val;
        convert(node.left);
    }
}
