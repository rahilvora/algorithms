package Trees;

import java.util.Stack;

/**
 * Created by rahilvora on 9/18/17.
 */
public class ValidBST {
    public boolean validBST(TreeNode root){
        Stack<TreeNode> stack = new Stack<>();
        TreeNode pre = null;
        while(root != null || !stack.isEmpty()){
            while(root != null){
                stack.push(root);
                root = root.left;
            }
            root = stack.pop();
            if( pre != null && root.val < pre.val) return false;
            pre = root;
            root = root.right;
        }
        return true;
    }
}
