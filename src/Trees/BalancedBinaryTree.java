package Trees;

/**
 * Created by rahilvora on 6/1/17.
 */
public class BalancedBinaryTree {
    public boolean isBalanced(TreeNode root) {
        return checkHeight(root) == -1 ? false:true;
    }
    public int checkHeight(TreeNode root){
        if(root == null) return 0;
        int leftHeight = checkHeight(root.left);
        if(leftHeight == -1)return -1;
        int rightHeight = checkHeight(root.right);
        if(rightHeight == -1) return -1;
        int height = leftHeight - rightHeight;
        if(Math.abs(height)>1)return -1;
        else return 1 + Math.max(leftHeight, rightHeight);
    }
}
