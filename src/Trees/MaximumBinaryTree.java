package Trees;

/**
 * Created by rahilvora on 9/29/17.
 */
public class MaximumBinaryTree {
    public TreeNode constructMaximumBinaryTree(int[] nums) {
        return helper(nums, 0, nums.length - 1);
    }
    public TreeNode helper(int[] nums,int start ,int end){
        if(start > end) return null;
        int idx = start;
        for(int i = start + 1; i < end; i++){
            if(nums[i] > nums[idx]){
                idx = i;
            }
        }
        TreeNode root = new TreeNode(nums[idx]);
        root.left = helper(nums, start, idx - 1);
        root.right = helper(nums,  idx + 1, end);
        return root;
    }
}
