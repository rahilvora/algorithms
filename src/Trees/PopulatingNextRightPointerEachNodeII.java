package Trees;

/**
 * Created by rahilvora on 6/2/17.
 */
public class PopulatingNextRightPointerEachNodeII {
    public void connect(TreeLinkNode root) {
        TreeLinkNode currentNode = root; //current node of the current level
        TreeLinkNode previous = null; //Leading node in the next leave
        TreeLinkNode head = null; //Head of the next level
        while(currentNode != null){
            while(currentNode != null){
                if (currentNode.left != null){
                    if(previous != null){
                        previous.next = currentNode.left;
                    }
                    else {
                        head = currentNode.left;
                    }
                    previous = currentNode.left;
                }
                if(currentNode.right != null){
                    if(previous != null){
                        previous.next = currentNode.right;
                    }
                    else{
                        head = currentNode.right;
                    }
                    previous = currentNode.right;
                }
                currentNode = currentNode.next;
            }
            currentNode = head;
            head = null;
            previous = null;
        }
    }
}
