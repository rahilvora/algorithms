package Trees;

import java.util.Stack;

/**
 * Created by rahilvora on 9/28/17.
 *          10
 *         /  \
 *        5   15
 *       / \  / \
 *      2  7 12 17
 *      Preorder: 10 5 2 7 15 12 17
 *      Inorder: 2 5 7 10 12 15 17
 *
 */
public class BinaryTreeFromPreOrderAndInOrder {
    public static void main(String args[]){
        int[] preorder = {10, 5, 2, 7, 15, 12, 17};
        int[] inorder = {2, 5, 7, 10, 12, 15, 17};
        new BinaryTreeFromPreOrderAndInOrder().buildTree(preorder, inorder);
    }
    public TreeNode buildTree(int[] preorder, int[] inorder) {
        if(preorder == null || preorder.length == 0) return null;
        Stack<TreeNode> stack = new Stack<>();
        TreeNode root = new TreeNode(preorder[0]);
        TreeNode curr = root;
        for(int i = 1, j = 0; i < preorder.length; i++){
            if(curr.val != inorder[j]){
                curr.left = new TreeNode(preorder[i]);
                stack.push(curr);
                curr = curr.left;
            }
            else{
                j++;
                while (!stack.isEmpty() && stack.peek().val == inorder[j]){
                    curr = stack.pop();
                    j++;
                }
                curr = curr.right = new TreeNode(preorder[i]);
            }
        }
        return root;
    }


}
