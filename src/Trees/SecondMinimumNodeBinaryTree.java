package Trees;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by rahilvora on 9/29/17.
 */
public class SecondMinimumNodeBinaryTree {
    public int findSecondMinimumValue(TreeNode root) {
        if(root == null) return -1;
        boolean diffFound = false;
        int firstMin = root.val, secondMin = Integer.MAX_VALUE;
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        while(queue.isEmpty()){
            TreeNode curr = queue.poll();
            if(curr.val != firstMin && curr.val < secondMin){
                secondMin = curr.val;
                diffFound = true;
            }
            if(curr.left != null){
                queue.offer(curr.left);
                queue.offer(curr.right);
            }
        }
        return (secondMin == Integer.MAX_VALUE && !diffFound) ? -1: secondMin;
    }
}
