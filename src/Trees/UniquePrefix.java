package Trees;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by rahilvora on 8/20/17.
 */
class TrieNode{
    char c;
    int frequency;
    HashMap<Character, TrieNode> children = new HashMap<>();
    TrieNode(){}
    public TrieNode(Character c) {
        this.c = c;
        frequency = 1;
    }

}

public class UniquePrefix {
    TrieNode root;
    UniquePrefix(){
        root = new TrieNode();
    }
    public void insertWord(String word){
        HashMap<Character, TrieNode> children =root.children;
        for(int i = 0; i < word.length(); i++){
            char c = word.charAt(i);
            TrieNode t;
            if(children.containsKey(c)){
                t = children.get(c);
                t.frequency++;
            }
            else{
                t = new TrieNode(c);
                children.put(c, t);
            }
            children = t.children;

        }
    }
    public String findPrefix(String word) {
        if (word == null || word.length() <= 0) {
            return null;
        }
        StringBuilder prefix = new StringBuilder();
        HashMap<Character, TrieNode> children = root.children;
        for (int i = 0; i < word.length(); i++) {
            char c = word.charAt(i);
            TrieNode t;
            if (children.containsKey(c)) {
                t = children.get(c);
                children = t.children;
                prefix.append(c);
                if (t.frequency == 1) {
                    return prefix.toString();
                }
            }
            else {
                return null;
            }
        }
        return prefix.toString();
    }
    public ArrayList<String> prefix(ArrayList<String> a) {
        if (a == null || a.size() <= 0) {
            return null;
        }
        ArrayList<String> result = new ArrayList<>();
        //UniquePrefix trie = new UniquePrefix();
        for (String word : a) {
            insertWord(word);
        }
        for (String word : a) {
            String prefix = findPrefix(word);
            result.add(prefix);
        }
        return result;
    }
}
