package Trees;

import java.util.ArrayList;
import java.util.List;

public class UniqueBinaryTreesII {
    public static void main(String args[]){
        new UniqueBinaryTreesII().generateBTrees(3);
    }
    public List<TreeNode> generateBTrees(int n){
        List<TreeNode> answer = new ArrayList<>();
        if(n == 0){
            return answer;
        }
        return generateBTrees(1, n);
    }

    public List<TreeNode> generateBTrees(int start, int end){
        List<TreeNode> allTrees = new ArrayList<>();
        if(start > end){
            allTrees.add(null);
            return allTrees;
        }

        for(int i = start; i  <= end; i++){

            List<TreeNode> left = generateBTrees(start, i-1);
            List<TreeNode> right = generateBTrees(i+1, end);

            for(TreeNode l : left){
                for(TreeNode r: right){
                    TreeNode curr = new TreeNode(i);
                    curr.left = l;
                    curr.right = r;
                    allTrees.add(curr);
                }
            }
        }
        return allTrees;
    }
}
