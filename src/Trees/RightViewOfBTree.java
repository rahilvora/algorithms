package Trees;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by rahilvora on 2/14/18.
 */
public class RightViewOfBTree {
    public static void main(String args[]){
        RightViewOfBTree obj = new RightViewOfBTree();
    }
    public static String foo(){
        System.out.println("Test toString called");
        return "";
    }
    private static void printRightView(TreeNode root){

        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        while(!queue.isEmpty()){
            int size = queue.size();
            for(int i = 0; i <=size-1; i++){
                TreeNode curr = queue.poll();
                if(i == size -1){ // Change this to 0 for left view of a tree
                    System.out.print(curr.val);
                }
                if(curr.left != null) queue.offer(curr.left);
                if(curr.right != null) queue.offer(curr.right);
            }
        }
    }
}
