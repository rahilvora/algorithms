package Trees;

/*
*
* Algo:
* 1. Inorder traverse BST and connect original BST
* 2. connect head and tail
*
* */
public class ConvertBSTtoSortedDoublyLinkedList {
    TreeNode prev = null;
    public TreeNode treeToDoublyList(TreeNode root) {
        if(root == null) return null;
        TreeNode dummy = new TreeNode(0);
        prev = dummy;
        helper(root);
        prev.right = dummy.right;
        dummy.right.left = prev;
        return dummy.right;
    }
    public void helper(TreeNode node){
        if(node == null) return;
        helper(node.left);
        prev.right = node;
        node.left = prev;
        prev = node;
        helper(node.right);
    }
}
