package Trees;

import java.util.Stack;

/**
 * Created by rahilvora on 2/28/18.
 */
public class MinimumDistBetweenTwoNodeBST {
    public int minDiffInBST(TreeNode root) {
        int min = Integer.MAX_VALUE;
        Stack<TreeNode> stack = new Stack<>();
        TreeNode pre = null;
        while(!stack.isEmpty()|| root != null){
            if(root != null){
                stack.push(root);
                root = root.left;
            }
            else{
                TreeNode node = stack.pop();
                if(pre != null){
                    min = Math.min(min,Math.abs(node.val - pre.val));
                }
                pre = node;
                root = node.right;
            }
        }
        return min;
    }
}
