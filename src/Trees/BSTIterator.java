package Trees;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Stack;

/**
 * Created by rahilvora on 5/22/17.
 */
public class BSTIterator{
    private Stack<TreeNode> stack = new Stack<>();
    BSTIterator(TreeNode root){
        pushAll(root);
    }


    public boolean hasNext() {
        return stack.isEmpty();
    }


    public int next() {
        TreeNode curr = stack.pop();
        pushAll(curr.right);
        return curr.val;
    }

    public void pushAll(TreeNode root){
        while(root != null){
            stack.push(root);
            root = root.left;
        }
    }
}
