package Trees;

/**
 * Created by rahilvora on 5/22/17.
 */
public class MaximumPathSum {
    int max;
    public int maxPathSum(TreeNode root) {
        if(root == null) return 0;
        max = Integer.MIN_VALUE;
        helper(root);
        return max;
    }
    public int helper(TreeNode root){
        if(root == null) return 0;
        int left = Math.max(helper(root.left),0);
        int right = Math.max(helper(root.right),0);
        max = Math.max(max, root.val + left + right);
        return root.val + Math.max(left, right);
    }
}
