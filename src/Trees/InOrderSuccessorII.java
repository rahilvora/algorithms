package Trees;
class NodeI {
    public int val;
    public NodeI left;
    public NodeI right;
    public NodeI parent;
}
public class InOrderSuccessorII {
    public NodeI inorderSuccessor(NodeI x) {
        if(x.right != null){
            NodeI temp = x.right;
            while(temp.left != null){
                temp = temp.left;
            }
            return temp;
        }
        else{
            NodeI temp = x;
            while(temp.parent != null && temp.parent.right == temp){
                temp = temp.parent;
            }
            return temp.parent;
        }
    }
}
