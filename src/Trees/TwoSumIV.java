package Trees;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by rahilvora on 8/23/17.
 */
public class TwoSumIV {
    public boolean findTarget(TreeNode root, int k) {
        if(root == null) return false;
        Set<Integer> set = new HashSet<>();
        return dfs(set, root, k);
    }
    public boolean dfs(Set<Integer> set, TreeNode root, int k){
        if(root == null) return false;
        if(set.contains(k - root.val)) return true;
        set.add(root.val);
        return dfs(set, root.left, k) || dfs(set, root.right, k);
    }
}
