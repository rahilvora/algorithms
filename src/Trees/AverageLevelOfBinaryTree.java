package Trees;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * Created by rahilvora on 7/28/17.
 */
public class AverageLevelOfBinaryTree {
    public List<Double> averageOfLevels(TreeNode root) {
        List<Double> answer = new ArrayList<>();
        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(root);
        while(!queue.isEmpty()){
            long sum = 0, count = 0;
            Queue<TreeNode> temp = new LinkedList<>();
            while(!queue.isEmpty()){
                TreeNode node = queue.remove();
                sum += node.val;
                count++;
                if(node.left != null) temp.add(node.left);
                if(node.right!= null) temp.add(node.right);
            }
            queue = temp;
            answer.add(sum*1.0/count);
        }
        return answer;
    }
}
