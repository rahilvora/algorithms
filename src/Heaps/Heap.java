package Heaps;

/**
 * Created by rahilvora on 9/7/17.
 * Time Complexity
 * Heapsort: O(NlogN)
 * Buildheap: O(N)
 * Maxheapify: O(logN)
 */
public class Heap {
    public static int[] heapArray;
    public static int heapsize;
    public static int largest;
    public static void main(String args[]){
        heapArray = new int[]{4, 1, 3, 2, 16, 9, 10, 14, 8, 7};
        new Heap().heapSort(heapArray);
    }

    public void maxHeapify(int[] heapArray, int index){
        int left = left(index);
        int right = right(index);
        if(left < heapsize && heapArray[left] > heapArray[index]){
            largest = left;
        }
        else{
            largest = index;
        }
        if(right < heapsize && heapArray[right] > heapArray[largest]){
            largest = right;
        }
        if(largest != index){
            int temp = heapArray[largest];
            heapArray[largest] = heapArray[index];
            heapArray[index] = temp;
            maxHeapify(heapArray, largest);
        }
    }
    public int left(int index){
        return 2*index + 1;
    }
    public int right(int index){
        return 2*index + 2;
    }
    public void buildHeap(int[] heapArray){
        heapsize = heapArray.length;
        for(int i = heapArray.length - 1; i>= 0; i--){
            maxHeapify(heapArray, i);
        }
    }
    public void heapSort(int[] heapArray){
        buildHeap(heapArray);
        for(int i = heapArray.length - 1; i >= 1; i--){
            int temp = heapArray[i];
            heapArray[i] = heapArray[0];
            heapArray[0] = temp;
            heapsize -= 1;
            maxHeapify(heapArray, 0);
        }
    }
}
