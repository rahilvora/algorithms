package Heaps;

import java.util.PriorityQueue;

public class KthLargestElementInAStream {
    final PriorityQueue<Integer> priorityQueue;
    final int k;
    public KthLargestElementInAStream(int k, int[] nums) {
        this.k = k;
        priorityQueue = new PriorityQueue<>();
        for(int n: nums){
            add(n);
        }
    }

    public int add(int val) {
        if(priorityQueue.size() < k) priorityQueue.offer(val);
        else if(priorityQueue.peek() < val){
            priorityQueue.poll();
            priorityQueue.offer(val);
        }
        return priorityQueue.peek();
    }
}
