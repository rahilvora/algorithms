package Heaps;

import java.util.Collections;
import java.util.PriorityQueue;

/**
 * Created by rahilvora on 9/7/17.
 * Time Complexity: Add O(log N)
 * find: O(1)
 */
public class FindMedianFromDataStream {

    // Small holds the smaller half of the interger in reverse order, basically a max heap
    // Large holds the larger half of the integer, basically a min heap;
    PriorityQueue<Integer> small, large;
    boolean even = true;
    public FindMedianFromDataStream() {
        small = new PriorityQueue<>(Collections.reverseOrder());
        large = new PriorityQueue<>();
    }

    public void addNum(int num) {
        if(even){
            large.offer(num);
            small.offer(large.poll());
        }
        else{
            small.offer(num);
            large.offer(small.poll());
        }
        even = !even;
    }

    public double findMedian() {
        if(even){
            return (small.peek() + large.peek()) / 2.0;
        }
        return small.peek();
    }
}
