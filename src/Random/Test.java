package Random;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by rahilvora on 9/24/17.
 */
public class Test {
    public static void main(String args[]) {
        //Ani//mal a = new Animal();   // Animal reference and object
        Dog b = new Dog();   // Animal reference but Dog object

        //a.move();   // runs the method in Animal class
        b.move();   // runs the method in Dog class
    }
}

 class Animal {
    public void move() {
        System.out.println("Animals can move");
    }
}

class Dog extends Animal {
//    public void move() {
//        System.out.println("Dogs can walk and run");
//    }

}
interface a{
    void b();
}
interface b{
    void c();
}
interface c extends a, b{

}
