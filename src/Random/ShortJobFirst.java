package Random;

import java.util.Scanner;

/**
 * Created by rahilvora on 8/31/17.
 */
public class ShortJobFirst {
    public static void main(String args[]){
        new ShortJobFirst().shortestJobFirst();
    }
    public void shortestJobFirst(){
        Scanner in = new Scanner(System.in);
        System.out.println("Enter number of processes");
        int numberOfProcesses = in.nextInt(), temp, total = 0;
        double avg = 0.0;
        int[] process = new int[numberOfProcesses];
        int[] burstTime = new int[numberOfProcesses];
        int[] waitTime = new int[numberOfProcesses];
        for(int i = 0; i < numberOfProcesses; i++){
            System.out.println("Enter process ID");
            process[i] = in.nextInt();
            System.out.print("Enter burstTime for " + process[i]);
            burstTime[i] = in.nextInt();
        }
        for(int i = 0; i < numberOfProcesses - 1; i++){
            for(int j = i + 1; j < numberOfProcesses; j++){
                if(burstTime[i] > burstTime[j]){
                    temp = burstTime[j];
                    burstTime[j] = burstTime[i];
                    burstTime[i] = temp;
                    temp = process[j];
                    process[j] = process[i];
                    process[i] = temp;
                }
            }
        }
        waitTime[0] = 0;
        for(int i = 1; i < numberOfProcesses; i++){
            waitTime[i] = waitTime[i - 1] + burstTime[i-1];
            total += waitTime[i];
        }
        avg = (double) total/ numberOfProcesses;
        for (int i = 0; i < numberOfProcesses; i++){
            System.out.println(process[i] + " Burst Time " + burstTime[i] + " Wait Time " + waitTime[i]);
        }
        System.out.println("Total " + total + " Average " + avg);
    }
}
