package Sorting;

/**
 * Created by rahilvora on 2/17/18.
 */
public class BubbleSort {
    public static void main(String args[]){

    }
    public void bubbleSort(int[] arr){
        for(int i = 0 ; i < arr.length; i++){
            for(int j = 0; j < arr.length - i - 1; j++){
                if(arr[j] > arr[j+1]){
                    int temp = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = temp;
                }
            }
        }
        for(int i = 0; i < arr.length; i++){
            System.out.print(arr[i]);
        }
    }
}
