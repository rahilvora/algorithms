package Sorting;

/**
 * Created by rahilvora on 9/14/17.
 * Time: Best O(N), Worst O(1)
 * Space: O(1)
 */
public class InsertionSort {
    public static void main(String args[]){
        int[] arr = {5,4,3,2,1};
        new InsertionSort().insertionSort(arr);
    }
    public void insertionSort(int[] arr){
        for(int i = 1; i < arr.length;i++){
            int val = arr[i];
            int j = i - 1;
            while(j >= 0 && arr[j]>val){
                arr[j+1] = arr[j];
                j--;
            }
            arr[j+1] = val;
        }
        for(int i = 0; i < arr.length; i++){
            System.out.print(arr[i]);
        }
    }
}
