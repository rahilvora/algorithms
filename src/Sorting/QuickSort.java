package Sorting;

/**
 * Created by rahilvora on 9/14/17.
 */
public class QuickSort {
    public static void main(String args[]){
        int[] arr = {2, 8, 5, 1, 3, 6, 4};
        new QuickSort().quickSort(arr, 0, arr.length - 1);
        for(int i = 0; i < arr.length; i++){
            System.out.print(arr[i]);
        }
    }
    public void quickSort(int[] arr, int start, int end){
        if(start < end){
            int q = partition(arr, start, end);
            quickSort(arr, start, q - 1);
            quickSort(arr, q + 1, end);
        }
    }
    public int partition(int[] arr, int start, int end){
        int pivot = arr[end];
        int j = start;
        int i = j - 1;
        while (j <= end - 1){
            if(arr[j] <= pivot){
                i = i + 1;
                swap(arr, i, j);
            }
            j++;
        }
        swap(arr, i+1,j);
        return i + 1;
    }
    public void swap(int[] arr, int i, int j){
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }
}
