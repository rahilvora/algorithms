package Sorting;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Comparator;

/**
 * Created by rahilvora on 8/22/17.
 * Time: O(nlogn)
 * Space: O(n)
 */
public class MeetingRoom {
    public boolean canAttendMeetings(Interval[] intervals) {
        if(intervals == null || intervals.length == 0){
            return false;
        }
        Arrays.sort(intervals, new Comparator<Interval>() {
            @Override
            public int compare(Interval o1, Interval o2) {
                return o1.start - o2.start;
            }
        });

        for(int i = 0; i < intervals.length - 1; i++){
            if(intervals[i].end > intervals[i+1].start) return false;
        }
        return true;
    }
}
