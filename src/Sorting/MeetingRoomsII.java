package Sorting;

import java.util.Arrays;
import java.util.Comparator;
import java.util.PriorityQueue;

/**
 * Created by rahilvora on 8/22/17.
 * Time: O(nlogn)
 * Space: O(n)
 */
public class MeetingRoomsII {
    public int minMeetingRooms(Interval[] intervals) {
        if(intervals == null || intervals.length == 0){
            return 0;
        }
        Arrays.sort(intervals, new Comparator<Interval>() {
            @Override
            public int compare(Interval o1, Interval o2) {
                return o1.start - o2.start;
            }
        });
        int count = 1;
        PriorityQueue<Integer> queue = new PriorityQueue<>();
        queue.offer(intervals[0].end);
        for(int i = 1; i < intervals.length; i++){
            if(intervals[i].start < queue.peek()) count++;
            else queue.poll();
            queue.offer(intervals[i].end);
        }
        return count;
    }
}
