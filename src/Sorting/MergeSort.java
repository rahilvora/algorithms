package Sorting;

/**
 * Created by rahilvora on 9/14/17.
 */
public class MergeSort {
    public static void main(String args[]){
        int[] arr = {2, 8, 5, 1};
        new MergeSort().mergerSort(arr, 0, arr.length - 1);
        for(int i = 0; i < arr.length; i++){
            System.out.print(arr[i]);
        }
    }
    public void mergerSort(int[] arr, int low, int high){
        if(low < high){
            int mid = (low + high) / 2;
            mergerSort(arr, low, mid);
            mergerSort(arr, mid+1, high);
            merge(arr, low, mid, high);
        }
    }
    public void merge(int[] a, int low, int mid, int high){
        int[] temp = new int[a.length];
        int l = low, i = low, j = mid + 1;

        while(i <= mid && j <= high){
            if(a[i]<a[j]){
                temp[l++] = a[i++];
            }
            else{
                temp[l++] = a[j++];
            }
        }
        while(i <= mid ){
            temp[l++] = a[i++];
        }

        while( j <= high){
            temp[l++] = a[j++];
        }

        for(int k = low; k <= high; k++){
            a[k] = temp[k];
        }

    }
}
