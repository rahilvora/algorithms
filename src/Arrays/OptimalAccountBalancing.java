package Arrays;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// time o(n^2)

public class OptimalAccountBalancing {
    public int minTransfers(int[][] transactions) {
        Map<Integer, Integer> map = new HashMap<>();
        for(int i = 0; i < transactions.length; i++){
            map.put(transactions[i][0], map.getOrDefault(transactions[i][0],0) - transactions[i][2]);
            map.put(transactions[i][1], map.getOrDefault(transactions[i][1],0) + transactions[i][2]);
        }
        return dfs(new ArrayList<>(map.values()), 0);
    }
    public int dfs(List<Integer> debts, int start){
        // debts with value zero does not help in transactions
        while(start < debts.size() && debts.get(start) == 0){
            start++;
        }

        if(start == debts.size()) return 0;

        int minTransactions = Integer.MAX_VALUE;
        for(int i = start + 1; i < debts.size(); i++){
            if(debts.get(i) * debts.get(start) < 0){
                debts.set(i, debts.get(i) + debts.get(start));
                minTransactions = Math.min(minTransactions, 1 + dfs(debts, start + 1));
                debts.set(i, debts.get(i) - debts.get(start));
            }
        }
        return minTransactions;
    }
}
