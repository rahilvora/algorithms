package Arrays;

import java.util.Arrays;

/**
 * Created by rahilvora on 1/18/18.
 */
public class SetMatrixZeroes {
    public void setZeroes(int[][] matrix) {
        int n = matrix.length;
        boolean[] rows = new boolean[n];
        boolean[] cols = new boolean[matrix[0].length];

        for(int i = 0; i < n; i++){
            for(int j = 0; j < matrix[0].length; j++){
                if(matrix[i][j] == 0){
                    rows[i] = true;
                    cols[j] = true;
                }
            }
        }

        for(int i = 0; i < n; i++){
            for(int j = 0; j < matrix[0].length; j++){
                if(rows[i] || cols[j]){
                    matrix[i][j] = 0;
                }
            }
        }
    }
}
