package Arrays;

import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;

/**
 * Created by rahilvora on 8/18/17.
 */
public class SubsetsII {
    public static void main(String args[]){
        int[] nums = {1,2,2};
        new SubsetsII().subsetsWithDup(nums);
    }
    public List<List<Integer>> subsetsWithDup(int[] nums) {
        List<List<Integer>> answer = new ArrayList<>();
        Arrays.sort(nums);
        backtrack(answer, new ArrayList<>(), nums, 0);
        return answer;
    }
    public void backtrack(List<List<Integer>> answer, List<Integer> tempList, int[] nums, int start){
        answer.add(new ArrayList<>(tempList));
        for(int i = start; i < nums.length; i++){
            if(i > start && nums[i-1] == nums[i]) continue; // Skip Duplicates
            tempList.add(nums[i]);
            backtrack(answer, tempList, nums, i+1);
            tempList.remove(tempList.size() - 1);
        }
    }
}
