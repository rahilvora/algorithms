package Arrays;

/**
 * Created by rahilvora on 1/10/18.
 */
public class PointOfRotationInArray {
    public static void main(String args[]){
        int[] nums = {5,6,4,1,2,3};
        new PointOfRotationInArray().findPoint(nums);
    }
    public void findPoint(int[] nums){
        int low = 0, high = nums.length - 1, mid = 0;
        int last = nums[high];
        while(low <= high){
            mid = low + (high - low)/2;
            if(nums[mid] > last){
                low = mid+1;
            }
            else if(nums[mid] < last){
                high = mid - 1;
            }
            else{
                break;
            }
        }
        System.out.print(nums[mid]);
    }
}
