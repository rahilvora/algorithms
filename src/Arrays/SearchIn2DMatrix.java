package Arrays;

/**
 * Created by rahilvora on 1/19/18.
 */
public class SearchIn2DMatrix {
    public boolean searchMatrix(int[][] matrix, int target) {
        if(matrix == null || matrix.length < 1 || matrix[0].length < 1) return false;
        int rows = 0, cols = matrix[0].length - 1;
        while(rows < matrix.length && cols >=0){
            int val = matrix[rows][cols];
            if (target == val) return true;
            else if(target < val){
                cols--;
            }
            else if(target > val){
                rows++;
            }
        }
        return false;
    }
}
