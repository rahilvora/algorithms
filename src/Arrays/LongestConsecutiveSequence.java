package Arrays;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by rahilvora on 7/24/17.
 */
public class LongestConsecutiveSequence {
    public static void main(String args[]){
        int[] num = {100, 4, 200, 1, 3, 2};
        System.out.print(new LongestConsecutiveSequence().longestConsecutive(num));
    }
    public int longestConsecutive(int[] nums) {
        int answer = 0;
        if(nums.length == 0) return answer;
        Map<Integer, Integer> map = new HashMap<>();
        for(int n : nums){
            if(!map.containsKey(n)){
                int left = (map.containsKey(n - 1)?map.get(n - 1):0);
                int right = (map.containsKey(n + 1)?map.get(n + 1):0);
                int sum = left + right + 1;
                answer = Math.max(sum, answer);
                map.put(n,sum);

                map.put(n - left,sum);
                map.put(n + right,sum);
            }
            else{
                continue;
            }
        }
        return  answer;
    }
}
