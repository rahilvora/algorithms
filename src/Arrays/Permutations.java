package Arrays;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rahilvora on 8/23/17.
 */
public class Permutations {
    public List<List<Integer>> permute(int[] nums) {
        List<List<Integer>> answer = new ArrayList<>();
        backTrack(answer, new ArrayList<Integer>(),nums);
        return answer;
    }
    public void backTrack(List<List<Integer>> list, List<Integer> temp, int[] nums){
        if(temp.size() == nums.length){
            list.add(temp);
        }
        else{
            for(int i = 0; i < nums.length; i++){
                if(temp.contains(nums[i])) continue; // Skip as element already exist.
                temp.add(nums[i]);
                backTrack(list, temp, nums);
                temp.remove(temp.size() - 1);
            }
        }
    }
}
