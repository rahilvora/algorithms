package Arrays;

import java.util.ArrayList;

/**
 * Created by rahilvora on 8/19/17.
 */
public class MinimumSwitchToLightBulbs {
    public int bulbs(ArrayList<Integer> a) {
        int count = 0, state = 0;
        for(int i = 0; i < a.size(); i++){
            if(a.get(i) == state) {
                count++;
                state = 1 - state;
            }
        }
        return count;
    }
}
