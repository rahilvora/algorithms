package Arrays;

public class NextGreaterCharacter {
    public static void main(String args[]){
        char[] letters = {'c', 'f', 'j'};
        System.out.println(new NextGreaterCharacter().nextGreatestLetter(letters, 'c'));
    }
    public char nextGreatestLetter(char[] letters, char target) {
        int start = 0;
        int end = letters.length - 1;
        while(start <= end){
            int mid = (end - start) / 2 + start;
            if(letters[mid] > target){
                end = mid - 1;
            }
            else
                start = mid + 1;
        }
        return start >= letters.length? letters[0] : letters[start];
    }
}
