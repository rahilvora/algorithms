package Arrays;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by rahilvora on 5/11/17.
 */
public class KDiffPairs {
    public static void main(String args[]){
        int[] nums = {3, 1, 4, 1, 5};
        new KDiffPairs().findPairs(nums, 2);
    }
    public int findPairs(int[] nums, int k) {
        int count = 0;
        if (nums == null || nums.length == 0 || k < 0)   return count;
        Map<Integer, Integer> map = new HashMap<>();
        for(int num: nums){
            map.put(num, map.getOrDefault(num,0)+1);
        }
        for(Map.Entry<Integer, Integer> entry: map.entrySet()){
           if(k == 0) {
               //count how many elements in the array that appear more than twice.
               if(entry.getValue() >= 2){
                   count++;
               }
           }
            else if(map.containsKey(entry.getKey() + k)){
               count++;
           }
        }
        return count;
    }

}
