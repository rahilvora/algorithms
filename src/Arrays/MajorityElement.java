package Arrays;

/**
 * Created by rahilvora on 5/12/17.
 */
public class MajorityElement {
    public static void main(String args[]){

    }
    public int majorityElement(int[] nums) {
        int candidate = -1;
        int count = 0;
        //Step 1
        for(int num : nums){
            if(count == 0){
                candidate = num;
                count++;
            }
            else if(candidate == num){
                count++;
            }
            else{
                count--;
            }
        }
        //Step 2
        if(count == 0) return -1;
        count = 0;
        for(int num : nums){
            if(num == candidate){
                count++;
            }
        }
        if(count > nums.length/2) return candidate;
        return -1;
    }
}
