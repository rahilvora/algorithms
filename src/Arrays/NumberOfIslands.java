package Arrays;

/**
 * Created by rahilvora on 6/23/17.
 * Time : O(m*n)
 */
public class NumberOfIslands {
    private int m,n;
    private static int count = -1;
    public static void main(String args[]){

    }
    public int getFood(char[][] grid) {
        int currR = -1, currC = -1;
        for(int i = 0; i < grid.length; i++){
            for(int j = 0; j < grid[0].length; j++){
                if(grid[i][j] == '*'){
                    helper(grid, i, j, 0);
                }
            }
        }
        return count;
    }

    public void helper(char[][] grid, int i, int j, int depth){
        if(i < 0 || j < 0 || i > grid.length || j > grid[0].length || (grid[i][j] == 'X')) return;
        if(grid[i][j] == '#') {
            count = Math.min(count, depth);
            return;
        }

        helper(grid, i+1, j, depth + 1);
        helper(grid, i-1, j, depth + 1);
        helper(grid, i, j+1, depth + 1);
        helper(grid, i, j-1, depth + 1);
        helper(grid, i+1, j+1, depth + 1);
        helper(grid, i-1, j+1, depth + 1);
        helper(grid, i-1, j-1, depth + 1);
        helper(grid, i+1, j-1, depth + 1);
    }
}
