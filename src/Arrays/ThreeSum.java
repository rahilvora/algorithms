package Arrays;

import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;

/**
 * Created by rahilvora on 8/3/17.
 * Time: O(N^2)
 */
public class ThreeSum {
    public List<List<Integer>> threeSum(int[] nums) {
        Arrays.sort(nums);
        List<List<Integer>> answer = new ArrayList<>();
        for(int i = 0; i < nums.length - 2; i++){
            if(i == 0 ||(i > 0 && nums[i] != nums[i-1])){
                int sum = 0 - nums[i], lo = i + 1, hi = nums.length - 1;
                while(lo < hi){
                    if(nums[lo] + nums[hi] == sum){
                        answer.add(Arrays.asList(nums[i], nums[lo], nums[hi]));
                        while(lo < hi && nums[lo] == nums[lo+1])lo++;
                        while(lo < hi && nums[hi] == nums[hi-1])hi--;
                        lo++; hi--;
                    }
                    else if(nums[lo] + nums[hi] < sum){
                        lo++;
                    }
                    else{
                        hi--;
                    }
                }
            }
        }
        return answer;
    }
}
