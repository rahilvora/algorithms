package Arrays;

/**
 * Created by rahilvora on 5/12/17.
 */
public class MoveZeros {
    public static void main(String args[]){
        int[] nums = {0,1,0,3,12};
        new MoveZeros().moveZeroes(nums);
    }
    public void moveZeroes(int[] nums) {
        int lastIndex = 0;
        for(int num: nums){
            if(num != 0){
                nums[lastIndex++] = num;
            }
        }
        for(int i = lastIndex; i < nums.length; i++){
            nums[i] = 0;
        }
    }
}
