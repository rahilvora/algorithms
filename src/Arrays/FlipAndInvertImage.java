package Arrays;

public class FlipAndInvertImage {
    public static void main(String args[]){
        int[][] A = {{1,1,0}, {1,0,1}, {0, 0, 0}};
        new FlipAndInvertImage().flipAndInvertImage(A);
    }

    public int[][] flipAndInvertImage(int[][] A) {
        if(A == null || A.length == 0) return A;
        int rows = A.length, cols = A[0].length;
        for(int i = 0; i < rows; i++){
            for(int j = 0; j < (cols + 1)/2; j++){
                int temp = A[i][j] == 0 ? 1 : 0;
                A[i][j] = A[i][cols - 1- j] == 0 ? 1: 0;
                A[i][cols - 1 - j] = temp;
            }
        }
        return A;
    }
}
