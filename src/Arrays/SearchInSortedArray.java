package Arrays;

/**
 * Created by rahilvora on 8/24/17.
 */
public class SearchInSortedArray {
    public int search(int[] nums, int target){
        int index = -1;
        int low = 0, high = nums.length - 1;
        for(int i = 0; i < nums.length; i++){
            int mid = (low + high) / 2;
            if(nums[mid] == target) return mid;
            if(nums[low] <= nums[mid]){
                if(target >= nums[low] && target < nums[mid]){
                    high = mid - 1;
                }
                else{
                    low = mid+1;
                }
            }
            if(nums[mid] <= nums[high]){
                if(target> nums[mid] && target <= nums[high]){
                    low = mid + 1;
                }
                else{
                    high = mid - 1;
                }
            }
        }
        return index;
    }
}
