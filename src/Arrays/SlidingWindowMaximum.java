package Arrays;

public class SlidingWindowMaximum {
    public static void main(String args[]){
        int[] nums = {1,3,-1,-3,5,3,6,7};
        int[] nums1 = {2,1,3,4,6,3,8,9,10,12,56};

        int[] answer = new SlidingWindowMaximum().maxSlidingWindowII(nums1, 4);
        for(int i = 0; i < answer.length; i++){
            System.out.print(answer[i] + "\t");
        }
    }

    // O(n*k) where n is size of nums array and k is size of window
    public int[] maxSlidingWindow(int[] nums, int k) {
        if(nums == null || nums.length == 0) return new int[0];
        int[] answer = new int[nums.length - k + 1];
        for (int i = 0; i <= nums.length - k; i++) {
            answer[i] = findMax(nums, i, i+k);
        }
        return answer;
    }
    public int findMax(int[] arr, int start, int end){
        int max = Integer.MIN_VALUE;
        for(int i = start; i < end; i++){
            max = Math.max(arr[i], max);
        }
        return max;
    }

    // O(n) where n is size of nums array
    public int[] maxSlidingWindowII(int[] nums, int k){
        int size = nums.length;
        int[] answer = new int[size - k + 1];
        int[] max_left = new int[size];
        int[] max_right = new int[size];
        max_left[0] = nums[0];
        max_right[size - 1] = nums[size - 1];

        for(int i = 1; i < size; i++){
            max_left[i] = (i%k == 0) ? nums[i] : Math.max(max_left[i-1], nums[i]);

            int j = size - i - 1;
            max_right[j] = (j % k == 0) ? nums[j] : Math.max(max_right[j+1], nums[j]);
        }

        for (int i = 0, j = 0; i + k <= size; i++){
            answer[j++] = Math.max(max_right[i], max_left[i + k - 1]);
        }
        return answer;
    }

}
