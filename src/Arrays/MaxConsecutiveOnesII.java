package Arrays;
/*
*
* Input: [1,0,1,1,0]
  Output: 4
  Explanation: Flip the first zero will get the the maximum number of consecutive 1s.
    After flipping, the maximum number of consecutive 1s is 4.
*
* */
public class MaxConsecutiveOnesII {
    public static void main(String args[]){
        int[] nums = {0, 1, 1, 1};
        System.out.println(new MaxConsecutiveOnesII().findMaxConsecutiveOnes(nums));
    }
    //O(N^2)
    public int findMaxConsecutiveOnes(int[] nums) {
        int maxOnes = 0;
        for(int i = 0; i < nums.length; i++){
            boolean flipped = false;
            int count = 0;
            if(nums[i] == 0){
                count++;
                flipped = true;
            }
            if(nums[i] == 1){
                count++;
            }
            for(int j = i+1; j < nums.length; j++){
                if(nums[j] == 0 && !flipped){
                    flipped = true;
                    count++;
                }
                else if(nums[j] == 1){
                    count++;
                }
                else{
                    break;
                }
            }
            maxOnes = Math.max(count, maxOnes);
        }
        return maxOnes;
    }

    //O(N)
    // Idea is to maintain a window [l,h] with at most k zero
    public int findMaxConsecutiveOnesOptimal(int[] nums) {
        int max = 0, zero = 0, k = 1; // flip at most k zero
        for (int l = 0, h = 0; h < nums.length; h++) {
            if (nums[h] == 0)
                zero++;
            while (zero > k)
                if (nums[l++] == 0)
                    zero--;
            max = Math.max(max, h - l + 1);
        }
        return max;
    }
}
