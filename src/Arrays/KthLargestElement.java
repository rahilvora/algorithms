package Arrays;

import java.util.PriorityQueue;

/**
 * Created by rahilvora on 8/12/17.
 * Time o(n) best
 * worst o(n^2)
 * memory o(1);
 */
public class KthLargestElement {
    public static void main(String args[]){
        int[]  nums = {5,10,15,-5,-7,16};
        System.out.print(new KthLargestElement().kthLargestElement(nums, 2));
    }
    public int kthLargestElement(int[] nums, int k){
        int n = nums.length;
        return quickSelect(nums, 0, n - 1, n - k);
    }
    // Return the index of kth smallest element
    public int quickSelect(int[] nums, int low, int high, int k){
        int i = low, j = high, pivot = nums[high];
        while( i < j ){
            if(nums[i++] > pivot) swap(nums, --i, --j);
        }
        swap(nums, i , high);

        if(i == k)
            return nums[i];
        else if(i > k)
            return quickSelect(nums, low, i-1, k);
        else
            return quickSelect(nums, i+1, high, k);
    }
    public void swap(int[] nums, int a, int b){
        int temp = nums[a];
        nums[a] = nums[b];
        nums[b] = temp;
    }


    // Time o(nlogk) memory o(k)
    public int kthLargestElementII(int[] nums, int k){
        final PriorityQueue<Integer> pq = new PriorityQueue<>();
        for(int val : nums) {
            pq.offer(val);

            if(pq.size() > k) {
                pq.poll();
            }
        }
        return pq.peek();
    }
}
