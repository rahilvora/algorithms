package Arrays;

/**
 * Created by rahilvora on 9/2/17.
 * There are N trees in a circle. Each tree has a fruit value associated with it.

 A bird has to sit on a tree for 0.5 sec to gather all the fruits present on the tree and then the bird can move to a
 neighboring tree. It takes the bird 0.5 seconds to move from one tree to another.
 Once all the fruits are picked from a particular tree, she can’t pick any more fruits from that tree.
 The maximum number of fruits she can gather is infinite.

 We are given N and M (the total number of seconds the bird has), and the fruit values of the trees.
 We have to maximize the total fruit value that the bird can gather. The bird can start from any tree

 in food: 2 1 3 5 0 1 4
 time = 3

 */
public class BirdAndMaxFruitGathering {
    public static void main(String args[]){
        int[] food = {9, 3, 4, 5, 6, 8, 9};
        int time = 3;
        new BirdAndMaxFruitGathering().maxFood(food, time, food.length);
    }
    public int maxFood(int[] food, int time, int size){
        int sum = 0;
        for(int i = 0; i < time; i++){
            sum += food[i];
        }
        int max = sum, previousStartIndex = 0;
        for(int j = time; j < size; j++){
            sum = sum - food[previousStartIndex] + food[j];
            max = Math.max(max, sum);
            previousStartIndex++;
        }
        return max;
    }
}
