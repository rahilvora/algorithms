package Arrays;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by rahilvora on 1/6/18.
 */
public class IntersectionOfTwoArrayII {
    public int[] intersect(int[] nums1, int[] nums2) {
        Map<Integer, Integer> map = new HashMap<>();
        List<Integer> result = new ArrayList<>();
        for(int i = 0; i < nums1.length; i++){
            map.put(nums1[i], map.getOrDefault(nums1[i], 0) + 1);
        }
        for(int j = 0; j < nums2.length; j++){
            if(map.containsKey(nums2[j]) && map.get(nums2[j]) > 0){
                result.add(nums2[j]);
                map.put(nums2[j], map.get(nums2[j])-1);
            }
        }
        int[] resultArray = new int[result.size()];
        int i = 0;
        for(int num: result){
            resultArray[i++] = num;
        }
        return resultArray;
    }
}
