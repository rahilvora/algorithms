package Arrays;

/**
 * Created by rahilvora on 5/9/17.
 */
public class MissingNumber {
    public static void main(String args[]){
        int[] nums = {0,1,3,4};
        System.out.print(new MissingNumber().missingNumber(nums));
    }
    public int missingNumber(int[] nums) {
        int xor = 0, i = 0;
        for(i = 0; i < nums.length; i++){
            xor = xor^i^nums[i];
        }
        return xor^i;
    }
}
