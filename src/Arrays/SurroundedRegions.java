package Arrays;

public class SurroundedRegions {
    public static void main(String args[]){

    }

    /*
    *
    *
  i/p X X X X
      X O O X
      X X O X
      X O X X

  o/p X X X X
      X X X X
      X X X X
      X O X X

      Steps
      1. Convert corners 0's to * as corners 0 can't be converted to X
      2. Convert remaining 0's to X
      3. Now convert * back to 0
    *
    * */
    public void solve(char[][] board) {
        if(board.length == 0) return;

        for(int i = 0; i < board[0].length; i++){ // First Row
            if(board[0][i] == 'O') dfs(0, i, board);
        }

        for(int i = 0; i < board[0].length; i++){
            if(board[board.length - 1][i] == 'O') dfs(board.length - 1, i, board);
        }
        for(int i = 0; i < board.length; i++){ // First Column
            if(board[i][0] == 'O') dfs(i, 0, board);
        }

        for(int i = 0; i < board.length; i++){ // last column
            if(board[i][board[0].length - 1] == 'O') dfs(i, board[0].length - 1, board);
        }

        for(int i = 0; i < board.length; i++){
            for(int j = 0; j < board[0].length; j++){
                if(board[i][j] == 'O') board[i][j] = 'X';
            }
        }

        for(int i = 0; i < board.length; i++){
            for(int j = 0; j < board[0].length; j++){
                if(board[i][j] == '-') board[i][j] = 'O';
            }
        }
    }

    public void dfs(int i,int j, char[][]board){
        if(i < 0 || i > board.length - 1 || j < 0 || j > board[0].length - 1 || board[i][j] != 'O') return;
        board[i][j] = '-';
        dfs(i - 1, j, board);
        dfs(i + 1, j, board);
        dfs(i, j + 1, board);
        dfs(i, j - 1, board);
    }
}
