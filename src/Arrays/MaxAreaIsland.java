package Arrays;

public class MaxAreaIsland {
    private int m, n;
    private int finalCount = 0;
    public int maxAreaOfIsland(int[][] grid) {
        m = grid.length;
        n = grid[0].length;
        for(int i = 0; i < grid.length; i++){
            for(int j = 0; j < grid[0].length; j++){
                if(grid[i][j] == 1) {
                    finalCount = Math.max(dfs(grid, i, j, m, n), finalCount);
                }
            }
        }
        return finalCount;
    }
    public int dfs(int[][] grid, int i, int j, int m, int n){
        if(i < 0 || i >= m || j < 0 || j >=n || grid[i][j] != 1) return 0;
        grid[i][j] = 0;
        return 1 + dfs(grid, i + 1, j, m, n) + dfs(grid, i - 1, j, m, n) + dfs(grid, i, j + 1, m, n) + dfs(grid, i, j - 1, m, n);
    }
}
