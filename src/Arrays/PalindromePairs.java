package Arrays;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//TIme o(n*K^2) where n is list of words and K is median length of word

public class PalindromePairs {

    public List<List<Integer>> palindromePairs(String[] words) {
        List<List<Integer>> result = new ArrayList<>();
        Map<String , Integer> map = new HashMap<>();
        for(int i = 0; i < words.length; i++){
            map.put(words[i], i);
        }

        for(int i = 0; i < words.length; i++){
            for(int j = 0; j <= words[i].length(); j++){
                String str1 = words[i].substring(0, j);
                String str2 = words[i].substring(j);
                if(isPalindrome(str1)){
                    String str2rvs = new StringBuffer(str2).reverse().toString();
                    if(map.containsKey(str2rvs) && map.get(str2rvs) != i){
                        List<Integer> list = new ArrayList<>();
                        list.add(map.get(str2rvs));
                        list.add(i);
                        result.add(list);
                    }
                }

                if(isPalindrome(str2)){
                    String str1rvs = new StringBuffer(str1).reverse().toString();
                    if(map.containsKey(str1rvs) && map.get(str1rvs) != i && str2.length() != 0){
                        List<Integer> list = new ArrayList<>();
                        list.add(i);
                        list.add(map.get(str1rvs));
                        result.add(list);
                    }
                }
            }
        }
        return result;
    }
    public boolean isPalindrome(String s1){
        int size = s1.length();
        for(int i = 0; i < size / 2; i++){
            if(s1.charAt(i) != s1.charAt(size - 1 - i)) return false;
        }
        return true;
    }

}
