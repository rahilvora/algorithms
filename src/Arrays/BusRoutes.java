package Arrays;

import java.util.*;
// Time Complexity = O(V+E) where V is number of buses and E is number of Stops

public class BusRoutes {
    public int numBusesToDestination(int[][] routes, int S, int T) {
        if(S == T) return 0;
        Set<Integer> visited = new HashSet<>();
        Queue<Integer> queue = new LinkedList<>();
        Map<Integer, List<Integer>> map = new HashMap<>();

        for(int i = 0; i < routes.length; i++){
            for(int j = 0; j < routes[i].length; j++){
                List<Integer> buses = map.getOrDefault(routes[i][j], new ArrayList<>());
                buses.add(i);
                map.put(routes[i][j], buses);
            }
        }

        queue.offer(S);
        int answer = 0;
        while(!queue.isEmpty()){
            answer++;
            int len = queue.size();
            for(int i = 0; i < len; i++){
                int curr = queue.poll();
                List<Integer> buses = map.get(curr);
                for(int bus : buses){
                    if(visited.contains(bus)) continue;
                    visited.add(bus);
                    for(int j = 0; j < routes[bus].length; j++){
                        if(routes[bus][j] == T) return answer;
                        queue.offer(routes[bus][j]);
                    }
                }
            }
        }

        return -1;

    }
}
