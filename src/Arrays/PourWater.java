package Arrays;

public class PourWater {
    public static void main(String args[]){

    }
    public int[] pourWater(int[] heights, int V, int K) {
        if(heights == null || heights.length == 0){
            return heights;
        }
        int dropSpot;
        while(V>0){
            dropSpot = K;
            //Check left
            for(int i = K-1; i>=0; i--){
                //Cannot go left as height is not less then current spot
                if(heights[i]>heights[dropSpot]){
                    break;
                }
                else if(heights[i]<heights[dropSpot]){
                    dropSpot = i;
                }
            }
            if(dropSpot!=K){
                heights[dropSpot]++;
                V--;
                continue;
            }

            //Check right
            for(int i = K+1; i<heights.length; i++){
                //Cannot go right as height is not less then current spot
                if(heights[i] > heights[dropSpot]){
                    break;
                }
                else if(heights[i] < heights[dropSpot]){
                    dropSpot = i;
                }
            }
            heights[dropSpot]++;
            V--;

        }
        return heights;
    }
}
