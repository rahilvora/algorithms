package Arrays;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rahilvora on 5/10/17.
 */
public class FindAllNumberDisappearedInArray {
    public static void main(String args[]){
        int[] nums = {4,3,2,7,8,2,3,1};
    }
    public List<Integer> findDisappearedNumbers(int[] nums) {
        List<Integer> arr = new ArrayList<>();
        for(int i = 0; i < nums.length; i++){
            int val =  Math.abs(nums[i]) - 1;
            if(nums[val] > 0){
                nums[val] = - nums[val];
            }
        }
        for(int i = 0; i < nums.length; i++){
            if(nums[i] > 0){
                arr.add(i+1);
            }
        }
        return arr;
    }
}
