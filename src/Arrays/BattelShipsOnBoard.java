package Arrays;

public class BattelShipsOnBoard {
    public static void main(String args[]){
        char[][] board = {
                {'X', '.', '.', 'X'},
                {'.', '.', '.', 'X'},
                {'.', '.', '.', 'X'}
        };
        System.out.println(new BattelShipsOnBoard().countBattleships(board));
    }
    public int countBattleships(char[][] board) {
        int count = 0;
        for(int i = 0; i < board.length; i++){
            for(int j = 0; j < board[0].length; j++){
                if(board[i][j] == 'X'){
                    count++;
                    DFS(board, i, j, board.length, board[0].length);
                }
            }
        }
        return count;
    }
    public void DFS(char[][] board, int i, int j, int row, int col){
        if(i < 0 || i>=row || j < 0 || j >= col || board[i][j] != 'X'){
            return;
        }
        board[i][j] = '.';
        DFS(board, i+1, j, row, col);
        DFS(board, i-1, j, row, col);
        DFS(board, i, j+1, row, col);
        DFS(board, i, j-1, row, col);
    }
}
