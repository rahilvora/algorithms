package Arrays;

/**
 * Created by rahilvora on 9/3/17.
 */
public class MaximumSwap {
    public static void main(String args[]){
        System.out.print(new MaximumSwap().maximumSwap(9937));
    }
    public int maximumSwap(int num) {
        char[] arr = (num + "").toCharArray();
        for(int i = 0; i < arr.length; i++){
            int k = -1, idx = -1;
            for(int j = i + 1; j < arr.length; j++){
                if(arr[j] >= k){
                    k = arr[j];
                    idx = j;
                }
            }
            if(arr[i] < k){
                arr[idx] = arr[i];
                arr[i] = (char)k;
                return Integer.parseInt(String.valueOf(arr));
            }
        }
        return num;
    }
}
