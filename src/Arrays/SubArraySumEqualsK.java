package Arrays;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by rahilvora on 9/29/17.
 */
public class SubArraySumEqualsK {
    public static void main(String args[]){

    }
    public int subarraySum(int[] nums, int k) {
        int sum = 0, result = 0;
        Map<Integer, Integer> map = new HashMap<>();
        for(int i = 0; i < nums.length; i++){
            sum += nums[i];
            if(map.containsKey(sum)){
                result += map.get(sum);
            }
            map.put(sum, map.getOrDefault(sum,0) + 1);
        }
        return result;
    }
}
