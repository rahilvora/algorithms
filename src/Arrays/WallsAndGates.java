package Arrays;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by rahilvora on 9/4/17.
 * Time: O(MN)
 * Space: O(MN)
 *
 */
public class WallsAndGates {
    public static void main(String args[]){
        int[][] rooms = {{Integer.MAX_VALUE, -1, 0, Integer.MAX_VALUE},
                        {Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE, -1},
                        {Integer.MAX_VALUE, -1, Integer.MAX_VALUE, -1},
                        {0, -1, Integer.MAX_VALUE, Integer.MAX_VALUE}};
        new WallsAndGates().wallsAndGates(rooms);
    }
    public void wallsAndGates(int[][] rooms) {
        Queue<int[]> queue = new LinkedList<>();
        for(int i = 0; i < rooms.length; i++){
            for(int j = 0; j < rooms[0].length; j++){
                if(rooms[i][j] == 0) queue.add(new int[]{i,j});
            }
        }

        while(!queue.isEmpty()){
            int[] gate = queue.poll();
            int row = gate[0], col = gate[1];
            if(row < rooms.length - 1  && rooms[row+1][col] == Integer.MAX_VALUE){
                rooms[row+1][col] = rooms[row][col] + 1;
                queue.add(new int[]{row+1, col});
            }
            if(row > 0 && rooms[row - 1][col] == Integer.MAX_VALUE){
                rooms[row - 1][col] = rooms[row][col] + 1;
                queue.add(new int[]{row - 1, col});
            }
            if(col < rooms[0].length - 1 && rooms[row][col + 1] == Integer.MAX_VALUE ){
                rooms[row][col + 1] = rooms[row][col] + 1;
                queue.add(new int[]{row, col + 1});
            }
            if(col > 0 && rooms[row][col - 1] == Integer.MAX_VALUE ){
                rooms[row][col - 1] = rooms[row][col] + 1;
                queue.add(new int[]{row, col - 1});
            }
        }
        System.out.print("");
    }
}
