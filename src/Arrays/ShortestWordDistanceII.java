package Arrays;

/**
 * Created by rahilvora on 10/28/17.
 */
public class ShortestWordDistanceII {
    public static void main(String args[]){
        String[] words = {"a", "b", "c", "d", "d"};
        new ShortestWordDistanceII().shortestWordDistance(words, "a", "d");
    }
    public int shortestWordDistance(String[] words, String word1, String word2) {
        Boolean equalWords = word1.equals(word2);
        int pointer1 = -1, pointer2 = -1, minDistance = Integer.MAX_VALUE;
        for(int i = 0; i < words.length; i++){
            if(words[i].equals(word1)){
                if(equalWords){
                    pointer2 = pointer1;
                }
                pointer1 = i;
            }
            else if(words[i].equals(word2)){
                pointer2 = i;
            }
            if(pointer1 != -1 && pointer2 != -1){
                minDistance = Math.min(minDistance, Math.abs(pointer1 - pointer2));
            }
        }
        return minDistance;
    }
}
