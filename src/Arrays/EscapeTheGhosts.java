package Arrays;

/**
 * Created by rahilvora on 2/28/18.
 *
 Input:
 ghosts = [[1, 0]]
 target = [2, 0]
 Output: false
 Explanation:
 You need to reach the destination (2, 0), but the ghost at (1, 0) lies between you and the destination.
 */
public class EscapeTheGhosts {
    public boolean escapeGhosts(int[][] ghosts, int[] target) {
        int[] start = {0,0};
        for(int[] ghost: ghosts){
            if(taxiDis(ghost, target) <= taxiDis(start, target)){
                return false;
            }
        }
        return true;
    }

    public int taxiDis(int[] p , int[] q){
        return Math.abs(p[0] - q[0]) + Math.abs(p[1] - q[1]);
    }
}
