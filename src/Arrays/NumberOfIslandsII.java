package Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;

public class NumberOfIslandsII {
    public static void main(String args[]){

    }
    public List<Integer> numIslands2(int m, int n, int[][] positions) {
        List<Integer> answer = new ArrayList<>();
        int[][] matrix = new int[m][n];
        for(int i = 0; i < m; i++){
            Arrays.fill(matrix[i], 0);
        }
        NumberOfIslands obj = new NumberOfIslands();
        for(int i = 0; i < positions.length; i++){
            matrix[positions[i][0]][positions[i][1]] = 1;

        }
        return answer;

    }
}
