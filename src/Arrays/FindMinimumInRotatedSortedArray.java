package Arrays;

/**
 * Created by rahilvora on 1/5/18.
 */
public class FindMinimumInRotatedSortedArray {
    public int findMin(int[] nums){
        int start = 0, end = nums.length, ans = Integer.MAX_VALUE;
        while (start < end){
            int mid = (start + end) / 2;
            ans = Math.min(nums[mid], ans);
            if(nums[mid] > nums[end]){
                ans = Math.min(nums[start], ans);
                start = mid + 1;
            }
            else if(nums[mid] > nums[start]){
                ans = Math.min(nums[start], ans);
                end = mid - 1;
            }
            else start++;
        }
        return ans;
    }
}
