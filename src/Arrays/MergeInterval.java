package Arrays;

import Sorting.Interval;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by rahilvora on 8/23/17.
 */
public class MergeInterval {
    public List<Interval> merge(List<Interval> intervals) {
        if(intervals.size() <= 1) return intervals;

        Collections.sort(intervals, new Comparator<Interval>() {
            @Override
            public int compare(Interval o1, Interval o2) {
                return o1.start - o2.start;
            }
        });

        List<Interval> answer = new ArrayList<>();
        int start = intervals.get(0).start;
        int end = intervals.get(0).end;
        for(Interval interval: intervals){
            if(interval.start <= end){
                end = Math.max(end, interval.end);
            }
            else{
                answer.add(new Interval(start, end));
                start = interval.start;
                end = interval.end;
            }
        }
        answer.add(new Interval(start, end));
        return answer;
    }
}
