package Arrays;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rahilvora on 7/19/17.
 */
public class SummaryRange {
    public List<String> summaryRanges(int[] nums) {
        List<String> answer = new ArrayList<>();
        for(int i = 0, j = 0; i < nums.length; j++){
            if(j + 1 < nums.length && nums[j] == nums[j+1] - 1){
                continue;
            }
            if(i == j){
                answer.add(nums[i] + "");
            }
            else{
                answer.add(nums[i] + "->" + nums[j]);
            }
            i = j+1;
        }

        return answer;
    }
}
