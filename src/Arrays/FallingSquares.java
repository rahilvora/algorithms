package Arrays;

import java.util.ArrayList;
import java.util.List;

// TIme Complexity = O(N^2)
// Space Complexity = O(N)

public class FallingSquares {
    public static void main(String args[]){
        int[][] positions = {{1,2}, {2,3}, {6,1}};
        new FallingSquares().fallingSquares(positions);
    }
    public List<Integer> fallingSquares(int[][] positions) {
        List<Integer> answer = new ArrayList<>();
        int[] qans = new int[positions.length];

        for(int i = 0; i < positions.length; i++){
            int left = positions[i][0];
            int size = positions[i][1];
            int right = left + size;
            qans[i] += size;
            for(int j = i + 1; j < positions.length; j++){
                int left2 = positions[j][0];
                int size2 = positions[j][1];
                int right2 = left2 + size2;
                if(left2 < right && left < right2)
                    qans[j] = Math.max(qans[i], qans[j]);
            }
        }
        int currMax = -1;
        for(int curr: qans){
            currMax = Math.max(curr,currMax);
            answer.add(currMax);
        }
        return answer;
    }
}
