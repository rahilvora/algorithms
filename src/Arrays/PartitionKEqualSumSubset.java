package Arrays;
//Time complexity: O(2^n) since the solution could potentially explore every single subset of the input array.

public class PartitionKEqualSumSubset {

    public boolean canPartitionKSubsets (int[] nums, int k) {
        int sum = 0;
        for(int num:nums)sum += num;
        if(k <= 0 || sum%k != 0)return false;
        boolean[] visited = new boolean[nums.length];
        return backtrack(nums, visited, 0, k, 0, 0, sum/k);
    }

    public boolean backtrack (int[] nums, boolean[] visited,int start_index, int k, int currSum, int currNumber, int target) {
        if(k == 1) return true;
        if(currNumber > 0 && currSum == target) return backtrack(nums, visited, 0, k-1, 0, 0, target);
        for(int i = start_index; i < nums.length; i++){
            if(!visited[i]){
                visited[i] = true;
                if(backtrack(nums, visited, i+1, k, currSum + nums[i], currNumber++, target)) return true;
                visited[i] = false;
            }
        }
        return false;
    }
}
