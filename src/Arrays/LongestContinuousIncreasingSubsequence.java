package Arrays;

/**
 * Created by rahilvora on 9/14/17.
 */
public class LongestContinuousIncreasingSubsequence {
    public static void main(String args[]){
        int[] arr = {1, 3, 5, 4, 7};
        int[] arr1 = {1,1,1,1,1};
        System.out.print(new LongestContinuousIncreasingSubsequence().findLengthOfLCIS(arr));
    }
    public int findLengthOfLCIS(int[] nums){
        if(nums.length == 0) return 0;
        if(nums.length == 1) return 1;
        int i = 0, j = 1;
        int max = Integer.MIN_VALUE;
        int count = 1;
        while(j < nums.length){
            if(nums[i++] < nums[j++]){
                count++;
            }
            else{
                max = Math.max(max, count);
                count = 1;
            }
        }
        max = Math.max(max, count);
        return max;
    }
}
