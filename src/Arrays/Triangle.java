package Arrays;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Triangle {

    public static void main(String args[]){
        List<List<Integer>> triangle = new ArrayList<>();
        triangle.add(new ArrayList<>(Arrays.asList(1)));
        triangle.add(new ArrayList<>(Arrays.asList(2, 3)));
        new Triangle().minimumTotal(triangle);
    }
    public int minimumTotal(List<List<Integer>> triangle) {
        if(triangle.size() == 1) return triangle.get(0).get(0);
        for(int i = 1; i < triangle.size(); i++){
            List<Integer> previous = triangle.get(i-1);
            List<Integer> curr = triangle.get(i);
            for(int j = 0; j < curr.size(); j++){
                if(j == 0){
                    curr.set(j,curr.get(j) + previous.get(0));
                }
                else if(j == curr.size() - 1){
                    curr.set(j,curr.get(j) + previous.get(previous.size() - 1));
                }
                else{
                    int leftValue = curr.get(j) + previous.get(j-1);
                    int right = curr.get(j) + previous.get(j);
                    int val = Math.min(leftValue, right);
                    curr.set(j, val);
                }

            }
        }
        int answer = Integer.MAX_VALUE;
        List<Integer> lastList = triangle.get(triangle.size() - 1);
        for(int i = 0; i < lastList.size(); i++){
            answer = Math.min(lastList.get(i), answer);
        }
        return answer;
    }
}
