package Arrays;

/**
 * Created by rahilvora on 5/12/17.
 */
public class MaxConsecutiveOnes {
    public static void main(String args[]){
        int[] nums = {1,1,0,1,0,1};
        new MaxConsecutiveOnes().findMaxConsecutiveOnes(nums);
    }
    public int findMaxConsecutiveOnes(int[] nums) {
        int length = 0;
        int count = 0;
        for(int i = 0; i < nums.length; i++){
            if(nums[i] == 1){
                count++;
            }
            else{
                count = 0;
            }
            length = Math.max(count, length);
        }
        return length;
    }

}
