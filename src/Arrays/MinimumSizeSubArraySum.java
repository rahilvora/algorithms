package Arrays;

/**
 * Created by rahilvora on 7/11/17.
 */
public class MinimumSizeSubArraySum {
    public static void main(String args[]){
        int[] nums = {2,3,1,2,4,3};
        System.out.print(new MinimumSizeSubArraySum().minSubArrayLen(7, nums));
    }
    public int minSubArrayLen(int s, int[] nums) {
        if(nums.length == 1 && nums[0] == s) return nums[0];
        int size = Integer.MAX_VALUE;
        int i = 0, j = 0, total = 0;
        while(j < nums.length){
            total += nums[j++];
            while(total >= s){
                size = Math.min(size, j - i);
                total -= nums[i++];
            }
        }
        return (size == Integer.MAX_VALUE)?0:size;
    }
}
