package Arrays;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rahilvora on 9/18/17.
 */
public class    CombinationalSumIII {

    public List<List<Integer>> combinationSum3(int k, int n) {
        List<List<Integer>> answer = new ArrayList<>();
        backTrack(answer, new ArrayList<Integer>(), 1, 9, n, k);
        return answer;
    }
    public void backTrack(List<List<Integer>> list, List<Integer> temp, int start,int end,int target, int k){
        if(target < 0) return;
        else if(target == 0 && temp.size() == k) list.add(new ArrayList<>(temp));
        else {
            for(int i = start; i <= end; i++){
                temp.add(i);
                backTrack(list, temp, i+1, end, target-i, k);
                temp.remove(temp.size()-1);
            }
        }
    }

}
