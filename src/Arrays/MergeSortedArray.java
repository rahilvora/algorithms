package Arrays;

/**
 * Created by rahilvora on 5/12/17.
 * nums1 = [1,2,5,10,-,-,-], num2 = [6,3,9]
 * Steps
 * Start with the end of both the array in this case from 10 and 9 in arr 1 and 3 respectively
 * Whichever is greater put it in the end of arry1 and decrement the counter
 *
 */
public class MergeSortedArray {
    public static void main(String args[]){

    }
    public void merge(int[] nums1, int m, int[] nums2, int n) {
        int i = m-1, j = n-1, k = m + n -1;
        while(i > -1 && j > -1){
            nums1[k--] = (nums1[i] > nums2[j]) ? nums1[i--]:nums2[j--];
        }
        while(j>-1) nums1[k--] = nums2[j--];
    }
}
