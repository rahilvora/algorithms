package Arrays;

/**
 * Created by rahilvora on 8/15/17.
 * Total time complexity is O(m^2*n^2)). Space complexity is O(4mn).
 */
public class WordSearch {
    public static void main(String args[]){
        char[][] board = {
                            {'A', 'B', 'C', 'E'},
                            {'S', 'F', 'C', 'S'},
                            {'A', 'D', 'E', 'E'}
        };
        System.out.print(new WordSearch().exist(board, "ABCCED"));
    }
    public boolean exist(char[][] board, String word) {
        char[] words = word.toCharArray();
        for(int row = 0; row < board.length; row++){
            for(int col = 0; col < board[0].length; col++){
                if(helper(board, row, col, words, 0)) return true;
            }
        }
        return false;
    }
    public boolean helper(char[][] board, int row, int col, char[] words, int i){
        if(i == words.length) return true;
        if(row < 0 || col < 0 || row>= board.length || col >= board[0].length) return false;
        if(board[row][col] != words[i]) return false;
        board[row][col] ^= 256;
        boolean exist = (
                        helper(board, row - 1, col, words, i + 1) ||
                        helper(board, row + 1, col, words, i + 1) ||
                        helper(board, row, col + 1, words, i + 1) ||
                        helper(board, row, col - 1, words, i + 1)
        );
        board[row][col] ^= 256;
        return exist;
    }
}
