package Arrays;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rahilvora on 10/28/17.
 * Time Complexity: O(N*sqrt(N))
 */
public class FactorCombinations {
    public static void main(String args[]){
        System.out.print(new FactorCombinations().getFactors(12));
    }
    public List<List<Integer>> getFactors(int n){
        List<List<Integer>> result =  new ArrayList<>();
        helper(result, new ArrayList<Integer>(), n, 2);
        return result;
    }
    public void helper(List<List<Integer>> result, List<Integer> temp, int n, int start){
        if(n <= 1){
            if(temp.size() > 1){
                result.add(new ArrayList<>(temp));
            }
            return;
        }
        for(int i = start; i <= n; i++){
            if(n%i == 0){
                temp.add(i);
                helper(result, temp, n/i, i);
                temp.remove(temp.size() - 1);
            }
        }

    }
}
