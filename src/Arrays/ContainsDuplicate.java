package Arrays;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by rahilvora on 5/12/17.
 */
public class ContainsDuplicate {
    public static void main(String args[]){
        int[] nums = {1,2,3,4,1};
        new ContainsDuplicate().containsDuplicate(nums);
    }
    public boolean containsDuplicate(int[] nums) {
        Set<Integer> set = new HashSet<>();
        for(int num : nums){
            if(set.contains(num)){
                return true;
            }
            set.add(num);
        }
        return false;
    }
}
