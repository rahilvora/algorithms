package Arrays;

/**
 * Created by rahilvora on 5/12/17.
 */
public class ShortestWordDistance {
    public static void main(String args[]){

    }
    public int shortestDistance(String[] words, String word1, String word2) {
        int firstIndex = -1, secondIndex = -1, min = Integer.MAX_VALUE;
        for(int i = 0; i < words.length; i++){
            if(words[i].equals(word1)){
                firstIndex = i;
            }
            if(words[i].equals(word2)){
                secondIndex = i;
            }
            if(firstIndex != -1 && secondIndex != -1){
                min = Math.min(min, secondIndex - firstIndex);
            }
        }
        return min;
    }
}
