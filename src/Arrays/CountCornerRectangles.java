package Arrays;

//Time O(m^2*n^2)
public class CountCornerRectangles {
    public int countCornerRectangles(int[][] grid) {
        if(grid == null || grid.length == 0) return 0;
        int ans = 0;
        for(int i = 0; i < grid.length; i++){
            for(int j = 0; j < grid[0].length; j++){
                if(grid[i][j] == 0) continue;

                for(int p = i+1; p < grid.length; p++){
                    if(grid[p][j] == 0) continue;
                    for(int k = j+1; k < grid[0].length; k++){
                        ans += grid[i][k] & grid[p][k];
                    }
                }
            }
        }
        return ans;
    }

}
