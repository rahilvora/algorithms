package Arrays;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rahilvora on 8/23/17.
 */
public class PalindromePartitioning {
    public static void main(String args[]){
        new PalindromePartitioning().partition("aab");
    }
    public List<List<String>> partition(String s) {
        List<List<String>> answer = new ArrayList<>();
        backtrack(answer, new ArrayList<String>(), s, 0);
        return answer;
    }

    public void backtrack(List<List<String>> list, List<String> tempList, String s, int start){
        if(start == s.length())
            list.add(new ArrayList<>(tempList));
        else{
            for(int i = start; i < s.length(); i++){
                if(isPalindrome(s, start, i)){
                    tempList.add(s.substring(start, i + 1));
                    backtrack(list, tempList, s, i + 1);
                    tempList.remove(tempList.size() - 1);
                }
            }
        }
    }

    public boolean isPalindrome(String s, int low, int high){
        while(low < high)
            if(s.charAt(low++) != s.charAt(high--)) return false;
        return true;
    }
}
