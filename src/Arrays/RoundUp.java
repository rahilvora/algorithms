package Arrays;
import java.util.Arrays;
import java.util.Comparator;

public class RoundUp {
    public static void main(String args[]){
        double[] arr = {1.2, 3.7, 2.3, 4.8};
        int[] newArr = new RoundUp().roundUp(arr);
        System.out.println(newArr);
    }
    public int[] roundUp(double[] arr) {
        int[] res = new int[arr.length];
        NumWithDiff[] numWithDiffs = new NumWithDiff[arr.length];
        int floorSum = 0;
        double doubleSum = 0.0;
        for(int i = 0; i < arr.length; i++){
            double d = arr[i];
            doubleSum += d;
            floorSum += Math.floor(d);
            numWithDiffs[i] = new NumWithDiff(d - Math.floor(d), i);
        }

        int numTobeUP = (int) Math.round(doubleSum) - floorSum;

        Arrays.sort(numWithDiffs, new Comparator<NumWithDiff>() {
            @Override
            public int compare(NumWithDiff o1, NumWithDiff o2) {
                return Double.compare(o2.diff , o1.diff);
            }
        });
        System.out.println("to be up: " + numTobeUP);;

        for(int i=0; i<numTobeUP; i++){
            NumWithDiff p = numWithDiffs[i];
            res[p.idx] = (int) Math.ceil(arr[p.idx]);
        }
        for(int i=numTobeUP; i<arr.length; i++){
            NumWithDiff p = numWithDiffs[i];
            res[p.idx] = (int) Math.floor(arr[p.idx]);
        }
        return res;
    }

    class NumWithDiff {
        double diff;
        int idx;

        public NumWithDiff(double dif, int index){
            this.diff = dif;
            this.idx = index;
        }
    }
}
