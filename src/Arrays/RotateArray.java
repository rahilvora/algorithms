package Arrays;

/**
 * Created by rahilvora on 5/13/17.
 */
public class RotateArray {
    public static void main(String args[]){
        int[] nums = {1,2,3,4,5,6,7};
        new RotateArray().rotate(nums, 3);
    }
    public void rotate(int[] nums, int k) {
        int size = nums.length;
        k = k%size;
        reverse(nums, 0 , size-1);
        reverse(nums,0, k - 1);
        reverse(nums, k, size-1);
    }
    public void reverse(int[] nums, int start, int end){
        while(start<end){
            int temp = nums[start];
            nums[start++] = nums[end];
            nums[end--] = temp;
        }
    }
}
