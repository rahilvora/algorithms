package Arrays;

/**
 * Created by rahilvora on 8/28/17.
 */
public class TappingRainWater {
    public static void main(String args[]){
        int[] height = {0,1,0,2,1,0,1,3,2,1,2,1};
        new TappingRainWater().trap(height);
    }
    public int trap(int[] height) {
        int left = 0, right = height.length - 1, maxLeft = 0, maxRight = 0, result = 0;
        while(left <= right){
            if(height[left] <= height[right]){
                if(height[left] >= maxLeft) maxLeft = height[left];
                else result += maxLeft - height[left];
                left++;
            }
            else{
                if(height[right] >= maxRight) maxRight = height[right];
                else result += maxRight - height[right];
                right--;
            }
        }
        return result;
    }
}
