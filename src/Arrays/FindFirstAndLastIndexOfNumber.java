package Arrays;

public class FindFirstAndLastIndexOfNumber {

    public static void main(String args[]){
        int[] nums = {5, 7, 7, 8, 8, 10};
        int[] ans = new FindFirstAndLastIndexOfNumber().binarySearch(nums, 6);
        System.out.println(ans[0] + " " + ans[1]);
    }

    public int findIndexHelper(int[] nums, int target, boolean left){
        int low = 0, high = nums.length;
        while(low < high){
            int mid = (low + high) / 2;
            if(target < nums[mid] || (nums[mid] == target && left)){
                high = mid;
            }
            else {
                low = mid + 1;
            }
        }
        return low;
    }
    public int[] binarySearch(int[] nums, int target){
        int[] answer = {-1, -1};
        if(nums == null || nums.length == 0) return answer;
        int leftIdx = findIndexHelper(nums, target, true);
        if(leftIdx == nums.length || nums[leftIdx] != target) return answer;
        answer[0] = leftIdx;
        int rightIdx = findIndexHelper(nums, target, false) - 1;
        answer[1] = rightIdx;

        return answer;

    }
}
