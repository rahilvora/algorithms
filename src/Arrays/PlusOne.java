package Arrays;

/**
 * Created by rahilvora on 5/11/17.
 */
public class PlusOne {
    public static void main(String args[]){
        int[] digits = {9,9,9};
        new PlusOne().plusOne(digits);
    }
    public int[] plusOne(int[] digits) {
        int carry = 1, len = digits.length;
        for(int i = len - 1; i>=0; i--){
            int updatedValue = (digits[i] + carry);
            digits[i] =  updatedValue % 10;
            carry = updatedValue / 10;
        }
        if(carry>0){
            int[] newArr = new int[len + 1];
            newArr[0] = carry;
            for(int i = 1; i < newArr.length; i++){
                newArr[i] = digits[i-1];
            }
            return newArr;
        }
        return digits;
    }
}
