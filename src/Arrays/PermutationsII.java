package Arrays;

import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;

/**
 * Created by rahilvora on 9/18/17.
 */
public class PermutationsII {
    public static void main(String args[]){
        int[] nums = {1,1,2};
        new PermutationsII().permuteII(nums);
    }
    public List<List<Integer>> permuteII(int[] nums) {
        List<List<Integer>> answer = new ArrayList<>();
        Arrays.sort(nums);
        backTrack(answer, new ArrayList<Integer>(),nums, new boolean[nums.length]);
        return answer;
    }
    public void backTrack(List<List<Integer>> list, List<Integer> temp, int[] nums, boolean[] used){
        if(temp.size() == nums.length){
            list.add(new ArrayList<Integer>(temp));
        }
        else{
            for(int i = 0; i < nums.length; i++){
                if(used[i] || i > 0 && nums[i] == nums[i-1] && !used[i - 1]) continue;
                used[i] = true;
                temp.add(nums[i]);
                backTrack(list, temp, nums, used);
                used[i] = false;
                temp.remove(temp.size() - 1);
            }
        }
    }
}
