package Arrays;

import java.util.HashMap;

/**
 * Created by rahilvora on 5/12/17.
 */
public class TwoSum {
    public static void main(String args[]){
        int[] nums = {1,2,1,3};
        new TwoSum().twoSum(nums, 5);
    }
    public int[] twoSum(int[] nums, int target) {
        HashMap<Integer, Integer> map = new HashMap<>();
        for(int i = 0; i < nums.length; i++){
            int remainder = target - nums[i];
            if(map.containsKey(remainder)){
                return new int[]{map.get(remainder), i};
            }
            else{
                map.put(nums[i], i);
            }
        }
        return new int[2];
    }
}
