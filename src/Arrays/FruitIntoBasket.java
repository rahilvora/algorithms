package Arrays;

import java.util.HashMap;
import java.util.Map;

//time complexity O(N)

public class FruitIntoBasket {
    public static void main(String args[]){
        int[] arr = {3,3,3,1,2,1,1,2,3,3,4};
        new FruitIntoBasket().totalFruit(arr);
    }
    public int totalFruit(int[] tree) {
        Map<Integer, Integer> map = new HashMap<>();
        int answer = 0, j = 0;
        for(int i = 0; i < tree.length; i++){
            map.put(tree[i], map.getOrDefault(tree[i], 0) + 1);
            while(map.size() >= 3){
                map.put(tree[j], map.get(tree[j]) - 1);
                if(map.get(tree[j]) == 0){
                    map.remove(tree[j]);
                }
                j++;
            }
            answer = Math.max(answer, i - j + 1);
        }
        return answer;
    }
}
