package Arrays;

/**
 * Created by rahilvora on 5/10/17.
 */
public class RemoveElement {
    public static void main(String args[]){
        int[] nums = {3, 2, 2, 3};
        System.out.print(new RemoveElement().removeElement(nums, 3));
    }
    public int removeElement(int[] nums, int val) {
        int begin = 0;
        for(int i = 0; i < nums.length; i++){
            if(nums[i] != val){
                nums[begin++] = nums[i];
            }
        }
        return begin;
    }
}
