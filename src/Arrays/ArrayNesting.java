package Arrays;

import java.util.HashSet;
import java.util.Set;

public class ArrayNesting {
    public static void main(String args[]){
        int[] nums = {5,4,0,3,1,6,2};
        System.out.println(new ArrayNesting().arrayNestingII(nums));
    }
    //O(n^2)
    public int arrayNesting(int[] nums) {
        int count = 0;
        Set<Integer> numbers;
        for(int i = 0; i < nums.length; i++){
            int startNumber = nums[i];
            numbers = new HashSet<>();
            int tempCount = 0;
            while(!numbers.contains(startNumber)){
                tempCount++;
                numbers.add(startNumber);
                startNumber = nums[startNumber];
            }
            count = Math.max(count, tempCount);
        }
        return count;
    }

    //O(n)
    public int arrayNestingII(int[] nums){
        int res = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] != Integer.MAX_VALUE) {
                int start = nums[i], count = 0;
                while (nums[start] != Integer.MAX_VALUE) {
                    int temp = start;
                    start = nums[start];
                    count++;
                    nums[temp] = Integer.MAX_VALUE;
                }
                res = Math.max(res, count);
            }
        }
        return res;
    }
}
