package Arrays;

/**
 * Created by rahilvora on 2/20/18.
 * Given:
 R number of Red Cards
 B number of Black cards
 K

 Cards needs to be placed in a circle.
 Start from a position and for every K moves remove that card
 And repeat the process until all the cards are eliminated.

 Question: Position the cards such that the red cards are completely
 eliminated before the blacks cards are selected for elimination.
 */
public class SetCards {
    public static void setCards(int blackCards, int redCards, int k){
        int index = -1;
        int[] array = new int[blackCards + redCards];
        for(int i = 0; i < redCards; i++){
            for(int j = 0; j < k;){
                index++;
                if(index == array.length){
                    index = 0;
                }
                if(array[index] != 1){
                    j++;
                }
            }
            array[index] = 1;
        }

    }
}
