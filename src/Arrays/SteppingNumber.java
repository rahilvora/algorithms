package Arrays;

import java.util.ArrayList;

/**
 * Created by rahilvora on 8/20/17.
 */
public class SteppingNumber {

    public static void main(String args[]){
        new SteppingNumber().stepnum(10,20);
    }
    public ArrayList<Integer> stepnum(int a, int b) {
        ArrayList<Integer> answer = new ArrayList<>();
        for(int i = a; i <=b; i++){
            if(isStepNum(i)){
                answer.add(i);
            }
        }
        return answer;
    }

    public boolean isStepNum(int num){
        if(num <=10 && num > 0){
            return true;
        }
        boolean res = false;
        while(num >10){
            int firstNum = num%10;
            num = num/10;
            int secondNum = num%10;
            if(Math.abs(firstNum - secondNum)  == 1) {
                res = true;
            }
            else {
                res = false;
                break;
            }

        }
        return res;
    }
}
