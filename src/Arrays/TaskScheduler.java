package Arrays;
import java.util.Arrays;
/**
 * Created by rahilvora on 9/1/17.
 * Time O(N) where N is number of tasks. We do sorting but its 26log26 which is constant
 * Space: O(1)
 */
public class TaskScheduler {
    public static void main(String args[]){
        char[] tasks =  {'A', 'A', 'A', 'B', 'B', 'B', 'B', 'A', 'C', 'C', 'C', 'D'};
        System.out.print(new TaskScheduler().leastInterval(tasks, 2));
    }
    public int leastInterval(char[] tasks, int n) {
        int[] map = new int[26];
        for(char t : tasks){
            map[t - 'A']++;
        }
        Arrays.sort(map);
        int max_value = map[25] - 1, idle_slot = max_value * n;
        for(int i = 24; i >= 0; i--){
            idle_slot -= Math.min(max_value, map[i]);
        }
        return idle_slot > 0 ? idle_slot + tasks.length: tasks.length;
    }
}
