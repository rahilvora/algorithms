package Arrays;

/**
 * Created by rahilvora on 9/2/17.
 * Time O(N)
 *
 */
class Relation{
    static boolean knows(int i , int j){
        return true;
    }
}
public class FindCelebrity extends Relation{
    public int findCelebrity(int n) {
        int candidate = 0;
        for(int i = 0; i < n; i++){
            if(knows(candidate, i)){
                candidate = i;
            }
        }
        for(int i = 0; i < n; i++){
            if(i != candidate &&(knows(candidate, i) || !knows(i,candidate)))return -1;
        }
        return candidate;

    }
}
