package Arrays;

/**
 * Created by rahilvora on 9/23/17.
 */
public class TraverseMatrixInDiagonalOrder {
    private static int row, col;

    public static void main(String args[]){
        int[][] arr = {
                {1, 2, 3, 4},
                {5, 6, 7, 8},
                { 9, 10, 11, 12},
                {13, 14, 15, 16},
                {17, 18, 19, 20}
        };
        row = arr.length;
        col = arr[0].length;
        new TraverseMatrixInDiagonalOrder().findDiagonalOrder(arr);
    }
    private void diagonalOrder(int[][] matrix){
        for(int k = 0; k < row; k++){
            System.out.print(matrix[k][0] + " ");
            int i = k-1;
            int j = 1;
            while(isValid(i, j)){
                System.out.print(matrix[i][j] + " ");
                i--;
                j++;
            }
            System.out.println("");
        }

        for(int k = 1; k < col; k++){
            System.out.print(matrix[row - 1][k] + " ");
            int i = row - 2;
            int j = k + 1;
            while(isValid(i,j)){
                System.out.print(matrix[i][j] + " ");
                i--;
                j++;
            }
            System.out.println("");
        }
    }
    private boolean isValid(int i, int j){
        if(i < 0 || i>=row || j<0|| j>=col)return false;
        return true;
    }
    //Leetcode Diagonal Traversal
    public int[] findDiagonalOrder(int[][] matrix) {
        if(matrix == null || matrix.length == 0) return new int[0];
        int m = matrix.length, n = matrix[0].length;
        int[] result = new int[m*n];
        int row = 0, col = 0, d = 0;
        int[][] dirs = {{-1, 1}, {1, -1}};

        for(int i = 0; i < m*n; i++) {
            result[i] = matrix[row][col];
            row += dirs[d][0];
            col += dirs[d][1];

            if(row>=m) {
                row = m-1;
                col += 2;
                d = 1 - d;
            }
            if(col >= n) {
                row += 2;
                col = n - 1;
                d = 1 - d;
            }
            if (row < 0)  { row = 0; d = 1 - d;}
            if (col < 0)  { col = 0; d = 1 - d;}
        }
        return result;
    }

}
