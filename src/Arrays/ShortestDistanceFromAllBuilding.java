package Arrays;

import java.util.LinkedList;
import java.util.Queue;

public class ShortestDistanceFromAllBuilding {
    public int shortestDistance(int[][] grid) {
        if(grid == null || grid.length == 0) return -1;
        int[] shift = {0,1,0,-1,0};
        int m = grid.length, n = grid[0].length;
        int buildingNum = 0;
        int[][] distance = new int[m][n];
        int[][] reach = new int[m][n];

        for(int i = 0; i < m; i++){
            for(int j = 0; j < n; j++){
                if(grid[i][j] == 1){
                    buildingNum++;
                    Queue<int[]> queue = new LinkedList<>();
                    queue.offer(new int[] {i,j});

                    boolean[][] visited = new boolean[m][n];
                    int level = 1;

                    while(!queue.isEmpty()){
                        int qsize = queue.size();
                        for(int q = 0; q < qsize; q++){
                            int[] curr = queue.poll();

                            for(int k = 0; k < 4; k++){
                                int nextRow = curr[0] + shift[k];
                                int nextCol = curr[1] + shift[k+1];
                                if(nextRow >=0
                                    && nextRow < m
                                    && nextCol >=0 && nextCol < n
                                    && grid[nextRow][nextCol] == 0
                                    && !visited[nextRow][nextCol]){
                                    distance[nextRow][nextCol] += level;
                                    reach[nextRow][nextCol]++;

                                    visited[nextRow][nextCol] = true;
                                    queue.offer(new int[]{nextRow, nextCol});
                                }
                            }
                        }
                        level++;
                    }
                }
            }
        }
        int shortest = Integer.MAX_VALUE;
        for(int i = 0; i < m; i++){
            for(int j = 0; j < n; j++){
                if(grid[i][j] == 0 && reach[i][j] == buildingNum){
                    shortest = Math.min(shortest, distance[i][j]);
                }
            }
        }
        return shortest == Integer.MAX_VALUE ? -1: shortest;
    }
}
