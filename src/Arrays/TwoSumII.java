package Arrays;

/**
 * Created by rahilvora on 5/10/17.
 */
public class TwoSumII {
    public static void main(String args[]){
        int[] nums = {0, 0, 3, 4};
        new TwoSumII().twoSum(nums, 0);
    }
    public int[] twoSum(int[] numbers, int target) {
        int[] answer = new int[2];
        int low = 0, high = numbers.length - 1;
        while(low < high){
            int value = numbers[low] + numbers[high];
            if(value == target){
                answer[0] = low + 1;
                answer[1] = high + 1;
                break;
            }
            else if(target > value){
                low++;
            }
            else if(target < value){

                high--;
            }
        }
        return answer;
    }
}
