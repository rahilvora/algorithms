package Arrays;

import java.util.*;

public class Test {
    public static void main(String args[]){
//        List<List<Integer>> allLocations = new ArrayList<>();
//        List<Integer> p1 = new ArrayList<>();
//        List<Integer> p2 = new ArrayList<>();
//        List<Integer> p3 = new ArrayList<>();
//
//        p1.add(1);
//        p1.add(-3);
//
//        p2.add(1);
//        p2.add(2);
//
//        p3.add(3);
//        p3.add(4);
//
//        allLocations.add(p1);
//        allLocations.add(p2);
//        allLocations.add(p3);
//

        List<List<Integer>> foregroundAppList = new ArrayList<>();
        List<List<Integer>> backgroundAppList = new ArrayList<>();

        List<Integer> f1 = new ArrayList<>();
        List<Integer> f2 = new ArrayList<>();
        List<Integer> f3 = new ArrayList<>();

        List<Integer> b1 = new ArrayList<>();
        List<Integer> b2 = new ArrayList<>();
        List<Integer> b3 = new ArrayList<>();

        f1.add(1);
        f1.add(8);

        f2.add(2);
        f2.add(7);

        f3.add(3);
        f3.add(14);
        foregroundAppList.add(f1);
        foregroundAppList.add(f2);
        foregroundAppList.add(f3);

        b1.add(1);
        b1.add(5);

        b2.add(2);
        b2.add(10);

        b3.add(3);
        b3.add(14);

        backgroundAppList.add(b1);
        backgroundAppList.add(b2);
        backgroundAppList.add(b3);
        Test obj = new Test();
        System.out.println(obj.optimalUtilization(20, foregroundAppList, backgroundAppList).size());
    }

    public List<List<Integer>> nearestVegetarianRestaurant(int totalRestaurants,
                                     List<List<Integer>> allLocations,
                                     int numRestaurants) {
        Map<Double, List<List<Integer>>> map = new TreeMap<>();

        for(int i = 0; i < allLocations.size(); i++){
            double dis = Math.hypot((0-allLocations.get(i).get(0)), (0-allLocations.get(i).get(1)));

            if(!map.containsKey(dis)){
                map.put(dis, new ArrayList<>());
            }
            map.get(dis).add(allLocations.get(i));
        }

        List<List<Integer>> answer = new ArrayList<>();
        for(Double key: map.keySet()){
            for(List<Integer> list: map.get(key)){
                answer.add(list);
                if(answer.size()>=numRestaurants) return answer;
            }
        }
        return answer;
    }

    List<List<Integer>> optimalUtilization(int deviceCapacity, List<List<Integer>> foregroundAppList, List<List<Integer>> backgroundAppList) {
        List<List<Integer>> answer = new ArrayList<>();
        int maxUtilization = Integer.MIN_VALUE;
        for(int i = 0; i < foregroundAppList.size(); i++){
            for(int j = 0; j < backgroundAppList.size(); j++){
                List<Integer> list = new ArrayList<>();
                int currentCapacity = foregroundAppList.get(i).get(1) + backgroundAppList.get(j).get(1);
                if(currentCapacity >= maxUtilization && currentCapacity <= deviceCapacity){
                    if(currentCapacity > maxUtilization){
                        answer.clear();
                    }
                    maxUtilization = currentCapacity;
                    list.add(foregroundAppList.get(i).get(0));
                    list.add(backgroundAppList.get(j).get(0));
                    answer.add(list);
                }
            }
        }
        return answer;
    }
}
