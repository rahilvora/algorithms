package Arrays;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rahilvora on 5/8/17.
 *
 * Given numRows, generate the first numRows of Pascal's triangle.

 For example, given numRows = 5,
 Return

 [
 [1],
 [1,1],
 [1,2,1],
 [1,3,3,1],
 [1,4,6,4,1]
 ]
 */
public class PascalTriangle {
    public static void main(String args[]){
        new PascalTriangle().generate(5);
    }
    public List<List<Integer>> generate(int numRows) {
        List<List<Integer>> triangle = new ArrayList<>();
        if(numRows <= 0){
            return triangle;
        }
        for(int i = 0; i < numRows; i++){
            List<Integer> list = new ArrayList<>();
            for(int j = 0; j <= i; j++){
                if(j== 0 || j == i){
                    list.add(1);
                }
                else {
                    list.add(triangle.get(i-1).get(j) + triangle.get(i-1).get(j-1));
                }
            }
            triangle.add(list);
        }
        return triangle;
    }
}