package Arrays;

/**
 * Created by rahilvora on 5/11/17.
 */
public class SearchInsertPosition {
    public static void main(String args[]){
        int[] nums = {1,3,5,6};
        new SearchInsertPosition().searchInsert(nums, 0);
    }
    public int searchInsert(int[] nums, int target) {
        int index = nums.length;
        for(int i = 0; i < nums.length; i++){
            if(nums[i] == target) return i;
            if(nums[i] > target) return i;
        }
        return index;
    }
}
