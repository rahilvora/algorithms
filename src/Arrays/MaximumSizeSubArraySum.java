package Arrays;

/**
 * Created by rahilvora on 7/12/17.
 */
public class MaximumSizeSubArraySum {
    public static void main(String args[]){
        int[] nums = {1,-1,5,-2,3};
        System.out.print(new MaximumSizeSubArraySum().maxSubArrayLen(nums, 3));
    }
    public int maxSubArrayLen(int[] nums, int s) {
        if(nums.length == 1 && nums[0] == s) return nums[0];
        int size = Integer.MIN_VALUE;
        int i = 0, j = 0, total = 0;
        while(j < nums.length){
            total += nums[j++];
            if(total == s){
                size = Math.max(size, j - i);
                total -= nums[i++];
            }
        }
        return (size == Integer.MIN_VALUE)?0:size;
    }
}
