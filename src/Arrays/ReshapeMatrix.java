package Arrays;

/**
 * Created by rahilvora on 5/8/17.
 */
public class ReshapeMatrix {
    public static void main(String args[]){
        int[][] nums = {{1,2},{3,4}};
        new ReshapeMatrix().matrixReshape(nums, 4, 1);
    }
    public int[][] matrixReshape(int[][] nums, int r, int c) {
        int[][] reshapedMatrix = new int[r][c];
        int rows = nums.length, cols = nums[0].length;
        if(rows*cols == r*c){
            int rowUpdated = 0, colUpdated = 0;
            for(int i = 0; i < rows; i++){
                for(int j = 0; j < cols; j++){
                    if(rowUpdated < r && colUpdated < c){
                        reshapedMatrix[rowUpdated][colUpdated++] = nums[i][j];
                    }
                    else if(colUpdated == c){
                        colUpdated = 0;
                        reshapedMatrix[++rowUpdated][colUpdated++] = nums[i][j];
                    }
                }
            }
            return reshapedMatrix;
        }
        return nums;
    }
}
