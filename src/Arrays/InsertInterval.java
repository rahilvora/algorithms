package Arrays;

import Sorting.Interval;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rahilvora on 8/23/17.
 * TIme O(N)
 */
public class InsertInterval {
    public List<Interval> insert(List<Interval> intervals, Interval newInterval) {
        List<Interval> answer = new ArrayList<>();
        int i = 0;
        while( i < intervals.size() && intervals.get(i).end < newInterval.start){
            answer.add(intervals.get(i++));
        }
        while (i < intervals.size() && intervals.get(i).start <= newInterval.end){
            newInterval = new Interval(Math.min(intervals.get(i).start, newInterval.start),
                                        Math.max(intervals.get(i).end, newInterval.end));
            i++;
        }
        answer.add(newInterval);
        while(i < intervals.size()) answer.add(intervals.get(i++));
        return answer;
    }
}
