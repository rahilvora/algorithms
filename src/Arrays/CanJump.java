package Arrays;

/**
 * Created by rahilvora on 12/9/17.
 */
public class CanJump {
    public static void main(String args[]){
        int[] nums = {2,3,1,1,4};
        new CanJump().canJump(nums);
    }
    public boolean canJump(int[] nums) {
        int lastPosition = nums.length - 1;
        for(int i = nums.length - 1; i>=0; i--){
            if(i + nums[i] >= lastPosition){
                lastPosition = i;
            }
        }
        return lastPosition == 0;
    }
}
