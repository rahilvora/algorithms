package Arrays;

import java.util.*;
//Time o(V + E) V is number of nodes in this case 6 and E is edge in this case 4 direction for 4
public class SlidingPuzzle {
    public static void main(String args[]){
        int[][] matrix = {
                {3, 1, 4},
                {6, 2, 0},
                {7, 5, 8}
        };

        System.out.println(new SlidingPuzzle().slidingPuzzleII(matrix));
    }
    public int slidingPuzzle(int[][] board) {
        if(board == null || board.length == 0) return -1;
        Set<String> visited = new HashSet<>();
        String target = "123450";
        String str = Arrays.deepToString(board).replaceAll("\\[|\\]|,|\\s","");
        Queue<String> queue = new LinkedList<>(Arrays.asList(str));
        visited.add(str);
        int answer = 0;
        while(!queue.isEmpty()){
            for(int i = queue.size() ; i > 0; i--){
                String currStr = queue.poll();
                if(currStr.equals(target)) return answer;
                int indexOfZero = currStr.indexOf("0");
                int[] d = {1,-1,-3,3};
                for(int k = 0; k < d.length; k++){
                    int j = indexOfZero + d[k];

                    if(j < 0 || j > 5 || indexOfZero == 2 && j == 3 || indexOfZero == 3 && j == 2) continue;

                    char[] ch = currStr.toCharArray();
                    char temp = ch[indexOfZero];
                    ch[indexOfZero] = ch[j];
                    ch[j] = temp;
                    str = String.valueOf(ch);
                    if(visited.add(str)) queue.offer(str);
                }
            }
            answer ++;
        }
        return -1;
    }

    // 3*3 matrix
    public boolean slidingPuzzleII(int[][] board) {
        String target = "123456780";
        StringBuffer sb = new StringBuffer();
        for(int i = 0; i < board.length; i++){
            for(int j = 0; j < board[0].length; j++){
                sb.append(board[i][j]);
            }
        }
        Queue<String> queue = new LinkedList<>();
        Set<String> visited = new HashSet<>();
        visited.add(sb.toString());
        queue.offer(sb.toString());
        while(!queue.isEmpty()){
            String str = queue.poll();
            if(str.equals(target)) return true;

            int index = str.indexOf("0");
            int[] k = {-1,1,3,-3};

            for(int j = 0; j < k.length; j++){
                int idx = index + k[j];

                if(idx < 0 || idx > 8 || idx == 2 && index == 3 || idx == 5 && index == 7 || index == 2 && idx == 3 || index == 5 && idx == 7) continue;

                char[] ch = str.toCharArray();

                char temp = ch[index];
                ch[index] = ch[idx];
                ch[idx] = temp;

                sb = new StringBuffer(String.valueOf(ch));

                if(visited.add(sb.toString())) queue.offer(sb.toString());
            }
        }
        return false;
    }
}
/*
* 1 2 3
* 4 5 6
* 7 8 -
*
* 1 2 3 4 5 6 7 8 -
*
*
* */