package Arrays;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by rahilvora on 8/23/17.
 * Time = O(2^n)
 *
 */
public class Subset {
    public static void main(String args[]){
        int[] subset = {1,2,3};
        new Subset().subsets(subset);
    }
    public List<List<Integer>> subsets(int[] nums) {
        List<List<Integer>> answer = new ArrayList<>();
        Arrays.sort(nums);
        backtrack(answer, new ArrayList<>(), nums, 0);
        return answer;
    }
    public void backtrack(List<List<Integer>> answer, List<Integer> tempList, int[] nums, int start){
        answer.add(new ArrayList<>(tempList));
        for(int i = start; i < nums.length; i++){
            tempList.add(nums[i]);
            backtrack(answer, tempList, nums, i+1);
            tempList.remove(tempList.size() - 1);
        }
    }



}
