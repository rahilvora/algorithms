package Arrays;

/**
 * Created by rahilvora on 8/23/17.
 *
 * Time: O(2n)
 */
public class SortColors {
    public void sortColors(int[] nums) {
        int zero = 0, second = nums.length - 1;
        for(int i = 0; i < nums.length; i++){
            while(nums[i] == 2 && i < second) {
                swap(nums,i,second);
                second--;
            }
            while(nums[i] == 0 && i > zero) {
                swap(nums,i, zero);
                zero++;
            }
        }
    }
    public void swap(int[] nums, int index1, int index2){
        int temp = nums[index1];
        nums[index1] = nums[index2];
        nums[index2] = temp;
    }
}
