package Arrays;

/**
 * Created by rahilvora on 10/5/17.
 * Time and space O(m*n)
 */
public class LongestIncreasingPath {
    public static void main(String args[]){
        int[][] arr = {{9,9,4},{6,6,8},{2,1,1}};
        System.out.print(new LongestIncreasingPath().longestIncreasingPath(arr));
    }
    private int[][] dirs = {{0, 1}, {1, 0}, {0, -1}, {-1, 0}};
    public int longestIncreasingPath(int[][] matrix){
        int max = 0;
        int[][] cache = new int[matrix.length][matrix[0].length];
        int m = matrix.length, n = matrix[0].length;
        for(int i = 0; i < m; i++){
            for(int j = 0; j < n; j++){
                max = Math.max(max, dfs(matrix, i, j, m, n, cache));
            }
        }
        return max;
    }
    private int dfs(int[][] matrix, int i, int j, int m, int n, int[][] cache){
        if(cache[i][j] != 0){
            return cache[i][j];
        }
        for(int[] dir: dirs){
            int x = i+ dir[0];
            int y = j + dir[1];
            if(x >= 0 && x < m && y >= 0 && y < n && matrix[x][y] > matrix[i][j]){
                cache[i][j] = Math.max(cache[i][j], dfs(matrix, x, y, m, n, cache));
            }
        }
        return ++cache[i][j];
    }
}
