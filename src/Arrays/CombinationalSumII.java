package Arrays;

import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;

/**
 * Created by rahilvora on 8/30/17.
 *  Time complexity O(#candidates^(target/min of candidates))
 */
public class CombinationalSumII {
    public static void main(String args[]){
        CombinationalSumII obj = new CombinationalSumII();
        double[] prices = {10.02, 1.11, 2.22, 3.01, 4.02, 2.00, 5.03};
        obj.combinationSum2Double(prices, 7.03);
    }

    public List<List<Integer>> combinationSum2(int[] candidates, int target){
        List<List<Integer>> answer = new ArrayList<>();
        Arrays.sort(candidates);
        backTrack(answer, new ArrayList<>(), candidates, target, 0);
        return answer;
    }

    public void backTrack(List<List<Integer>> answer, List<Integer> temp, int[] candidates, int remain, int start){
        if(remain < 0) return;
        else if(remain == 0) answer.add(new ArrayList<>(temp));
        else{
            for(int i = start; i < candidates.length; i++){
                if(i > start && candidates[i] == candidates[i-1]) continue;
                temp.add(candidates[i]);
                backTrack(answer, temp, candidates, remain - candidates[i], i+1);
                temp.remove(temp.size() - 1);
            }
        }
    }

    public List<List<Double>> combinationSum2Double(double[] menuItems, double target){
        List<List<Double>> answer = new ArrayList<>();
        Arrays.sort(menuItems);
        backTrackDouble(answer, new ArrayList<>(), menuItems, target, 0);
        return answer;
    }

    public void backTrackDouble(List<List<Double>> answer, List<Double> temp, double[] menuItems, double remain, int start){
        if(remain < 0) return;
        else if(remain == 0) answer.add(new ArrayList<>(temp));
        else{
            for(int i = start; i < menuItems.length; i++){
                if(i == 0 || i > 0 && menuItems[i] != menuItems[i-1]){
                    temp.add(menuItems[i]);
                    backTrackDouble(answer, temp, menuItems, remain - menuItems[i], i + 1);
                    temp.remove(temp.size() - 1);
                }
            }
        }
    }
}
