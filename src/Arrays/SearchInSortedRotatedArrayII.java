package Arrays;

/**
 * Created by rahilvora on 1/5/18.
 */
public class SearchInSortedRotatedArrayII {
    public boolean search(int[] nums, int target) {
        int start = 0, end = nums.length - 1;
        while(start <= end){
            int mid = (start + end) / 2;
            if(nums[mid] == target) return true;
            // if left part is unsorted and right part is sorted
            if(nums[mid] < nums[end] || nums[mid] < nums[start]){
                if(target > nums[mid] && target <= nums[end]){
                    start = mid + 1;
                }
                else{
                    end = mid - 1;
                }
            }
            // if left part is sorted and right part is unsorted
            else if(nums[mid] > nums[start] || nums[mid] > nums[end]){
                if( target < nums[mid]  && target >= nums[start]){
                    end = mid - 1;
                }
                else{
                    start = mid + 1;
                }
            }
            else{
                start++;
            }

        }
        return false;
    }

}
