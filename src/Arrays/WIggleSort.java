package Arrays;

import java.util.Arrays;

public class WIggleSort {
    public static void main(String args[]){
        int[] nums = {1, 5, 1, 1, 6, 4};
        new WIggleSort().wiggleSort(nums);
        for(int num: nums){
            System.out.print(num + "\t");
        }
    }
    public void wiggleSort(int[] nums) {
        Arrays.sort(nums);
        int n = nums.length, mid = n%2 == 0 ? n/2-1:n/2;
        int[] temp = Arrays.copyOf(nums, n);
        int index = 0;
        for(int i = 0; i <=mid; i++){
            nums[index] = temp[mid - 1];
            if(index + 1 < n){
                nums[index+1] = temp[n - i -1];
            }
            index += 2;
        }

    }
}
