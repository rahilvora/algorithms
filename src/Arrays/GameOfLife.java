package Arrays;

/**
 * Created by rahilvora on 10/2/17.
 */
public class GameOfLife {
    public static void main(String args[]){

    }
    public void gameOfLife(int[][] board) {
        if(board == null || board.length == 0) return;
        int rows = board.length, cols = board[0].length;
        for(int i = 0; i < rows; i++){
            for(int j = 0; j < cols; j++){
                int lives = getLives(board, i, j);
                if(board[i][j] == 1 && lives >= 2 && lives <= 3 ){
                    board[i][j] = 3;
                }
                if(board[i][j] == 0 && lives == 3){
                    board[i][j] = 2;
                }
            }
        }
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                board[i][j] >>= 1;  // Get the 2nd state.
            }
        }
    }
    public int getLives(int[][] board, int row, int col){
        int lives = 0;
        for(int x = row-1; x <= row+1; x++){
            for(int y = col-1; y <= col+1; y++){
                lives += board[x][y]&1;
            }
        }
        //remove live of self
        lives -= board[row][col]&1;
        return lives;
    }
}
