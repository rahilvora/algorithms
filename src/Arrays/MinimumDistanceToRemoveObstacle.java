package Arrays;

public class MinimumDistanceToRemoveObstacle {
    static int minimumDistance = 0;
    public static void main(String args[]){
        int[][] lot = {
                {1, 0, 0},
                {1, 1, 0},
                {1, 9, 1}
        };
        System.out.print(new MinimumDistanceToRemoveObstacle().removeObstacle(lot.length, lot[0].length, lot));
    }
    public int removeObstacle(int numRows, int numCols, int[][] obstacleGrid){
        if(obstacleGrid[0][0] == 9){
            return 1;
        }
//        obstacleGrid[obstacleGrid.length-1][obstacleGrid[0].length-1] = -1;
//        for(int i = obstacleGrid.length-1;i>=0;i--){
//            for(int j = obstacleGrid[0].length-1;j>=0;j--){
//                if(obstacleGrid[i][j]!=9){
//                    if(i+1<obstacleGrid.length && obstacleGrid[i+1][j]!=9){
//                        obstacleGrid[i][j] += obstacleGrid[i+1][j];
//                    }
//                    if(j+1<obstacleGrid[0].length && obstacleGrid[i][j+1]!=9){
//                        obstacleGrid[i][j] += obstacleGrid[i][j+1];
//                    }
//                }
//            }
//        }
        int answer = Integer.MAX_VALUE;
        for(int i = 0; i < obstacleGrid.length; i++){
            for(int j = 0; j < obstacleGrid.length; j++){
                if(obstacleGrid[i][j] >= 1 && obstacleGrid[i][j] != 9){
                    if(i+1<obstacleGrid.length && obstacleGrid[i+1][j]!=9 && obstacleGrid[i+1][j]==1){
                        //obstacleGrid[i][j] += obstacleGrid[i+1][j];
                        obstacleGrid[i+1][j] += obstacleGrid[i][j];
                    }
                    if(j+1<obstacleGrid[0].length && obstacleGrid[i][j+1]!=9 && obstacleGrid[i][j+1]==1){
                        //obstacleGrid[i][j] += obstacleGrid[i][j+1];
                        obstacleGrid[i][j+1] += obstacleGrid[i][j];
                    }
                }
                else if(obstacleGrid[i][j] == 9){
                    answer = Math.min(answer, Math.max(obstacleGrid[i-1][j], obstacleGrid[i][j-1]));
                }
            }
        }
        return answer;
    }
    public void help(int[][] lot, int row, int col, int numRows, int numCols){
        if(row >= numRows ||
           row < 0 ||
           col >= numCols ||
           col < 0 ||
           lot[row][col] != 1
            || lot[row][col] == 9){
            return ;
        }

        lot[row][col] = 0;
        help(lot, row + 1, col, numRows, numCols);
        //help(lot, row - 1, col, numRows, numCols);
        help(lot, row, col + 1, numRows, numCols);
        //help(lot, row, col - 1, numRows, numCols);
        lot[row][col] = 1;
    }
}
