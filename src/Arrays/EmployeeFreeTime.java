package Arrays;

import Sorting.Interval;

import java.util.*;

// Time complexity: O(NLogN) where N in total number of intervals


public class EmployeeFreeTime {
    public static void main(String args[]){
    }
    public List<Interval> employeeFreeTime(List<List<Interval>> schedules) {
        List<Interval> intervals = new ArrayList<>();
        List<Interval> result = new ArrayList<>();
        schedules.forEach(schedule -> intervals.addAll(schedule));

        intervals.sort(new Comparator<Interval>() {
            @Override
            public int compare(Interval o1, Interval o2) {
                return o1.start- o2.start;
            }
        });

        /*
        * Java 8 Version
        * intervals.sort((a,b) -> a.start - b.start);
        * */

        Interval temp = intervals.get(0);
        for(Interval interval: intervals){
            if(temp.end < interval.start){
                result.add(new Interval(temp.end, interval.start));
                temp = interval;
            }
            else{
                temp = (temp.end < interval.end) ? interval: temp;
            }
        }

        return result;
    }
}
