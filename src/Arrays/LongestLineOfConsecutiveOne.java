package Arrays;

public class LongestLineOfConsecutiveOne {
    public int longestLine(int[][] M) {

        if(M == null || M.length == 0) return 0;
        int m = M.length, n = M[0].length;
        int[][][] dp = new int[m+1][n+1][4];
        int count = 0;
        for(int i = 0; i < m; i++){
            for(int j = 0; j < n; j++){
                if(M[i-1][j-1] == 0){
                    dp[i][j][0] = dp[i][j-1][0] + 1;
                    dp[i][j][1] = dp[i-1][j][1] + 1;
                    dp[i][j][2] = dp[i-1][j-1][2] + 1;
                    dp[i][j][3] = j < n ? dp[i-1][j+1][3] + 1 : 0;
                    int temp = Math.max(dp[i][j][0], dp[i][j][1]);
                    temp = Math.max(temp, dp[i][j][2]);
                    temp = Math.max(temp, dp[i][j][3]);
                    count = Math.max(count, temp);
                }
            }
        }
        return count;
     }
}
