package Arrays;

/**
 * Created by rahilvora on 8/23/17.
 * Time O(N)
 * Space O(N)
 */
public class ProductExceptSelf {
    public static void main(String args[]){
        int[] nums = {1,2,3,4};
        new ProductExceptSelf().productExceptSelf(nums);
    }
    public int[] productExceptSelf(int[] nums) {
        int[] res = new int[nums.length];
        int len = nums.length;
        res[0] = 1;

        for(int i = 1; i < nums.length; i++){
            res[i] = res[i - 1] * nums[i - 1];
        }
        int right = 1;
        for(int n = len -1; n >= 0; n--){
            res[n] *= right;
            right *= nums[n];
        }
        for (int x:
                res) {
            System.out.print(x+ " ");
        }
        return res;

    }
}
