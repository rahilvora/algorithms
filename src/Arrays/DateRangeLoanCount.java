package Arrays;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by rahilvora on 1/30/18.
 *
Your previous Python 2 content is preserved below:

# You are a restaurant owner and you have list of date ranges (start and end times) representing start/end times of loan applications. All times are positive 64-bit integer timestamps, and the ranges are inclusive on left and exclusive on right.

# The goal is to generate a time series (list of time and count, sorted by time) of the number of active loans over time.

# For example, given:

# start | end

# 3 | 7
# 5 | 7
# 7 | 15
# 8 | 12
# 8 | 20
# 14 | 20

# the output is:

# time | count

# 3 | 1
# 5 | 2
# 7 | 1
# 8 | 3
# 12 | 2
# 14 | 3
# 15 | 2
# 20 | 0

*/
public class DateRangeLoanCount {
    public static void main(String[] args) {
        ArrayList<Integer> starts = new ArrayList<Integer>();
        ArrayList<Integer> ends = new ArrayList<Integer>();
        List<Loan> loans = new ArrayList<>();
        starts.add(3);
        starts.add(5);
        starts.add(7);
        starts.add(8);
        starts.add(8);
        starts.add(14);

        ends.add(7);
        ends.add(7);
        ends.add(15);
        ends.add(12);
        ends.add(20);
        ends.add(20);

        for(int i = 0; i < starts.size(); i++){
            loans.add(new Loan(starts.get(i), ends.get(i)));

        }
        new DateRangeLoanCount().loanCount(loans);
        // for (String string : strings) {
        //   System.out.println(string);
        // }
    }

    public void loanCount(List<Loan> loans){

        TreeMap<Integer, Integer> map = new TreeMap<>();

        for(Loan l: loans){
            if(!map.containsKey(l.start)){
                map.put(l.start,1);
            }
            if(!map.containsKey(l.end)){
                map.put(l.end, 0);
            }
            if(map.firstKey() < l.start && l.start < map.lastKey()){
                map.put(l.start, map.getOrDefault(l.start, 0) + 1);
            }

//            if(map.firstKey() < l.end && l.end < map.lastKey()){
//                    map.put(l.end, map.get(l.end) - 1);
//            }


        }

        for(Map.Entry<Integer, Integer> entry: map.entrySet()){

            System.out.println(entry.getKey() +" - "+ entry.getValue());
        }

    }


}

class Loan{
    int start, end;
    Loan(int start,int end){
        this.start = start;
        this.end = end;
    }
}

