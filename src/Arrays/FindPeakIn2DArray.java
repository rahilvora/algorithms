package Arrays;

public class FindPeakIn2DArray {
    public static void main(String args[]){
        int[][] arr = {{ 10, 8, 10, 10 },
                { 14, 13, 12, 11 },
                { 15, 9, 11, 21 },
                { 16, 17, 19, 20 } };
        System.out.println(new FindPeakIn2DArray().findPeak(arr));
    }

    public int findPeak(int[][] arr){
        return findPeakRec(arr, arr.length, arr[0].length, arr[0].length/2 );
    }

    public int findMax(int[][] arr, int rows, int mid){
        int index = 0;
        int max = Integer.MIN_VALUE;
        for(int i = 0; i < rows; i++){
            if(arr[i][mid] > max){
                max = arr[i][mid];
                index = i;
            }
        }
        return index;
    }

    public int findPeakRec(int[][] arr, int rows, int cols, int mid){
        int max_index = findMax(arr, rows, mid);
        int max = arr[max_index][mid];

        // If we are on the first or last column,
        // max is a peak
        if (mid == 0 || mid == cols-1)
            return max;

        // If mid column maximum is also peak
        if(max >= arr[max_index][mid - 1] && max >= arr[max_index][mid+1]){
            return max;
        }

        // If max is less than its left
        if(max < arr[max_index][mid - 1]){
            return findPeakRec(arr, rows, cols, mid - mid/2);
        }

        // If max is less than its left
        return findPeakRec(arr, rows, cols, mid + mid/2);

    }
}
