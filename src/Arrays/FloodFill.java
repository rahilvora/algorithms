package Arrays;

public class FloodFill {
    public static void main(String args[]){
        int[][] image = {{1,1,1},{1,1,0},{1,0,1}};

        new FloodFill().floodFill(image, 1, 1, 2);
    }
    public int[][] floodFill(int[][] image, int sr, int sc, int newColor) {
        if(image == null || image.length == 0){
            return null;
        }
        DFS(image, sr, sc, newColor, image[sr][sc]);
        return image;
    }
    private void DFS(int[][] image, int sr, int sc, int oldColor, int newColor){
        if(sr>=0 && sc >=0 && sr <= image.length-1 && sc <= image[0].length-1 && image[sr][sc] == oldColor && oldColor != newColor){
            image[sr][sc] = newColor;
            DFS(image, sr-1, sc, oldColor, newColor);
            DFS(image, sr+1, sc, oldColor, newColor);
            DFS(image, sr, sc-1, oldColor, newColor);
            DFS(image, sr, sc+1, oldColor, newColor);
        }
    }
}
