package Arrays;

import java.util.*;

class Buddy {
    int similarity;
    String name;
    Set<String> wishList;
    Buddy(int similarity, String name, Set<String> wishList){
        this.similarity = similarity;
        this.name = name;
        this.wishList = wishList;
    }
}
public class TravelBuddy {
    Set<String> myWishList;
    List<Buddy> buddies;
    TravelBuddy(Set<String> myWishList, Map<String, Set<String>> map){
        this.myWishList = myWishList;
        for(String key: map.keySet()){
            Set<String> wishList = map.get(key);
            Set<String> intersection = new HashSet<>(wishList);
            intersection.retainAll(myWishList);
            int similarity = intersection.size();
            if(similarity >= wishList.size()/2){
                buddies.add(new Buddy(similarity, key, wishList));
            }
        }
    }

    public List<Buddy> getSortedBuddies() {
        Collections.sort(buddies, new Comparator<Buddy>() {
            @Override
            public int compare(Buddy o1, Buddy o2) {
                return o1.similarity - o2.similarity;
            }
        });
        List<Buddy> res = new ArrayList<>(buddies);
        return res;
    }

    public List<String> recommendCities(int k) {
        List<String> res = new ArrayList<>();
        List<Buddy> list = getSortedBuddies();
        int i = 0;
        while(k>0 && i < list.size()){
            Set<String> diff = list.get(i).wishList;
            diff.removeAll(myWishList);
            if(diff.size() >= k){
                res.addAll(diff);
                k -= diff.size();
            }
            else{
                Iterator<String> it = diff.iterator();
                while(k > 0){
                    res.add(it.next());
                    k--;
                }
            }
        }
        return res;
    }
}
