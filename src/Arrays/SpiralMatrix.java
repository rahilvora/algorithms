package Arrays;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rahilvora on 6/15/17.
 */
public class SpiralMatrix {
    public List<Integer> spiralOrder(int[][] matrix) {
        int top = 0,
            right = matrix[0].length - 1,
            left = 0,
            bottom = matrix.length - 1,
            direction = 0;
        List<Integer> answer = new ArrayList<>();
        while(top <= bottom && left <= right){
            if(direction == 0){
               for(int i = left; i <= right; i++){
                   answer.add(matrix[top][i]);
               }
                top++;
            }
            else if(direction == 1){
                for(int i = top; i < bottom; i++){
                    answer.add(matrix[i][right]);
                }
                right--;
            }
            else if(direction == 2){
                for(int i = right; i >= left; i--){
                    answer.add(matrix[bottom][i]);
                }
                bottom--;
            }
            else if(direction == 3){
                for(int i = bottom; i>=top; i--){
                    answer.add(matrix[i][left]);
                }
                left++;
            }
            direction = (direction+1)%4;
        }
        return answer;
    }
}
