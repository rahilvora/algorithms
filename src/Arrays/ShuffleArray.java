package Arrays;
import java.util.Random;

/**
 * Created by rahilvora on 10/5/17.
 */
public class ShuffleArray {
    int[] nums;
    Random random;
    ShuffleArray(int[] nums){
        this.nums = nums;
        random = new Random();
    }
    public static void main(String args[]){

    }
    public int[] reset(){
        return nums;
    }
    public int[] shuffle(){
        if(nums == null) return null;
        int[] a = nums.clone();
        for(int j = 1; j < a.length; j++){
            int i = random.nextInt(j + 1);
            swap(a, i, j);
        }
        return a;
    }
    public void swap(int[] a, int i, int j){
        int temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }
}
