package Arrays;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by rahilvora on 5/13/17.
 */
public class ContainesDuplicate {
    public static void main(String args[]){

    }
    public boolean containsNearbyDuplicate(int[] nums, int k) {
        Set<Integer> set = new HashSet<>();
        for(int i = 0; i < nums.length; i++){
            if(i > k) set.remove(nums[i-k-1]);
            if(!set.add(nums[i])) return true;
        }
        return false;
    }
}
