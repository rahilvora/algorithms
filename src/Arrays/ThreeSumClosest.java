package Arrays;
import java.util.Arrays;
/**
 * Created by rahilvora on 8/3/17.
 */
public class ThreeSumClosest {
    public int threeSumClosest(int[] nums, int target) {
        Arrays.sort(nums);
        int answer = nums[0] + nums[1] + nums[nums.length - 1];
        for(int i = 0; i < nums.length - 2; i++){
            if(i == 0 || (i > 0 && nums[i] != nums[i-1])){
                int lo = i + 1, hi = nums.length - 1;
                while(lo < hi){
                    int sum = nums[i] + nums[lo] + nums[hi];
                    if(sum > target){
                        hi--;
                    }
                    else{
                        lo++;
                    }
                    if(Math.abs(sum- target) < Math.abs(answer - target)){
                        answer = sum;
                    }
                }
            }
        }
        return answer;
    }
}
