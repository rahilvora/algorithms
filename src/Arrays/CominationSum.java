package Arrays;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rahilvora on 8/29/17.
 */
public class CominationSum {
    public static void main(String args[]){

    }
    public List<List<Integer>> combinationSum(int[] candidates, int target) {
        List<List<Integer>> result = new ArrayList<>();
        backTrack(result, new ArrayList<>(), candidates, target, 0);
        return result;
    }
    public void backTrack(List<List<Integer>> result ,List<Integer> temp, int[] candidates, int target, int start){
        if(target < 0) return;
        else if(target == 0) result.add(new ArrayList<>(temp));
        else{
            for(int i = start; i < candidates.length; i++){
                temp.add(candidates[i]);
                backTrack(result, temp, candidates, target - candidates[i], i);
                temp.remove(temp.size() - 1);
            }
        }
    }

}
