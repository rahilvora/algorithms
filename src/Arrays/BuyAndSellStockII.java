package Arrays;

/**
 * Created by rahilvora on 5/12/17.
 */
public class BuyAndSellStockII {
    public static void main(String args[]){
        int[] prices = {2,1,2,0,1};
        System.out.print(new BuyAndSellStockII().maxProfit(prices));
    }
    public int maxProfit(int[] prices) {
        int profit = 0;
        for(int i = 0; i < prices.length - 1; i++){
            if(prices[i+1] > prices[i]) profit += prices[i+1] - prices[i];
        }
        return profit;
    }
}
