package DynammicProgramming;

import java.lang.reflect.Array;
import java.util.Arrays;

public class LargestPlusSign {
    public static void main(String args[]){
        int N = 5;
        int[][] mines = {{4,2}};
        System.out.print(new LargestPlusSign().orderOfLargestPlusSign(N, mines));
    }
    public int orderOfLargestPlusSign(int N, int[][] mines) {
        int[][] grid = new int[N][N];
        for(int i = 0; i < grid.length; i++){
            Arrays.fill(grid[i], 1);
        }
        for(int i = 0; i < mines.length; i++){
            int[] temp = mines[i];
            grid[temp[0]][temp[1]] = 0;
        }
        int maxPlus = 0;
        if(mines.length<N*N) maxPlus = 1;
        int[][] left  = new int[N][N];
        int[][] right = new int[N][N];
        int[][] top  = new int[N][N];
        int[][] bottom  = new int[N][N];

        for(int i = 0; i < grid.length-1; i++){
            for(int j = 0; j < grid[0].length-1; j++){
                if(grid[i][j] == 1){
                    top[i][j] = (i-1>=0) ? top[i-1][j] + 1:1;
                    left[i][j] = (j - 1 >= 0) ? left[i][j - 1] + 1: 1;
                }
                else{
                    top[i][j] = 0;
                    left[i][j] = 0;
                }
            }
        }

        for(int i = N - 1; i>=0; i--){
            for(int j = N - 1; j >= 0; j--){
                if(grid[i][j] == 1){
                    bottom[i][j] = (i+1<N) ? bottom[i+1][j] + 1:1;
                    right[i][j]  = (j+1<N) ? right[i][j+1]+1:1;
                }
                else{
                    bottom[i][j] = 0;
                    right[i][j] = 0;
                }
                maxPlus = Math.max(maxPlus, Math.min(Math.min(right[i][j], bottom[i][j]), Math.min(top[i][j], left[i][j])));
            }
        }

        return maxPlus;
    }
}
