package DynammicProgramming;

/**
 * Created by rahilvora on 9/12/17.
 * Time and Space O(N^2)
 */
public class LogestCommonSubstring {
    public static void main(String args[]){
        System.out.print(new LogestCommonSubstring().logestCommonSubString("abcdef","zbcdf"));
    }
    public int logestCommonSubString(String str1, String str2){
        int[][] T = new int[str2.length() + 1][str1.length() + 1];
        for(int i = 0; i <= str1.length(); i++){
           T[0][i] = 0;
        }
        for(int j = 0; j <= str2.length(); j++){
            T[j][0] = 0;
        }
        int len = Integer.MIN_VALUE;
        for(int i = 1; i < T.length; i++){
            for(int j = 1; j < T[0].length; j++){
                if(str2.charAt(i - 1) == str1.charAt(j - 1)){
                    T[i][j] = 1 + T[i-1][j-1];
                    len = Math.max(len, T[i][j]);
                }
                else{
                    T[i][j] = 0;
                }
            }
        }
        return len;
    }
}
