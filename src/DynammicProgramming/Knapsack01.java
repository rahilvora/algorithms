package DynammicProgramming;

/**
 * Created by rahilvora on 9/10/17.
 * Time and Space = O(W*total nums of items)
 */
public class Knapsack01 {
    public static void main(String args[]){
        Knapsack01 k = new Knapsack01();
        int val[] = {22, 20, 15, 30, 24, 54, 21, 32, 18, 25};
        int wt[] = {4, 2, 3, 5, 5, 6, 9, 7, 8, 10};
        int r = k.bottomUpDP(val, wt, 30);
    }
    public int bottomUpDP(int val[], int wt[], int W){
        int[][] T = new int[val.length + 1][W + 1];

        for(int i = 0; i < T.length; i++){
            for(int j = 1; j < T[0].length; j++){
                if(i == 0 || j == 0){
                    T[i][j] = 0;
                    continue;
                }
                if(j - wt[i-1] >= 0){
                    T[i][j] = Math.max(T[i-1][j], val[i-1] + T[i-1][j - wt[i - 1]]);
                }
                else{
                    T[i][j] = T[i-1][j];
                }
            }
        }
        return T[val.length][W];
    }
}
