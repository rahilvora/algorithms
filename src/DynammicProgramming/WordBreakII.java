package DynammicProgramming;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by rahilvora on 9/18/17.
 * Time Complexity  O(len(wordDict) ^ len(s / minWordLenInDict)),
 */
public class WordBreakII {
    public static void main(String args[]){
        String s = "catsanddog";
        List<String> wordDict = new ArrayList<>();
        wordDict.add("cat");
        wordDict.add("cats");
        wordDict.add("and");
        wordDict.add("sand");
        wordDict.add("dog");
        List<String> ans = new WordBreakII().wordBreak(s, wordDict);
        System.out.print(ans);
    }
    public List<String> wordBreak(String s, List<String> wordDict) {
        return DFE(s, wordDict, new HashMap<>());
    }

    public List<String> DFE(String s, List<String> wordDict, HashMap<String, LinkedList<String>> map){
        if(map.containsKey(s)){
            return map.get(s);
        }
        LinkedList<String> res = new LinkedList<>();
        if(s.length() == 0){
            res.add("");
            return res;
        }
        for(String word: wordDict){
            if(s.startsWith(word)){
                List<String> subList = DFE(s.substring(word.length()), wordDict, map);
                for(String sub: subList){
                    res.add(word + (sub.isEmpty() ? "" : " " + sub));
                }
            }
        }
        map.put(s, res);
        return res;

    }

}
