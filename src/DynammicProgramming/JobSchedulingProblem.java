package DynammicProgramming;

import java.util.Arrays;
import java.util.Comparator;
import java.util.PriorityQueue;

/**
 * Created by rahilvora on 9/10/17.
 */
class Jobs{
    int start, end, profit;
    Jobs(int start, int end, int profit){
        this.start = start;
        this.end = end;
        this.profit = profit;
    }
}
public class JobSchedulingProblem {
    public static void main(String args[]){
        Jobs jobs[] = new Jobs[4];
        jobs[0] = new Jobs(1,2,50);
        jobs[2] = new Jobs(3,10,20);
        jobs[1] = new Jobs(6,19,100);
        jobs[3] = new Jobs(2,100,200);
        System.out.print(new JobSchedulingProblem().scheduleJobsII(jobs));
    }

    class FinishTimeComparator implements Comparator<Jobs>{

        @Override
        public int compare(Jobs o1, Jobs o2) {
            if(o1.end <= o2.end)return -1;
            return 1;
        }
    }
    // Time complexity O(n^n)
    public int scheduleJobs(Jobs[] jobs){
        Arrays.sort(jobs, new FinishTimeComparator());
        int[] T = new int[jobs.length];
        T[0] = jobs[0].profit;
        for(int i = 1; i < jobs.length; i++){
            T[i] =  Math.max(jobs[i].profit, T[i-1]);
            for(int j = 0; j < i; j++){
                if(jobs[j].end <= jobs[i].start){
                    T[i] = Math.max(T[i], T[j] + jobs[i].profit);
                }
            }
        }
        int maxVal = Integer.MIN_VALUE;
        for(int profit: T){
            maxVal = Math.max(maxVal, profit);
        }
        return maxVal;
    }

    //Time Complexity O(nlogn)
    public int scheduleJobsII(Jobs[] jobs){
        Arrays.sort(jobs, new FinishTimeComparator());
        int[] T = new int[jobs.length];
        T[0] = jobs[0].profit;
        for(int i = 1; i < jobs.length; i++){
            int currProfit = jobs[i].profit;
            int l = binarySearch(jobs, i);
            if(l != -1){
                currProfit += jobs[l].profit;
            }

            T[i] = Math.max(T[i-1], currProfit);
        }

        return T[jobs.length - 1];
    }
    static public int binarySearch(Jobs jobs[], int index)
    {
        // Initialize 'lo' and 'hi' for Binary Search
        int lo = 0, hi = index - 1;

        // Perform binary Search iteratively
        while (lo <= hi)
        {
            int mid = (lo + hi) / 2;
            if (jobs[mid].end <= jobs[index].start)
            {
                if (jobs[mid + 1].end <= jobs[index].start)
                    lo = mid + 1;
                else
                    return mid;
            }
            else
                hi = mid - 1;
        }

        return -1;
    }
}
