package DynammicProgramming;

/**
 * Created by rahilvora on 9/12/17.
 * Time = O(total*coins)
 * space = O(coins)
 */
public class CoinChangeProblem {
    public static void main(String args[]){
        int total = 13;
        int coins[] = {7, 3, 2, 6};
        System.out.print(new CoinChangeProblem().minimumCoinBottomUp(total, coins));
    }
    public int minimumCoinBottomUp(int total, int[] coins){
        int[] T = new int[total + 1];
        int[] R = new int[total + 1];
        T[0] = 0;
        for(int i = 1; i < T.length; i++){
            T[i] = Integer.MAX_VALUE - 1;
            R[i] = -1;
        }
        for(int j = 0; j < coins.length; j++){
            for(int i = 1; i < T.length; i++){
                if(i >= coins[j]){
                    if(T[i - coins[j]] + 1 < T[i]){
                        T[i] = T[i - coins[j]] + 1;
                        R[i] = j;
                    }
                }
            }
        }
        printCoins(R, coins);
        return T[T.length - 1];
    }
    public void printCoins(int[] R, int[] coins){
        if(R[R.length - 1] == -1){
            System.out.print("No coins can form total");
        }
        int start = R.length - 1;
        while (start != 0){
            int j = R[start];
            System.out.println(coins[j]);
            start = start - coins[j];
        }
    }

}
