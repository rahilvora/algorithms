package DynammicProgramming;

/**
 * Created by rahilvora on 9/14/17
 *
 * Its basically a fibonaci number with 1 and 2 instead of 1 and 1.
 */
public class ClimbingStairs {
    public static void main(String args[]){

    }
    // Time O(N) and space O(n)

    public int climbingStairsDP(int n){
        if (n == 1) return 1;
        int[] dp = new int[n + 1];
        dp[1] = 1;
        dp[2] = 2;
        for(int i = 3; i <= n; i++){
            dp[i] = dp[i - 1] + dp[i - 2];
        }
        return dp[n];
    }

    // Time O(N) and space O(1)
    public int climbingStairsII(int n){
        if(n == 1) return 1;
        int first = 1;
        int second = 2;
        for (int i = 3; i <= n; i++){
            int third = first + second;
            first = second;
            second = third;
        }
        return second;
    }
}
