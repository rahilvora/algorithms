package DynammicProgramming;

import java.util.HashMap;
import java.util.Map;

public class NumMatrix {
    int[][] matrix;
    Map<String, Integer> map;
    int[][] dp;
    public NumMatrix(int[][] matrix) {
        this.matrix = matrix;
        map = new HashMap<>();
        dp = new int[this.matrix.length + 1][this.matrix[0].length + 1];
    }

    public int sumRegion(int row1, int col1, int row2, int col2) {
        String key = String.valueOf(row1) + String.valueOf(col1) +  String.valueOf(row2) + String.valueOf(col2);
        if (map.containsKey(key)) return map.get(key);
        int answer = 0;
        for (int i = row1; i <= row2; i++) {
            for (int j = col1; j <=col2; j++) {
                answer += this.matrix[i][j];
            }
        }
        map.put(key, answer);
        return answer;
    }

    // O(M*N)
    public void buildDPMatrix() {
        for (int i = 1; i <= matrix.length; i++) {
            for (int j = 1; j <= matrix[0].length; j++) {
                dp[i][j] = dp[i-1][j] + dp[i][j-1] - dp[i-1][j-1] + matrix[i-1][j-1];
            }
        }
    }

    // O(1)
    public int sumRegionII(int row1, int col1, int row2, int col2) {
        return dp[row2 + 1][col2 + 1] - dp[row1][col2+1] - dp[row2 + 1][col1] + dp[row1][col1];
    }


    public static void main (String args[]) {
        int[][] arr = {
                {3, 0, 1, 4, 2},
                {5, 6, 3, 2, 1},
                {1, 2, 0, 1, 5},
                {4, 1, 0, 1, 7},
                {1, 0, 3, 0, 5}};
        NumMatrix obj = new NumMatrix(arr);
        System.out.println(obj.sumRegion(2, 1, 4, 3));
        System.out.println(obj.sumRegion(1, 1, 2, 2));
        System.out.println(obj.sumRegion(1, 2, 2, 4));
        obj.buildDPMatrix();
        System.out.println(obj.sumRegionII(2, 1, 4, 3));
        System.out.println(obj.sumRegionII(1, 1, 2, 2));
        System.out.println(obj.sumRegionII(1, 2, 2, 4));
    }
}