package DynammicProgramming;

/**
 * Created by rahilvora on 9/12/17.
 */
public class MinimumCostPath {
    public static void main(String args[]){
        int[][] arr = {
                {1,3,5,8},
                {4,2,1,7},
                {4,3,2,3},
        };
        System.out.print(new MinimumCostPath().minimunCostPath(arr));
    }
    public int minimunCostPath(int[][] arr){
        int[][] answer = new int[arr.length][arr[0].length];
        answer[0][0] = arr[0][0];
        for(int i = 1; i < arr.length; i++){
            answer[i][0] = answer[i-1][0] + arr[i][0];
        }
        for(int i = 1; i < arr[0].length; i++){
            answer[0][i] = answer[0][i-1] + arr[0][i];
        }
        for(int i = 1; i < arr.length; i++){
            for(int j = 1; j < arr[0].length; j++){
                answer[i][j] = arr[i][j] + Math.min(answer[i-1][j], answer[i][j-1]);
            }
        }
        String str = arr[arr.length - 1][arr[0].length - 1] + "";
        int i = arr.length - 1, j = arr[0].length - 1;
        while (i > 0 && j > 0){
            int value = 0;
            if(arr[i-1][j] < arr[i][j-1]){
                value = arr[i-1][j];
                i--;
            }
            else{
                value = arr[i][j-1];
                j--;
            }
            str = value + " -> " + str;
        }
        str = arr[0][0]  + " -> " + str;
        System.out.println(str);
        return answer[answer.length - 1][answer[0].length - 1];

    }
}
