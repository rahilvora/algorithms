package DynammicProgramming;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by rahilvora on 9/2/17.
 *
 * Given a list of non-negative numbers and a target integer k,
 * write a function to check if the array has a continuous subarray of size at least 2
 * that sums up to the multiple of k, that is, sums up to n*k where n is also an integer.


 Input: [23, 2, 4, 6, 7],  k=6
 Output: True
 Explanation: Because [2, 4] is a continuous subarray of size 2 and sums up to 6.
 */

public class ContinuousSubArraySum {
    public static void main(String args[]){
        int[] nums = {23, 2, 4, 6, 7};
        new ContinuousSubArraySum().checkSubarraySum(nums, 6);
    }
    public boolean checkSubarraySum(int[] nums, int k) {
        Map<Integer, Integer> map = new HashMap<>();
        map.put(0,-1);
        int remainingSum = 0;
        for(int i = 0; i < nums.length; i++){
            remainingSum += nums[i];
            if(k != 0){
                remainingSum %= k;
            }
            Integer prev = map.get(remainingSum);
            if(prev != null){
                if(i - prev > 1) return true;
            }
            else{
                map.put(remainingSum, i);
            }
        }
        return false;
    }

}
