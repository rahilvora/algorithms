package DynammicProgramming;

/**
 * Created by rahilvora on 9/11/17.
 */
public class CountNumberofSBSTPossibleWithNKeys {
    public static void main(String args[]){
        System.out.println(new CountNumberofSBSTPossibleWithNKeys().numberOfBST(6));
    }
    public int numberOfBST(int key){
        int[] T = new int[key + 1];
        T[0] = 1;
        T[1] = 1;
        for(int i = 2; i <= key; i++){
            for(int j = 0; j < i; j++){
                T[i] += T[j]*T[i-j-1];
            }
        }
        return T[key];
    }
}
