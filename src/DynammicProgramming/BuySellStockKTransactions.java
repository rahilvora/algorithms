package DynammicProgramming;

/**
 * Created by rahilvora on 9/5/17.
 */
public class BuySellStockKTransactions {
    //Solution with Time Complexity of O(days*Trasactions)
    public int maxProfitFastSolution(int prices[], int K) {
        if(K == 0 || prices.length == 0) return 0;
        //If K is greater than half of prices length means we can make maximum number of transaction
        if(K >= prices.length/2){
            return quickSolve(prices);
        }
        int T[][] = new int[K+1][prices.length];

        for (int i = 1; i < T.length; i++) {
            int maxDiff = -prices[0];
            for (int j = 1; j < T[0].length; j++) {
                T[i][j] = Math.max(T[i][j-1], prices[j] + maxDiff);
                maxDiff = Math.max(maxDiff, T[i-1][j] - prices[j]);
            }
        }
        return T[K][prices.length-1];
    }

    public int quickSolve(int[] prices){
        int profit = 0;
        for(int i = 1; i < prices.length; i++){
            if(prices[i]>prices[i-1])
                profit += prices[i] - prices[i-1];
        }
        return profit;
    }
    //Solution with Time Complexity of O(days*days*Trasactions
    public int maxProfitSlowSolution(int prices[], int K) {
        if(K == 0 || prices.length == 0){
            return 0;
        }
        int T[][] = new int[K + 1][prices.length];
        for(int i = 1; i < T.length; i++){
            for(int j = 1; j < T[0].length; j++){
                int maxVal = 0;
                for(int m = 0; m < j; m++){
                    maxVal = Math.max(maxVal, prices[j] - prices[m] + T[i-1][m]);
                }
                T[i][j] = Math.max(T[i][j-1], maxVal);
            }
        }
        return T[K][prices.length-1];
    }
}
