package DynammicProgramming;

/**
 * Created by rahilvora on 9/12/17.
 */
public class CuttingRod {
    public static void main(String args[]){
        int[] arr = {1, 5, 8, 9, 10, 17, 17, 20};
        int size = arr.length;
        System.out.print(new CuttingRod().cuttingRod(arr, size));
    }
    public int cuttingRod(int[] prices, int rodlen){
        int[] val = new int[rodlen + 1];
        val[0] = 0;
        for(int i = 1; i <= rodlen; i++){
            int maxValue = Integer.MIN_VALUE;
            for(int j = 0; j < i; j++){
                maxValue = Math.max(maxValue, prices[j] + val[i-j-1]);
            }
            val[i] = maxValue;
        }
        return val[rodlen];
    }
}
