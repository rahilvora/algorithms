package DynammicProgramming;

/**
 * Created by rahilvora on 12/9/17.
 */
public class CanJumpII {
    public int minJump(int arr[],int result[]){
        int[] jump = new int[arr.length];
        jump[0] = 0;
        for(int i = 1; i < jump.length; i++){
            jump[i] = Integer.MAX_VALUE -1;
        }
        for(int i = 1; i < arr.length; i++){
            for(int j = 0; j < i; j++){
                if((j + arr[j] >= i)){
                    if(jump[i] > jump[j] + 1){
                        result[j] = j;
                        jump[i] = jump[j] + 1;
                    }
                }
            }
        }
        return jump[jump.length -1];
    }
}
