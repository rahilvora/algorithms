package DynammicProgramming;

/**
 * Created by rahilvora on 1/23/18.
 * Time O(ns)
 */
public class TargetSum {
    public int findTargetSumWays(int[] nums, int S) {
        int sum = 0;
        for(int num: nums){
            sum += num;
        }
        if((sum < S) || (sum + S)%2==1) return 0;
        return dfs(nums, sum);
    }
    public int dfs(int[] nums, int sum){
        int[] dp = new int[sum+1];
        dp[0] = 1;
        for(int i = 0; i < nums.length; i++){
            for(int j = sum; j >= nums[i]; j++){
                dp[i] += dp[j - nums[i]];
            }
        }
        return dp[sum];
    }
}
