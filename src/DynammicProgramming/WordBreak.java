package DynammicProgramming;

import java.util.*;

/**
 * Created by rahilvora on 8/15/17.
 */
public class WordBreak {
    public static void main(String args[]){
        List<String> wordDict = new ArrayList<>();
        wordDict.add("leet");
        wordDict.add("code");
        new WordBreak().wordBreakII("leetcode", wordDict);
    }
    public boolean wordBreak(String s, List<String> wordDict) {
        Set<String> wordDictSet=new HashSet(wordDict);
        Queue<Integer> queue = new LinkedList<>();
        int[] visited = new int[s.length()];
        queue.add(0);
        while (!queue.isEmpty()) {
            int start = queue.remove();
            if (visited[start] == 0) {
                for (int end = start + 1; end <= s.length(); end++) {
                    if (wordDictSet.contains(s.substring(start, end))) {
                        queue.add(end);
                        if (end == s.length()) {
                            return true;
                        }
                    }
                }
                visited[start] = 1;
            }
        }
        return false;
    }
    public boolean wordBreakII(String s, List<String> wordDict){
        boolean[] F = new boolean[s.length() + 1];
        F[0] = true;
        for(int i = 1; i <= s.length(); i++){
            for(int j = 0; j < i; j++){
                if(F[j] && wordDict.contains(s.substring(j,i))){
                    F[i] = true;
                    break;
                }
            }
        }
        return F[F.length -  1];
    }
}
