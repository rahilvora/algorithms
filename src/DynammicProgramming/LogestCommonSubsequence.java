package DynammicProgramming;

/**
 * Created by rahilvora on 9/10/17.
 * Time and Space Complexity: O(str1.length * str2.length);
 */
public class LogestCommonSubsequence {
    public static void main(String args[]){
        System.out.print(new LogestCommonSubsequence().longestCommonSubsequence("abcdaf","acbcf"));
    }
    public int longestCommonSubsequence(String str1, String str2){
        int[][] T = new int[str1.length() + 1][str2.length() + 1];
        for(int i = 0; i < T.length; i++){
            for(int j = 0; j < T[0].length; j++){
                if(i == 0 || j == 0){
                    T[i][j] = 0;
                    continue;
                }
                if(str1.charAt(i - 1) == str2.charAt(j - 1)){
                    T[i][j] = T[i-1][j-1] + 1;
                }
                else{
                    T[i][j]= Math.max(T[i-1][j],T[i][j-1]);
                }
            }
        }
        StringBuilder str = new StringBuilder();
        int i = T.length -1, j = T[0].length - 1;
        while(i > 0 && j > 0){
            if(i != 0 && j != 0){

                if(T[i][j] == T[i-1][j]){
                    i--;
                }
                else if(T[i][j] == T[i][j-1]){
                    j--;
                }
                else if(T[i][j] == T[i-1][j-1] + 1){
                    str.append(str2.charAt(j-1));
                    i--;
                    j--;
                }
            }
        }
        System.out.print(str.reverse());
        return T[T.length - 1][T[0].length - 1];
    }
}
