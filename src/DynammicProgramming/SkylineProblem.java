package DynammicProgramming;

import java.util.*;

/**
 * Created by rahilvora on 9/22/17.
 */

// Time Complexity is NlogN
public class SkylineProblem {
    public static void main(String args[]){
        int[][] building = {
                {0, 2, 3},
                {2, 5, 3}
        };
        new SkylineProblem().getSkyline(building);
    }
    public List<int[]> getSkyline(int[][] buildings) {
        List<int[]> heights = new ArrayList<>();
        for(int[] building: buildings){
            // -ve height to mark start or +ve height to mark the end of a building
            heights.add(new int[]{building[0], -building[2]});
            heights.add(new int[]{building[1], building[2]});
        }
        Collections.sort(heights, (a, b) -> (a[0] == b[0]) ? a[1] - b[1] : a[0] - b[0]);
        TreeMap<Integer, Integer> heightMap = new TreeMap<>(Collections.reverseOrder());
        heightMap.put(0,1);
        List<int[]> skyline = new ArrayList<>();
        int prevHeight = 0;
        for (int[] h: heights) {
            if (h[1] < 0) {
                Integer cnt = heightMap.get(-h[1]);
                cnt = ( cnt == null ) ? 1 : cnt + 1;
                heightMap.put(-h[1], cnt);
            } else {
                Integer cnt = heightMap.get(h[1]);
                if (cnt == 1) {
                    heightMap.remove(h[1]);
                } else {
                    heightMap.put(h[1], cnt - 1);
                }
            }
            int currHeight = heightMap.firstKey();
            if (prevHeight != currHeight) {
                skyline.add(new int[]{h[0], currHeight});
                prevHeight = currHeight;
            }
        }
        return skyline;
    }
}
