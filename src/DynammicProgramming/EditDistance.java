package DynammicProgramming;

/**
 * Created by rahilvora on 12/10/17.
 */
public class EditDistance {
    public static void main(String args[]){

    }
    public int minDistance(String s1, String s2){
        char[] str1 = s1.toCharArray();
        char[] str2 = s2.toCharArray();
        int[][] temp = new int[s1.length() + 1][ s2.length() + 1];
        for(int i = 0; i < temp[0].length; i++){
            temp[0][i] = i;
        }
        for(int i = 0; i < temp.length; i++){
            temp[i][0] = i;
        }

        for(int i = 1; i < temp.length; i++){
            for(int j = 1; j < temp[0].length; j++){
                if(str1[i-1] == str2[j-1]){
                    temp[i][j] = temp[i-1][j-1];
                }
                else{
                    temp[i][j] = 1 + min(temp[i-1][j-1], temp[i-1][j], temp[i][j-1]);
                }
            }
        }
        return temp[temp.length - 1][temp[0].length - 1];
    }
    private int min(int a, int b, int c){
        int l = Math.min(a,b);
        return Math.min(l,c);
    }
}
/*
* function min(a,b,c){
    var l = Math.min(a,b);
    return Math.min(l,c);
}
var minDistance = function(word1, word2) {
    var matrix = [];

    for(var j = 0; j <= word1.length; j++){
        var rows = [];
        for(var i = 0; i <= word2.length; i++){
            rows.push(0);
        }
        matrix.push(rows);
    }
    for(var a = 0; a < matrix[0].length; a++){
        matrix[0][a] = a;
    }
    for(var b = 0; b < matrix.length; b++){
        matrix[b][0] = b;
    }
    for(var x = 1; x < matrix.length; x++){
        for(var y = 1; y < matrix[0].length; y++){
            if(word1.charAt(x-1) == word2.charAt(y-1)){
                matrix[x][y] = matrix[x-1][y-1];
            }
            else{
                matrix[x][y] = 1 + min(matrix[x-1][y-1], matrix[x-1][y], matrix[x][y-1]);
            }
        }
    }
    return matrix[matrix.length - 1][matrix[0].length - 1];
};
minDistance("","a")
* */

