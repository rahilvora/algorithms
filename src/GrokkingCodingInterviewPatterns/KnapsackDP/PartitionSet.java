package GrokkingCodingInterviewPatterns.KnapsackDP;

public class PartitionSet {
    // Brute Force O(2^n)
    static boolean canPartition(int[] nums) {
        int sum = 0;

        for (int num: nums) {
            sum += num;
        }

        if (sum % 2 != 0) return false;

        return canPartitionRecursive(nums, sum / 2, 0);
    }

    public static boolean canPartitionRecursive(int[] nums, int sum, int currIndex) {
        if (sum == 0) return true;

        if (currIndex >= nums.length || sum < 0) return false;

        if (nums[currIndex] <= sum) {
            if (canPartitionRecursive(nums, sum - nums[currIndex], currIndex + 1)) return true;
        }

        return canPartitionRecursive(nums, sum, currIndex + 1);
    }


    // O(n*s) n nums length and s is sum/2
    public static boolean bottomUpDPCanPartition(int[] nums, int sum) {
        boolean[][] dp = new boolean[nums.length][sum + 1];
        for (int i = 0; i < nums.length; i++) {
            dp[i][0] = true;
        }

        for (int i = 1; i <= sum; i++) {
            dp[0][i] = nums[0] == sum ? true : false;
        }

        for (int i = 1; i < dp.length; i++) {
            for (int j = 0; j < dp[0].length; j++) {
                if (dp[i-1][j]) {
                    dp[i][j] = dp[i-1][j];
                }
                else if (j >= nums[i]) {
                    dp[i][j] = dp[i-1][j-nums[i]];
                }
            }
        }

        return dp[dp.length - 1][sum];
    }
}
