package GrokkingCodingInterviewPatterns.KnapsackDP;

public class Knapsack {

    public int solveKnapsack(int[] profits, int[] weights, int capacity) {
        return knapSackRecursive(profits, weights, capacity, 0);
    }

    // O(2^n)
    public int knapSackRecursive(int[] profits, int[] weights, int capacity, int currIndex) {

        // Base check
        if (capacity <= 0 || currIndex >= profits.length) return 0;

        int profit1 = 0;
        // if we select curr element
        if (weights[currIndex] <= capacity){
            profit1 = knapSackRecursive(profits, weights, capacity - weights[currIndex], currIndex + 1);
        }

        // if do not select current element
        int profit2 = knapSackRecursive(profits, weights, capacity, currIndex + 1);

        return Math.max(profit1, profit2);
    }

    // O (N*C) n is number of weights and c is capacity
    public int knapSackRecursiveTopDown(Integer[][] dp, int[] profits, int[] weights, int capacity, int currIndex) {
        // Base Case
        if (capacity <= 0 || currIndex >= profits.length) return 0;

        if (dp[currIndex][capacity] != null) return dp[currIndex][capacity];

        int profit1 = 0;
        if (weights[currIndex] <= capacity) {
            profit1 = knapSackRecursiveTopDown(dp, profits, weights, capacity - weights[currIndex], currIndex + 1);
        }

        int profit2 = knapSackRecursiveTopDown(dp, profits, weights, capacity, currIndex + 1);

        dp[currIndex][capacity] = Math.max(profit1, profit2);
        return dp[currIndex][capacity];
    }

    public int knapSackRecursiveBottomUp(int[] profits, int[] weights, int capacity, int currIndex) {
        int[][] dp = new int[profits.length][capacity+1];

        for (int i = 0; i <= capacity; i++) {
            if (weights[0] <= i) {
                dp[0][i] = profits[0];
            }
        }

        for (int i = 1; i < profits.length; i++) {
            for (int j = 1; j <= capacity; j++) {
                int profit1 = 0, profit2 = 0;
                if (weights[i] <= j) {
                    profit1 = profits[i] + dp[i-1][j - weights[i]];
                }

                profit2 = dp[i-1][j - weights[i]];
                dp[i][j] = Math.max(profit1, profit2);
            }
        }
        return dp[dp.length - 1][capacity];
    }
}
