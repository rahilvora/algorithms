package GrokkingCodingInterviewPatterns.KnapsackDP;

public class CountSubsetSum {

    //Bottom Up DP
    public int countSubsets(int[] num, int sum) {
        int[][] dp = new int[num.length][sum + 1];
        //
        for (int i = 0; i < dp.length; i++) {
            dp[i][0] = 1;
        }

        for (int i = 1; i <= sum; i++) {
            dp[0][i] = num[0] <= sum ? 1: 0;
        }

        for (int i = 1; i < num.length; i++) {
            for (int j = 1; j <= sum; j++) {
                // Exclude the number
                dp[i][j] = dp[i-1][j];

                if (j >= num[i]) {
                    dp[i][j] += dp[i-1][j-num[i]];
                }
            }
        }
        return dp[num.length - 1][sum];
    }
}
