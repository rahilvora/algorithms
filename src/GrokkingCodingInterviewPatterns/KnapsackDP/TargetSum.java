package GrokkingCodingInterviewPatterns.KnapsackDP;

public class TargetSum {
    // The above solution has time and space complexity of O(N*S),
    // where ‘N’ represents total numbers and ‘S’ is the desired sum.
    public int findTargetSubsets(int[] nums, int s) {
        int sum = 0;
        for (int num: nums) {
            sum += num;
        }

        if (sum < s || (s + sum) % 2 == 1) return 0;

        return new CountSubsetSum().countSubsets(nums, (s + sum) / 2);
    }
}
