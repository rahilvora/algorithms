package GrokkingCodingInterviewPatterns.KnapsackDP;

public class SubsetSum {
    public static void main (String args[]) {
        int[] nums = {1, 3, 4, 8};
        System.out.println(new SubsetSum().bottomUpDPSubsetSum(nums, 6));
    }

    // O(2^n)
    public boolean subsetSum (int[] nums, int sum) {
        return subsetSumRecursive(nums, sum, 0);
    }

    public boolean subsetSumRecursive (int[] nums, int sum, int currIndex) {
        if (sum == 0) return true;

        if (sum < 0 || currIndex >= nums.length) return false;

        if (nums[currIndex] <= sum) {
            if (subsetSumRecursive(nums, sum - nums[currIndex], currIndex + 1)) return true;
        }

        return subsetSumRecursive(nums, sum, currIndex + 1);
    }

    // Dynamic Programming O(N*S) S is sum and N is len of nums

    public boolean bottomUpDPSubsetSum(int[] nums, int sum) {
        boolean[][] dp = new boolean[nums.length][sum + 1];

        for (int i = 0; i < nums.length; i++) {
            dp[i][0] = true;
        }

        for (int i = 1; i <= sum; i++) {
            dp[0][i] = nums[0] == sum ? true: false;
        }

        for (int i = 1; i < nums.length; i++) {
            for (int j = 1; j <= sum; j++) {
                if (dp[i-1][j]) {
                    dp[i][j] = dp[i-1][j];
                }
                else if (j >= nums[i]) {
                    dp[i][j] = dp[i-1][j-nums[i]];
                }
            }
        }

        return dp[dp.length - 1][sum];
    }
}
