package GrokkingCodingInterviewPatterns.InPlaceReversalLinkedList;

import LinkedList.ListNode;

public class ReverseEveryKElements {
    public static void main (String args[]) {

    }

    public static ListNode reverse(ListNode head, int k) {
        if (head == null || k <=1) return head;
        ListNode current = head, previous = null;
        while (true){
            ListNode lastNodeOfPreviousList = previous;
            ListNode lastNodeOfCurrSubList = current;

            ListNode next = null;
            for (int i = 0; current != null && i < k; i++) {
                next = current.next;
                current.next = previous;
                previous = current;
                current = next;
            }

            if (lastNodeOfPreviousList != null) {
                lastNodeOfPreviousList.next = previous;
            }
            else head = previous;

            lastNodeOfCurrSubList.next = current;
            if ( current == null) break;

            previous = lastNodeOfCurrSubList;
        }
        return head;
    }
}
