package GrokkingCodingInterviewPatterns.InPlaceReversalLinkedList;

import LinkedList.ListNode;

public class ReverseLinkedList {

    public static void main (String args[]) {

    }

    public static ListNode reverse(ListNode head) {
        ListNode newHead = null;
        while (head != null) {
            ListNode next = head.next;
            head.next = newHead;
            newHead = head;
            head = next;
        }
        return newHead;
    }
}
