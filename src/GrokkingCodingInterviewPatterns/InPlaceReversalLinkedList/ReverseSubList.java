package GrokkingCodingInterviewPatterns.InPlaceReversalLinkedList;

import LinkedList.ListNode;

import java.util.List;

public class ReverseSubList {

    public static void main(String args[]) {
        ListNode head = new ListNode(1);
        head.next = new ListNode(2);
        head.next.next = new ListNode(3);
        head.next.next.next = new ListNode(4);
        head.next.next.next.next = new ListNode(5);

        ListNode result = ReverseSubList.reverse(head, 2, 4);
        System.out.print("Nodes of the reversed LinkedList are: ");
        while (result != null) {
            System.out.print(result.val + " ");
            result = result.next;
        }
    }

    public static ListNode reverse(ListNode head, int p, int q) {
        ListNode previous = null;
        ListNode current = head;
        for (int i = 0; i < p - 1; i++) {
            previous = current;
            current = current.next;
        }

        ListNode lastNodeOfFirstPart = previous;
        ListNode lastNodeOfSecondPart = current;
        ListNode next = null;
        for (int i = 0; current != null && i < q - p; i++) {
            next = current.next;
            current.next = previous;
            previous = current;
            current = next;
        }

        if (lastNodeOfFirstPart != null) {
            lastNodeOfFirstPart.next = previous;
        }
        else head = previous;

        lastNodeOfSecondPart.next = current;

        return head;
    }
}
