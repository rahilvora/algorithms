package GrokkingCodingInterviewPatterns.InPlaceReversalLinkedList;

import LinkedList.ListNode;

// Input: 1-2-3-4-5-6, 3
// Output: 4-5-6-1-2-3
public class RotateKElementsRight {
    public static ListNode rotate(ListNode head, int rotations) {
        if (head == null || rotations == 0) return head;

        ListNode lastNode = head;
        int length = 1;

        while (lastNode.next != null) {
            lastNode = lastNode.next;
            length++;
        }
        lastNode.next = head;
        rotations %= length;
        int skipLength = length - rotations;
        ListNode lastNodeOfList = head;

        for ( int i = 0; i < skipLength - 1; i++) {
            lastNodeOfList = lastNodeOfList.next;
        }
        head = lastNodeOfList.next;
        lastNodeOfList.next = null;

        return head;
    }
}
