package GrokkingCodingInterviewPatterns.TwoPointer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SubarrayProductLessThanTarget {

    public static void main (String args[]) {
        int[] arr = {2, 5, 3, 10};
        SubarrayProductLessThanTarget.findSubarrays(arr, 30);
    }
    // O (N^3) Time Complexity
    // o (N^2) Space Complexity
    public static List<List<Integer>> findSubarrays(int[] arr, int target) {
        List<List<Integer>> subarrays = new ArrayList<>();
        if (arr == null || arr.length == 0) return subarrays;
        int windowStart = 0;
        int currProduct = 1;
        for (int windowEnd = 0; windowEnd < arr.length; windowEnd++) {
            currProduct *= arr[windowEnd];
            while (currProduct >= target && windowStart < arr.length) {
                currProduct /= arr[windowStart++];
            }

            subarrays.add(subArray(arr, windowStart, windowEnd));
        }
        return subarrays;
    }

    public static List<Integer> subArray(int[] arr, int start, int end) {
        List<Integer> list = new ArrayList<>();
        for(int i = start ; i <= end; i++) {
            list.add(arr[i]);
        }
        return list;
    }
}
