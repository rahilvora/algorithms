package GrokkingCodingInterviewPatterns.TwoPointer;

import java.util.ArrayList;
import java.util.List;

public class SquaringSortedArray {

    public static void main (String args[]) {
        int[] arr = {-3, -1, 0, 1, 2};
        System.out.println(SquaringSortedArray.makeSquares(arr).toString());
        int[] nums = {4,3,2,7,8,2,3,1};
        SquaringSortedArray.findDuplicates(nums);
    }

    public static int[] makeSquares(int[] arr) {
        int[] squares = new int[arr.length];
        int left = 0, right = arr.length - 1;
        int highestindex = arr.length - 1;
        while (left < right) {
            int leftValue = arr[left]*arr[left];
            int rightValue = arr[right]*arr[right];
            if (leftValue>=rightValue) {
               squares[highestindex--] = leftValue;
               left++;
            }
            else {
                squares[highestindex--] = rightValue;
                right--;
            }
        }
        return squares;
    }

    public static List<Integer> findDuplicates(int[] nums) {
        int i = 0;
        int size = nums.length;
        while (i < size) {
            if (nums[i] + 1 != i) {
                if (nums[i] != nums[nums[i] - 1]){
                    swap(nums, i, nums[i] - 1);
                    continue;
                }
            }
            i++;

        }
        List<Integer> answer = new ArrayList<>();
        for (int j = 0; j < nums.length; j++) {
            if (nums[j] != j + 1) {
                answer.add(nums[j]);
            }
        }
        return answer;
    }

    public static void swap(int[] nums, int i , int j) {
        int temp = nums[i];
        nums[i] = nums[j];
        nums[j] = temp;
    }

}
