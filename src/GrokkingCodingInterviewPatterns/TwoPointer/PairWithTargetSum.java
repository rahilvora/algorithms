package GrokkingCodingInterviewPatterns.TwoPointer;

/*
 * Created by Rahil Vora 07/28/2019
 * */

public class PairWithTargetSum {

    public static void main (String args[]) {

    }

    public static int[] search(int[] arr, int targetSum) {
        int firstPointer = 0;
        int secondPointer = arr.length - 1;
        while (firstPointer < secondPointer ) {
            int currSum = arr[firstPointer] + arr[secondPointer];
            if (currSum == targetSum) {
                return new int[] {firstPointer, secondPointer};
            }
            if (currSum < targetSum) {
                firstPointer++;
            } else {
                secondPointer--;
            }
        }
        return new int[] {-1, -1};
    }
}
