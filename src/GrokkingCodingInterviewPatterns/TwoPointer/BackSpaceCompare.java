package GrokkingCodingInterviewPatterns.TwoPointer;

import java.util.Stack;

public class BackSpaceCompare {
    public static void main(String args[]) {
        System.out.println(BackSpaceCompare.compareII("xy#z", "xzz#"));
        System.out.println(BackSpaceCompare.compareII("xy#z", "xyz#"));
        System.out.println(BackSpaceCompare.compareII("xp#", "xyz##"));
        System.out.println(BackSpaceCompare.compareII("xywrrmp", "xywrrmu#p"));
    }
    // Time Complexity o(m) space O(M)
    public static boolean compare (String str1, String str2) {
        Stack<Character> stack = new Stack<>();
        Stack<Character> stack1 = new Stack<>();
        for (int i = 0; i < str1.length(); i++) {
            char currChar = str1.charAt(i);
            if (currChar == '#') {
                if(!stack.isEmpty()) {
                    stack.pop();
                }
            }
            else {
                stack.push(currChar);
            }
        }
        for (int i = 0; i < str2.length(); i++) {
            char currChar = str2.charAt(i);
            if (currChar == '#') {
                if(!stack1.isEmpty()) {
                    stack1.pop();
                }
            }
            else {
                stack1.push(currChar);
            }
        }
        if (stack.size() != stack1.size()) return false;
        while (!stack.isEmpty() && !stack1.isEmpty()) {
            if (stack.pop() != stack1.pop()) return false;
        }
        return true;
    }

    // Time Complexity o(m) space O(1)
    public static boolean compareII (String str1, String str2) {
        int index1 = str1.length() - 1;
        int index2 = str2.length() - 1;
        while(index1 >=0 || index2 >=0) {
            int id1 = getNextValidChar(str1, index1);
            int id2 = getNextValidChar(str2, index2);
            if(id1 < 0 && id2 < 0) return true;
            if(id1 < 0 || id2 < 0) return false;
            if (str1.charAt(id1) != str2.charAt(id2)) // check if the characters are equal
                return false;
            index1 = id1 - 1;
            index2 = id2 - 1;
        }
        return true;
    }

    public static int getNextValidChar (String str, int index) {
        int backspaceCount = 0;
        while (index >= 0) {
            if (str.charAt(index) == '#') // found a backspace character
                backspaceCount++;
            else if (backspaceCount > 0) // a non-backspace character
                backspaceCount--;
            else
                break;

            index--; // skip a backspace or a valid character
        }
        return index;
    }
}
