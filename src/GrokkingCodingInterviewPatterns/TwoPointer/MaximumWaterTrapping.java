package GrokkingCodingInterviewPatterns.TwoPointer;

public class MaximumWaterTrapping {

    public static void main (String args[]) {
        System.out.println(MaximumWaterTrapping.findMaxWater(new int[] { 1, 3, 5, 4, 1 }));
        System.out.println(MaximumWaterTrapping.findMaxWater(new int[] { 3, 2, 5, 4, 2 }));
        System.out.println(MaximumWaterTrapping.findMaxWater(new int[] { 1, 4, 3, 2, 5, 8, 4 }));
    }

    public static int findMaxWater(int[] buildingHeights) {
        int left = 0, right = buildingHeights.length - 1;
        int result = 0;
        while (left < right) {
            int curr = Math.min(buildingHeights[left], buildingHeights[right]) * (right - left);
            result = Math.max(result, curr);
            if(buildingHeights[left] <= buildingHeights[right]) left++;
            else right--;
        }
        return result;
    }
}
