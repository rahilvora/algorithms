package GrokkingCodingInterviewPatterns.TwoPointer;

public class MinimumWindowSort {
    public static void main (String args[]) {
        System.out.println(MinimumWindowSort.sort(new int[] { 1, 2, 5, 3, 7, 10, 9, 12 }));
        System.out.println(MinimumWindowSort.sort(new int[] { 1, 3, 2, 0, -1, 7, 10 }));
        System.out.println(MinimumWindowSort.sort(new int[] { 1, 2, 3 }));
        System.out.println(MinimumWindowSort.sort(new int[] { 3, 2, 1 }));
    }

    public static int sort(int[] arr) {
        int size = arr.length;
        int low = 0, high = size - 1;

        while (low < size-1 && arr[low] <= arr[low + 1]) {
            low++;
        }

        if (low == size-1) return 0;

        while (high > 0 && arr[high] >= arr[high -1]) high--;

        int subarrayMax = Integer.MIN_VALUE, subarrayMin = Integer.MAX_VALUE;

        for (int i = low; i <= high; i++) {
            subarrayMax = Math.max(subarrayMax, arr[i]);
            subarrayMin = Math.min(subarrayMin, arr[i]);
        }

        while (low > 0 && arr[low-1] > subarrayMin) low--;

        while (high < size - 1 && arr[high+1] < subarrayMax) high++;

        return high - low + 1;
    }
}
