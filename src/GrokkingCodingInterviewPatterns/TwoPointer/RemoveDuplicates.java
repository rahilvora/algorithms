package GrokkingCodingInterviewPatterns.TwoPointer;

/*
 * Created by Rahil Vora 07/28/2019
 * */

public class RemoveDuplicates {

    public static void main (String args[]) {
        int[] arr = {2, 3, 3, 3, 6, 9, 9};
        System.out.println(RemoveDuplicates.remove(arr));
    }

    public static int remove(int[] arr) {
        int nextNonDuplicate = 1; // index of the next non-duplicate element
        for (int i = 1; i < arr.length; i++) {
            if (arr[nextNonDuplicate - 1] != arr[i]) {
                arr[nextNonDuplicate] = arr[i];
                nextNonDuplicate++;
            }
        }
        return nextNonDuplicate;
    }
}
