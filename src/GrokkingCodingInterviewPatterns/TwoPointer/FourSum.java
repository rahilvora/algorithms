package GrokkingCodingInterviewPatterns.TwoPointer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FourSum {

    public static void main (String args[]) {
        int[] arr = {2, 0, -1, 1, -2, 2};
        FourSum.searchQuadruplets(arr, 2);
    }

    public static List<List<Integer>> searchQuadruplets(int[] arr, int target) {
        List<List<Integer>> quadruplets = new ArrayList<>();;
        if (arr == null || arr.length == 0) return quadruplets;
        Arrays.sort(arr);
        int size = arr.length;
        for (int i = 0; i < size - 3; i++) {
            if(i == 0 || (i > 0 && arr[i] != arr[i-1])) {
                if( arr[i] + arr[i + 1] + arr[i + 2] + arr[i + 3] > target) break;
                if( arr[i] + arr[size - 3] + arr[size - 2] + arr[size - 1] < target) continue;
                for(int j = i + 1; j < size - 2; j++){
                    if(j == i + 1 || (j > i + 1 && arr[j] != arr[j-1])) {
                        if( arr[i] + arr[j] + arr[j + 1] + arr[j + 2] > target) break;
                        if( arr[i] + arr[j] + arr[size - 2] + arr[size - 1] < target) continue;
                        int left = j+1, right = size - 1;
                        while (left < right) {
                            int sum = arr[i] + arr[j] + arr[left] + arr[right];
                            if(sum == target) {
                                quadruplets.add(new ArrayList<Integer>(Arrays.asList(arr[i], arr[j], arr[left], arr[right])));
                                while(left<right && arr[left] == arr[left+1]) left++;
                                while(left<right && arr[right] == arr[right-1]) right--;
                                left++;
                                right--;
                            }
                            else if (sum < target) {
                                left++;
                            }
                            else {
                                right--;
                            }
                        }
                    }
                }
            }
        }
        return quadruplets;
    }
}
