package GrokkingCodingInterviewPatterns.TwoPointer;

import java.util.Arrays;

public class ThreeSumSmallest {

    public static void main (String args[]) {
        int[] arr = {-1, 4, 2, 1, 3};
        System.out.println(ThreeSumSmallest.searchTriplets(arr, 5));
    }

    public static int searchTriplets(int[] arr, int target) {
        int count  = 0;
        if(arr == null || arr.length <3) return -1;
        Arrays.sort(arr);
        for (int i = 0; i < arr.length; i++) {
            if(i == 0 || (i > 0 && arr[i] != arr[i-1])) {
                int left = i+1;
                int right = arr.length - 1;
                while ( left < right) {
                    int sum = arr[i] + arr[left] + arr[right];
                    if(sum < target){
                        // since arr[right] >= arr[left], therefore, we can replace arr[right] by any number between
                        // left and right to get a sum less than the target sum
                        count += right - left;
                        left++;
                    }
                    else {
                        right--;
                    }
                }
            }
        }
        return count;
    }
}
