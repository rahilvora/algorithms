package GrokkingCodingInterviewPatterns.TwoPointer;

public class DutchFlagProblem {

    public static void main (String args[]) {
        int[] arr = {2, 2, 0, 1, 2, 0};
        DutchFlagProblem.sort(arr);
    }

    public static void sort(int[] arr) {
        int zero = 0, second = arr.length - 1;
        for (int i = 0; i <= second;) {
            if(arr[i] == 0) {
                swap(arr, i, zero);
                zero++;
                i++;
            }
            else if (arr[i] == 1) i++;
            else  {
                swap(arr, i, second);
                second--;
            }
        }
        System.out.println(arr.toString());
    }

    public static void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }
}
