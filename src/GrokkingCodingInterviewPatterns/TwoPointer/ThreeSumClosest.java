package GrokkingCodingInterviewPatterns.TwoPointer;

import java.util.Arrays;

public class ThreeSumClosest {

    public static void main (String args[]) {
        int[] arr = {1, 0, 1, 1};
        System.out.println(ThreeSumClosest.searchTriplet(arr, 100));
    }

    public static int searchTriplet(int[] arr, int targetSum) {
        if(arr == null || arr.length <3) return -1;
        Arrays.sort(arr);
        int closest = arr[0] + arr[1] + arr[arr.length - 1];
        for (int i = 0; i < arr.length; i++) {
            if(i == 0 || (i > 0 && arr[i] != arr[i-1])) {
                int left = i+1;
                int right = arr.length - 1;
                while ( left < right) {
                    int sum = arr[i] + arr[left] + arr[right];
                    if(Math.abs(sum - targetSum) < Math.abs(closest - targetSum)) {
                        closest = sum;
                    }
                    while (left < right && arr[left] == arr[left + 1]) left++;
                    while (left < right && arr[right] == arr[right - 1]) right--;
                    if(sum < targetSum) left++;
                    if(sum > targetSum) right--;
                }
            }
        }
        return closest;
    }
}
