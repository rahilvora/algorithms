package GrokkingCodingInterviewPatterns.TwoPointer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ThreeSum {

    public static void main (String args[]) {
        int[] arr = {-3, 0, 1, 2, -1, 1, -2};
        System.out.println(ThreeSum.searchTriplets(arr).toString());
    }

    public static List<List<Integer>> searchTriplets(int[] arr) {
        List<List<Integer>> triplets = new ArrayList<>();
        if (arr == null || arr.length == 0) return triplets;
        Arrays.sort(arr);
        for (int i = 0; i < arr.length - 2; i++) {
            if(i == 0 || (i > 0 && arr[i] != arr[i-1])) {
                int left = i+1, right = arr.length - 1;
                while (left < right) {
                    int sum = arr[i] + arr[left] + arr[right];
                    if(sum == 0) {
                        triplets.add(new ArrayList<Integer>(Arrays.asList(arr[i], arr[left], arr[right])));
                        while(left<right && arr[left] == arr[left+1]) left++;
                        while(left<right && arr[right] == arr[right-1]) right--;
                        left++;
                        right--;
                    }
                    else if (sum < 0) {
                        left++;
                    }
                    else {
                        right--;
                    }
                }
            }
        }
        return triplets;
    }
}
