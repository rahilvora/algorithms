package GrokkingCodingInterviewPatterns.TwoHeaps;

import java.util.PriorityQueue;

public class MaximizeCapital {
    public static int findMaximumCapital(int[] capital, int[] profits, int numberOfProjects, int initialCapital) {
        int size = capital.length;
        PriorityQueue<Integer> minHeap = new PriorityQueue<>(size, (i1, i2) -> capital[i1] - capital[i2]);
        PriorityQueue<Integer> maxHeap = new PriorityQueue<>(size, (i1, i2) -> profits[i2] - profits[i1]);
        for (int i = 0; i < size; i++) {
            minHeap.offer(i);
        }
        int availableCapital = initialCapital;
        for (int i = 0; i < numberOfProjects; i++) {
            while (!minHeap.isEmpty() && capital[minHeap.peek()] <= availableCapital) {
                maxHeap.offer(minHeap.poll());
            }

            if(maxHeap.isEmpty()) break;
            availableCapital += profits[maxHeap.poll()];
        }
        return availableCapital;
    }

    public static void main(String[] args) {
        int result = MaximizeCapital.findMaximumCapital(new int[] { 0, 1, 2 }, new int[] { 1, 2, 3 }, 2, 1);
        System.out.println("Maximum capital: " + result);
        result = MaximizeCapital.findMaximumCapital(new int[] { 0, 1, 2, 3 }, new int[] { 1, 2, 3, 5 }, 3, 0);
        System.out.println("Maximum capital: " + result);
    }
}
