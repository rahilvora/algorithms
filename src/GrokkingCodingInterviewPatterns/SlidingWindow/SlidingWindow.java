package GrokkingCodingInterviewPatterns.SlidingWindow;

/*
* Created by Rahil Vora 07/27/2019
* */

/* Sliding Window Generic approach

    Given an array, find the average of all subarrays of size ‘K’ in it.
    I/P: Array: [1, 3, 2, 6, -1, 4, 1, 8, 2], K=5
    Output: [2.2, 2.8, 2.4, 3.6, 2.8]
 */

public class SlidingWindow {

    public static void main(String args[]){

    }

    public double[] avgSubArraySumSizeK(int[] arr, int k){
        int windowSum = 0;
        int windowStart = 0;
        double[] answer = new double[arr.length - k + 1];
        for(int windowEnd = 0; windowEnd < arr.length; windowEnd++){
            windowSum += arr[windowEnd];
            if(windowEnd >= k) {
                answer[windowStart] = windowSum / k;
                windowSum -= arr[windowStart];
                windowStart++;
            }
        }
        return answer;
    }
}
