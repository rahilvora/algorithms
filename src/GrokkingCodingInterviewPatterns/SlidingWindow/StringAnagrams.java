package GrokkingCodingInterviewPatterns.SlidingWindow;
/*
    Created By Rahil Vora 07/28/2019
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StringAnagrams {
    public static void main (String args[]) {
        System.out.println(StringAnagrams.findStringAnagrams("abbcabc", "abc").toString());
    }

    public static List<Integer> findStringAnagrams (String str, String pattern) {
        List<Integer> resultIndices = new ArrayList<Integer>();
        Map<Character, Integer> charToFreq = new HashMap<>();
        for (char ch: pattern.toCharArray()) {
            charToFreq.put(ch, charToFreq.getOrDefault(ch, 0) + 1);
        }
        int windowStart = 0;
        int matched = 0;
        for (int windowEnd = 0; windowEnd < str.length(); windowEnd++) {
            char rightChar = str.charAt(windowEnd);
            if (charToFreq.containsKey(rightChar)) {
                charToFreq.put(rightChar, charToFreq.get(rightChar) - 1);
                if (charToFreq.get(rightChar) == 0) matched++;
            }

            if (matched == charToFreq.size()) resultIndices.add(windowStart);

            if (windowEnd - windowStart + 1 >= pattern.length()) {
                char leftChar = str.charAt(windowStart);
                if (charToFreq.containsKey(leftChar)) {
                    if(charToFreq.get(leftChar) == 0) matched--;
                    charToFreq.put(leftChar, charToFreq.get(leftChar) + 1);
                }
                windowStart++;
            }
        }

        return resultIndices;
    }
}
