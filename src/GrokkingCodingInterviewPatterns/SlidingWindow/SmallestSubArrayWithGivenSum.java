package GrokkingCodingInterviewPatterns.SlidingWindow;

public class SmallestSubArrayWithGivenSum {
    public static void main (String args[]) {
        int[] arr = {3, 4, 1, 1, 6};
        System.out.println(new SmallestSubArrayWithGivenSum().findMinSubArray(8, arr));
    }
    /*
    The time complexity of the above algorithm will be O(N).
    The outer for loop runs for all elements and the inner while loop processes each element only once,
    therefore the time complexity of the algorithm will be O(N+N)O(N+N) which is asymptotically equivalent to O(N).
     */
    public int findMinSubArray( int S, int[] arr ) {
        int subArraySize = Integer.MAX_VALUE;
        int sum = 0;
        int windowStart = 0;
        for ( int windowEnd = 0; windowEnd < arr.length; windowEnd++ ) {
            sum += arr[windowEnd];
            while( sum >= S) {
                subArraySize = Math.min(subArraySize, windowEnd - windowStart + 1);
                sum -= arr[windowStart];
                windowStart++;
            }
        }
        return subArraySize == Integer.MAX_VALUE ? -1: subArraySize;
    }
}
