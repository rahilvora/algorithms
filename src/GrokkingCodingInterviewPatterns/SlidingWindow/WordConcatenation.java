package GrokkingCodingInterviewPatterns.SlidingWindow;

/*
 * Created by Rahil Vora 07/28/2019
 * */

import java.util.*;

public class WordConcatenation {

    public static void main (String args[]) {
        String[] words = {"cat", "fox"};
        System.out.println(WordConcatenation.findWordConcatenation("catfoxcat", words).toString());
    }

    public static List<Integer> findWordConcatenation(String str, String[] words) {
        List<Integer> resultIndices = new ArrayList<Integer>();
        Map<String, Integer> wordFreq = new HashMap<>();
        for (String word: words) {
            wordFreq.put(word, wordFreq.getOrDefault(word, 0) + 1);
        }
        int wordLen = words[0].length();
        int wordCount = words.length;
        for (int i = 0; i <= str.length() - wordLen*wordCount; i++) {
            Map<String, Integer> wordSeen = new HashMap<>();
            for (int j = 0; j < wordCount; j++) {
                int nextWordIdx = i + j*wordLen;
                String nextWord = str.substring(nextWordIdx, nextWordIdx + wordLen);
                if (!wordFreq.containsKey(nextWord)) break;
                wordSeen.put(nextWord, wordSeen.getOrDefault(nextWord, 0) + 1);
                if(wordSeen.get(nextWord) > wordFreq.getOrDefault(nextWord, 0)) break;

                if(j+1 == wordCount) resultIndices.add(i);
            }
        }
        return resultIndices;
    }
}
