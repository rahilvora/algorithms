package GrokkingCodingInterviewPatterns.SlidingWindow;

/*
    Created By Rahil Vora 07/27/2019
 */

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class NonRepeatingSubString {

    public static void main (String args[]) {
        System.out.println(NonRepeatingSubString.findLength("abccde"));
    }

    // HashSet Approach
    public static int findLength(String str) {
        int maxSubStringLen = -1;
        int windowStart = 0;
        Set<Character> set = new HashSet<>();
        for (int windowEnd = 0; windowEnd < str.length(); windowEnd++) {
            char currChar = str.charAt(windowEnd);
            while (set.contains(currChar)) {
                set.remove(currChar);
                windowStart = windowEnd;
            }
            set.add(currChar);
            maxSubStringLen = Math.max(maxSubStringLen, windowEnd - windowStart + 1);

        }
        return maxSubStringLen;
    }

    // HashMap Approach
    public static int findLengthII (String str) {
        int maxSubStringLen = -1;
        int windowStart = 0;
        Map<Character, Integer> charToIndexMap = new HashMap<>();
        for (int windowEnd = 0; windowEnd < str.length(); windowEnd++) {
            char rightChar = str.charAt(windowEnd);
            if (charToIndexMap.containsKey(rightChar)) {
                windowStart = Math.max(windowStart, charToIndexMap.get(rightChar) + 1);
            }
            charToIndexMap.put(rightChar, windowEnd);
            maxSubStringLen = Math.max(maxSubStringLen, windowEnd - windowStart + 1);
        }
        return maxSubStringLen;
    }
}
