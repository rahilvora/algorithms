package GrokkingCodingInterviewPatterns.SlidingWindow;

/*
    Created By Rahil Vora 07/28/2019
 */

import java.util.HashMap;
import java.util.Map;

public class PermutationInString {

    public static void main(String args[]) {

    }

    public static boolean findPermutation(String str, String pattern) {
        if (pattern == null || pattern.length() == 0) return false;
        Map<Character, Integer> charToFreq = new HashMap<>();
        for (char ch : pattern.toCharArray()) {
            charToFreq.put(ch, charToFreq.getOrDefault(ch, 0) + 1);
        }
        int windowStart = 0;
        int matched = 0;
        for (int windowEnd = 0; windowEnd < str.length(); windowEnd++) {
            char rightChar = str.charAt(windowEnd);
            if (charToFreq.containsKey(rightChar)) {
                charToFreq.put(rightChar, charToFreq.get(rightChar) - 1);
                if (charToFreq.get(rightChar) == 0) matched++;
            }

            if (matched == charToFreq.size()) return true;

            if (windowEnd - windowStart + 1 > pattern.length()) {
                char leftChar = str.charAt(windowStart);
                if (charToFreq.containsKey(leftChar)) {
                    if(charToFreq.get(leftChar) == 0) matched--;
                    charToFreq.put(leftChar, charToFreq.get(leftChar) + 1);
                }
                windowStart++;
            }
        }
        return false;
    }
}