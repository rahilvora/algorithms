package GrokkingCodingInterviewPatterns.SlidingWindow;

/*
 * Created by Rahil Vora 07/27/2019
 * */

import java.util.HashMap;
import java.util.Map;

public class LongestSubStringKDistinctChars {

    public static void main (String args[]) {
        System.out.println( LongestSubStringKDistinctChars.findLength("araaci", 2));
    }

    public static int findLength(String str, int k) {
        if(str == null || str.length() == 0) return -1;
        int windowStart = 0;
        int maxLen = -1;
        Map<Character, Integer> charFrequency = new HashMap<>();
        for (int windowEnd = 0; windowEnd < str.length(); windowEnd++) {
            char currChar = str.charAt(windowEnd);
            charFrequency.put(currChar, charFrequency.getOrDefault(currChar, 0) + 1);
            while (charFrequency.size() >= k) {
                char leftChar = str.charAt(windowStart);
                charFrequency.put(leftChar, charFrequency.get(leftChar) - 1);
                if (charFrequency.get(leftChar) == 0) {
                    charFrequency.remove(leftChar);
                }
                windowStart++;
            }
            maxLen = Math.max(maxLen, windowEnd - windowStart + 1);
        }
        return maxLen;
    }
}
