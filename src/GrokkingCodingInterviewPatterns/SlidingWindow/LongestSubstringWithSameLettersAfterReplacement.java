package GrokkingCodingInterviewPatterns.SlidingWindow;

/*
    Created By Rahil Vora 07/27/2019
 */

import java.util.HashMap;
import java.util.Map;

public class LongestSubstringWithSameLettersAfterReplacement {

    public static void main (String args[]) {

    }

    public static int findLength(String str, int k) {
        int maxLength = 0;
        Map<Character, Integer> charToFreqMap = new HashMap<>();
        int windowStart = 0;
        int maxRepeatFreq = 0;
        for (int windowEnd = 0; windowEnd < str.length(); windowEnd++) {
            char rightChar = str.charAt(windowEnd);
            charToFreqMap.put(rightChar, charToFreqMap.getOrDefault(rightChar, 0) + 1);
            maxRepeatFreq = Math.max(maxRepeatFreq, charToFreqMap.get(rightChar));
            if (windowEnd - windowStart + 1 - maxRepeatFreq > k) {
               char leftChar = str.charAt(windowStart);
               charToFreqMap.put(leftChar, charToFreqMap.get(leftChar) - 1);
               windowStart++;
            }
            maxLength = Math.max(maxLength, windowEnd - windowStart + 1);
        }
        return maxLength;
    }
}
