package GrokkingCodingInterviewPatterns.SlidingWindow;

/*
    Created By Rahil Vora 07/27/2019
*/

public class MaximumSumSubarray {
    public static void main(String args[]) {
        int[] arr = {2, 3, 4, 1, 5};
        System.out.println(new MaximumSumSubarray().maxSumSubArray(arr, 2));
    }

    public int maxSumSubArray(int[] arr, int k) {
        int maxSum = Integer.MIN_VALUE;
        int start = 0;
        int currentSum = 0;
        for (int i = 0; i < arr.length; i++) {
            currentSum += arr[i];
            if (i >= k-1) {
                maxSum = Math.max(maxSum, currentSum);
                currentSum -= arr[start];
                start++;
            }
        }
        return maxSum;
    }
}
