package GrokkingCodingInterviewPatterns.SlidingWindow;

/*
 * Created by Rahil Vora 07/27/2019
 * */

import java.util.HashMap;
import java.util.Map;

public class FruitInBasket {

    public static void main (String args[]) {
        char[] arr = { 'C', 'A', 'C', 'B', 'A' };
        System.out.println(FruitInBasket.findLength(arr));
    }

    public static int findLength (char[] arr) {
        int maxFruits = -1;
        int windowStart = 0;
        int windowEnd = 0;
        Map<Character, Integer> map = new HashMap<>();
        for (windowEnd = 0; windowEnd < arr.length; windowEnd++) {
            char currChar = arr[windowEnd];
            map.put(currChar, map.getOrDefault(currChar, 0) + 1);
            while (map.size() > 2) {
                char leftChar = arr[windowStart];
                map.put(leftChar, map.get(leftChar) - 1);
                if(map.get(leftChar) ==0) {
                    map.remove(leftChar);
                }
                windowStart++;
            }
            maxFruits = Math.max(maxFruits, windowEnd - windowStart + 1);
        }
        return maxFruits;
    }
}
