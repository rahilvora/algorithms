package GrokkingCodingInterviewPatterns.BFS;

import Trees.TreeNode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class ZigZagTraversal {
    public static List<List<Integer>> traverse(TreeNode root) {
        List<List<Integer>> result = new LinkedList<List<Integer>>();
        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(root);
        boolean isEven = true;
        while (!queue.isEmpty()) {
            int level = queue.size();
            List<Integer> temp = new ArrayList<>();
            while (level > 0) {
                TreeNode curr = queue.poll();
                if (curr.left != null) queue.offer(curr.left);
                if (curr.right != null) queue.offer(curr.right);
                if ((isEven)) {
                    temp.add(curr.val);
                } else {
                    temp.add(0, curr.val);
                }
                level--;
            }
            isEven = !isEven;
            result.add(temp);
        }
        return result;
    }
}
