package GrokkingCodingInterviewPatterns.BFS;

import Trees.TreeNode;

import java.util.LinkedList;
import java.util.Queue;

public class LevelOrderSuccessor {
    public static void main (String args[]) {

    }

    public static TreeNode findSuccessor(TreeNode root, int key) {
        if (root == null) return null;
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        while (!queue.isEmpty()) {
            TreeNode curr = queue.poll();
            if (curr.left != null) queue.offer(curr.left);
            if (curr.right != null) queue.offer(curr.right);
            if(curr.val == key) return queue.peek();

        }
        return null;
    }
}
