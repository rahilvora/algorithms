package GrokkingCodingInterviewPatterns.BFS;

import java.util.LinkedList;
import java.util.Queue;

public class ConnectAllLevelSibling {
    public static void connectII(TreeNode root) {
        if (root == null)
            return;

        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        TreeNode previousLevelEnd = queue.peek();
        while (!queue.isEmpty()) {
            TreeNode previousNode = null;
            int levelSize = queue.size();
            // connect all nodes of this level
            for (int i = 0; i < levelSize; i++) {
                TreeNode currentNode = queue.poll();
                if(i == levelSize - 1) previousLevelEnd = currentNode;
                if (previousNode != null)
                    previousNode.next = currentNode;
                previousNode = currentNode;

                // insert the children of current node in the queue
                if (currentNode.left != null)
                    queue.offer(currentNode.left);
                if (currentNode.right != null)
                    queue.offer(currentNode.right);
            }
            previousLevelEnd.next = queue.peek();
        }
    }
}
