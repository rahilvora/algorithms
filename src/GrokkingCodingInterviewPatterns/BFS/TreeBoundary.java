package GrokkingCodingInterviewPatterns.BFS;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class TreeBoundary {
    public static boolean isLeaf(TreeNode t) {
        return t.left == null && t.right == null;
    }

    public static void addLeaves(List<TreeNode> res, TreeNode root) {
        if (isLeaf(root)) {
            res.add(root);
        } else {
            if (root.left != null) {
                addLeaves(res, root.left);
            }
            if (root.right != null) {
                addLeaves(res, root.right);
            }
        }
    }
    public static List<TreeNode> findBoundary(TreeNode root) {
        List<TreeNode> res = new ArrayList<>();
        if (root == null) {
            return res;
        }
        if (!isLeaf(root)) {
            res.add(root);
        }
        TreeNode t = root.left;
        while (t != null) {
            if (!isLeaf(t)) {
                res.add(t);
            }
            if (t.left != null) {
                t = t.left;
            } else {
                t = t.right;
            }

        }
        addLeaves(res, root);
        Stack<TreeNode> s = new Stack<>();
        t = root.right;
        while (t != null) {
            if (!isLeaf(t)) {
                s.push(t);
            }
            if (t.right != null) {
                t = t.right;
            } else {
                t = t.left;
            }
        }
        while (!s.empty()) {
            res.add(s.pop());
        }
        return res;
    }
}
