package GrokkingCodingInterviewPatterns.BFS;

import java.util.LinkedList;
import java.util.Queue;

class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;
    TreeNode next;

    TreeNode(int x) {
        val = x;
        left = right = next = null;
    }

    // level order traversal using 'next' pointer
    void printLevelOrder() {
        TreeNode nextLevelRoot = this;
        while (nextLevelRoot != null) {
            TreeNode current = nextLevelRoot;
            nextLevelRoot = null;
            while (current != null) {
                System.out.print(current.val + " ");
                if (nextLevelRoot == null) {
                    if (current.left != null)
                        nextLevelRoot = current.left;
                    else if (current.right != null)
                        nextLevelRoot = current.right;
                }
                current = current.next;
            }
            System.out.println();
        }
    }
}

public class ConnectLevelOrderSibling {

    public static void main (String args[]) {

    }

    public static void connect(TreeNode root) {
        TreeNode current = root;
        TreeNode previous = null;
        TreeNode head = null;
        while (current != null) {
            while (current != null) {
                if (current.left != null){
                    if (previous != null) {
                        previous.next = current.left;
                    }
                    else {
                        head = current.left;
                    }
                    previous = current.left;
                }
                if (current.right != null) {
                    if(previous != null) {
                        previous.next = current.right;
                    }
                    else {
                        head = current.right;
                    }
                    previous = current.right;
                }
                current = current.next;
            }
            current = head;
            previous = null;
            head = null;
        }
    }

    // Simple approach but with extra space.
    public static void connectII(TreeNode root) {
        if (root == null)
            return;

        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        while (!queue.isEmpty()) {
            TreeNode previousNode = null;
            int levelSize = queue.size();
            // connect all nodes of this level
            for (int i = 0; i < levelSize; i++) {
                TreeNode currentNode = queue.poll();
                if (previousNode != null)
                    previousNode.next = currentNode;
                previousNode = currentNode;

                // insert the children of current node in the queue
                if (currentNode.left != null)
                    queue.offer(currentNode.left);
                if (currentNode.right != null)
                    queue.offer(currentNode.right);
            }
        }
    }
}
