package GrokkingCodingInterviewPatterns.CyclicSort;

import java.util.Arrays;

public class CyclicSort {
    public static void main (String args[]) {
        int[] nums = {5, 4, 1, 2, 3, 6};
        CyclicSort.sort(nums);
    }

    public static void sort(int[] nums) {
        int i = 0;
        while (i < nums.length) {
            int j = nums[i] - 1;
            if (j != i) {
                swap(nums, i, j);
            }
            else {
                i++;
            }
        }
        System.out.println(Arrays.toString(nums));;
    }

    public static void swap(int[] nums, int i, int j) {
        int temp = nums[i];
        nums[i] = nums[j];
        nums[j] = temp;
    }
}
