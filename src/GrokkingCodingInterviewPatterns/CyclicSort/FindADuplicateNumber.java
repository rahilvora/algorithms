package GrokkingCodingInterviewPatterns.CyclicSort;

public class FindADuplicateNumber {
    public static void main (String args[]) {
        System.out.println(FindADuplicateNumber.findNumber(new int[] { 1, 4, 4, 3, 2 }));
        System.out.println(FindADuplicateNumber.findNumber(new int[] { 2, 1, 3, 3, 5, 4 }));
        System.out.println(FindADuplicateNumber.findNumber(new int[] { 2, 4, 1, 4, 4 }));
    }

    public static int findNumber(int[] nums) {
        int start = 0;
        while (start < nums.length) {
            if (start+1 != nums[start]) {
                if ( nums[start] != nums[nums[start] - 1]){
                    CyclicSort.swap(nums, start, nums[start] - 1);
                }
                else return nums[start];
            }
            else start++;
        }
        return -1;
    }
}
