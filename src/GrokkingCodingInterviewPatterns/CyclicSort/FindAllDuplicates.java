package GrokkingCodingInterviewPatterns.CyclicSort;

import GrokkingCodingInterviewPatterns.TwoPointer.PairWithTargetSum;

import java.util.ArrayList;
import java.util.List;

public class FindAllDuplicates {

    public static void main (String args[]) {
        List<Integer> duplicates = FindAllDuplicates.findNumbers(new int[] { 3, 4, 4, 5, 5 });
        System.out.println("Duplicates are: " + duplicates);

        duplicates = FindAllDuplicates.findNumbers(new int[] { 4,3,2,7,8,2,3,1});
        System.out.println("Duplicates are: " + duplicates);
    }

    public static List<Integer> findNumbers(int[] nums) {
        List<Integer> duplicateNumbers = new ArrayList<>();
        int start = 0;
        while (start < nums.length) {
            if (start + 1 != nums[start]) {
                if (nums[start] != nums[nums[start] - 1]){
                    CyclicSort.swap(nums, start, nums[start] - 1);
                }
                else {
                    duplicateNumbers.add(nums[start]);
                    start++;
                }
            }
            else {
                start++;
            }
        }
        return duplicateNumbers;
    }
}
