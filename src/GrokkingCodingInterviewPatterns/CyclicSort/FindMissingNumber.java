package GrokkingCodingInterviewPatterns.CyclicSort;

public class FindMissingNumber {
    public static void main (String args[]) {
        int[] nums = {4, 0, 3, 1};
        FindMissingNumber.findMissingNumber(nums);
    }

    public static int findMissingNumber(int[] nums) {
        int i = 0;
        while (i < nums.length) {
            if (nums[i] < nums.length && nums[i] != nums[nums[i]])
                CyclicSort.swap(nums, i, nums[i]);
            else
                i++;
        }

        // find the first number missing from its index, that will be our required number
        for (i = 0; i < nums.length; i++)
            if (nums[i] != i)
                return i;

        return nums.length;
    }
}
