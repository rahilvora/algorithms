package GrokkingCodingInterviewPatterns.CyclicSort;

import java.util.ArrayList;
import java.util.List;

public class FindAllMissingNumber {
    public static void main (String arg[]) {
        List<Integer> missing = FindAllMissingNumber.findNumbers(new int[] { 2, 3, 1, 8, 2, 3, 5, 1 });
        System.out.println("Missing numbers: " + missing);

        missing = FindAllMissingNumber.findNumbers(new int[] { 2, 4, 1, 2 });
        System.out.println("Missing numbers: " + missing);

        missing = FindAllMissingNumber.findNumbers(new int[] { 2, 3, 2, 1 });
        System.out.println("Missing numbers: " + missing);

    }

    public static List<Integer> findNumbers(int[] nums) {
        List<Integer> missingNumbers = new ArrayList<>();
        int start = 0;
        while (start < nums.length) {
            int j = nums[start] - 1;
            if (j != start && nums[j] != nums[start]) {
                CyclicSort.swap(nums, start, j);
            }
            else {
                start++;
            }
        }
        for (int i = 0; i < nums.length; i++)
            if (nums[i] - 1!= i) {
                missingNumbers.add(i + 1);
            }
        return missingNumbers;
    }
}
