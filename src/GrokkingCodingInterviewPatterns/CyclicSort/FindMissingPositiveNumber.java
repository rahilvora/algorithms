package GrokkingCodingInterviewPatterns.CyclicSort;

public class FindMissingPositiveNumber {

    public static void main (String args[]) {
        System.out.println(FindMissingPositiveNumber.findNumber(new int[] { -3, 1, 5, 4, 2 }));
        System.out.println(FindMissingPositiveNumber.findNumber(new int[] { 3, -2, 0, 1, 2 }));
        System.out.println(FindMissingPositiveNumber.findNumber(new int[] { 3, 2, 5, 1 }));
    }

    public static int findNumber(int[] nums) {
        int answer = Integer.MAX_VALUE;
        int start = 0;
        while (start < nums.length) {
            if (nums[start] > 0 && nums[start] <= nums.length && nums[start] != nums[nums[start] - 1]) {
                CyclicSort.swap(nums, start, nums[start] - 1);
            }else start++;
        }

        for (int i = 0; i < nums.length; i++) {
            if(nums[i] != i+1){
                answer = Math.min(answer, i+1);
            }
        }
        return answer;
    }
}
