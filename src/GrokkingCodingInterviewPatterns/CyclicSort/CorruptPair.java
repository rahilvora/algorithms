package GrokkingCodingInterviewPatterns.CyclicSort;

public class CorruptPair {
    public static void main (String args[]) {
        int[] nums = CorruptPair.findNumbers(new int[] { 3, 1, 2, 5, 2 });
        System.out.println(nums[0] + ", " + nums[1]);
        nums = CorruptPair.findNumbers(new int[] { 3, 1, 2, 3, 6, 4 });
        System.out.println(nums[0] + ", " + nums[1]);
    }

    public static int[] findNumbers(int[] nums) {
        int start = 0;
        while (start < nums.length) {
            if (nums[start] != nums[nums[start] - 1]) {
                CyclicSort.swap(nums, start, nums[start] - 1);
            }
            else {
                start++;
            }
        }
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] != i + 1) {
                return new int[] { nums[i], i+1 };
            }
        }
        return new int[] { -1, -1 };
    }
}
