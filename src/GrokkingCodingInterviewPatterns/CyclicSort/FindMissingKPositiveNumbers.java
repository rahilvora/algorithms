package GrokkingCodingInterviewPatterns.CyclicSort;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class FindMissingKPositiveNumbers {

    public static void main (String args[]) {

    }

    public static List<Integer> findNumbers(int[] nums, int k) {
        List<Integer> missingNumbers = new ArrayList<>();
        Set<Integer> extraNumbers = new HashSet<>();
        int answer = Integer.MAX_VALUE;
        int start = 0;
        while (start < nums.length) {
            if (nums[start] > 0 && nums[start] <= nums.length && nums[start] != nums[nums[start] - 1]) {
                CyclicSort.swap(nums, start, nums[start] - 1);
            }else start++;
        }

        for (int i = 0; i < nums.length; i++) {
            if(nums[i] != i+1){
                missingNumbers.add(i+1);
                extraNumbers.add(nums[i]);
            }
        }
        // add the remaining missing numbers
        for (int i = 1; missingNumbers.size() < k; i++) {
            int candidateNumber = i + nums.length;
            // ignore if the array contains the candidate number
            if (!extraNumbers.contains(candidateNumber))
                missingNumbers.add(candidateNumber);
        }
        return missingNumbers;
    }
}
