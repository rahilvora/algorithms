package GrokkingCodingInterviewPatterns.KWayMerge;

import java.awt.*;
import java.util.Comparator;
import java.util.PriorityQueue;

class ListNode {
    int value;
    ListNode next;

    ListNode(int value) {
        this.value = value;
    }
}

// O(N*logK)
public class MergeKSortedLists {
    public static ListNode merge(ListNode[] lists) {
        ListNode result = new ListNode(-1);
        ListNode temp = result;
        PriorityQueue<ListNode> pq = new PriorityQueue<>(new Comparator<ListNode>() {
            @Override
            public int compare(ListNode o1, ListNode o2) {
                return o1.value - o2.value;
            }
        });
        for (ListNode list: lists) {
            pq.offer(list);
        }
        while (!pq.isEmpty()) {
            ListNode currList = pq.poll();
            temp.next = new ListNode(currList.value);
            temp = temp.next;
            currList = currList.next;
            if (currList != null)
                pq.offer(currList);
        }
        return result.next;
    }

    public static void main(String[] args) {
        ListNode l1 = new ListNode(2);
        l1.next = new ListNode(6);
        l1.next.next = new ListNode(8);

        ListNode l2 = new ListNode(3);
        l2.next = new ListNode(6);
        l2.next.next = new ListNode(7);

        ListNode l3 = new ListNode(1);
        l3.next = new ListNode(3);
        l3.next.next = new ListNode(4);

        ListNode result = MergeKSortedLists.merge(new ListNode[] { l1, l2, l3 });
        System.out.print("Here are the elements form the merged list: ");
        while (result != null) {
            System.out.print(result.value + " ");
            result = result.next;
        }
    }

}
