package GrokkingCodingInterviewPatterns.KWayMerge;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;

class KthSmallestInMSortedArrays {

    public static int findKthSmallest(List<Integer[]> lists, int k) {
        PriorityQueue<Node> maxHeap = new PriorityQueue<>(new Comparator<Node>() {
            @Override
            public int compare(Node o1, Node o2) {
                return lists.get(o2.arrayIndex)[o2.elementIndex] - lists.get(o1.arrayIndex)[o1.elementIndex];
            }
        });

        for (int i = 0; i < lists.size(); i++) {
            maxHeap.offer(new Node(0, i));
        }

        int numberOfElement = 0, result = 0;
        while (!maxHeap.isEmpty()) {
            Node node = maxHeap.poll();
            result = lists.get(node.arrayIndex)[node.elementIndex];
            if (++numberOfElement == k) break;
            node.elementIndex++;
            if (node.elementIndex < lists.get(node.arrayIndex).length){
                maxHeap.add(node);
            }
        }

        return result;
    }

    public static void main(String[] args) {
        Integer[] l1 = new Integer[] { 2, 6, 8 };
        Integer[] l2 = new Integer[] { 3, 6, 7 };
        Integer[] l3 = new Integer[] { 1, 3, 4 };
        List<Integer[]> lists = new ArrayList<Integer[]>();
        lists.add(l1);
        lists.add(l2);
        lists.add(l3);
        int result = KthSmallestInMSortedArrays.findKthSmallest(lists, 5);
        System.out.print("Kth smallest number is: " + result);
    }
}
