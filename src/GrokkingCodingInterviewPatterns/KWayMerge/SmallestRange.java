package GrokkingCodingInterviewPatterns.KWayMerge;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;

class Node {
    int elementIndex;
    int arrayIndex;

    Node(int elementIndex, int arrayIndex) {
        this.elementIndex = elementIndex;
        this.arrayIndex = arrayIndex;
    }
}

public class SmallestRange {
    public static int[] findSmallestRange(List<Integer[]> lists) {
        PriorityQueue<Node> pq = new PriorityQueue<>(new Comparator<Node>() {
            @Override
            public int compare(Node o1, Node o2) {
                return lists.get(o1.arrayIndex)[o1.elementIndex] - lists.get(o2.arrayIndex)[o2.elementIndex];
            }
        });

        int rangeStart = 0, rangeEnd = Integer.MAX_VALUE, currMax = Integer.MIN_VALUE;

        for (int i = 0; i < lists.size(); i++) {
            if (lists.get(i) != null) {
                pq.add(new Node(0, i));
                currMax = Math.max(currMax, lists.get(i)[0]);
            }
        }

        while (pq.size() == lists.size()) {
            Node currNode = pq.poll();
            if (rangeEnd - rangeStart > currMax - lists.get(currNode.arrayIndex)[currNode.elementIndex]){
                rangeEnd = currMax;
                rangeStart = lists.get(currNode.arrayIndex)[currNode.elementIndex];
            }
            currNode.elementIndex++;
            if (lists.get(currNode.arrayIndex).length > currNode.elementIndex) {
                pq.add(currNode); // insert the next element in the heap
                currMax = Math.max(currMax, lists.get(currNode.arrayIndex)[currNode.elementIndex]);
            }
        }
        return new int[] { rangeStart, rangeEnd };
    }

    public static void main(String[] args) {
        Integer[] l1 = new Integer[] { 1, 5, 8 };
        Integer[] l2 = new Integer[] { 4, 12 };
        Integer[] l3 = new Integer[] { 7, 8, 10 };
        List<Integer[]> lists = new ArrayList<Integer[]>();
        lists.add(l1);
        lists.add(l2);
        lists.add(l3);
        int[] result = SmallestRange.findSmallestRange(lists);
        System.out.print("Smallest range is: [" + result[0] + ", " + result[1] + "]");
    }
}
