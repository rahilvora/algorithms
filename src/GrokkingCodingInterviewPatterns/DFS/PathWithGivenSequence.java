package GrokkingCodingInterviewPatterns.DFS;

import Trees.TreeNode;
import com.sun.source.tree.Tree;

public class PathWithGivenSequence {

    public static void main(String[] args) {
        TreeNode root = new TreeNode(1);
        root.left = new TreeNode(0);
        root.right = new TreeNode(1);
        root.left.left = new TreeNode(1);
        root.right.left = new TreeNode(6);
        root.right.right = new TreeNode(5);

        System.out.println("Tree has path sequence: " + PathWithGivenSequence.findPath(root, new int[] { 1, 0, 7 }));
        System.out.println("Tree has path sequence: " + PathWithGivenSequence.findPath(root, new int[] { 1, 1, 6 }));
    }

    public static boolean findPath(TreeNode root, int[] sequence) {
        int sequenceInt = 0;
        for (int seq: sequence) {
            sequenceInt = sequenceInt*10 + seq;
        }
        return backTrack(root, sequenceInt, 0);
    }

    public static boolean backTrack (TreeNode root, int answer, int sum) {
        if (root == null) return false;
        if (root != null && root.val+10*sum == answer && root.left == null && root.right == null) {
            return true;
        }
        return backTrack(root.left, answer, 10*sum + root.val) || backTrack(root.right, answer, 10*sum + root.val);
    }
}
