package GrokkingCodingInterviewPatterns.DFS;

import Trees.TreeNode;

import java.util.ArrayList;
import java.util.List;

public class SumOfPathNumbers {

    public static void main (String args[]) {
        TreeNode root = new TreeNode(1);
        root.left = new TreeNode(0);
        root.right = new TreeNode(1);
        root.left.left = new TreeNode(1);
        root.right.left = new TreeNode(6);
        root.right.right = new TreeNode(5);
        System.out.println("Total Sum of Path Numbers: " + SumOfPathNumbers.findSumOfPathNumbers(root));
    }

    public static int findSumOfPathNumbers(TreeNode root) {
        int answer = 0;
        List<String> list = new ArrayList<>();
        backTrack(root, list, "");
        for (String str: list) {
            answer += Integer.valueOf(str);
        }
        return answer;
    }

    public static void backTrack(TreeNode root, List<String> list, String pathString) {
        if (root == null) return;
        if (root != null && root.left == null && root.right == null) {
            list.add(pathString+root.val);
        }
        else {
            backTrack(root.left, list, pathString+root.val);
            backTrack(root.right, list, pathString+root.val);
        }
    }

    private static int findRootToLeafPathNumbers(TreeNode currentNode, int pathSum) {
        if (currentNode == null)
            return 0;

        // calculate the path number of the current node
        pathSum = 10 * pathSum + currentNode.val;

        // if the current node is a leaf, return the current path sum.
        if (currentNode.left == null && currentNode.right == null) {
            return pathSum;
        }

        // traverse the left and the right sub-tree
        return findRootToLeafPathNumbers(currentNode.left, pathSum) +
                findRootToLeafPathNumbers(currentNode.right, pathSum);
    }
}
