package GrokkingCodingInterviewPatterns.DFS;

import Trees.TreeNode;

import java.util.ArrayList;
import java.util.List;

public class AllPathSums {

    public static void main (String args[]) {
        TreeNode root = new TreeNode(12);
        root.left = new TreeNode(7);
        root.right = new TreeNode(1);
        root.left.left = new TreeNode(4);
        root.right.left = new TreeNode(10);
        root.right.right = new TreeNode(5);
        int sum = 23;
        List<List<Integer>> result = AllPathSums.findPaths(root, sum);
        System.out.println("Tree paths with sum " + sum + ": " + result);
    }

    public static List<List<Integer>> findPaths(TreeNode root, int sum) {
        List<List<Integer>> allPaths = new ArrayList<>();
        backTrack(root, sum, allPaths, new ArrayList<>());
        return allPaths;
    }

    public static void backTrack(TreeNode root, int sum, List<List<Integer>> allPaths, List<Integer> temp) {
        if (root == null) return;
        temp.add(root.val);
        if (root.val == sum && root.left == null && root.right == null) {
            allPaths.add(new ArrayList<>(temp));
        }
        else {
            backTrack(root.left, sum - root.val, allPaths, temp);
            backTrack(root.right, sum - root.val, allPaths, temp);
        }
        temp.remove(temp.size() -1);
    }


}
