package GrokkingCodingInterviewPatterns.DFS;

import Trees.TreeNode;

public class TreeDiameter {

    private static int treeDiameter = 0;

    public static int findDiameter(TreeNode root) {
        calculateHeight(root);
        return treeDiameter;
    }

    public static int calculateHeight(TreeNode root) {
        if (root == null) return 0;
        int leftHeight = calculateHeight(root.left);
        int rightHeight = calculateHeight(root.right);
        int diameter = leftHeight + rightHeight + 1;
        treeDiameter = Math.max(diameter, treeDiameter);
        return Math.max(leftHeight, rightHeight) + 1;
    }
}
