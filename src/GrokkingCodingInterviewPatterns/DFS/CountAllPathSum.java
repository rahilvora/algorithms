package GrokkingCodingInterviewPatterns.DFS;

import Trees.TreeNode;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class CountAllPathSum {

    public static void main(String[] args) {
        TreeNode root = new TreeNode(12);
        root.left = new TreeNode(7);
        root.right = new TreeNode(1);
        root.left.left = new TreeNode(4);
        root.right.left = new TreeNode(10);
        root.right.right = new TreeNode(5);
        System.out.println("Tree has path: " + CountAllPathSum.countPaths(root, 11));
    }

    public static int countPaths(TreeNode root, int S) {
        return backTrack(root, S, new ArrayList<>());
    }
    public static int backTrack(TreeNode root, int S, List<Integer> currPath) {
        if (root == null) return 0;

        currPath.add(root.val);

        int pathSum = 0, pathCount = 0;
        ListIterator<Integer> currPathIterator = currPath.listIterator(currPath.size());
        while (currPathIterator.hasPrevious()) {
            pathSum += currPathIterator.previous();
            if (pathSum == S) pathCount++;
        }

        pathCount += backTrack(root.left, S, currPath);
        pathCount += backTrack(root.right, S, currPath);
        currPath.remove(currPath.size() - 1);
        return pathCount;
    }
}
