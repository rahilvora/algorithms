package GrokkingCodingInterviewPatterns.ModifiedBinarySearch;

public class FindRange {
    public static int[] findRange(int[] arr, int key) {
        int[] result = new int[] { -1, -1 };
        int n = arr.length - 1;
        if (key < arr[0] || key > arr[n]) return result;
        result[0] = binarySearch(arr, key, true);
        result[1] = binarySearch(arr, key, false);
        return result;
    }

    public static int binarySearch(int[] arr, int key, boolean moveLeft) {
        int n = arr.length - 1;
        int low = 0, high = n;
        int keyIndex = -1;
        while (low <= high) {
            int mid = (low + high) / 2;
            if (arr[mid] < key) {
                low = mid+1;
            }
            else if (arr[mid] > key) {
                high = mid - 1;
            }
            else {
                keyIndex = mid;
                if (moveLeft) {
                    high--;
                }
                else {
                    low++;
                }

            }
        }
        return keyIndex;
    }

    public static void main(String[] args) {
        int[] result = FindRange.findRange(new int[] { 4, 6, 6, 6, 9 }, 6);
        System.out.println("Range: [" + result[0] + ", " + result[1] + "]");
        result = FindRange.findRange(new int[] { 1, 3, 8, 10, 15 }, 10);
        System.out.println("Range: [" + result[0] + ", " + result[1] + "]");
        result = FindRange.findRange(new int[] { 1, 3, 8, 10, 15 }, 12);
        System.out.println("Range: [" + result[0] + ", " + result[1] + "]");
    }
}
