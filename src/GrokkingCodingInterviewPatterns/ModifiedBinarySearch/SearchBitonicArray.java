package GrokkingCodingInterviewPatterns.ModifiedBinarySearch;

public class SearchBitonicArray {
    public static int search(int[] arr, int key) {
        int n = arr.length;
        int low = 0, high = n-1;
        while (low < high) {
            int mid = low + (high - low) / 2;
            if (arr[mid] < arr[mid + 1]) {
                if (arr[mid + 1] <= key){
                    low = mid + 1;
                }
                else {
                    high = mid;
                }
            }
            else if(arr[mid] > arr[mid + 1]) {
                if (arr[mid + 1] >= key){
                    low = mid + 1;
                }
                else {
                    high = mid;
                }
            }
        }
        if (arr[low] == key) return low;
        return -1;
    }

    // Other Approach
    /*
    * 1. Find Max element in an array
    * 2. Search from 0, maxElement Index and from maxElementIndex to end of array
    * */

    public static int searchII(int[] arr, int key) {
        int maxIndex = findMax(arr);
        int keyIndex = binarySearch(arr, key, 0, maxIndex);
        if (keyIndex != -1) return keyIndex;
        return binarySearch(arr, key, maxIndex, arr.length - 1);

    }

    // find index of the maximum value in a bitonic array
    public static int findMax(int[] arr) {
        int start = 0, end = arr.length - 1;
        while (start < end) {
            int mid = start + (end - start) / 2;
            if (arr[mid] > arr[mid + 1]) {
                end = mid;
            } else {
                start = mid + 1;
            }
        }
        // at the end of the while loop, 'start == end'
        return start;
    }

    public static int binarySearch(int[] arr, int key, int start, int end) {
        while (start <= end) {
            int mid = (start + end) / 2;
            if (arr[mid] == key) return mid;
            if (arr[start] < arr[end]){
                if (arr[mid] > key) {
                    end = mid - 1;
                }
                else {
                    start = mid + 1;
                }
            }
            else {
                if (arr[mid] < key) {
                    end = mid - 1;
                }
                else {
                    start = mid + 1;
                }

            }
        }
        return -1;
    }

    public static void main(String[] args) {
        System.out.println(SearchBitonicArray.searchII(new int[] { 1, 3, 8, 4, 3 }, 4));
        System.out.println(SearchBitonicArray.searchII(new int[] { 3, 8, 3, 1 }, 8));
        System.out.println(SearchBitonicArray.searchII(new int[] { 1, 3, 8, 12 }, 12));
        System.out.println(SearchBitonicArray.searchII(new int[] { 10, 9, 8 }, 10));
        System.out.println(SearchBitonicArray.searchII(new int[] { 1,2,3,4,10}, 10));
    }
}
