package GrokkingCodingInterviewPatterns.ModifiedBinarySearch;

public class BinarySearch {
    public static void main (String args[]) {
        System.out.println(BinarySearch.search(new int[] { 4, 6, 10 }, 10));
        System.out.println(BinarySearch.search(new int[] { 1, 2, 3, 4, 5, 6, 7 }, 5));
        System.out.println(BinarySearch.search(new int[] { 10, 6, 4 }, 10));
        System.out.println(BinarySearch.search(new int[] { 10, 6, 4 }, 4));
    }
    public static int search(int[] arr, int key) {
        int low = 0, high = arr.length - 1;
        boolean isAscending = true;
        if (arr[low] > arr[high]) isAscending = false;
        while (low <= high) {
            int mid = (low + high) / 2;
            if (arr[mid] == key) return mid;
            if (arr[mid] > key && isAscending || (arr[mid] < key && !isAscending)) high--;
            if (arr[mid] < key && isAscending || (arr[mid] > key && !isAscending)) low++;
        }
        return -1;
    }
}
