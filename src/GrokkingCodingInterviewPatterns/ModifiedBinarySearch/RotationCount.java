package GrokkingCodingInterviewPatterns.ModifiedBinarySearch;

public class RotationCount {
    public static int countRotations(int[] arr) {
        if (arr == null || arr.length == 0) return -1;

        int n = arr.length;
        int low = 0, high = n -1;

        while ( low < high) {
            int mid = (low + high) / 2;
            if (mid < high && arr[mid] > arr[mid + 1])
                return mid + 1;
            if (mid > low && arr[mid - 1] > arr[mid])
                return mid;

            if (arr[low] == arr[mid] && arr[high] == arr[mid]) {
                if (arr[low] > arr[low + 1]) // if element at low+1 is not the smallest
                    return low + 1;
                ++low;
                if (arr[high - 1] > arr[high]) // if the element at high is not the smallest
                    return high;
                --high;
                // left side is sorted, so the pivot is on right side
            } else if (arr[low] < arr[mid] || (arr[low] == arr[mid] && arr[mid] > arr[high])) {
                low = mid + 1;
            } else { // right side is sorted, so the pivot is on the left side
                high = mid - 1;
            }

        }
        return 0;
    }
    public static void main(String[] args) {
        System.out.println(RotationCount.countRotations(new int[] { 3, 3, 7, 3 }));
    }
}
