package GrokkingCodingInterviewPatterns.ModifiedBinarySearch;

public class MinimumDifference {

    public static int searchMinDiffElement(int[] arr, int key) {
        int answer = Integer.MAX_VALUE;
        int n = arr.length;
        if (key< arr[0]) return arr[0];
        if (key > arr[n - 1]) return arr[n-1];
        int low = 0, high = n - 1;
        while (low <= high) {
            int mid = (low + high) / 2;
            if (arr[mid] > key) {
                high = mid - 1;
            }
            else if (arr[mid] < key) {
                low = mid + 1;
            }
            else {
                return arr[mid];
            }
        }
        if (arr[low] - key < key - arr[high])
            return arr[low];
        return arr[high];
    }

    public static void main(String[] args) {
        System.out.println(MinimumDifference.searchMinDiffElement(new int[] { 4, 6, 10 }, 7));
        System.out.println(MinimumDifference.searchMinDiffElement(new int[] { 4, 6, 10 }, 4));
        System.out.println(MinimumDifference.searchMinDiffElement(new int[] { 1, 3, 8, 10, 15 }, 12));
        System.out.println(MinimumDifference.searchMinDiffElement(new int[] { 4, 6, 10 }, 17));
    }
}
