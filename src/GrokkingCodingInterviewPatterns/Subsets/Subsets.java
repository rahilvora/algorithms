package GrokkingCodingInterviewPatterns.Subsets;

import java.util.ArrayList;
import java.util.List;

public class Subsets {
    public static List<List<Integer>> findSubsets(int[] nums) {
        List<List<Integer>> subsets = new ArrayList<>();
        backTrack(subsets, new ArrayList<>(), nums, 0);
        return subsets;
    }

    // Time and Space complexity O(2^N) (for both approaches)
    public static void backTrack(List<List<Integer>> subsets, List<Integer> temp, int[] nums, int index) {
        subsets.add(new ArrayList<>(temp));
        for (int i = index; i < nums.length; i++) {
            temp.add(nums[i]);
            backTrack(subsets, temp, nums, i+1);
            temp.remove(temp.size() - 1);
        }
    }

    public static List<List<Integer>> findSubsetsII(int[] nums) {
        List<List<Integer>> subsets = new ArrayList<>();
        subsets.add(new ArrayList<>());
        for (int num: nums) {
            int size = subsets.size();
            for (int i = 0; i < size; i++) {
                List<Integer> set = new ArrayList<>(subsets.get(i));
                set.add(num);
                subsets.add(set);
            }
        }
        return subsets;
    }
    public static void main(String[] args) {
        List<List<Integer>> result = Subsets.findSubsets(new int[] { 1, 3 });
        System.out.println("Here is the list of subsets: " + result);

        result = Subsets.findSubsets(new int[] { 1, 5, 3 });
        System.out.println("Here is the list of subsets: " + result);
    }
}
