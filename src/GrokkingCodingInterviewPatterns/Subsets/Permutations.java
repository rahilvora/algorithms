package GrokkingCodingInterviewPatterns.Subsets;

import java.util.*;

public class Permutations {

    public static List<List<Integer>> findPermutations(int[] nums) {
        List<List<Integer>> result = new ArrayList<>();
        Arrays.sort(nums);
        return result;
    }

    public static void backTrack(List<List<Integer>> results, List<Integer> temp, int[] nums) {
        if (temp.size() == nums.length) {
            results.add(new ArrayList<>(temp));
            return;
        }
        for (int i = 0; i < nums.length; i++) {
            if(temp.contains(nums[i])) continue; // Skip as element already exist.
            temp.add(nums[i]);
            backTrack(results, temp, nums);
            temp.remove(temp.size() - 1);
        }
    }

    public static List<List<Integer>> findPermutationsII(int[] nums) {
        List<List<Integer>> result = new ArrayList<>();
        Queue<List<Integer>> permutations = new LinkedList<>();
        permutations.offer(new ArrayList<>());
        for (int i = 0; i < nums.length; i++) {
            int n = permutations.size();
            for (int j = 0; j < n; j++) {
                List<Integer> oldPermutation = permutations.poll();
                for (int k = 0; k <= oldPermutation.size(); k++) {
                    List<Integer> newPermutation = new ArrayList<>(oldPermutation);
                    newPermutation.add(j, nums[i]);
                    if (newPermutation.size() == nums.length) {
                        result.add(newPermutation);
                    }
                    else {
                        permutations.add(newPermutation);
                    }
                }
            }
        }
        return result;
    }

    public static void main(String[] args) {
        List<List<Integer>> result = Permutations.findPermutationsII(new int[] { 1, 3, 5 });
        System.out.print("Here are all the permutations: " + result);
    }
}
