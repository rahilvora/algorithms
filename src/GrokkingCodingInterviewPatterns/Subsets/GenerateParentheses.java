package GrokkingCodingInterviewPatterns.Subsets;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

class ParenthesesString {
    String str;
    int openCount;
    int closeCount;
    ParenthesesString(String str, int openCount, int closeCount){
        this.str = str;
        this.openCount = openCount;
        this.closeCount = closeCount;
    }
}
public class GenerateParentheses {

    // time complexity O(N*2^N) => 2^N for permutations and N for string concatenation
    public static List<String> generateValidParentheses(int num) {
        List<String> result = new ArrayList<String>();
        Queue<ParenthesesString> queue = new LinkedList<>();
        queue.add(new ParenthesesString("", 0, 0));
        while (!queue.isEmpty()) {
            ParenthesesString currStr = queue.poll();
            if (currStr.openCount == num && currStr.closeCount == num) {
                result.add(currStr.str);
            }
            else {
                if (currStr.openCount < num){
                    queue.add(new ParenthesesString(currStr.str + "(", currStr.openCount+1, currStr.closeCount));
                }
                if (currStr.closeCount < currStr.openCount) {
                    queue.add(new ParenthesesString(currStr.str + ")", currStr.openCount, currStr.closeCount + 1));
                }
            }
        }
        return result;
    }

    public static void main(String[] args) {
        List<String> result = GenerateParentheses.generateValidParentheses(2);
        System.out.println("All combinations of balanced parentheses are: " + result);

        result = GenerateParentheses.generateValidParentheses(3);
        System.out.println("All combinations of balanced parentheses are: " + result);
    }
}
