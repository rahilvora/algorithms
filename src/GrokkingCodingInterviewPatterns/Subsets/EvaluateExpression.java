package GrokkingCodingInterviewPatterns.Subsets;

import java.util.ArrayList;
import java.util.List;

public class EvaluateExpression {

    // Time complexity is O(N*2^N):
    public static List<Integer> diffWaysToEvaluateExpression(String input) {
        List<Integer> results = new ArrayList<>();
        // Base Case
        if (!input.contains("+") && !input.contains("-") && !input.contains("*")) {
            results.add(Integer.parseInt(input));
        }
        else {
            for (int i = 0; i < input.length(); i++) {
                char currChar = input.charAt(i);
                if (!Character.isDigit(currChar)) {
                    List<Integer> leftList = diffWaysToEvaluateExpression(input.substring(0, i));
                    List<Integer> rightList = diffWaysToEvaluateExpression(input.substring(i+1));
                    for (int part1: leftList) {
                        for (int part2: rightList) {
                            if (currChar == '+'){
                                results.add(part1 + part2);
                            }
                            else if (currChar == '-'){
                                results.add(part1 - part2);
                            }
                            else if (currChar == '*'){
                                results.add(part1 * part2);
                            }
                        }
                    }
                }
            }

        }
        return results;
    }
    public static void main(String[] args) {
        List<Integer> result = EvaluateExpression.diffWaysToEvaluateExpression("1+2*3");
        System.out.println("Expression evaluations: " + result);

        result = EvaluateExpression.diffWaysToEvaluateExpression("2*3-4-5");
        System.out.println("Expression evaluations: " + result);
    }
}
