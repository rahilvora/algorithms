package GrokkingCodingInterviewPatterns.Subsets;

import Trees.TreeNode;

import java.util.ArrayList;
import java.util.List;

public class UniqueTrees {
    public static List<TreeNode> findUniqueTrees(int n) {
        List<TreeNode> result = new ArrayList<>();
        if (n <= 1) return new ArrayList<>();
        return findUniqueTreesRecursive(1, n);
    }

    public static List<TreeNode> findUniqueTreesRecursive(int start, int end) {
        List<TreeNode> result = new ArrayList<>();
        if (start > end) {
            result.add(null);
        }
        else{
            for (int i = start; i <= end; i++) {
                List<TreeNode> leftSubtrees = findUniqueTreesRecursive(start, i - 1);
                List<TreeNode> rightSubtrees = findUniqueTreesRecursive(i + 1, end);
                for (TreeNode leftChild: leftSubtrees) {
                    for (TreeNode rightChild: rightSubtrees) {
                        TreeNode root = new TreeNode(i);
                        root.left = leftChild;
                        root.right = rightChild;
                        result.add(root);
                    }
                }
            }
        }
        return result;
    }

    public static int countTrees(int n) {
        if (n <= 1) return 1;
        int count = 0;
        for (int i = 1; i <= n; i++) {
            int countLeftTrees = countTrees(i-1);
            int countRightTrees = countTrees(n-i);
            count += (countLeftTrees*countRightTrees);
        }
        return count;
    }

    public static void main(String[] args) {
        List<TreeNode> result = UniqueTrees.findUniqueTrees(3);
        System.out.print("Total trees: " + result.size());
    }
}
