package GrokkingCodingInterviewPatterns.FastSlowPointer;

import LinkedList.ListNode;

public class FindMiddleNode {

    public static void main (String args[]) {

    }

    public static ListNode findMiddle(ListNode head) {
        ListNode slow = head, fast = head;
        while (fast != null && fast.next != null) {
            slow = slow.next;
            fast = fast.next;
        }
        return slow;
    }
}
