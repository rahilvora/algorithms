package GrokkingCodingInterviewPatterns.FastSlowPointer;

import LinkedList.ListNode;

public class FindStartNodeLinkedListCycle {

    public static void main (String args[]) {

    }

    public static ListNode findCycleStart(ListNode head) {
        if (head == null || head.next == null) return null;

        ListNode slow = head, fast = head;
        int cycleLength = 0;
        while (fast != null && fast.next != null) {
            slow = slow.next;
            fast = fast.next.next;
            if (slow == fast) {
                cycleLength = findCycleLength(slow);
                break;
            }
        }
        return findStart(head, cycleLength);
    }

    public static int findCycleLength(ListNode slow) {
        ListNode currNode = slow;
        int cycleLength = 0;
        do {
            currNode = currNode.next;
            cycleLength++;
        } while (currNode != slow);
        return cycleLength;
    }

    public static ListNode findStart(ListNode head, int length) {
        ListNode pointer1 = head, pointer2 = head;
        while (length > 0) {
            pointer2 = pointer2.next;
            length--;
        }

        while (pointer1 != pointer2) {
            pointer1 = pointer1.next;
            pointer2 = pointer2.next;
        }
        return pointer1;
    }
}
