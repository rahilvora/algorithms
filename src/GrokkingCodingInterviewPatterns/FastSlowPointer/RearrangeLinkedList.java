package GrokkingCodingInterviewPatterns.FastSlowPointer;

import LinkedList.ListNode;

public class RearrangeLinkedList {

    public static void main (String args[]) {
        ListNode head = new ListNode(2);
        head.next = new ListNode(4);
        head.next.next = new ListNode(6);
        head.next.next.next = new ListNode(8);
        head.next.next.next.next = new ListNode(10);
        head.next.next.next.next.next = new ListNode(12);
        RearrangeLinkedList.reorder(head);
        while (head != null) {
            System.out.print(head.val + " ");
            head = head.next;
        }
    }

    public static void reorder(ListNode head) {
        ListNode slow = head, fast = head;
        while (fast != null && fast.next != null) {
            slow = slow.next;
            fast = fast.next.next;
        }
        ListNode secondHalfHead = ReverseLinkedList.reverse(slow);
        ListNode firstHalfHead = head;
        while (firstHalfHead != null && secondHalfHead != null) {
            ListNode temp1 = firstHalfHead.next;
            firstHalfHead.next = secondHalfHead;
            firstHalfHead = temp1;

            ListNode temp2 = secondHalfHead.next;
            secondHalfHead.next = firstHalfHead;
            secondHalfHead = temp2;
        }
        // set the next of the last node to 'null'
        if (firstHalfHead != null)
            firstHalfHead.next = null;
    }
}
