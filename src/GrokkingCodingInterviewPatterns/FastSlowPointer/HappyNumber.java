package GrokkingCodingInterviewPatterns.FastSlowPointer;

import java.util.HashSet;
import java.util.Set;

public class HappyNumber {

    public static void main (String args[]) {
        System.out.println(HappyNumber.find(12));
    }

    public static boolean find(int num) {
        Set<Integer> set = new HashSet<>();
        while (set.add(num)) {
            int sum = 0;
            while (num != 0) {
                sum += Math.pow(num%10, 2);
                num /= 10;
            }
            if (sum == 1) return true;
            num = sum;
        }
        return false;
    }
}
