package GrokkingCodingInterviewPatterns.FastSlowPointer;

import GrokkingCodingInterviewPatterns.TwoPointer.PairWithTargetSum;

import java.util.HashSet;
import java.util.Set;

public class CycleInCircularArray {

    public static void main (String args[]) {
        System.out.println(CycleInCircularArray.loopExists(new int[] { 1, 2, -1, 2, 2 }));
        System.out.println(CycleInCircularArray.loopExists(new int[] { 2, 2, -1, 2 }));
        System.out.println(CycleInCircularArray.loopExists(new int[] { 2, 1, -1, -2 }));
    }
    // Time complexity O(N^2)`
    public static boolean loopExists(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            boolean isForward = arr[i] >= 0;
            int slow = i, fast = i;

            do {
                slow = findNextIndex(arr, isForward, slow);
                fast = findNextIndex(arr, isForward, fast);
                if (fast != -1) {
                    fast = findNextIndex(arr, isForward, fast);
                }

            } while (slow != -1 && fast != -1 && slow != fast);

            if(slow != -1 && slow == fast) return true;
        }
        return false;
    }

    public static int findNextIndex(int[] arr, boolean isForward, int currentIndex) {
         boolean direction = arr[currentIndex] >= 0;
        if (isForward != direction)
            return -1; // change in direction, return -1

        int nextIndex = (currentIndex + arr[currentIndex]) % arr.length;
        if (nextIndex < 0)
            nextIndex += arr.length; // wrap around for negative numbers

        // one element cycle, return -1
        if (nextIndex == currentIndex)
            nextIndex = -1;

        return nextIndex;
    }
}
