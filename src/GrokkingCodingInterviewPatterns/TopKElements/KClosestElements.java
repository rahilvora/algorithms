package GrokkingCodingInterviewPatterns.TopKElements;

import java.util.*;

class Entry {
    int key;
    int value;

    public Entry(int key, int value) {
        this.key = key;
        this.value = value;
    }
}

class KClosestElements {
// The time complexity of the above algorithm is O(logN + K*logK)O(logN+K∗logK).
// We need O(logN)O(logN) for Binary Search and O(K*logK)O(K∗logK)
// to insert the numbers in the Min Heap, as well as to sort the output array.
    public static List<Integer> findClosestElements(int[] arr, int K, Integer X) {
        List<Integer> result = new ArrayList<>();
        int index = binarySearch(arr, K);
        PriorityQueue<Entry> pq = new PriorityQueue<>(new Comparator<Entry>() {
            @Override
            public int compare(Entry o1, Entry o2) {
                return o1.key - o2.key;
            }
        });
        int low = index - K, high = index + K;
        low = Math.max(low, 0);
        high = Math.max(high, arr.length - 1);
        for (int i = low; i < high; i++ ) {
            pq.offer(new Entry(Math.abs(arr[i] - X), i));
        }

        for (int i = 0; i < K; i++)
            result.add(arr[pq.poll().value]);

        Collections.sort(result);
        return result;
    }

    public static int binarySearch(int[] arr, int k) {
        int low = 0, high = arr.length - 1;
        while (low < high) {
            int mid = (low + high) / 2;
            if (arr[mid] == k) return mid;
            if (arr[mid] < k) {
                low = mid + 1;
            }
            else {
                high = mid - 1;
            }
        }
        if (low > 0) return low -1;
        return low;
    }

    public static void main(String[] args) {
        List<Integer> result = KClosestElements.findClosestElements(new int[] { 5, 6, 7, 8, 9 }, 3, 7);
        System.out.println("'K' closest numbers to 'X' are: " + result);

        result = KClosestElements.findClosestElements(new int[] { 2, 4, 5, 6, 9 }, 3, 6);
        System.out.println("'K' closest numbers to 'X' are: " + result);

        result = KClosestElements.findClosestElements(new int[] { 2, 4, 5, 6, 9 }, 3, 10);
        System.out.println("'K' closest numbers to 'X' are: " + result);
    }
}

