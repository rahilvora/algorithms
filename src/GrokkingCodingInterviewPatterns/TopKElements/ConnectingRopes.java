package GrokkingCodingInterviewPatterns.TopKElements;

import java.util.Comparator;
import java.util.PriorityQueue;

public class ConnectingRopes {
    public static int minimumCostToConnectRopes(int[] ropeLengths) {
        int answer = 0;

        PriorityQueue<Integer> pq = new PriorityQueue<>(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o1 - o2;
            }
        });

        for( int len: ropeLengths) {
            pq.offer(len);
        }

        while (pq.size() > 1) {
            int temp = pq.poll() + pq.poll();
            answer += temp;
            pq.offer(temp);
        }

        return answer;
    }
}
