package GrokkingCodingInterviewPatterns.TopKElements;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FrequencySort {
    public static String sortCharacterByFrequency(String str) {
        Map<Character, Integer> freqToCharMap = new HashMap<>();
        for (char ch: str.toCharArray()) {
            freqToCharMap.put(ch, freqToCharMap.getOrDefault(ch, 0) + 1);
        }
        List<Character>[] bucket = new List[str.length() + 1];
        for (Map.Entry<Character, Integer> entry: freqToCharMap.entrySet()) {
            if (bucket[entry.getValue()] == null) {
                bucket[entry.getValue()] = new ArrayList<>();
            }
            bucket[entry.getValue()].add(entry.getKey());
        }

        StringBuilder sb = new StringBuilder();

        for (int pos = str.length() - 1; pos >=0; pos--) {
            if (bucket[pos] != null) {
                for (char ch: bucket[pos]) {
                    for (int i = 0; i < freqToCharMap.get(ch); i++) {
                        sb.append(ch);
                    }
                }
            }
        }

        return sb.toString();
    }

    public static void main(String[] args) {
        String result = FrequencySort.sortCharacterByFrequency("Programming");
        System.out.println("Here is the given string after sorting characters by frequency: " + result);

        result = FrequencySort.sortCharacterByFrequency("abcbab");
        System.out.println("Here is the given string after sorting characters by frequency: " + result);
    }
}
