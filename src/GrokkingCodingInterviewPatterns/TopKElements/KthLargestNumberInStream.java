package GrokkingCodingInterviewPatterns.TopKElements;

import java.util.Comparator;
import java.util.PriorityQueue;

//Add function is O(Logk)
public class KthLargestNumberInStream {

    PriorityQueue<Integer> pq;
    int k;
    public KthLargestNumberInStream(int[] nums, int k) {
        pq = new PriorityQueue<>();
        this.k = k;

        for (int i = 0; i < nums.length; i++){
            add(nums[i]);
        }
    }

    public int add(int num) {
        pq.offer(num);
        if (pq.size()>k) {
            pq.poll();
        }
        return pq.isEmpty()? -1: pq.peek();
    }

    public static void main(String[] args) {
        int[] input = new int[] { 3, 1, 5, 12, 2, 11 };
        KthLargestNumberInStream kthLargestNumber = new KthLargestNumberInStream(input, 4);
        System.out.println("4th largest number is: " + kthLargestNumber.add(6));
        System.out.println("4th largest number is: " + kthLargestNumber.add(13));
        System.out.println("4th largest number is: " + kthLargestNumber.add(4));
    }
}
