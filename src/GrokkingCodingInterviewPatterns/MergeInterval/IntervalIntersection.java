package GrokkingCodingInterviewPatterns.MergeInterval;

import Sorting.Interval;

import java.util.ArrayList;
import java.util.List;

public class IntervalIntersection {
    public static void main (String args[]) {

    }

    public static Interval[] merge(Interval[] arr1, Interval[] arr2) {
        List<Interval> intervalsIntersection = new ArrayList<Interval>();
        if(arr1 == null || arr2 == null) return  intervalsIntersection.toArray(new Interval[intervalsIntersection.size()]);
        int i = 0, j = 0;

        while (i < arr1.length && j < arr2.length) {
            if (arr1[i].start >= arr2[j].start && arr1[i].start <= arr2[j].end ||
                arr2[j].start >= arr1[i].start && arr2[j].start <= arr1[i].end) {
                intervalsIntersection.add(new Interval(Math.max(arr1[i].start, arr2[j].start), Math.min(arr1[i].end, arr2[j].end)));
            }
            // move next from the interval which is finishing first
            if (arr1[i].end < arr2[j].end) {
                i++;
            } else {
                j++;
            }
        }
        return intervalsIntersection.toArray(new Interval[intervalsIntersection.size()]);
    }
}
