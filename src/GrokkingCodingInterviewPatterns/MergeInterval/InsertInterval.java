package GrokkingCodingInterviewPatterns.MergeInterval;

import Sorting.Interval;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class InsertInterval {

    public static void main (String args[]) {
        List<Interval> input = new ArrayList<Interval>();
        input = new ArrayList<Interval>();
        input.add(new Interval(1, 3));
        input.add(new Interval(5, 7));
        input.add(new Interval(8, 12));
        System.out.print("Intervals after inserting the new interval: ");
        for (Interval interval : InsertInterval.insert(input, new Interval(4, 10)))
            System.out.print("[" + interval.start + "," + interval.end + "] ");
        System.out.println();
    }

    public static List<Interval> insert(List<Interval> intervals, Interval newInterval) {
        List<Interval> mergedIntervals = new ArrayList<>();
        Collections.sort(intervals, new Comparator<Interval>() {
            @Override
            public int compare(Interval o1, Interval o2) {
                return o1.start - o2.start;
            }
        });
        int size = intervals.size();
        int currIndex = 0;
        while (currIndex < size && intervals.get(currIndex).end < newInterval.start) {
            mergedIntervals.add(intervals.get(currIndex++));
        }

        while (currIndex < size && intervals.get(currIndex).start <= newInterval.end) {
            int currStart = intervals.get(currIndex).start;
            int currEnd = intervals.get(currIndex).end;
            newInterval = new Interval(Math.min(currStart, newInterval.start), Math.max(currEnd, newInterval.end));
            currIndex++;
        }
        mergedIntervals.add(newInterval);
        while (currIndex < size) {
            mergedIntervals.add(intervals.get(currIndex++));
        }
        return mergedIntervals;
    }
}
