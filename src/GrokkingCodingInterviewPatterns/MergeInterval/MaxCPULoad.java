package GrokkingCodingInterviewPatterns.MergeInterval;

import java.util.*;

public class MaxCPULoad {

    public static void main (String args[]) {
        List<Job> jobs = new ArrayList<>();
        jobs.add(new Job(1,4,2));
        jobs.add(new Job(2,4,1));
        jobs.add(new Job(3,6,5));
        System.out.println(MaxCPULoad.findMaxCPULoad(jobs));
    }

    public static int findMaxCPULoad(List<Job> jobs) {
        // sort the jobs by start time
        Collections.sort(jobs, (a, b) -> Integer.compare(a.start, b.start));

        int maxCPULoad = 0;
        int currentCPULoad = 0;
        PriorityQueue<Job> minHeap = new PriorityQueue<>(jobs.size(), (a, b) -> Integer.compare(a.end, b.end));
        for (Job job : jobs) {
            // remove all jobs that have ended
            while (!minHeap.isEmpty() && job.start > minHeap.peek().end)
                currentCPULoad -= minHeap.poll().cpuLoad;

            // add the current job into the minHeap
            minHeap.offer(job);
            currentCPULoad += job.cpuLoad;
            maxCPULoad = Math.max(maxCPULoad, currentCPULoad);
        }
        return maxCPULoad;
    }
}
