package GrokkingCodingInterviewPatterns.MergeInterval;

import Sorting.Interval;

import java.util.*;

public class MinimumMeetingRooms {

    public static void main (String args[]) {

    }

    public static int findMinimumMeetingRooms(Interval[] intervals) {
        if(intervals == null || intervals.length == 0){
            return 0;
        }
        Arrays.sort(intervals, new Comparator<Interval>() {
            @Override
            public int compare(Interval o1, Interval o2) {
                return o1.start - o2.start;
            }
        });
        int count = 1;
        PriorityQueue<Integer> queue = new PriorityQueue<>();
        queue.offer(intervals[0].end);
        for(int i = 1; i < intervals.length; i++){
            if(intervals[i].start < queue.peek()) count++;
            else queue.poll();
            queue.offer(intervals[i].end);
        }
        return count;
    }
}
