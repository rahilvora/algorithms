package Math;

import java.util.HashMap;
import java.util.Map;

public class CollatzConjecture {
    Map<Integer, Integer> map = new HashMap<>();
    public static void main(String args[]){
        System.out.println(new CollatzConjecture().findLondSteps(20));
    }

    public int findLondSteps(int num){
        if (num < 1) return 0;
        int res = 0;
        for (int i = 1; i <= num; i++) {
            int t = findSteps(i);
            map.put(i, t);
            res = Math.max(res, t);
        }

        return res;
    }
    private int findSteps(int num) {
        if (num <= 1) return 1;
        if (map.containsKey(num)) return map.get(num);
        if (num % 2 == 0) num = num / 2;
        else num = 3 * num + 1;
        if (map.containsKey(num)) return map.get(num) + 1;
        int t = findSteps(num);
        map.put(num, t);
        return t + 1;
    }
}
