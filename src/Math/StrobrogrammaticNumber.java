package Math;

/**
 * Created by rahilvora on 9/5/17.
 *
 * A strobogrammatic number is a number that looks the same when rotated 180 degrees (looked at upside down).
 * Write a function to determine if a number is strobogrammatic. The number is represented as a string.
 * For example, the numbers "69", "88", and "818" are all strobogrammatic.
 *
 */
public class StrobrogrammaticNumber {
    public boolean isStrobogrammatic(String num) {
        int left = 0, right = num.length() -1;
        while(left <= right){
            if(!isGood(num.charAt(left), num.charAt(right) )){
                return false;
            }
            left++;
            right--;
        }
        return true;
    }
    public boolean isGood(char left, char right){
        if((left == '0' && right == '0')||
                (left == '1' && right == '1')||
                (left == '8' && right == '8')||
                (left == '6' && right == '9')||
                (left == '9' && right == '6'))
            return true;
        return false;
    }
}
