package Math;

/**
 * Created by rahilvora on 9/16/17.
 */
public class ExcelSheetColumnNumber {
    public static void main(String args[]){
        System.out.print(new ExcelSheetColumnNumber().excelSheetColumnTitle("AB"));
    }
    public int excelSheetColumnTitle(String column){
        int result = 0;
        for(int i = 0; i < column.length(); i++){
            result = result * 26 + (column.charAt(i) - 'A' + 1);
        }
        return result;
    }
}
