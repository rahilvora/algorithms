package Math;

/**
 * Created by rahilvora on 5/28/17.
 */
public class CountPrimes {
    public int countPrimes(int n) {
        int count = 0;
        boolean[] numberOfPrimes = new boolean[n];
        for(int i = 2; i < n; i++){
            if(numberOfPrimes[i] == false){
                count++;
                for(int j = 2; i*j<n;j++){
                    numberOfPrimes[i*j] = true;
                }
            }
        }
        return count;
    }
}
