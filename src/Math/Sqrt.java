package Math;

/**
 * Created by rahilvora on 8/12/17.
 */
public class Sqrt {
    public int mySqrt(int x) {
        if(x == 0 || x == 1) return x;
        int start = 1, end = x, ans = 0;
        while(start <= end) {
            int mid = (start + end) / 2;
            if (mid > x / mid) {
                end = mid - 1;
            } else if ((mid + 1) < x / (mid + 1)) {
                start = mid + 1;
                ans = mid;
            }
        }
        return ans;
    }
}
