package Math;

/**
 * Created by rahilvora on 9/16/17.
 */
public class ExcelSheetColumnTitle {
    public static void main(String args[]){
        System.out.print(new ExcelSheetColumnTitle().excelSheetColumnTitle(26));
    }
    public String excelSheetColumnTitle(int n){
        StringBuffer result = new StringBuffer();
        while(n > 0){
            n--;
            result.insert(0, (char)('A' + n%26));
            n /= 26;
        }
        return result.toString();
    }
}
