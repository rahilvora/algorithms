package Math;

/**
 * Created by rahilvora on 2/17/18.
 */
public class PerfectNumber {
    public boolean checkPerfectNumber(int num) {
        if(num == 1) return false;
        int sum = 0;
        for(int i = 2; i <= Math.sqrt(num); i++){
            if(num%i == 0){
                sum += i;
                sum += num/i;
            }
        }
        // Increament sum as 1 is always a factor of a number
        sum++;
        return sum == num;
    }
}
