package Math;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by rahilvora on 1/26/18.
 */
public class HappyNumber {
    public boolean isHappy(int num) {
        Set<Integer> set = new HashSet<>();
        while(set.add(num)){
            int sumSqaure = 0;
            while(num!= 0){
                int temp = num%10;
                sumSqaure += temp*temp;
            }
            if(sumSqaure == 1) return true;
            num = sumSqaure;
        }
        return false;
    }
}
