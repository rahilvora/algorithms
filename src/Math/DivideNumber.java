package Math;

/**
 * Created by rahilvora on 9/23/17.
 */
public class DivideNumber {
    public static void main(String args[]) {
        System.out.print(new DivideNumber().divide(12,2));
    }

    public int divide(int dividend, int divisor) {
        int sign = 1;
        if((dividend < 0 && divisor >0)|| (dividend > 0 && divisor < 0)) sign = -1;
        if(divisor == 0) return Integer.MAX_VALUE;
        else if(dividend == 0 || dividend<divisor) return 0;
        long ldividend = (long) Math.abs(dividend);
        long ldivisor = (long) Math.abs(divisor);

        long lans = ldivide(ldividend, ldivisor);

        int ans;
        if (lans > Integer.MAX_VALUE){ //Handle overflow.
            ans = (sign == 1)? Integer.MAX_VALUE : Integer.MIN_VALUE;
        } else {
            ans = (int) (sign * lans);
        }
        return ans;

    }

    public long ldivide(long ldividend, long ldivisor){
        if(ldividend < ldivisor) return 0;
        long sum = ldivisor;
        long multiple = 1;
        while((sum + sum) < ldividend){
            sum += sum;
            multiple += multiple;
        }
        return multiple + ldivide(ldividend - sum, ldivisor);

    }
}
