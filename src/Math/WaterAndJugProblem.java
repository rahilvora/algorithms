package Math;

/**
 * Created by rahilvora on 10/11/17.
 */
public class WaterAndJugProblem {
    public static void main(String args[]){
        System.out.print(new WaterAndJugProblem().canMeasureWater(3,5,4));
    }
    public boolean canMeasureWater(int x, int y, int z) {
        if(x + y < z) return false;
        if(x == z || y == z || x + y == z) return true;
        return z%GCD(x,y) == 0;
    }
    public int GCD(int x, int y){
        while(y != 0){
            int temp = y;
            y = x%y;
            x = temp;
        }
        return x;
    }
}
