package Math;

/**
 * Created by rahilvora on 1/26/18.
 * Time O(LogN); where N is binary representation of y
 */
public class Pow {
    public static void main(String args[]){
        System.out.print(new Pow().myPowII(2, 7));
    }
    public double myPow(int x, int y){
        double answer = 1;
        double xx = (double) x;
        long power = y;
        if(y == 0) return 1;
        if(y<0){
            x = 1/x;
            power = -power;
        }
        int currentTotal = (int)x;
        for(long i = power; i>0; i /=2){
            if(i%2 == 1){
                answer = answer*currentTotal;
            }
            currentTotal = currentTotal*currentTotal;
        }
        return answer;
    }

    public double myPowII(int x, int n){
        if(n == 0)
            return 1;
        if(n<0){
            n = -n;
            x = 1/x;
        }
        return (n%2 == 0) ? myPowII(x*x, n/2) : x*myPowII(x*x, n/2);
    }
}
