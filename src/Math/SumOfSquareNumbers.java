package Math;

/**
 * Created by rahilvora on 10/11/17.
 */
public class SumOfSquareNumbers {
    public static void main(String args[]){

    }
    public boolean judgeSquareSum(int c) {
        int left = 0, right = (int) Math.sqrt(c);
        while(left < right){
            int curr = left*left + right*right;
            if(curr < c){
                left++;
            }
            else if(curr > c){
                right--;
            }
            else{
                return true;
            }
        }
        return false;
    }
}
